/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureS_SpendByCategory {

    static testMethod void myUnitTest() {
    	
        // TO DO: implement unit test
         BSureS_Basic_Info__c objSupplier = new BSureS_Basic_Info__c(Contact_name__c='KingFisher');
   		insert objSupplier;
   		
   		BSureS_Zone__c Bzone = new BSureS_Zone__c(Name ='Test Supplier Zone');
        insert Bzone;  
        
        Bzone = [select id,Name from BSureS_Zone__c where id=:Bzone.Id];
        BSureS_SubZone__c BSzone = new BSureS_SubZone__c();
        BSzone.Name = 'Test Supplier Sub Zone';
        BSzone.ZoneID__c = Bzone.Id;
        insert BSzone;
        
        system.assertEquals('Test Supplier Sub Zone',BSzone.Name);
        
        BSzone= [select id,Name from BSureS_SubZone__c where id=:BSzone.Id];
		
		BSureS_Country__c BsCountry = new BSureS_Country__c(Name = 'Test Supplier Country',Sub_Zone_ID__c=BSzone.Id);
		insert BsCountry;
		system.assertEquals('Test Supplier Country',BsCountry.Name);
		
		BsCountry = [select Id,Name from BSureS_Country__c  where id=:BsCountry.Id];
		
        BSureS_SpendByCategory objspend=new BSureS_SpendByCategory();
        Date dtYear = Date.Today().addDays(-365);           
        Date dtToday = Date.Today();        
        objspend.strFromDate = dtYear.format();
        objspend.strToDate =  dtToday.format();
        BSureS_SpendByCategory.resultwrapper obj= new BSureS_SpendByCategory.resultwrapper();
        BSureS_Basic_Info__c objSup=new BSureS_Basic_Info__c();
        objSup.Supplier_Name__c='Dummy Supplier';
        //objSup.Fiscal_Year_End__c=system.today();
        //objSup.Next_Review_Date__c=system.today();
        //objNSF.fromdate=objSup.Fiscal_Year_End__c;
        //objNSF.todate=objSup.Next_Review_Date__c;
        Database.SaveResult MySaveResult =Database.insert(objSup);
        id idsup=MySaveResult.id;
        system.assertEquals('Dummy Supplier',objSup.Supplier_Name__c);
        
        //objspend.dtfromdate= system.Today();
        //objspend.dttodate= system.Today();
        objspend.getUserAttributes();
        objspend.getSZone();
        objspend.getSSubZoneList();
        objspend.getSCountriesList();
        objspend.getResults();
         //objspend.dtfromdate= system.Today();
       // objspend.dttodate= system.Today();
        set<id> setids=new set<id>();
        setids.add(idsup);
        map<id, double> mapresult=objspend.getTotalSpend(setids);
        objspend.getSupDetails();
        objspend.pageSize=10;
      	pagereference pgrefnxtbtn = objspend.nextBtnClick();
        pagereference pgrefprvbtn = objspend.previousBtnClick();
        integer i=objspend.getTotalPageNumber();
        integer j=1;
        objspend.BindData(j);
        objspend.pageData(j);
        objspend.LastpageData(j);
        integer k=objspend.getPageNumber();
        integer l=objspend.getPageSize();
        boolean a=objspend.getPreviousButtonEnabled();
        boolean b=objspend.getNextButtonDisabled();
        pagereference pgreflastbtn =objspend.LastbtnClick();
        pagereference pgreffrstbtn =objspend.FirstbtnClick();
        objspend.getSortDirection();
        objspend.Cancel();
        objspend.showErrorMessage('msg');
    }
}