/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class TestBSureS_PerformCreditAnalysis {

    static testMethod void myUnitTest() {

        BSureS_Basic_Info__c supplierObj = new BSureS_Basic_Info__c();
        supplierObj.Contact_name__c = 'Steve';
        supplierObj.Supplier_Name__c = 'george M';
        supplierObj.Analyst__c = UserInfo.getUserId();
        supplierObj.Manager__c = UserInfo.getUserId();
        supplierObj.Bakup_Analysts__c = UserInfo.getUserId() + ',' + UserInfo.getUserId();
        insert supplierObj;
        
        system.assertEquals('Steve',supplierObj.Contact_name__c);
        
        /*BSureS_Assigned_Buyers__c bObj = new BSureS_Assigned_Buyers__c();
        bObj.Buyer_ID__c = UserInfo.getUserId();
        bObj.Supplier_ID__c = supplierObj.Id;
        insert bObj;
        */
        BSureS_Credit_Analysis__c pca = new BSureS_Credit_Analysis__c();
       
        
        Apexpages.currentPage().getParameters().put('sid',supplierObj.id);
        ApexPages.StandardController suplcontroller = new ApexPages.StandardController(pca); 
        BSureS_PerformCreditAnalysis pObj2= new BSureS_PerformCreditAnalysis(suplcontroller);
        pObj2.CAInfo.Review_Status__c = 'Completed';
        pObj2.strReviewStatus = 'Completed';
        pObj2.strSupplierId  =  supplierObj.id;
        pObj2.strCurrentSupplierId = supplierObj.id;
        //pObj2.creditanalysisId = pca.Id;
        pObj2.visibilityNextRDate();         
        pObj2.CASave(); 
        pObj2.CAInfo.Next_Review_Date__c = date.today();
        pObj2.Approved();
        pObj2.Rejected();
        pObj2.redirectDetailPage();
        pObj2.redirect();
        pObj2.submitApproval();
        pObj2.SendNotification();
        pObj2.sendNotificationReject();
        
        //Apexpages.currentPage().getParameters().put('sid',pca.id);
        
    }
    static testMethod void myUnitTest2()
    {
        BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
        objCSettings1.Name = 'BSureS_Run_Triggers2';
        objCSettings1.Parameter_Key__c = 'BSureS_Run_Triggers';
        objCSettings1.Parameter_Value__c = 'True';
        insert objCSettings1;
        system.assertEquals('True', objCSettings1.Parameter_Value__c);
        
        BSure_Configuration_Settings__c objCSettings5 = new BSure_Configuration_Settings__c();
        objCSettings5.Name = 'TestBSureS_BuyerShare2';
        objCSettings5.Parameter_Key__c = 'BSureS_BuyerShare';
        objCSettings5.Parameter_Value__c = 'TRUE';
        insert objCSettings5;
    
        BSureS_Basic_Info__c supplierObj = new BSureS_Basic_Info__c();
        supplierObj.Contact_name__c = 'Steve';
        supplierObj.Supplier_Name__c = 'george M';
        supplierObj.Analyst__c = UserInfo.getUserId();
        supplierObj.Manager__c = UserInfo.getUserId();
        supplierObj.Bakup_Analysts__c = UserInfo.getUserId() + ',' + UserInfo.getUserId();
        supplierObj.Next_Review_Date__c = date.today();
        supplierObj.Planned_Review_Date__c = date.today();
        insert supplierObj;
        
        BSureS_Assigned_Buyers__c bObj = new BSureS_Assigned_Buyers__c();
        bObj.Buyer_ID__c = UserInfo.getUserId();
        bObj.Supplier_ID__c = supplierObj.Id;
        insert bObj;
        
        
        BSureS_Credit_Analysis__c pca = new BSureS_Credit_Analysis__c();
        pca.Review_Name__c = 'Test Review123';
        pca.Comment__c = 'text Comments';
        pca.Risk_Level__c =  'High';
        pca.Rating__c = 'A';
        pca.Review_Status__c = 'Started';
        pca.Spend__c = double.valueOf('4543525');
        pca.Actual_Review_Start_Date__c = date.today();
        pca.Expected_Review_End_Date__c = date.today();
        pca.Supplier_ID__c = supplierObj.id;
        //pca.Review_Period__c = date.today();
        //pca.Review_Start_Date__c = date.today();
        pca.Spend_Start_Date__c = date.today();
        pca.Spend_End_Date__c = date.today();
        pca.Next_Review_Date__c = date.today();
        insert pca;
        system.assertEquals('Test Review123',pca.Review_Name__c);
        
        Apexpages.currentPage().getParameters().put('id',pca.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(pca); 
        BSureS_PerformCreditAnalysis pObj= new BSureS_PerformCreditAnalysis(controller);
        pObj.strReviewStatus = 'Completed';
        pObj.strSupplierId  =  supplierObj.id;
        pObj.strCurrentSupplierId = supplierObj.id;
        pObj.creditanalysisId = pca.Id;
        pObj.blnFutureReviewdateV = true;
        pObj.blnNotificationFlag = true;
        pObj.blnratingVisible = true;
        pObj.blnSubmitApprovebtns = true;
        
        pObj.visibilityNextRDate();    
        pObj.CASave();
        pObj.cancel();
        pObj.CAInfo.Next_Review_Date__c = date.today();
        pObj.Approved();
        pObj.strSupplierId = supplierObj.id;
        pObj.CAInfo.Next_Review_Date__c = date.today();
        pObj.CAInfo.Review_Status__c = 'Started';
        pObj.Rejected();
        pObj.redirect();
        pObj.CAInfo.Next_Review_Date__c = date.today();
        pObj.submitApproval();
        //pObj.buyersNotification();
        pObj.redirectDetailPage();
        pObj.SendNotification();
        pObj.sendNotificationReject();
    } 
}