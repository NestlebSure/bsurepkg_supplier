/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData=true)
private class TestBSureS_DiscussionBoard {
 
    static testMethod void myUnitTest() 
    {
        /*
        BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
        objCSettings1.Name = 'BSureS_ZoneManager';
        objCSettings1.Parameter_Key__c = 'BSureS_ZoneManager';
        objCSettings1.Parameter_Value__c = 'Zone Manager';
        insert objCSettings1;
        system.assertEquals('BSureS_ZoneManager', objCSettings1.Name);
        
        BSure_Configuration_Settings__c objCSettings2 = new BSure_Configuration_Settings__c();
        objCSettings2.Name = 'BSureS_SubZoneManager';
        objCSettings2.Parameter_Key__c = 'BSureS_SubZoneManager';
        objCSettings2.Parameter_Value__c = 'Sub-Zone Manager';
        insert objCSettings2;
        
        BSure_Configuration_Settings__c objCSettings3 = new BSure_Configuration_Settings__c();
        objCSettings3.Name = 'BSureS_countrymanager';
        objCSettings3.Parameter_Key__c = 'BSureS_countrymanager';
        objCSettings3.Parameter_Value__c = 'Country Manager';
        insert objCSettings3;
        
        BSure_Configuration_Settings__c objCSettings5 = new BSure_Configuration_Settings__c();
        objCSettings5.Name = 'BSureS_analystrole';
        objCSettings5.Parameter_Key__c = 'BSureS_analystrole';
        objCSettings5.Parameter_Value__c = 'Analyst';
        insert objCSettings5;
        
        BSure_Configuration_Settings__c cSettings4 = new BSure_Configuration_Settings__c();
        cSettings4.Name = 'SupplierFileSizeLimit';
        cSettings4.Parameter_Key__c = 'SupplierFileSizeLimit';
        cSettings4.Parameter_Value__c = '26214400';
        insert cSettings4;
        
        system.assertEquals('BSureS_analystrole', objCSettings5.Name);
        */
        list<BSureS_DiscussionBoard.DiscussBean> objListBean = new list<BSureS_DiscussionBoard.DiscussBean>();
        list<BSureS_Basic_Info__c> objListSupp = new list<BSureS_Basic_Info__c>();
        Apexpages.currentPage().getParameters().put('mode', 'SCRS-000001');
        
        UserRole objRole = [SELECT Id, Name FROM UserRole WHERE Name = 'Analyst'];
        Profile objProf = [SELECT Id, Name FROM Profile WHERE Name = 'BSureS_Analyst'];
        
        User objUser = new User(Alias = 'bsurean', Email='suppl_analyst@bsurenestle.com', 
                    EmailEncodingKey='UTF-8', LastName='Analyst_supplUser', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = objProf.Id, UserRoleId = objRole.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='suppl_analyst@bsurenestle.com');
              
        BSureS_Basic_Info__c supplierObj = new BSureS_Basic_Info__c();
        supplierObj.Contact_name__c = 'Steve';
        supplierObj.Supplier_Name__c = 'george M';
        supplierObj.Analyst__c = objUser.Id;
        Database.Saveresult objSavesup=  Database.insert(supplierObj);
        objListSupp.add(supplierObj);
        
        system.assertEquals('george M', supplierObj.Supplier_Name__c);
        
        BSureS_Credit_Review_Section__c objSection = new BSureS_Credit_Review_Section__c();
        objSection.Comment__c = 'Test Comment &lt;&quot;\nTest&quot;&gt;';
        objSection.TitleDescription__c = 'Test Description';
        objSection.DiscussionType__c = 'Link';
        objSection.Source__c = 'Test Source';
        objSection.DiscussionsCount__c = 1;
        objSection.Supplier_ID__c = objSavesup.Id;
        Database.Saveresult objSaveSec =  Database.insert(objSection);
        
        Apexpages.currentPage().getParameters().put('supid', objSavesup.Id);
        Apexpages.currentPage().getParameters().put('prtid', objSaveSec.Id);
        Apexpages.currentPage().getParameters().put('id', objSaveSec.Id);
        Apexpages.currentPage().getParameters().put('mode', 'SCRS-000001');
        Apexpages.currentPage().getParameters().put('type', 'Link');
        Apexpages.currentPage().getParameters().put('type', 'Topic');
        Apexpages.currentPage().getParameters().put('type', 'File');
        
        BSureS_DiscussionBoard objBSureDisc = new BSureS_DiscussionBoard();
        
        objBSureDisc.SetSObjectInfo('SCSS');
        objBSureDisc.SetSObjectInfo('SCIS');
        objBSureDisc.SetSObjectInfo('SCAS');
        objBSureDisc.SetSObjectInfo('');
        objBSureDisc.SetSObjectInfo('SCRS');
        objBSureDisc.strTimezoneSIDKey = 'America/Los_Angeles';
        
        objBSureDisc.GetQueryParams();
        objBSureDisc.ClosePostFields();
        objBSureDisc.DeletePost();
        objBSureDisc.PostNewReply();
        objBSureDisc.GetThreadSObjectData(objSaveSec.Id);
        objBSureDisc.GetIDsForCreditReviews(objListSupp);
        objBSureDisc.GetIDsForCreditStatus(objListSupp);
        
        BSureS_Credit_Review_Discussion__c objDummy = new BSureS_Credit_Review_Discussion__c();
        objDummy.Comment__c = 'Test Comment';
        objDummy.Title__c = 'Test Title';
        objDummy.Link__c = 'Test Link';
        objDummy.Credit_Review_ID__c = 'SCRS-000001';
        objDummy.BSureS_Credit_Review_Section__c = objSaveSec.Id;
        
        Database.Saveresult objSave =  Database.insert(objDummy);
        objBSureDisc.strFileName = 'FileName';
        objBSureDisc.UploadFile(objSave.Id);
        
        objBSureDisc.SendNotifications();
        
        //HardCoded for SCRS - Begin
        objBSureDisc.GetDiscSObjectData('SCRS-000001');
        
        BSureS_DiscussionBoard.DiscussBean objNewBean = new BSureS_DiscussionBoard.DiscussBean();
        
        objNewBean.strComment = 'Test Comment';
        objNewBean.strTitle = 'Test Title';
        objNewBean.strThreadID = 'SCRS-000001';
        objNewBean.strCreatedBy = 'Test User';
        objNewBean.strParentID = '12345';
        objNewBean.strFileName = 'TestFile.txt';
        
        objNewBean.strThreadSFDCID = objSaveSec.Id;
        objListBean.add(objNewBean);
        objBSureDisc.UpdateDiscussionSObject(objNewBean);
        objBSureDisc.GenerateHTML(objListBean);
        
        objBSureDisc.GetParentSObjData('SCRS-000001');
        objBSureDisc.hdnParentID = 'SCRS-000001';
        objBSureDisc.ReplyOnPost();
        objBSureDisc.showErrorMessage('All is Well !!!');
        objBSureDisc.Cancel();
        objBSureDisc.ClosePostFields();
        //HardCoded for SCRS - END
    }
}