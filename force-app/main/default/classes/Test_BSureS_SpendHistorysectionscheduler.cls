/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_BSureS_SpendHistorysectionscheduler {


    
    static testMethod void myUnitTest1() 
    {
          
        String strfile='11/15/2012,123456,11/14/2012,1234'+'\n'+'11/15/2013,123456,11/14/2012,1234';
        blob bfile=blob.valueof(strfile);
        BsureS_SpendHistorySectionScheduler spend_history=new BsureS_SpendHistorySectionScheduler();
        BsureS_SpendHistorySectionScheduler.createBatchesFromCSVFile(bfile,'test');
        String asign='123';
        system.Assertequals('123',asign);

    }
    
        static testMethod void myUnitTest29() 
    {
        String strfile='11/14/2012,43434,11/14/2012, ,'+'\n'+'11/14/2012,42343,11/14/2012, ,';
        blob bfile=blob.valueof(strfile);
        BsureS_SpendHistorySectionScheduler spend_history=new BsureS_SpendHistorySectionScheduler();
        BsureS_SpendHistorySectionScheduler.createBatchesFromCSVFile(bfile,'test');
        String asign='123';
        system.Assertequals('123',asign);

    }
    
    static testMethod void myUnitTest2() 
    {
        String strfile='11/15/2012dasd,123456,11/14/2012,1234'+'\n'+'11/15/2013asdasd,123456,11/14/2012,1234';
        blob bfile=blob.valueof(strfile);
        BsureS_SpendHistorySectionScheduler spend_history=new BsureS_SpendHistorySectionScheduler();
        BsureS_SpendHistorySectionScheduler.createBatchesFromCSVFile(bfile,'test');
        String asign='123';
        system.Assertequals('123',asign);

    }
    
    static testMethod void myUnitTest3() 
    {
        String strfile='11/15/2012,123456,11/14/2012dfgdg,1234'+'\n'+'11/15/2013,123456,11/14/2012sadasd,1234';
        blob bfile=blob.valueof(strfile);
        BsureS_SpendHistorySectionScheduler spend_history=new BsureS_SpendHistorySectionScheduler();
        BsureS_SpendHistorySectionScheduler.createBatchesFromCSVFile(bfile,'test');
        String asign='123';
        system.Assertequals('123',asign);

    }
    
    static testMethod void myUnitTest4() 
    {
        String strfile='11/15/2012d,123456gfhfgh,11/14/2012,1234'+'\n'+'11/15/2013,123456dasdas,11/14/2012,1234';
        blob bfile=blob.valueof(strfile);
        BsureS_SpendHistorySectionScheduler spend_history=new BsureS_SpendHistorySectionScheduler();
        BsureS_SpendHistorySectionScheduler.createBatchesFromCSVFile(bfile,'test');
        String asign='123';
        system.Assertequals('123',asign);

    }
    
    static testMethod void myUnitTest5() 
    {
        String strfile=',123456gfhfgh,11/14/2012,1234'+'\n'+',123456dasdas,11/14/2012,1234';
        blob bfile=blob.valueof(strfile);
        BsureS_SpendHistorySectionScheduler spend_history=new BsureS_SpendHistorySectionScheduler();
        BsureS_SpendHistorySectionScheduler.createBatchesFromCSVFile(bfile,'test');
        String asign='123';
        system.Assertequals('123',asign);

    }
    
    static testMethod void myUnitTest6() 
    {
        String strfile='11/14/2012,123456gfhfgh,,1234'+'\n'+'11/14/2012,123456dasdas,,1234';
        blob bfile=blob.valueof(strfile);
        BsureS_SpendHistorySectionScheduler spend_history=new BsureS_SpendHistorySectionScheduler();
        BsureS_SpendHistorySectionScheduler.createBatchesFromCSVFile(bfile,'test');
        String asign='123';
        system.Assertequals('123',asign);

    }
    
    static testMethod void myUnitTest7() 
    {
        String strfile='11/14/2012,,11/14/2012,1234'+'\n'+'11/14/2012,,11/14/2012,1234';
        blob bfile=blob.valueof(strfile);
        BsureS_SpendHistorySectionScheduler spend_history=new BsureS_SpendHistorySectionScheduler();
        BsureS_SpendHistorySectionScheduler.createBatchesFromCSVFile(bfile,'test');
        String asign='123';
        system.Assertequals('123',asign);

    }
    
    static testMethod void myUnitTest8() 
    {
        String strfile='11/14/2012,43434,11/14/2012,1234sdsa'+'\n'+'11/14/2012,42343,11/14/2012,1234dasds';
        blob bfile=blob.valueof(strfile);
        BsureS_SpendHistorySectionScheduler spend_history=new BsureS_SpendHistorySectionScheduler();
        BsureS_SpendHistorySectionScheduler.createBatchesFromCSVFile(bfile,'test');
        String asign='123';
        system.Assertequals('123',asign);

    }
    

    
    static testMethod void myUnitTest9() 
    {
        String strfile=',,,1234dsdf'+'\n'+',,,1234fdsf';
        blob bfile=blob.valueof(strfile);
        BsureS_SpendHistorySectionScheduler spend_history=new BsureS_SpendHistorySectionScheduler();
        BsureS_SpendHistorySectionScheduler.createBatchesFromCSVFile(bfile,'test');
        String asign='123';
        system.Assertequals('123',asign);

    }
    
    static testMethod void myUnitTest10() 
    {
        String strfile='11/14/2012sdfsd,43434sdfds,11/14/2012fsdf,1234'+'\n'+'11/14/2012jghjghj,42343jghj,11/14/2012jghjhg,1234';
        blob bfile=blob.valueof(strfile);
        BsureS_SpendHistorySectionScheduler spend_history=new BsureS_SpendHistorySectionScheduler();
        BsureS_SpendHistorySectionScheduler.createBatchesFromCSVFile(bfile,'test');
        String asign='123';
        system.Assertequals('123',asign);

    }
    
    static testMethod void myUnitTest11() 
    {
        String strfile=',,, ,'+'\n'+',,, ,';
        blob bfile=blob.valueof(strfile);
        BsureS_SpendHistorySectionScheduler spend_history=new BsureS_SpendHistorySectionScheduler();
        BsureS_SpendHistorySectionScheduler.createBatchesFromCSVFile(bfile,'test');
        String asign='123';
        system.Assertequals('123',asign);

    }
    
    
}