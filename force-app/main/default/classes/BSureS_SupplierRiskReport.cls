/***********************************************************************************************
*       Controller Name : BSureS_SupplierRiskReport
*       Date            : 11/02/2012 
*       Author          : B.Anupreethi
*       Purpose         : Its the report class where user can see the Risk level of an supplier
*       Change History  :
*       Date                      Programmer                     Reason
*       --------------------      -------------------    -------------------------
*       12/26/2012                B.Anupreethi              Initial Version
*       01/19/2013                VINU PRATHAP              Modified to show up messages from Labels
*       03/04/2013                B.Anupreethi              //Anu:changed the review dates Review_Start_Date__c to Actual_Review_Start_Date__c 
                                                             and Review_End_Date__c to Expected_Review_End_Date__c
*       03/11/2013                B.Anupreethi               //Anu:changed the status to Approved to Completed as DBA loaded as completed
**************************************************************************************************/
global with sharing class BSureS_SupplierRiskReport 
{
    public Id LoggedInUserId{get;set;}//To hold logged in user id
    public string strSupplierName{get;set;}//To hold Supplier name
    public string strSupplierZone{get;set;}//To Hold supplier Zone
    public string strSupplierSubZone{get;set;}//To Hold supplier Sub Zone
    public string strSupplierCountry{get;set;}//To hold the sup state
    public list<SelectOption> lstSZone{get;set;}//to hold Zone list
    public list<SelectOption> lstSSubZone{get;set;}//to hold Sub Zone list
    public list<SelectOption> lstSCountry{get;set;}//to hold country list   
    public string strFromDate{get;set;}//To Hold Search from data
    public string strToDate{get;set;}//To Hold Search To data
    public string strTodayDate{get;set;}
    public list<string> lstRiskHigh{get;set;}//To hold list of High records
    public list<string> lstRiskMedium{get;set;}//To hold list of Medium records
    public list<string> lstRiskLow{get;set;}//To hold list of Low records
    public list<BSureS_Credit_Analysis__c> lstSupDetails{get;set;}
    list<string> lstHighSupIds{get;set;}
    list<string> lstMediumSupIds{get;set;}
    list<string> lstLowSupIds{get;set;}
    public boolean isSearch{get;set;}
    public string strAnalystName{get;set;}
    public Integer RReportSize{get;set;}
    public Integer pageNumber;//used for pagination
    public Integer pageSize;//used for pagination
    public Integer totalPageNumber;//used for pagination
    public Integer PrevPageNumber {get;set;}//used for pagination
    public Integer NxtPageNumber {get;set;}//used for pagination
    public Integer NlistSize {get;set;}//used for pagination
    public Integer Endlst {get;set;}//used for pagination
    public integer totallistsize{get;set;}//total size of list SPFolderset and SPWFilesList for pagination
    public list<BSureS_Credit_Analysis__c> lstSupDetailsPagination{get;set;}//to hold list supplier info    
    
    //sort
    public string strSelectedRiskLvl{get;set;}
    private string  sortExp = 'Supplier_ID__r.Supplier_Name__c';
    private string  sortDirection = 'ASC';   
    
    
    public boolean showeditrecs {get; set;} //lookup icon visibility purpose
    public string strAnalystSearch{get;set;}
    public list<User> lstAnalystUser{get;set;}
    public  String[] strAnalystProfiles = new String[] {'BSureS_Analyst'};
    
     public String sortExpression{get{return sortExp;}set{ 
       if (value == sortExp)
         sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
       else
         sortDirection = 'ASC';
         sortExp = value;
       }
    }

    public String getSortDirection(){
    //if no column is selected 
        if (sortExpression == null || sortExpression == '')
          return 'ASC';
        else
         return sortDirection;
    }

    public void setSortDirection(String value){  
        sortDirection = value;
    }  
    
    public void Sort() {  
        getSupDetails();
    }
    /// <summary>
    /// wrapper class 
    /// </summary>
    public class resultwrapper
    {
        public string strSupId{get;set;}
        public String strProfileRisk{get;set;}
        public Decimal strTotalCount{get;set;}
    }
    
     /// <summary>
    /// Method to Show Buyer list
    /// </summary> 
    public void showBuyerDetails()
    {   
        showeditrecs=true;        
        lstAnalystUser=[Select Id, Name from User where Profile.Name IN:strAnalystProfiles and UserRole.Name != 'Procurement Manager' and IsActive=true order By Name Asc];
        
    }
    /// <summary> 
    /// Method to close the Buyer list, after select the Buyer
    /// </summary> 
    public void closepopup() 
    {
        string strSelectedAnalyst = apexpages.currentpage().getparameters().get('Analystid');
        strAnalystName=strSelectedAnalyst;        
        showeditrecs=false; 
    }
    
    /// <summary>
    /// Method to Search the Buyer Names
    /// </summary>
    public void searchBtn()
    {
        //system.debug('strSearchBuyer========'+strAnalystSearch);
        string strAnalSrch = '%'+strAnalystSearch+'%';
        if(strAnalystSearch != null && strAnalystSearch != '') 
        {
            lstAnalystUser = [Select Id, Name from User where Name like: strAnalSrch AND Profile.Name IN:strAnalystProfiles and UserRole.Name != 'Procurement Manager' AND IsActive=true order By Name Asc];
        }
        else
        {
            lstAnalystUser = [Select Id, Name from User where IsActive=true AND Profile.Name IN:strAnalystProfiles and UserRole.Name != 'Procurement Manager' order By Name Asc];           
        }
    }
    
    /// <summary>
    /// Method to Close popup Search
    /// </summary>
    public void closepopupBtn() 
    {
        showeditrecs=false; 
    } 
    
    
    /// <summary>
    /// Constructor 
    /// </summary>
    public BSureS_SupplierRiskReport()
    {
    	RReportSize = Integer.valueOf(BSureS_CommonUtil.getConfigurationValues('BSureS_Report_Page_Size').get(0));
        isSearch=false;
        getUserAttributes();
        getSZone();
        lstRiskHigh=new list<string>();
        lstRiskMedium=new list<string>();
        lstRiskLow=new list<string>();
        lstHighSupIds=new list<string> ();
        lstMediumSupIds=new list<string> ();
        lstLowSupIds=new list<string> ();
        Date dtYear = Date.Today().addDays(-365);      
        Date dtToday = Date.Today();    
        //strFromDate = dtYear.format();
        //strToDate =  dtToday.format();      
        strTodayDate = Date.today().format();
        totallistsize=0;
        pageNumber = 0;      
        totalPageNumber = 0;
    }
    
    /// <summary>
    /// Method is used to Bind the default values of logged in users
    /// </summary>
    public void getUserAttributes()
    {
        LoggedInUserId=UserInfo.getUserId();
        //system.debug('LoggedInUserId---'+LoggedInUserId);
        list<BSureS_User_Additional_Attributes__c> lstAttributes = [SELECT User__c, BSureS_Country__c, BSureS_Sub_Zone__c, BSureS_Zone__c 
                                                                    FROM BSureS_User_Additional_Attributes__c 
                                                                    WHERE User__r.IsActive=: true AND User__c=:LoggedInUserId];
        //system.debug('lstAttributes---'+lstAttributes);
        if(lstAttributes.size()>0)
        {
            strSupplierZone = lstAttributes[0].BSureS_Zone__c;
            strSupplierSubZone = lstAttributes[0].BSureS_Sub_Zone__c;
            strSupplierCountry = lstAttributes[0].BSureS_Country__c;
        }
    }
    
     /// <summary>
    /// Method to get Zone list for Supplier search 
    /// </summary>
    public void getSZone()
    {
        BSureS_CommonUtil.isReport=true;
        lstSZone=new list<SelectOption>();          
        lstSZone = BSureS_CommonUtil.getZones();
        getSSubZoneList();  
    }
    
    /// <summary>
    /// Method to get Subzone list for Supplier search 
    /// </summary>
    public void getSSubZoneList()
    {
        
        //system.debug('strSupplierZone---'+strSupplierZone); 
        
        BSureS_CommonUtil.isReport=true;
        lstSSubZone=new list<SelectOption>();
        if(strSupplierZone=='ALL')
        {
            strSupplierSubZone='ALL';
            strSupplierCountry='ALL';
        }       
        lstSCountry = new list<selectOption>();
        lstSCountry.add(new selectOption('ALL','ALL'));
        //system.debug('strSupplierZone---'+strSupplierZone); 
        lstSSubZone = BSureS_CommonUtil.getSubZones(strSupplierZone);    
        
        getSCountriesList();
    }
    
    /// <summary>
    /// Method to get Country list for Supplier search 
    /// </summary>
    public void getSCountriesList()
    {
        BSureS_CommonUtil.isReport=true;
        lstSCountry=new list<SelectOption>();
        lstSCountry = BSureS_CommonUtil.getCountries(strSupplierSubZone);
    }
    
    /// Updated by VINU PRATHAP to Show error message from Labels
    /// <summary>
    /// showErrorMessage method for displaying error message
    /// </summary>
    /// <returns>pageReference</returns>
    public pageReference showErrorMessage(String msg)
    {
        ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,msg);
        ApexPages.addMessage(errormsg);
        return null;
    }    
    
    public pagereference Cancel(){
        return page.BSureS_Reports;
    } 
    
    public void getResults()
    {
        isSearch=true;
        //fromdate=date.valueOf(objSupplier.Fiscal_Year_End__c);
        //todate=date.valueOf(objSupplier.Next_Review_Date__c);     
        ////system.debug('-objSupplier-'+objSupplier+'-dateFrom--'+fromdate+'-dateTo--'+todate);
        //getSupDetails();
        lstSupDetails = new list<BSureS_Credit_Analysis__c>();
        lstSupDetails.clear();
        lstSupDetailsPagination = new list<BSureS_Credit_Analysis__c>();
        lstSupDetailsPagination.clear();
        lstRiskHigh=new list<string>();
        lstRiskMedium=new list<string>();
        lstRiskLow=new list<string>();  
        lstHighSupIds=new list<string> ();
        lstMediumSupIds=new list<string> ();
        lstLowSupIds=new list<string> ();
        
        if(strFromDate!='' && strToDate!='')
        {
            list<BSureS_Basic_Info__c> lstSup=new list<BSureS_Basic_Info__c>();
            string strQuery =   'SELECT Id,Name,Supplier_Name__c,Spend__c, State_Province__c,Supplier_Category_Name__c,Analyst_Name__c, '+
                                'Sub_Zone__c,Zone__c,BSureS_Country__c,Globe_ID__c, '+
                                'Sub_Zone__r.Name,Zone__r.Name,BSureS_Country__r.Name '+                                 
                                'from BSureS_Basic_Info__c WHERE Id!=null';  
           // //system.debug('--strSupplierName---'+strSupplierName+'--strCategory----'+strCategory+'--strSupplierZone--'+strSupplierZone); 
            if(strSupplierName != null && strSupplierName != '')
            {
                //string strsupname='%'+strSupplierName+'%';
                strQuery += ' AND Supplier_Name__c =: strSupplierName ';
            }
            if(strAnalystName != null && strAnalystName != '')
            {
                //string strsupname='%'+strSupplierName+'%';
                strQuery += ' AND Analyst_Name__c =: strAnalystName ';
            }
            if(strSupplierZone != null && strSupplierZone != 'ALL')
                strQuery +=' AND Zone__c =: strSupplierZone ';
            if(strSupplierSubZone != null && strSupplierSubZone != 'ALL')
                strQuery +=' AND Sub_Zone__c =: strSupplierSubZone ';
            if(strSupplierCountry != null && strSupplierCountry != 'ALL')
                strQuery +=' AND BSureS_Country__c =: strSupplierCountry ';
            
            //system.debug('strQuery----'+strQuery); 
            lstSup=database.query(strQuery);  
            //system.debug('lstSup----'+lstSup); 
            strQuery +=' limit 1000';
            
            set<id> setids=new set<id>();
            
            //mapSupIdDetails = new map<string,BSureS_Basic_Info__c>([select Id,name,Supplier_Name__c,Supplier_Category_Name__c,Globe_ID__c from BSureS_Basic_Info__c ]);
            
            for(BSureS_Basic_Info__c sup:lstSup)  
            {
                setids.add(sup.Id);
                //mapSupIdDetails.put(sup.Id,sup);
            }
            getRiskCount(setids);
        }
        else
        {   
            //ApexPages.Message errormsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select date range for the results');
            //ApexPages.addMessage(errormsg);  
            showErrorMessage(system.Label.BSureS_Please_select_date_range);
            
        }
    }
    /// <summary>
    /// To get the data from performcreditanalysis - to get Risk count
    /// </summary>
    public void getRiskCount(set<id> supIDs)
    {
        Date dtFrom = ((strFromDate != null && strFromDate != '')?date.parse(strFromDate):null);
        Date dtTo= ((strToDate != null && strToDate != '')?date.parse(strToDate):null);         
        //system.debug('fromdate--'+dtFrom+'todate--'+dtTo);
        //Anu:changed the review dates Review_Start_Date__c to Actual_Review_Start_Date__c and Review_End_Date__c to Expected_Review_End_Date__c
        list<BSureS_Credit_Analysis__c> lstSpends=[SELECT Id,Spend__c ,Supplier_ID__c, Review_Start_Date__c, Actual_Review_Start_Date__c, Expected_Review_End_Date__c, Review_End_Date__c, Review_Status__c, Risk_Level__c
                                                   FROM BSureS_Credit_Analysis__c
                                                   WHERE Supplier_ID__c in: supIDs AND Actual_Review_Start_Date__c >=:dtFrom  AND Expected_Review_End_Date__c <=:dtTo
                                                        AND Review_Status__c=:'Completed'];//Anu:changed the status to Approved to Completed as DBA loaded as completed
        //system.debug('--lstSpends---'+lstSpends);
        if(lstSpends.size()>0)
        {
            for(BSureS_Credit_Analysis__c objspend: lstSpends)
            {
                if(objspend.Risk_Level__c=='High')
                {
                    lstRiskHigh.add(objspend.Review_Status__c);
                    lstHighSupIds.add(objspend.Supplier_ID__c);
                }
                if(objspend.Risk_Level__c=='Medium')
                {
                    lstRiskMedium.add(objspend.Review_Status__c);
                    lstMediumSupIds.add(objspend.Supplier_ID__c);
                }
                if(objspend.Risk_Level__c=='Low')
                {
                    lstRiskLow.add(objspend.Review_Status__c);
                    lstLowSupIds.add(objspend.Supplier_ID__c);
                }
            }
        }
        //system.debug(lstRiskHigh.size()+'---lstRiskHigh---'+lstRiskMedium.size()+'--lstRiskMedium--'+lstRiskLow.size());
    }
    
    public void getSupDetails()
    {
        
        //system.debug('Test....');
        totallistsize=0;
        pageNumber = 0;      
        totalPageNumber = 0;
        //showSupDet=true;
        string strSelectedRiskLevel  = apexpages.currentpage().getparameters().get('Level');
        strSelectedRiskLvl=strSelectedRiskLevel;
        ////system.debug('strSelectedRiskLevel--'+strSelectedRiskLevel+'--mapSupIdDetails---'+mapSupIdDetails);
        lstSupDetails = new list<BSureS_Credit_Analysis__c>();
        lstSupDetailsPagination = new list<BSureS_Credit_Analysis__c>();
         Date dtFrom = ((strFromDate != null && strFromDate != '')?date.parse(strFromDate):null);
        Date dtTo= ((strToDate != null && strToDate != '')?date.parse(strToDate):null);         
        //system.debug('fromdate--'+dtFrom+'todate--'+dtTo);
        String strCompleted='Completed';//Anu:changed the status to Approved to Completed as DBA loaded as completed
        //system.debug('fromdate--./........'+strSelectedRiskLevel);
        //system.debug('lstMediumSupIds........'+lstMediumSupIds);
        
        
        
        if(strSelectedRiskLevel!='' && strSelectedRiskLevel!=null)
        {
            
            string strQuery =  'SELECT Id, Spend__c ,Supplier_ID__c,  Supplier_ID__r.Supplier_Category_Name__c, Supplier_ID__r.Supplier_Name__c, '+
                                        'Supplier_ID__r.Globe_ID__c, Review_Start_Date__c, Review_End_Date__c, Actual_Review_Start_Date__c, Expected_Review_End_Date__c, Review_Status__c, '+ 
                                        'Risk_Level__c, Rating__c, Rating_Type__c '+
                                ' FROM BSureS_Credit_Analysis__c '+
                                'WHERE Actual_Review_Start_Date__c >=: dtFrom AND Expected_Review_End_Date__c <=: dtTo AND Review_Status__c=: strCompleted AND Risk_Level__c=:strSelectedRiskLevel';
        
        
            //system.debug('strSelectedRiskLevel.........'+strSelectedRiskLevel);
            
            if(strSelectedRiskLevel=='High')            
            {
                strQuery += ' AND Supplier_ID__c in: lstHighSupIds ';
            
            }else if(strSelectedRiskLevel=='Medium')          
            {
                strQuery += ' AND Supplier_ID__c in: lstMediumSupIds ';
            
            }else if(strSelectedRiskLevel=='Low')         
            {
                strQuery += ' AND Supplier_ID__c in: lstLowSupIds ';
            
            }
            //strQuery+= ' order by Supplier_ID__r.Supplier_Name__c';
            string sortFullExp = sortExpression  + ' ' + sortDirection; 
            strQuery += ' order by ' + sortFullExp +' limit 1000' ;
            //system.debug('strQuery--'+strQuery);
            lstSupDetails =database.query(strQuery);  
            
            
           // //system.debug('lstSupDetails .......'+supps.size());
            
            
            /*if(mapSupIdDetails!=null)
            {
                if(strSelectedRiskLevel=='High')            
                {
                    for(string supid:lstHighSupIds)
                    {
                        BSureS_Basic_Info__c supObj= mapSupIdDetails.get(supid);
                        lstSupDetails.add(supObj);
                    }
                }
                if(strSelectedRiskLevel=='Medium')          
                {
                    for(string supid:lstMediumSupIds)
                    {
                        BSureS_Basic_Info__c supObj= mapSupIdDetails.get(supid);
                        lstSupDetails.add(supObj);
                    }
                }
                if(strSelectedRiskLevel=='Low')         
                {
                    for(string supid:lstLowSupIds)
                    {
                        BSureS_Basic_Info__c supObj= mapSupIdDetails.get(supid);
                        lstSupDetails.add(supObj);
                    }
                }
            }*/
        }
        totallistsize = lstSupDetails.size();// this size for pagination
        //lstSupDetails=supps;
        //system.debug('lstSupDetails^^^^^^^^^^^^^^^^^'+lstSupDetails.size());
        //pageSize = 10;// default page size will be 10
        pageSize = RReportSize;
        //system.debug('totallistsize================='+totallistsize);
        if(totallistsize==0)
        {
            BindData(0);// this method is for pagination, if no records found then passing zero to the pagination method
            NxtPageNumber=lstSupDetailsPagination.size();
            PrevPageNumber=0;
        }
        else
        {
            BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
            NxtPageNumber=lstSupDetailsPagination.size();
            PrevPageNumber=1;
        }
        //system.debug('lstSupids--'+lstSupDetails);
    }
    
     /// <summary>
    /// Below method fires when user clicks on next button of pagination.
    /// </summary>    
    /// <returns>pagereference</returns>
    public PageReference nextBtnClick() 
    {
        //system.debug('nextbutton'+pageNumber);
        BindData(pageNumber + 1);
        pageData(pageNumber + 1);
        
        return null;
    }

    // <summary>
    // Below method fires when user clicks on previous button of pagination.
    // </summary>
    // <returns>pagereference</returns>
    public PageReference previousBtnClick() 
    {
        pageData(pageNumber - 1);
        BindData(pageNumber - 1);
        return null;
    }
    
    /// <summary>
    /// Below method fires when user clicks on next button of pagination.
    /// <returns>Integer</returns>
    /// </summary>
    public Integer getPageNumber()
    {
        return pageNumber; 
    }
    
    /// <summary>
    /// Below method is for getting pagesize how many records per page .
    /// <returns>Integer</returns>  
    /// </summary> 
    public Integer getPageSize()
    {
        return pageSize;
    }

    /// <summary>
    /// Below method is for enabling and disabling the previous button of pagination.
    /// <param name="PreviousButtonEnabled"></param>
    /// <returns>boolean</returns>      
    /// </summary>
    public Boolean getPreviousButtonEnabled()
    {
        return !(pageNumber > 1);
    }

    /// <summary>
    /// Below method is for enabling and disabling the nextbutton of pagination.
    /// <param name="NextButtonDisabled"></param>
    /// <returns>boolean</returns>      
    /// </summary>
    public Boolean getNextButtonDisabled()
    {
        if (totallistsize ==0) 
            return true;
        else
            return ((pageNumber * pageSize) >= totallistsize);
    }


    /// <summary>
    /// Below method gets the total no.of pages.
    /// <param name="TotalPageNumber"></param>
    /// <returns>Integer</returns>      
    /// </summary>
    public Integer getTotalPageNumber()
    {
        totalPageNumber = 0;
        if (totalPageNumber == 0 && totallistsize!=0)
        {
            //system.debug(totalPageNumber+'-totallistsize--'+totallistsize);
            totalPageNumber = totallistsize / pageSize;
            Integer mod = totallistsize - (totalPageNumber * pageSize);
            if (mod > 0)
                totalPageNumber++;
        }


        return totalPageNumber;
    }
    
     /// <summary>
    /// Below method is the main method of pagination if have atleast one record then we pass 1 to this method otherwise zero.
    /// </summary>
    public void BindData(Integer newPageIndex)
    {
        //system.debug('*************88pavajflajf');
        try
        {
            lstSupDetailsPagination=new list<BSureS_Credit_Analysis__c>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            
            //system.debug('*************newPageIndex == '+newPageIndex);
            //system.debug('*************totalPageNumber =='+totalPageNumber);
            //system.debug('*************pageNumber =='+pageNumber);
            //system.debug('*************pageSize =='+pageSize);
            if(pageSize == null)pageSize = 10;
            if (newPageIndex > pageNumber)
            {
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;
            }
            else
            {
                max = newPageIndex * pageSize;
                min = max - pageSize;
            }

            if(lstSupDetails!=NULL)
            {
                for(BSureS_Credit_Analysis__c b : lstSupDetails)
                {
                    counter++;
                    
                        if (counter > min && counter <= max) 
                        {
                            lstSupDetailsPagination.add(b);// here adding files list
                        }
                }
            }
            
            pageNumber = newPageIndex;
            NlistSize=lstSupDetailsPagination.size();
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }
    
    /// <summary>
    /// Below method bnds the values for Next and previous buttons.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void pageData(Integer newPageIndex)
    {   
        if (newPageIndex > pageNumber)
        {
            PrevPageNumber=PrevPageNumber+pagesize;   
            NxtPageNumber=NxtPageNumber+NlistSize; 
        }
        if (newPageIndex < pageNumber)               
        {                                 
            PrevPageNumber=PrevPageNumber-pagesize;               
            NxtPageNumber=NxtPageNumber-NlistSize;      
        }
        //system.debug('NlistSize============='+NlistSize);
    }
    
    /// <summary>
    /// Below method binds the data for last button.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void LastpageData(Integer newPageIndex)
    {
        try
        {
            lstSupDetailsPagination=new list<BSureS_Credit_Analysis__c>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;          
            //system.debug('NlistSizeLastpage1============='+NlistSize);
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;    
            for(BSureS_Credit_Analysis__c a : lstSupDetails)
            {
                counter++;
                
                if (counter > min && counter <= max) 
                {
                       lstSupDetailsPagination.add(a);// here adding the folders list      
                }
            }
            pageNumber = newPageIndex;
            NlistSize=lstSupDetailsPagination.size();
                
            PrevPageNumber=totallistsize - NlistSize +1;   
            NxtPageNumber=totallistsize;
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }
    
    /// <summary>
    /// Below method bnds the value for last button.
    /// </summary>
    public PageReference LastbtnClick() 
    {
        BindData(totalpagenumber - 1);
        //system.debug('Lastbutton++'+pagenumber);
        LastpageData(totalpagenumber);
        
        return null;
    }
    
    /// <summary>
    /// Below method bnds the value for first button.
    /// </summary>
    public PageReference FirstbtnClick() 
    {
        BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
        NxtPageNumber=NlistSize;
        PrevPageNumber=1;
        return null;
    }
    
    public Pagereference ExportToExcel(){
        Pagereference pageref = new Pagereference('/apex/BSureS_SupplierRiskExport');    
        return pageref;
    } 
}