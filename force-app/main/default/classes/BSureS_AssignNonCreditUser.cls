/***********************************************************************************************
* Controller Name   : BSureS_AssignNonCreditUser
* Date              : 14/11/2012 
* Author            : KISHOREKUMAR A
* Purpose           : Class for Assign Non Credit Users 
* Change History    : 
* Date                  Programmer              Reason
* -------------------- ------------------- ------------------------- 
* 14/11/2012            KISHOREKUMAR A          Initial Version
* 07/18/2013            Kishorekumar A          Added Security Review condtions for Fields and DML statements
**************************************************************************************************/
   
global with sharing class BSureS_AssignNonCreditUser {
    public string strCurrentSupplierId{get;set;} // page returned id
    public string buyerId{get;set;}
    public boolean showeditrecs {get; set;} //lookup icon visibility purpose
    //public List<BSureS_User_Additional_Attributes__c> userList {get; set;} 
    public List<Contact> userList {get; set;} 
    public string strbuyer{get;set;} //variable for Buyer Id(Assign Non Credit user)
    //public string strBuyerName{get;set;} //variable for Buyer name(Assign Non Credit user)
    public string strBuyerId{get;set;}
    public string strBuyerCategory{get;set;}
    public Id strsupplierId{get;set;} //displays supplier 
    public string strsupplierName{get;set;} //displays Supplier Name 
    public string strsupplierCategoryId{get;set;} //variable for get Supplier Category Id
    public string srtCName{get;set;} //variable for get Supplier Category name
    public BSureS_Category__c strsupplierCategoryName{get;set;} //variable for get Supplier Category Information
    public map<string,string> mapuserid{get;set;}
    public string strSearchBuyer{get;set;}
    public BSureS_Assigned_Buyers__c  Buyer{get;set;}
    public list<BSureS_Assigned_Buyers__c> lst_buyerInfo{get;set;}
   // public list<BSureS_User_Additional_Attributes__c> lstsBuyer{get;set;}
    public List<Contact> lstsBuyer {get; set;} 
    
    //Email Variables
    public string strEmailCSV {get; set;}
    public string strSubject {get; set;}
    public string strEmailBody {get; set;}
    public set<Id> setEmailRcpts {get; set;}
    public List<Id> editct = new List<Id>();
    public string strSendMails{get;set;}
    
    public list<BSureS_Zone_Manager__c> lstZoneManager{get;set;}
    public list<BSureS_Sub_Zone_Manager__c> lstSubZoneManager{get;set;}
    public list<BSureS_Country_Manager__c> lstCountryManager{get;set;}
    public set<string> lstChatterSubscribe{get;set;}
    public boolean blndeletebtnVisibility{get;set;}
    public Id currentUserId{get;set;}
    public string strCEO{get;set;}
    public string strBuyerRole{get;set;}
    public string  strCategoryInBuyerSection{get;set;}
    
    public boolean blnNotificationFlag{get;set;}
    public string strSupplierForum{get;set;}
    
    /// <summary>
    /// Constructor 
    /// </summary> 
    public BSureS_AssignNonCreditUser(ApexPages.StandardController controller)   
    { 
        blnNotificationFlag = true;
        strCategoryInBuyerSection = 'FALSE';
        setEmailRcpts = new set<Id>();
        lstZoneManager = new list<BSureS_Zone_Manager__c>();
        lstSubZoneManager = new list<BSureS_Sub_Zone_Manager__c>();
        lstCountryManager = new list<BSureS_Country_Manager__c>();
        Buyer = new BSureS_Assigned_Buyers__c();
        blndeletebtnVisibility = false;
        
        strSendMails = BSureS_CommonUtil.getConfigurationValues('BSureS_SendBuyerAssnMails').get(0);
        
         if(Apexpages.currentPage().getParameters().get('sid') != null)
         {
            strCurrentSupplierId = Apexpages.currentPage().getParameters().get('sid');
         }  
         else
         {
             buyerId = apexpages.currentpage().getParameters().get('id');
             if(buyerId!=null)
             {
                Buyer = [select Id,Supplier_ID__c,Buyer_Name__c,Buyer_Id__c,Buyer_Contact_ID__c from BSureS_Assigned_Buyers__c where Id=:buyerId ];//Added new field to refernce Buyer Contact             
             }
             strCurrentSupplierId = Buyer.Supplier_ID__c;
         }
        
        currentUserId = UserInfo.getUserId();
        
        /*List<String> pValue = BSureS_CommonUtil.getConfigurationValues('BSureS_SupplierRoles');
        ////system.debug('pValue========='+pValue);
        if(pValue != null && pValue.size() > 0)
        {
            string[] strRole = pValue.get(0).split(',');
            strCEO = strRole[0];
            strBuyerRole = strRole[7];
        } */
        strCEO = BSureS_CommonUtil.getConfigurationValues('BSureS_CEORole').get(0);
        strBuyerRole = BSureS_CommonUtil.getConfigurationValues('BSureS_BuyerRole').get(0);
        strCategoryInBuyerSection = BSureS_CommonUtil.getConfigurationValues('BSureS_includeCategoryInBuyerSection').get(0);
        strSupplierForum = BSureS_CommonUtil.getConfigurationValues('BSureS_SupplierForum').get(0);
        ////system.debug('strCategoryInBuyerSection========'+strCategoryInBuyerSection); 
        //List<user> AdminProfile = [select id from user where Profile.Name='System Administrator' and id =:currentUserId];
        list<user> AdminRole;
        if(strCEO != null && strCEO !='')
        { 
            AdminRole = [select id from user where userrole.name =:strCEO ];
        }   
        if((AdminRole.size() > 0 && currentUserId == AdminRole.get(0).Id) && buyerId != null)
        {
            blndeletebtnVisibility = true;
        }
            
        strbuyer = Buyer.Buyer_Name__c;
        //strBuyerName = Buyer.Buyer_Name__c;
        strBuyerId = Buyer.Buyer_Contact_ID__c; //Buyer.Buyer_Id__c; //Added to reference Buyer Contact Ref instead of Buyer User     
        List<BSureS_Basic_Info__c> lstSupplierInfo;
        
        if(strCurrentSupplierId != null)
        {
            lstSupplierInfo = [select id,Supplier_Name__c,Supplier_Category_Name__c,Zone__c,Sub_Zone__c,BSureS_Country__c,Supplier_Category__c,Analyst__c,Manager__c,Bakup_Analysts__c,Notification_Flag__c from BSureS_Basic_Info__c where id=:strCurrentSupplierId limit 1];
        }
        
        if(lstSupplierInfo != null && lstSupplierInfo.size() > 0)
        {
            for(BSureS_Basic_Info__c name: lstSupplierInfo)
            {
                if(name.Supplier_Name__c != null)
                {
                    strsupplierName = name.Supplier_Name__c; 
                }
                ////system.debug('name.Supplier_Category_Name__c=============='+name.Supplier_Category_Name__c);
                if(name.Supplier_Category_Name__c != null)
                {
                    strBuyerCategory = name.Supplier_Category_Name__c;
                }
                strsupplierId = name.Id; 
                if(name.Supplier_Category__c != null)
                {
                    strsupplierCategoryId = name.Supplier_Category__c; 
                    ////system.debug('category=====Supplier==='+ strsupplierCategoryId);
                }
                
                //Email sending..
                if(name.Analyst__c != null)
                {
                    //setEmailRcpts.add(name.Analyst__c);
                }
                
                if(name.Manager__c != null)
                {
                    if( (name.Manager__c == currentUserId ) && buyerId != null)
                    {
                        blndeletebtnVisibility = true;
                    }
                    //setEmailRcpts.add(name.Manager__c);
                }
                
                if(name.Bakup_Analysts__c != null )
                {
                    if(name.Bakup_Analysts__c.contains(','))
                    {
                        editct = name.Bakup_Analysts__c.split(',');
                    }
                    else
                    {
                        editct.add(name.Bakup_Analysts__c);
                    }
                    //setEmailRcpts.addAll(editct);
                }
               
                if(strsupplierCategoryId!=null)
                {
                    strsupplierCategoryName = [select Name  from BSureS_Category__c where id =:strsupplierCategoryId];              
                    if(strsupplierCategoryName.Name != null &&  strsupplierCategoryName.Name!='')
                    {
                        srtCName = strsupplierCategoryName.Name;
                    }
                }
            }
            
            if(lstSupplierInfo.get(0).Zone__c != null)
            {
                lstZoneManager = [select Zone_Manager__c from BSureS_Zone_Manager__c where Zone__c =: lstSupplierInfo.get(0).Zone__c];
            }
            if(lstSupplierInfo.get(0).Notification_Flag__c != null)
            {
                 blnNotificationFlag = lstSupplierInfo.get(0).Notification_Flag__c;
            }
            if(lstZoneManager != null && lstZoneManager.size() > 0)
            {
                for(BSureS_Zone_Manager__c objZM :lstZoneManager )
                {
                    if((objZM.Zone_Manager__c == currentUserId )&& buyerId != null)
                    {
                        blndeletebtnVisibility = true;
                    }
                    //setEmailRcpts.add(objZM.Zone_Manager__c);
                }
            }
            
            if(lstSupplierInfo.get(0).Sub_Zone__c != null)
            {
                lstSubZoneManager = [select Sub_Zone_Manager__c  from BSureS_Sub_Zone_Manager__c where Sub_Zone__c =: lstSupplierInfo.get(0).Sub_Zone__c];
            }
            
            if(lstSubZoneManager != null && lstSubZoneManager.size() > 0)
            {
                for(BSureS_Sub_Zone_Manager__c objSZM:lstSubZoneManager)
                {
                    if( (objSZM.Sub_Zone_Manager__c == currentUserId) && buyerId != null)
                    {
                        blndeletebtnVisibility = true;
                    }
                    //setEmailRcpts.add(objSZM.Sub_Zone_Manager__c);
                }
            }
            
            if(lstSupplierInfo.get(0).BSureS_Country__c != null)
            {
                lstCountryManager = [select Country_Manager__c from BSureS_Country_Manager__c where Country__c =: lstSupplierInfo.get(0).BSureS_Country__c];
            }
            
            if(lstCountryManager != null && lstCountryManager.size() > 0)
            {
                for(BSureS_Country_Manager__c objCM:lstCountryManager)
                {
                    if( (objCM.Country_Manager__c == currentUserId) && buyerId != null)
                    {
                        blndeletebtnVisibility = true;
                    }
                    //setEmailRcpts.add(objCM.Country_Manager__c);
                }
            }
        }   
    }
    
    /// <summary>
    /// Method to Show Buyer list
    /// </summary> 
    public void showBuyerDetails()
    {
        showeditrecs=true;
        userList=[Select Name,id,FirstName,LastName,Email,Receive_Daily_Digests__c,Receive_Email_Notifications__c,isActive__c from Contact where isActive__c=true limit 1000];
        //profile getprofileId = [select id from profile where name = 'BSureS_Buyer'];
       /*
        List<BSureS_userCategoryMapping__c> lstcategoryMaping ;
        mapuserid = new map<string,string>();
        
        if(strCategoryInBuyerSection != null && strCategoryInBuyerSection.equalsIgnoreCase('TRUE'))
        {
            if(srtCName != null && (strBuyerRole != null && strBuyerRole !=''))
            {
                
                //new Changes on Buyer Supplier Category Mapping
                string strSupplCategoryChk = 'select UserID__c,CategoryName__c from BSureS_userCategoryMapping__c where id != null';
                if(srtCName != null)
                {
                    strSupplCategoryChk += ' and CategoryName__c =:srtCName ';
                }
                if(strBuyerRole != null)
                {
                    strSupplCategoryChk += ' and UserID__r.IsActive = true and UserID__r.userrole.name =: strBuyerRole';
                }
                lstcategoryMaping = Database.query(strSupplCategoryChk);
                //lstcategoryMaping = [select UserID__c from BSureS_userCategoryMapping__c 
                                                               // where CategoryName__c =:srtCName and UserID__r.IsActive = true and UserID__r.userrole.name =: strBuyerRole];//UserID__r.ProfileId =:getprofileId.id 
            }
            
            if(lstcategoryMaping!= null && lstcategoryMaping.size() > 0)
            {
                for(BSureS_userCategoryMapping__c getUserId:lstcategoryMaping)
                {
                    if(getUserId.UserID__c != null)
                    {
                        mapuserid.put(getUserId.UserID__c,getUserId.UserID__c);
                    }
                }
            }  
              
            if(mapuserid.keySet() != null)
            {
                ////system.debug('strCategoryInBuyerSection======255=='+strCategoryInBuyerSection); 
                userList = [select name,User__c,User__r.Name,Zone_Name__c,Sub_Zone_Name__c,Country_Name__c from BSureS_User_Additional_Attributes__c where User__r.Id IN :mapuserid.keySet()];
            }
        } 
        else
        {
            String strUserlistQuery = '';
            strUserlistQuery += 'select name,User__c,User__r.Name,Zone_Name__c,Sub_Zone_Name__c,Country_Name__c ';
            strUserlistQuery += ' from BSureS_User_Additional_Attributes__c where id != null and ';
            strUserlistQuery += ' User__r.IsActive = true and ';
            if(strBuyerRole != null && strBuyerRole != '')
            {
                strUserlistQuery += ' User__r.userrole.name =: strBuyerRole';
                ////system.debug('strUserlistQuery==========='+strUserlistQuery);
            }
            userList = Database.query(strUserlistQuery) ;
            if(!userList.isEmpty())
            {
                for(BSureS_User_Additional_Attributes__c cAddObj : userList)
                {
                    if(cAddObj.User__c != null)
                    {
                        mapuserid.put(cAddObj.User__c, cAddObj.User__c);
                    }
                }
            }
            ////system.debug('userList========'+userList);
        }  
        */
    }
    
    /// <summary> 
    /// Method to close the Buyer list, after select the Buyer
    /// </summary> 
    public void closepopup() 
    {
        string selectbuyer = apexpages.currentpage().getparameters().get('buyerid');
        //lstsBuyer = new list<BSureS_User_Additional_Attributes__c>(); 
        lstsBuyer = new list<Contact>();
        if(selectbuyer != null)
        { 
         //lstsBuyer = [select id,User__r.Name,User__r.FirstName,User__r.LastName from BSureS_User_Additional_Attributes__c where id =:selectbuyer];
         lstsBuyer=[Select Name,id,Email,FirstName,LastName from Contact where id=:selectbuyer];
     	 strbuyer = lstsBuyer.get(0).FirstName +' '+ lstsBuyer.get(0).LastName;
     	 strBuyerId = lstsBuyer.get(0).id;
         //strBuyerName = lstsBuyer.get(0).User__r.FirstName +' '+ lstsBuyer.get(0).User__r.LastName;
        }
        
        showeditrecs=false; 
    }
    
    /// <summary>
    /// Method to Search the Buyer Names
    /// </summary>
    public void searchBtn()
    {
        string strSerachB = '%'+strSearchBuyer+'%';
        //system.debug('strSerachB===================='+strSerachB);
        //system.debug('mapuserid.keySet()===='+mapuserid.keySet());
        if(strSearchBuyer != null && strSearchBuyer != '') 
        {
        	userList=[Select Name,id,FirstName,LastName,Email,Receive_Daily_Digests__c,Receive_Email_Notifications__c,isActive__c from Contact 
        						where isActive__c=true and (name like :  strSerachB or Email like : strSerachB) LIMIT 1000];
           // userList = [select name,User__c,User__r.Name,Zone_Name__c,Sub_Zone_Name__c,Country_Name__c from BSureS_User_Additional_Attributes__c 
                           // where User__r.Name like : strSerachB and  User__r.Id IN :mapuserid.keySet()];
            ////system.debug('userList============111==='+userList);
        }
        else
        {
           userList=[Select Name,id,FirstName,Email,LastName,Receive_Daily_Digests__c,Receive_Email_Notifications__c,isActive__c from Contact 
        						where isActive__c=true];
           // userList = [select name,User__c,User__r.Name,Zone_Name__c,Sub_Zone_Name__c,Country_Name__c from BSureS_User_Additional_Attributes__c where User__r.Id IN :mapuserid.keySet()];           
            ////system.debug('userList============222==='+userList);
        }
    }
    
    /// <summary>
    /// Method to Close popup Search
    /// </summary>
    public void closepopupBtn() 
    {
        showeditrecs=false; 
    } 
    
    /// <summary>
    /// Method to Save Buyer information
    /// </summary>
    public pageReference  buyerSave()
    {
        ApexPages.Message errormsg;
        pagereference pageref;
        lstChatterSubscribe = new set<string>();
        ////system.debug('strBuyerId=================='+strBuyerId);
        ////system.debug('strbuyer========================='+strbuyer);
        if(strbuyer == null || strbuyer == '' )
        { 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,System.label.BSureS_VBuyer));
            return null;
        }  
        else
        {
            lst_buyerInfo = [select Buyer_Id__c,Buyer_Contact_ID__c from BSureS_Assigned_Buyers__c where Supplier_ID__c =:strsupplierId limit 200];
            if(lst_buyerInfo != null && lst_buyerInfo.size() > 0)
            {
                /*for(BSureS_Assigned_Buyers__c objBuyerId: lst_buyerInfo)
                {
                    setEmailRcpts.add(objBuyerId.Buyer_Id__c);
                }*/
            }
            List<BSureS_Assigned_Buyers__c> buyerV = null;
            
            if(buyerId != null && buyerId !=''){
            	//buyerV = [select id from BSureS_Assigned_Buyers__c where Supplier_ID__c =:strsupplierId and Buyer_Id__c =:strBuyerId and Buyer_Id__c !=: Buyer.Buyer_ID__c];
            	buyerV = [select id from BSureS_Assigned_Buyers__c where Supplier_ID__c =:strsupplierId and Buyer_Contact_ID__c =:strBuyerId and Buyer_Contact_ID__c !=: Buyer.Buyer_Contact_ID__c];
            }else{
            	//buyerV = [select id from BSureS_Assigned_Buyers__c where Supplier_ID__c =:strsupplierId and Buyer_Id__c =:strBuyerId];
            	buyerV = [select id from BSureS_Assigned_Buyers__c where Supplier_ID__c =:strsupplierId and Buyer_Contact_ID__c =:strBuyerId];
            }
            system.debug(buyerId+'Buyers@@'+strBuyerId+'BuyerID**'+buyerV);
            
            if(buyerV != null && buyerV.size() > 0)
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,System.label.BSureS_VExistsBuyer));
                return null; 
            }
            else 
            {
                pageref = new pagereference('/apex/Bsures_viewSupplierDetails?Id='+strCurrentSupplierId);
                BSureS_Assigned_Buyers__c bsave = new BSureS_Assigned_Buyers__c();
                if(buyerId != null )
                {
                     bsave = buyer; 
                }
                ////system.debug('strbuyer========='+strbuyer);
                setEmailRcpts.add(strBuyerId);
               // bsave.Buyer_Id__c = strBuyerId; 
                bsave.Buyer_Contact_ID__c = strBuyerId; 
                bsave.Buyer_Name__c = strbuyer;
                
                //if(buyerId == null || buyerId == '')
                //{ 
                    ////system.debug('strBuyerCategory-----------------'+strBuyerCategory);
                if(strsupplierId != null )
                {
                    bsave.Supplier_ID__c = strsupplierId;
                }
                if(strsupplierName != null && strsupplierName != '')
                {
                    bsave.Supplier_Name__c  = strsupplierName;
                }
                if( strBuyerCategory != null && strBuyerCategory != '' )
                {    
                    list<BSureS_userCategoryMapping__c> lstCB = new 
                                        list<BSureS_userCategoryMapping__c>([select UserID__c,CategoryName__c from BSureS_userCategoryMapping__c where id != null and UserID__c =: strBuyerId and CategoryName__c =: strBuyerCategory]);
                                        
                    //bsave.Buyer_Category__c = strBuyerCategory;
                    //if(strCategoryInBuyerSection.equalsIgnoreCase('TRUE'))
                    if(lstCB != null && lstCB.size() > 0)
                    { 
                        bsave.Buyer_Category__c = strBuyerCategory;
                    }
                    else
                    {
                        bsave.Buyer_Category__c = '';
                    }
                }
                //} 
                
                lstChatterSubscribe.add(strBuyerId);
                //Email sending..
                Database.UpsertResult resultDB= Database.upsert(bsave);
                if(resultDB.isSuccess())
                {
                    BSureS_CommonUtil.InsertFollowers(strsupplierId,lstChatterSubscribe); 
                    if(strSendMails.equalsIgnoreCase('TRUE'))
                    {
                        SendNotification();
                    }
                    
                }
                //upsert bsave; 
                return pageref;
            }    
        } 
    }
    public void SendNotification()
    {
        strEmailCSV = '';
        strEmailBody = '';
        strSubject = 'Buyer Assigned for Supplier :'+ strsupplierName;
        if(setEmailRcpts != null && setEmailRcpts.size() > 0)
        {
            /*
            list<User> objUserEmails = [select Id, Email, Name From User Where Id IN: setEmailRcpts];
            for(User objEachUser : objUserEmails)
            {
                strEmailCSV += objEachUser.id + ',';
            }
            strEmailCSV = strEmailCSV.substring(0, strEmailCSV.length() -1);
            */ 
            strEmailBody += '<table cellspacing="2" cellpadding="0">';
            strEmailBody += 'The following Buyer is Assigned to the Supplier :  '+strSupplierName;
            strEmailBody += '</table>';
            strEmailBody += '<table cellspacing="2" cellpadding="0" border="1">';
            strEmailBody += '<tr><td height="40px" colspan="2" width="100%"><font name="Verdana" ><center> <b> '+ strSupplierForum +' </b> </center></font></td></tr>';
            strEmailBody += '<tr><td height="32px" width="22%">Buyer Name </td><td height="21px">: '+  strbuyer +'</td></tr>';
            strEmailBody += '<tr><td height="32px" width="22%">Posted By </td><td height="21px">: '+UserInfo.getName() +'</td></tr>';
            strEmailBody += '<tr><td height="32px" width="22%">Posted Date </td><td height="21px">: '+ System.dateTime.Now().format('EEEE, MMM d, yyyy HH:mm:ss aaa z') +'</td></tr>';
            strEmailBody += '</table>';
            BSureS_CommonUtil.SendEmailToQueue(setEmailRcpts,strSubject,strEmailBody,blnNotificationFlag+'',system.today(),false);
        }
        /*strEmailBody += '<table cellspacing="0" cellpadding="0" >';
        strEmailBody += '<tr><td>';
        strEmailBody += 'Buyer : ' + strbuyer + ' is Assigned to Supplier : '+strsupplierName;
        strEmailBody += '</td></tr></table>';*/
        
    }
    /*
    public pagereference Bdelete()
    { 
        pagereference pageref = new pagereference('/apex/Bsures_viewSupplierDetails?Id='+strCurrentSupplierId);
        ////system.debug('buyerId======='+buyerId);
        if(buyerId != null)
        {
            delete[select id from BSureS_Assigned_Buyers__c where id =: buyerId limit 1];
        }
            
        return pageref;
        //return null;
    }*/
    public pagereference cancel()  
    {
        pagereference pageref = new pagereference('/apex/Bsures_viewSupplierDetails?Id='+strCurrentSupplierId);
        return pageref;
    }
}