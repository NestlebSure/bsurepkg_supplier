@isTest
private class TestbSureS_ExperianEnquiry {
    static testMethod void myUnitTest() {
      System.test.startTest();
      
      BSureS_Basic_Info__c objBasicInfo = new BSureS_Basic_Info__c();
      objBasicInfo.Supplier_Name__c = 'Demo Supplier';
      objBasicInfo.BISFileNumber__c = '7986548975';
      objBasicInfo.Globe_ID__c = '589647';
      objBasicInfo.Planned_Review_Date__c = system.today();
      insert objBasicInfo;
      
      DateTime d = Date.Today() ;
	  string dateStr =  d.format('ddMMyy') ;
      string fName = dateStr+'_Experian_Rpt_'+objBasicInfo.Globe_ID__c;
    
      BSureS_Credit_Review_Section__c objCreditInfo = new BSureS_Credit_Review_Section__c();
      objCreditInfo.Supplier_ID__c = objBasicInfo.id;
      objCreditInfo.TitleDescription__c = fName;
      insert objCreditInfo;
      
      Attachment objAttach = new Attachment();
      objAttach.Name = dateStr+'_Experian_Rpt_'+objBasicInfo.Globe_ID__c;
      objAttach.parentId = objCreditInfo.Id;
      objAttach.Body = blob.valueof('This is to insert file for testing');
      insert objAttach;
      ApexPages.currentPage().getParameters().put('parentId',objCreditInfo.Id);
      ApexPages.currentPage().getParameters().put('attachName',fName);
      bSureS_ExperianEnquiry objExpEnq = new bSureS_ExperianEnquiry();
      
      string assign='1234';
        system.assertEquals('1234',assign);
        System.test.stopTest();
    }
}