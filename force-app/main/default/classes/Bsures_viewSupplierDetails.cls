/*************************************************************************************************
* Controller Name   : Bsures_viewSupplierDetails
* Date              : 26/11/2012
* Author            : Kishore  
* Purpose           : Class for Supplier Detail Page
* Change History    : 
* Date                  Programmer              Reason
* -------------------- ------------------- -------------------------
* 26/11/2012                                Initial Version
**************************************************************************************************/
global with sharing class Bsures_viewSupplierDetails {
    public string strSupplierId{get;set;} //Variable for get Supplier ID
    public string strAnalystName{get;set;} //Variable for get Supplier Alalyst ID
    public string strManagerName{get;set;} //Variable for get Supplier Manager ID
    public string strBackUpAnalystsNames{get;set;} //Variable for get Supplier Backup Alalysts ID
    public boolean visibilityEdit{get;set;} //standard Edit button visibility
    public boolean visibilityCreditTeamAssignment{get;set;}
    public boolean visibilityNewCreditAnalysisBtn{get;set;}
    public boolean visibilityNewSpendhistory{get;set;}
    public Id currentUserId{get;set;} // get current user id
    public boolean deleteBuyerInfo{get;set;}
    
    public boolean visibilityNewBuyerBtn{get;set;}//New Buyer button Visibility
    public list<BSureS_Zone_Manager__c> lstZoneManager{get;set;}
    public list<BSureS_Sub_Zone_Manager__c> lstSubZoneManager{get;set;}
    public list<BSureS_Country_Manager__c> lstCountryManager{get;set;}
    //public set<string> lstprofileNames{get;set;}
    public boolean blnSuppplierEdit{get;set;}
    //public boolean blnSupplierNonEdit{get;set;}
    public string strCEO{get;set;}
    
    public list<BSureS_Credit_Analysis__c> lstCreditAnalysis{get;set;}
     
    /// <summary>
    /// constructor 
    /// </summary>
    public Bsures_viewSupplierDetails(ApexPages.StandardController controller) 
    {
        strSupplierId=Apexpages.currentPage().getParameters().get('Id'); //get the current supplier id   
        //strSupplierId=Apexpages.currentPage().getParameters().get('sid'); //get the current supplier id
        lstZoneManager = new list<BSureS_Zone_Manager__c>();
        lstSubZoneManager = new list<BSureS_Sub_Zone_Manager__c>(); 
        lstCountryManager = new list<BSureS_Country_Manager__c>();
        lstCreditAnalysis = new list<BSureS_Credit_Analysis__c>();
        blnSuppplierEdit = false;
        visibilityEdit = false;
        visibilityCreditTeamAssignment = false;
        //blnSupplierNonEdit = false;
        list<Profile> lst_profile = [SELECT Name FROM Profile WHERE Id =: Userinfo.getProfileId()];
        
        list<BSureS_Basic_Info__c> sDetailsView;
        if(strSupplierId != null)
        {
             sDetailsView = [select id,Analyst__c,Analyst_Name__c,Manager__c,Bakup_Analysts__c,Supplier_ID__c,
                                                Zone__c,Sub_Zone__c,BSureS_Country__c
                                                from BSureS_Basic_Info__c where id =: strSupplierId];
                                                
             lstCreditAnalysis = [select id,Review_Status__c,Supplier_ID__c from BSureS_Credit_Analysis__c where Supplier_ID__c =:strSupplierId and Review_Status__c = 'Started'];
       
        }
        //system.debug('lstCreditAnalysis======size====='+lstCreditAnalysis.size());
        if(sDetailsView != null && sDetailsView.size() >0 )
        {
            if(sDetailsView.get(0).Analyst__c != null )
            {
                strAnalystName = sDetailsView.get(0).Analyst__c;
            }
            if(sDetailsView.get(0).Manager__c!= null )
            {
                strManagerName = sDetailsView.get(0).Manager__c;
            }
            if(sDetailsView.get(0).Bakup_Analysts__c != null)
            {
                strBackUpAnalystsNames = sDetailsView.get(0).Bakup_Analysts__c;
            }
            
            if(sDetailsView.get(0).Zone__c != null)
            {
                lstZoneManager = [select Zone_Manager__c from BSureS_Zone_Manager__c where Zone__c =: sDetailsView.get(0).Zone__c];
            }
            if(sDetailsView.get(0).Sub_Zone__c != null)
            {
                lstSubZoneManager = [select Sub_Zone_Manager__c  from BSureS_Sub_Zone_Manager__c where Sub_Zone__c =: sDetailsView.get(0).Sub_Zone__c];
            }
            if(sDetailsView.get(0).BSureS_Country__c != null)
            {
                lstCountryManager = [select Country_Manager__c from BSureS_Country_Manager__c where Country__c =: sDetailsView.get(0).BSureS_Country__c];
            }
        }   
        
        currentUserId = UserInfo.getUserId();
        //lstprofileNames = new set<string>();
        /*List<String> pValue = BSureS_CommonUtil.getConfigurationValues('BSureS_SupplierRoles');
        //system.debug('pValue========='+pValue);
        if(pValue != null && pValue.size() > 0)
        {
            string[] strRole = pValue.get(0).split(',');
            strCEO = strRole[0];
        }*/
        strCEO = BSureS_CommonUtil.getConfigurationValues('BSureS_CEORole').get(0);     
        //system.debug('strCEO============='+strCEO);
        //lstprofileNames.add('System Administrator');
        //lstprofileNames.add('BSureS_Credit_User_Admin');
        //lstprofileNames.add('BSureS_Credit_User_SuplerAdmin');
        /*List<user> AdminProfile;
        if(lstprofileNames != null && lstprofileNames.size()>0)
        {
            AdminProfile= [select id from user where Profile.Name IN :lstprofileNames and id =:currentUserId];
        }*/
        set<id> setUserIds = new set<id>();
        if(strCEO != null && strCEO != '')
        {
            for(User eachUser : [select id from user where userrole.name =:strCEO])
            {
                setUserIds.add(eachUser.Id);
            }
        } 
        
        
        if(setUserIds.size() > 0 && setUserIds.contains(currentUserId))
        {
            //system.debug('lstuser====='+setUserIds);
            blnSuppplierEdit = false;
            visibilityEdit = true;
            visibilityCreditTeamAssignment = true; 
            visibilityNewBuyerBtn = true;
            visibilityNewCreditAnalysisBtn = true;
            deletebuyerInfo = true;
            visibilityNewSpendhistory = true;
            
        }
        
        if(strBackUpAnalystsNames != null )
        {
            if(strBackUpAnalystsNames.contains(','))
            {
                for(String ba:strBackUpAnalystsNames.split(','))
                {
                    if(currentUserId == ba)
                    {
                        visibilityEdit = true;
                        visibilityNewCreditAnalysisBtn = true;
                        break;
                    }
                }
            }
            else
            {
                if(currentUserId == strBackUpAnalystsNames)
                {
                    visibilityEdit = true;
                    visibilityNewCreditAnalysisBtn = true;
                }
            }   
        }
        
        if(strManagerName != null && currentUserId == strManagerName)
        {
            visibilityCreditTeamAssignment = true;
            visibilityNewBuyerBtn = true;
            visibilityNewCreditAnalysisBtn = true;
            deletebuyerInfo = true;
        }
        //system.debug('currentUserId========'+currentUserId);
        //system.debug('strAnalystName=========='+strAnalystName);
        //system.debug('strManagerName======='+strManagerName);
        if(currentUserId == strAnalystName || currentUserId == strManagerName)
        {
            //system.debug('testttttttttttttttt');
            if(setUserIds.size() > 0 && setUserIds.contains(currentUserId))
            {
                blnSuppplierEdit = true;
                //system.debug('blnSuppplierEdit========='+blnSuppplierEdit);
            }   
            visibilityEdit = true;
            visibilityNewCreditAnalysisBtn = true;
            visibilityNewSpendhistory = true;
        }
        
        if(lstZoneManager != null && lstZoneManager.size() > 0)
        {
            for(BSureS_Zone_Manager__c ObjZM:lstZoneManager)
            {
                if(ObjZM.Zone_Manager__c == currentUserId)
                {
                    blnSuppplierEdit = true;
                    visibilityCreditTeamAssignment = true; 
                    visibilityNewBuyerBtn = true;
                    visibilityNewCreditAnalysisBtn = true;
                    visibilityEdit = true;
                    deletebuyerInfo = true;
                    visibilityNewSpendhistory = true;
                }
            }
        }
        
        if(lstSubZoneManager != null && lstSubZoneManager.size() >0)
        {
            for(BSureS_Sub_Zone_Manager__c ObjSZM : lstSubZoneManager)
            {
                if(ObjSZM.Sub_Zone_Manager__c == currentUserId)
                {
                    blnSuppplierEdit = true;
                    visibilityCreditTeamAssignment = true; 
                    visibilityNewBuyerBtn = true;
                    visibilityNewCreditAnalysisBtn = true;
                    visibilityEdit = true;
                    deletebuyerInfo = true;
                    visibilityNewSpendhistory = true;
                }
            }
        }
        
        if(lstCountryManager != null && lstCountryManager.size() >0)
        {
            for(BSureS_Country_Manager__c ObjCM : lstCountryManager)
            {
                if(ObjCM.Country_Manager__c == currentUserId)
                {
                    blnSuppplierEdit = true;
                    visibilityCreditTeamAssignment = true; 
                    visibilityNewBuyerBtn = true;
                    visibilityNewCreditAnalysisBtn = true;
                    visibilityEdit = true;
                    deletebuyerInfo = true;
                    visibilityNewSpendhistory = true;
                }
            }
        }
        //If the Review Status = Started the Next Review Date is not editable within the Last Review section.
        if(lstCreditAnalysis != null && lstCreditAnalysis.size() > 0)
        {
            //system.debug('testEditNDate==test=========='+blnSuppplierEdit);
            blnSuppplierEdit = false;
        }
         //system.debug('testEditNDate======123======'+blnSuppplierEdit);
    }  
   
      
    /// <summary>
    /// Method to view the Supplier Details
    /// </summary>
    public BSureS_Basic_Info__c getSupplierDetails() 
    {
        BSureS_Basic_Info__c sDetails;
        if(strSupplierId != null)
        {
            sDetails = [select id,Supplier_ID__c from BSureS_Basic_Info__c where id =: strSupplierId limit 1];  
        }
        return sDetails;
    }
    
    
}