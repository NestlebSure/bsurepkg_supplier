/************************************************************************************************       
Controller Name 		: BSureS_SpendHistoryDataPublish_Scheduler       
Date                    : 11/16/2012        
Author                  : Praveen Sappati       
Purpose         		: To validate and insert the records in the spend history stage object.       
Change History 			: Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
						  11/16/2012      Praveen Sappati            Initial Version
						  11/29/2012      Praveen Sappati            Changes done on the feedback of demo
						  12/10/2012      Praveen Sappati            Changes done on the feedback of demo
**************************************************************************************************/
global with sharing class BSureS_SpendHistoryDataPublish_Scheduler implements Database.Batchable<SObject>
{
	global String str_Spend_history_data_stage=null;
	public list<BSureS_Spend_History_Stage__c> lts_spendhistorystage_id=new list<BSureS_Spend_History_Stage__c>();
	public List<BSureS_Spend_History_Publish__c> lst_spend_history_publish = new List<BSureS_Spend_History_Publish__c>();

	/// <summary>
	/// constructor
	/// </summary>
	public BSureS_SpendHistoryDataPublish_Scheduler()
	{
		//below qureying the spend history stage records
		str_Spend_history_data_stage='SELECT id,Date__c,Globe_ID__c,Spend_Amount__c,Spend_Period__c,Supplier_ID__c FROM BSureS_Spend_History_Stage__c WHERE Exception__c=NULL and Publish_Flag__c=false order by Supplier_ID__c,Date__c';
	}
	
	/// <summary>
	/// start method fires when class get executes
	/// </summary>
	///<returns>query string of object</returns>
	global Iterable<sObject> start (Database.BatchableContext ctx)
	{         
	   return Database.query(str_Spend_history_data_stage);
	}	
	 
	/// <summary>
	/// execute method executes after start method to insert records in batch into publish object
	/// </summary>
	/// <param name="BC"></param>
	/// <param name="scope"></param>
     global void execute(Database.BatchableContext BC, list<BSureS_Spend_History_Stage__c> scope)
     {
     	lts_spendhistorystage_id=new list<BSureS_Spend_History_Stage__c>();
 		for(BSureS_Spend_History_Stage__c  obj : scope)
		{
			BSureS_Spend_History_Publish__c rec_spend_history_publish = new BSureS_Spend_History_Publish__c();
	        rec_spend_history_publish.Date__c= obj.Date__c;
            //rec_spend_history_publish.SAP_Number__c = obj.SAP_Number__c;       
            rec_spend_history_publish.Spend_Amount__c = obj.Spend_Amount__c;
            rec_spend_history_publish.Spend_Period__c = obj.Spend_Period__c;
            rec_spend_history_publish.Globe_ID__c = obj.Globe_ID__c;
            rec_spend_history_publish.Supplier_ID__c=obj.Supplier_ID__c;
			obj.Publish_Flag__c=true;//for inserted records making flag as true to update in stgae
			obj.Status__c='Published';
			obj.Publish_Date__c=Date.valueof(system.now());
			obj.Status_Resource_Value__c='/resource/1242640894000/Green';
			lst_spend_history_publish.add(rec_spend_history_publish);
			lts_spendhistorystage_id.add(obj);
		}
		Database.insert(lst_spend_history_publish);//inserting records in publish
		Database.update(lts_spendhistorystage_id);//updating the stage records

     }
     
    /// <summary>
	/// finish method executes after completing all batches execution
	/// </summary>
	/// <param name="BC"></param>
     global void finish(Database.BatchableContext BC)
     {
     	//below written function for sending a mail after completing of batch
		/*AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
	     										TotalJobItems, CreatedBy.Email
	      											FROM AsyncApexJob 
	      											WHERE Id =:BC.getJobId()];
      
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      String[] toAddresses = new String[] {'praveenkumar.s@vertexcs.com'};
      mail.setToAddresses(toAddresses);
      mail.setSubject('batch job for  ' + system.today()+ a.Status);
      mail.setPlainTextBody
       ('The batch Apex job processed on spend history publish list '+'Number of records' + a.TotalJobItems +
       ' inserted during batch job'+ a.NumberOfErrors + ' number of failures.');
       
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/
     } 

}