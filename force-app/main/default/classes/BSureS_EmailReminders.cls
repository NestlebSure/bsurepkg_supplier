global with sharing class BSureS_EmailReminders {
     //private string                      strHtmlBodyContent;
    private static set<string>          setAutoNumbers  =   new set<string>();
    private static map<Id,User>                mapUser        =   new map<Id,User>();
    private static list<BSureS_Email_Queue__c> lstEmailQueue   =   new list<BSureS_Email_Queue__c>();
    private static map<integer,Integer>        mapDays         =   new map<integer,integer> {0 => -30,1 => -15,2 => -7,3 => -1};   
    
    
    public static void CreateEmailReminders(map<Id,BSureS_Basic_Info__c> newSuppliers,map<Id,BSureS_Basic_Info__c> oldSuppliers) {
        getUserMap();
        for(Id supId:newSuppliers.keySet()){
            if( (newSuppliers.get(supId).Next_Review_Date__c != null && 
                 newSuppliers.get(supId).Next_Review_Date__c > system.today() &&
                 newSuppliers.get(supId).Next_Review_Date__c  != oldSuppliers.get(supId).Next_Review_Date__c ) 
                 || (newSuppliers.get(supId).Manager__c != null && oldSuppliers.get(supId).Manager__c != null
                 && newSuppliers.get(supId).Manager__c != oldSuppliers.get(supId).Manager__c ))
            {
                if(newSuppliers.get(supId).Manager__c != null && mapUser.get(newSuppliers.get(supId).Manager__c) != null){
                    setAutoNumbers.add(newSuppliers.get(supId).Name);
                    PrepareMailContent(newSuppliers.get(supId));
                }
            }       
        }
        
        if(!setAutoNumbers.IsEmpty()) {          
            DeleteEmailQueues(setAutoNumbers);
        }
        if(lstEmailQueue.size() > 0){
            upsert lstEmailQueue; 
        }
    }
   
    // <summary>
    // This Method is Used to Prepare Map of user Ids and User Object
    // </summary>
    private static void getUserMap(){
        mapUser = new map<Id,User>([SELECT  Id,Name,Email FROM User 
                                            WHERE Email != null AND isActive=true limit 10000]);
    }
    
    // <summary>
    // This Method is Used to deleting old Email Queue Records in BSureS_Email_Queue__c Object
    // </summary>
    private static void DeleteEmailQueues(set<string> setANumbers)
    {   
    	list<BSureS_Email_Queue__c> objEmailQ = new list<BSureS_Email_Queue__c>([SELECT  Id FROM BSureS_Email_Queue__c 
    																		WHERE Supplier_ID__c != null AND Supplier_ID__c IN :setANumbers]);
        if(objEmailQ != null && objEmailQ.size() > 0)
        {
        	delete objEmailQ;
        }
    }
    public static void PrepareMailContent(BSureS_Basic_Info__c objSup)
    {
        BSureS_Email_Queue__c emailQueue;     
        set<User> setUsers = new set<User>();
        try
        {
	        string strHtmlBodyContent = '<table cellpadding=\'2\'cellspacing=\'0\' >';
	        strHtmlBodyContent += '<tr><td style="color:red;" >'+system.label.BSureS_Review_Reminder_Notification+'</td></tr>';
	        strHtmlBodyContent += '<tr><td> ----------------------- </td></tr>';            
	        strHtmlBodyContent += '<tr><td>'+system.label.BSureS_You_have_a_review_Coming_up+'</td></tr> ';
	        strHtmlBodyContent += '<tr><td>&nbsp;</td></tr>';
	        strHtmlBodyContent += '<tr><td> '+ system.label.BSureS_Review_Date  +'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: '+ objsup.Next_Review_Date__c.Month()+'/'+ objsup.Next_Review_Date__c.Day()+'/'+objsup.Next_Review_Date__c.Year() +'</td></tr>';
	        strHtmlBodyContent += '<tr><td> '+ system.label.Globe_ID +'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: '+ objsup.Globe_ID__c+'</td></tr>';
	        strHtmlBodyContent += '<tr><td> '+ system.Label.Supplier_Name  +':'+ objsup.Supplier_Name__c+'</td></tr>';
	        strHtmlBodyContent += '<tr><td>&nbsp;</td></tr>';
	        strHtmlBodyContent += '<tr><td><a href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/'+objsup.Id+'" > Link to the Suppliers Detail Page </a> </td></tr>';
	        strHtmlBodyContent += '</table>';        
	      
	        if(objSup.Manager__c != null ) 
	        {   
	            setUsers.add(mapUser.get(objSup.Manager__c));
	            setUsers.add(mapUser.get(objSup.Analyst__c));
	        }
	        
	        for(integer i=0 ;i<=3;i++)
	        {
	            if(objSup.Next_Review_Date__c.addDays(mapDays.get(i)) >= system.today() && setUsers.size() > 0)
	            {
	                for(User U:setUsers)
	                {
	                	if(U != null)
	                	{
		                    emailQueue = new BSureS_Email_Queue__c();
		                    emailQueue.Email_Status__c      = 'NEW';
		                    emailQueue.Email_Subject__c =   'Upcoming Review Notification'; 
		                    emailQueue.Supplier_ID__c       = objSup.Name; 
		                    emailQueue.Recipient_Address__c = U.Email; 
		                    emailQueue.Is_Daily_Digest__c   = 'NO';
		                    emailQueue.Email_Body__c        = strHtmlBodyContent;
		                    emailQueue.Recipient_Name__c    = U.Name; 
		                    emailQueue.Send_On_Date__c      = objSup.Next_Review_Date__c.addDays(mapDays.get(i));
		                    lstEmailQueue.add(emailQueue);
	                	} 
	                }
	            }
	        }
        }
        catch(Exception ex)
        {
        	
        }
    }
}