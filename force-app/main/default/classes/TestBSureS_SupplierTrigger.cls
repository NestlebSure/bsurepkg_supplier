/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData = true)
private class TestBSureS_SupplierTrigger {

    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
        BSureS_Zone__c SZone = new BSureS_Zone__c();
        SZone.Name = 'Test1 Zone';
        SZone.IsActive__c = true;
        insert SZone; 
        
        SZone.Name = 'Test2 Zone';
        update SZone;
        //list<BSureS_Zone__c> lstZone = new list<BSureS_Zone__c>([select Name,IsActive__c from BSureS_Zone__c where Name =:SZone.Name]);
        //System.assertEquals(1, lstZone.size());
        
        BSureS_SubZone__c SSubZone = new BSureS_SubZone__c();
        SSubZone.Name = 'Test1 SubZone';
        SSubZone.IsActive__c = true;
        SSubZone.ZoneID__c =SZone.Id;
        insert SSubZone;
        SSubZone.Name = 'Test2 SubZone';
        update SSubZone; 
        
        BSureS_Country__c SCountry = new BSureS_Country__c();
        SCountry.Name = 'Test1 Country2';
        SCountry.IsActive__c = true;
        SCountry.Sub_Zone_ID__c = SSubZone.Id;
        insert SCountry;
        
        SCountry.Name = 'Test2 CVountry';
        update SCountry;
        
        BSureS_Zone__c bsZone = new BSureS_Zone__c();
        bsZone.Name= 'AOA test';
        bsZone.IsActive__c = true;
        insert bsZone;
        
        system.assertEquals('AOA test',bsZone.Name);
        
        BSureS_SubZone__c bsSubZone = new BSureS_SubZone__c();
        bsSubZone.Name = 'Test1 SubZone';
        bsSubZone.IsActive__c = true;
        bsSubZone.ZoneID__c = bsZone.id;
        insert bsSubZone;
        
        
        BSureS_Country__c bsCountry = new BSureS_Country__c();
        bsCountry.Name = 'Test1 Country1';
        bsCountry.IsActive__c = true;
        bsCountry.Sub_Zone_ID__c = bsSubZone.id;
        insert bsCountry;
        
        BSureS_State__c SState =  new BSureS_State__c();
        SState.Name = 'Test1 CO';
        SState.IsActive__c = true;
        SState.Supplier_Country__c = bsCountry.id;
        insert SState;
        SState.Name = 'Test2 CO';
        update SState; 
        
        BSureS_Category__c scategory = new BSureS_Category__c();
        scategory.Name = 'Account';
        insert scategory;
        scategory.Name = 'test Account';
        update scategory;
        
        BSureS_Basic_Info__c Exsupplier = new BSureS_Basic_Info__c();
        Exsupplier.Contact_name__c = 'Steve';
        Exsupplier.Supplier_Name__c = 'george M';
        insert Exsupplier;
       
        BSureS_Basic_Info__c supplier = new BSureS_Basic_Info__c();
        supplier.Contact_name__c = 'Steve';
        supplier.Supplier_Name__c = 'george M';
        supplier.Supplier_Category__c = scategory.id;
        //supplier.Parent_Supplier_ID__c = Exsupplier.id;
        supplier.Bakup_Analysts__c = UserInfo.getUserId() + ',' +UserInfo.getUserId();
        supplier.Address_Line_2__c = 'test add line2';
        supplier.BSureS_Country__c = bsCountry.id;
        supplier.City__c ='test City';
        supplier.BSureS_Country__c = SCountry.id;
        supplier.Email_address__c ='testEmail@test.com';
        supplier.Description__c = 'test Description';
        supplier.Street_Name_Address_Line_1__c = 'test add line 1';
        insert supplier;
        
        BSureS_Credit_Analysis__c SPCA = new BSureS_Credit_Analysis__c();
        SPCA.Comment__c = 'text Comments';
        SPCA.Risk_Level__c =  'High';
        SPCA.Rating__c = 'A';
        SPCA.Spend__c = 123;
        SPCA.Review_Status__c = 'Completed';
        SPCA.Supplier_ID__c = supplier.id;
        SPCA.Review_Period__c = date.today();
        SPCA.Review_Start_Date__c = date.today();
        SPCA.Next_Review_Date__c = date.today();
        insert SPCA; 
        
        BSureS_Basic_Info_Stage__c stageObj1 = new BSureS_Basic_Info_Stage__c();
        stageObj1.SUPPLIER_NAME__C = 'Test Supplier1';
        stageObj1.City__c = 'test City';
        stageObj1.STREET_NAME_ADDRESS_LINE_1__C = 'test Add1'; 
        stageObj1.FINANCIAL_INFORMATION__C = 'YES';
        stageObj1.Exception__c =  'test Exception';
        stageObj1.Status__c =  'Test Status';
        insert stageObj1;
        String str11 = String.valueOf(stageObj1.Id);
        list<BSureS_Basic_Info_Stage__c> stageObj = new list<BSureS_Basic_Info_Stage__c>([select Id from BSureS_Basic_Info_Stage__c where Id =: str11 limit 1]);
        
        stageObj.get(0).SUPPLIER_NAME__C = 'Test Supplier';
        stageObj.get(0).City__c = 'test City';
        stageObj.get(0).STREET_NAME_ADDRESS_LINE_1__C = 'test Add1'; 
        stageObj.get(0).FINANCIAL_INFORMATION__C = 'YES';
        stageObj.get(0).Exception__c =  'test Exception';
        stageObj.get(0).Status__c =  'Test Status';
        update stageObj;
        
        
        
    }
    
    static testMethod void UnitTest2()
    {
        Map<String,ID> profiles = new Map<String,ID>();
        Map<string,ID> roleIds = new Map<string,ID>();
        
        List<Profile> ps = [select id, name from Profile where name = 'System Administrator' or name ='BSureS_Manager'];
                                 
        for(Profile p : ps){
         profiles.put(p.name, p.id);
        }
        
        //User Role
        List<UserRole> lObj = [select id,Name from UserRole  where name like '%Manager' ];
        for(UserRole uObj : lObj)
        {
            roleIds.put(uObj.Name,uObj.id);
        }
        ////system.debug('lObj============'+lObj);
        ////system.debug('zone role-========='+roleIds);
        User standardZM = new User(
                alias = 'standt',
                email='standarduser1@testorg.com',
                emailencodingkey='UTF-8',
                UserRoleId = roleIds.get('Zone Manager'),
                lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US',
                profileid = profiles.get('BSureS_Manager'),
                timezonesidkey='America/Los_Angeles',
                username='standardusertest1@testorg.com.bsures');
        insert standardZM;
        
        System.runAs(standardZM){
            BSureS_Zone__c szone1=new BSureS_Zone__c();
            szone1.IsActive__c = true;
            szone1.Name ='TESTAOA';
            insert szone1;
            
            BSureS_Basic_Info__c b=new BSureS_Basic_Info__c();
            b.Contact_name__c = 'Steve A';
            b.Supplier_Name__c = 'george MA';
            b.Zone__c = szone1.Id;
            insert b;
            BSureS_Zone_Manager__c zmObj = new BSureS_Zone_Manager__c();
            zmObj.Zone__c = szone1.Id;
            zmObj.Zone_Manager__c = standardZM.Id;
            insert zmObj;
             
            BSure_Configuration_Settings__c objCSettings6 = new BSure_Configuration_Settings__c();
            objCSettings6.Name = 'BSureS_CEORole1';
            objCSettings6.Parameter_Key__c = 'BSureS_CEORole1';
            objCSettings6.Parameter_Value__c = 'CEO';
            insert objCSettings6;
            system.assertEquals('CEO', objCSettings6.Parameter_Value__c); 
            Apexpages.currentPage().getParameters().put('Id',b.id);
            
            system.Test.startTest();
            ApexPages.StandardController controller = new ApexPages.StandardController(b); 
            Bsures_viewSupplierDetails sviewObj= new Bsures_viewSupplierDetails(controller);
            sviewObj.getSupplierDetails();
            system.Test.stopTest();
        
        }
    }
    
    static testMethod void UnitTest3()
    {
        Map<String,ID> profiles = new Map<String,ID>();
        Map<string,ID> roleIds = new Map<string,ID>();
        
        List<Profile> ps = [select id, name from Profile where name = 'System Administrator' or name ='BSureS_Manager'];
                                 
        for(Profile p : ps){
         profiles.put(p.name, p.id);
        }
        
        //User Role
        List<UserRole> lObj = [select id,Name from UserRole  where name like '%Manager' ];
        for(UserRole uObj : lObj)
        {
            roleIds.put(uObj.Name,uObj.id);
        }
        ////system.debug('lObj============'+lObj);
        ////system.debug('zone role-========='+roleIds);
        User standardZM = new User(
                alias = 'standt',
                email='standarduser1@testorg.com',
                emailencodingkey='UTF-8',
                UserRoleId = roleIds.get('Sub-Zone Manager'),
                lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US',
                profileid = profiles.get('BSureS_Manager'),
                timezonesidkey='America/Los_Angeles',
                username='standardusertest1@testorg.com.bsures');
        insert standardZM;
        
        System.runAs(standardZM){
            BSureS_Zone__c szone1=new BSureS_Zone__c();
            szone1.IsActive__c = true;
            szone1.Name ='TESTAOA';
            insert szone1;
            BSureS_SubZone__c sSubZone=new BSureS_SubZone__c();
            sSubZone.Name = 'TEST Latin America'; 
            sSubZone.IsActive__c = true; 
            sSubZone.ZoneID__c=szone1.Id; 
            insert sSubZone;
            BSureS_Basic_Info__c b=new BSureS_Basic_Info__c();
            b.Contact_name__c = 'Steve';
            b.Supplier_Name__c = 'george M';
            b.Zone__c = szone1.Id;
            b.Sub_Zone__c = sSubZone.Id;
            insert b;
            BSureS_Sub_Zone_Manager__c  szmObj = new BSureS_Sub_Zone_Manager__c ();
            szmObj.Sub_Zone__c = sSubZone.Id;
            szmObj.Sub_Zone_Manager__c  = standardZM.Id;
            insert szmObj;
            
            BSure_Configuration_Settings__c objCSettings6 = new BSure_Configuration_Settings__c();
            objCSettings6.Name = 'BSureS_CEORole1';
            objCSettings6.Parameter_Key__c = 'BSureS_CEORole';
            objCSettings6.Parameter_Value__c = 'CEO';
            insert objCSettings6;
            system.assertEquals('CEO', objCSettings6.Parameter_Value__c);
        
            system.Test.startTest(); 
            Apexpages.currentPage().getParameters().put('Id',b.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(b); 
            Bsures_viewSupplierDetails sviewObj= new Bsures_viewSupplierDetails(controller);
            sviewObj.getSupplierDetails();
            system.Test.stopTest();
            
        }
    }
    
    static testMethod void UnitTest4()
    {
        Map<String,ID> profiles = new Map<String,ID>();
        Map<string,ID> roleIds = new Map<string,ID>();
        
        List<Profile> ps = [select id, name from Profile where name = 'System Administrator' or name ='BSureS_Manager'];
                                 
        for(Profile p : ps){
         profiles.put(p.name, p.id);
        }
        
        //User Role
        List<UserRole> lObj = [select id,Name from UserRole  where name like '%Manager' ];
        for(UserRole uObj : lObj)
        {
            roleIds.put(uObj.Name,uObj.id);
        }
        ////system.debug('lObj============'+lObj);
        ////system.debug('zone role-========='+roleIds);
        User standardZM = new User(
                alias = 'standt',
                email='standarduser1@testorg.com',
                emailencodingkey='UTF-8',
                UserRoleId = roleIds.get('Country Manager'),
                lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US',
                profileid = profiles.get('BSureS_Manager'),
                timezonesidkey='America/Los_Angeles',
                username='standardusertest1@testorg.com.bsures');
        insert standardZM;
        
        System.runAs(standardZM){
            BSureS_Zone__c szone1=new BSureS_Zone__c();
            szone1.IsActive__c = true;
            szone1.Name ='TESTAOA';
            insert szone1;
            BSureS_SubZone__c sSubZone=new BSureS_SubZone__c();
            sSubZone.Name = 'TEST Latin America'; 
            sSubZone.IsActive__c = true; 
            sSubZone.ZoneID__c=szone1.Id; 
            insert sSubZone;
            BSureS_Country__c sCountryObj = new BSureS_Country__c();
            sCountryObj.Name = 'Test Country123';
            sCountryObj.IsActive__c = true;
            sCountryObj.Sub_Zone_ID__c=sSubZone.Id;
            insert sCountryObj;
            
            BSureS_Basic_Info__c b=new BSureS_Basic_Info__c();
            b.Contact_name__c = 'Steve';
            b.Supplier_Name__c = 'george M';
            b.Zone__c = szone1.Id;
            b.Sub_Zone__c = sSubZone.Id;
            b.BSureS_Country__c = sCountryObj.Id;
            insert b;
            
            BSureS_Country_Manager__c cmObj = new BSureS_Country_Manager__c();
            cmObj.Country__c = sCountryObj.Id;
            cmObj.Country_Manager__c = standardZM.Id;
            insert cmObj;
            
            BSure_Configuration_Settings__c objCSettings6 = new BSure_Configuration_Settings__c();
            objCSettings6.Name = 'BSureS_CEORole2';
            objCSettings6.Parameter_Key__c = 'BSureS_CEORole';
            objCSettings6.Parameter_Value__c = 'CEO';
            insert objCSettings6;
            system.assertEquals('CEO', objCSettings6.Parameter_Value__c);
            
            system.Test.startTest();
            Apexpages.currentPage().getParameters().put('Id',b.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(b); 
            Bsures_viewSupplierDetails sviewObj= new Bsures_viewSupplierDetails(controller);
            sviewObj.getSupplierDetails();
            system.Test.stopTest();
        }
    }
}