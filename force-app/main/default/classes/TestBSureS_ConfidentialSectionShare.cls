/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureS_ConfidentialSectionShare {

    static testMethod void myUnitTest() { 
    	
    	Profile pfl = [select id from profile where name='System Administrator' limit 1];

    	User testUser = new User(alias = 'u1', email='u1@testorg.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = pfl.Id,  country='United States', CommunityNickname = 'u1',
            timezonesidkey='America/Los_Angeles', username='u1@testorg32.com');
        insert testUser;
    	
    	system.assertEquals('u1@testorg32.com',testUser.Username);
    	
    	
        BSureS_Basic_Info__c objSupplier = new BSureS_Basic_Info__c(Contact_name__c='KingFisher');
   		insert objSupplier;
   		system.assertEquals('KingFisher',objSupplier.Contact_Name__c);
   		BSureS_Confidential_Info_Section__c objConf = new BSureS_Confidential_Info_Section__c();
   		objConf.Comment__c = 'Test Comments';
   		objConf.Supplier_ID__c =  objSupplier.Id;
   		insert objConf; 
   		system.assertEquals('Test Comments',objConf.Comment__c); 
   		apexpages.currentpage().getParameters().put('supId',objSupplier.Id) ;
   		apexpages.currentpage().getParameters().put('parentId',objConf.Id);
   		apexpages.currentpage().getParameters().put('Name','SCISS-000001');
   		BSureS_ConfidentialSectionShare objConfShare = new BSureS_ConfidentialSectionShare();
   		objConfShare.strConShareId =  objConf.Id;
   		objConfShare.strGroupName = 'Users'; 
   		objConfShare.getGroups();
   		objConfShare.lstAvailUser.add(Userinfo.getUserId());
   		objConfShare.lstShareUser.add(Userinfo.getUserId());
   		objConfShare.selectclick();
   		objConfShare.unselectclick();
   		objConfShare.getSelectedValues();
   		objConfShare.getunSelectedValues();
   		objConfShare.Add();
   		objConfShare.strGroupName = 'Users';
   		objConfShare.strGroupSearch = 'ee';
   		objConfShare.Find();
   		objConfShare.strGroupName = 'Roles';
   		objConfShare.strGroupSearch = 'sys';
   		objConfShare.Find();
   		objConfShare.getSelectedValues();
   		objConfShare.getunSelectedValues();
   		objConfShare.lstAvailUser.add(testUser.Id);
   		//objConfShare.DeleteShare();
   		objConfShare.Cancel();
   		objConfShare.strGroupName = 'Users';
   		objConfShare.strGroupSearch = 'ee';
   		objConfShare.lstShareUser.add(testUser.Id);
   		objConfShare.selectclick();
   		objConfShare.getSelectedValues();
   		Pagereference pageref = objConfShare.Save();
   		
   		
   		 
    }
}