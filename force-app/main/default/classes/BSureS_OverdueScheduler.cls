/************************************************************************************************       
Controller Name         : BSureS_OverdueScheduler       
Date                    : 02/14/2013        
Author                  : kishorekumar A       
Purpose                 : Scheduler       
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          02/14/2013     Kishorekumar A           Initial Version
**************************************************************************************************/
global with sharing class BSureS_OverdueScheduler implements schedulable  
{
    public list<BSureS_Basic_Info__c> lstSupplierInfo;
    public list<BSureS_Basic_Info__c> lstSuppInsert;
    public string strEmailSubject = '';
    public string strEmailBody = '';   
    public list<BSureS_Email_Queue__c> objEmailQueueList{get;set;}
    public list<BSureS_Email_Queue__c> objEmailQueueList2{get;set;}
    public list<BSureS_Email_Queue__c> objEmailQueueList3{get;set;}
    public list<BSureS_Email_Queue__c> objEmailQueueList4{get;set;}
    public list<BSureS_Email_Queue__c> objEmailQueueList5{get;set;}
    public list<BSureS_Email_Queue__c> objEmailQueueList6Overdue{get;set;}
    public list<BSureS_Email_Queue__c> lstEmailQueueReminerIntervel{get;set;} 
    public list<BSureS_Email_Queue__c> lstEmailQueueOverdueAfer7days{get;set;}
    
    public list<BSureS_Credit_Analysis__c> lstCreditAnalysis{get;set;}
    public list<BSureS_Credit_Analysis__c> lstCAEmailReminder3days{get;set;}
    public list<BSureS_Credit_Analysis__c> lstCAEmailRemainder3daysPendingApprov{get;set;}//Review Status='Pending Approval'
    public list<BSureS_Credit_Analysis__c> lstCAEndDatepast{get;set;}
    public list<BSureS_Credit_Analysis__c> lstCASubmitForApprovalPending{get;set;}
    public list<BSureS_Credit_Analysis__c> lstCreditAnalysisInsert;
    public list<BSureS_Credit_Analysis__c> lstCreditAnalysisReminderEmailIntervel;
    //30,15,7,1 days in advance of the Expected Review End Date stating tha the Expected Review End Date is arriving and the review is still not in the Completed Status.
    public list<BSureS_Credit_Analysis__c> lstEmailSubscribeIntervel{get;set;}
    public list<BSureS_Credit_Analysis__c> lstCAOverdueAfer7days{get;set;}
    public map<Id,User> mapUserlist{get;set;}
    //Task
    public Task lst_create_task{get;set;}
    public Task lst_create_task_RSStarted{get;set;}
    public list<Task> lstTaskInsert{get;set;}
    public list<Task> lstTaskRSStarted{get;set;}
    public list<BSureS_Basic_Info__c> lstSupplier15beforePRD{get;set;}
    public list<string> lstString{get;set;}  
    DateTime dateObj;
    public string strPlannedReviewDate{get;set;}
    public string strExceptedReviewEndDate{get;set;}
    global BSureS_OverdueScheduler()
    {
        
    }
    
    public void execute(SchedulableContext sc) 
    {
        lstSuppInsert = new list<BSureS_Basic_Info__c>();
        lstCreditAnalysisInsert = new list<BSureS_Credit_Analysis__c>();
        lstCreditAnalysis = new list<BSureS_Credit_Analysis__c>();
        
        //mapUserlist = new map<Id,user>([select id,Name,Email,isActive from User where isActive= true limit 20000]);
        
        lstSupplierInfo = new list<BSureS_Basic_Info__c>([select id,Supplier_Name__c,Analyst__c,Manager__c,Manager__r.Receive_Email_Notifications__c, 
                                Manager__r.Email,Analyst__r.Email,Analyst__r.Name,Manager__r.Name, Next_Review_Date__c,Bakup_Analysts__c,Planned_Review_Date__c
                                from BSureS_Basic_Info__c where Review_Status__c = 'Scheduled' and Next_Review_Date__c != null and Next_Review_Date__c < TODAY limit 500 ]);
                                
        /*lstSupplierInfo = new list<BSureS_Basic_Info__c>([select id,Supplier_Name__c,Analyst__c,Manager__c,Manager__r.Receive_Email_Notifications__c, 
                                Manager__r.Email,Analyst__r.Email,Analyst__r.Name,Manager__r.Name, Next_Review_Date__c,Bakup_Analysts__c,Planned_Review_Date__c
                                from BSureS_Basic_Info__c where Review_Status__c != 'Overdue' and Planned_Review_Date__c != null and Planned_Review_Date__c < TODAY limit 700 ]);*/
        
        /*lstCreditAnalysis = new list<BSureS_Credit_Analysis__c>([select id,Actual_Review_Start_Date__c,Expected_Review_End_Date__c,Review_Status__c,Supplier_ID__r.Analyst__c,Supplier_ID__c,Supplier_Name__c
                                from BSureS_Credit_Analysis__c where Actual_Review_Start_Date__c != null and Review_Status__c = 'Scheduled' and Actual_Review_Start_Date__c = TODAY limit 800 ]);
        
        lstCAEmailReminder3days = new list<BSureS_Credit_Analysis__c>([select id,Actual_Review_Start_Date__c,
                                Expected_Review_End_Date__c,Supplier_ID__c,Supplier_ID__r.Bakup_Analysts__c,
                                Supplier_ID__r.Analyst__r.Receive_Email_Notifications__c, Supplier_ID__r.Analyst__r.Name, 
                                Supplier_ID__r.Analyst__r.Email, Supplier_ID__r.Supplier_Name__c
                                from BSureS_Credit_Analysis__c where Expected_Review_End_Date__c =: system.today().addDays(3) 
                                and Review_Status__c = 'Started' limit 700]);   
        
        lstCAEndDatepast = new list<BSureS_Credit_Analysis__c>([select id,Actual_Review_Start_Date__c,
                                Expected_Review_End_Date__c,Supplier_ID__c,Supplier_ID__r.Bakup_Analysts__c,
                                Supplier_ID__r.Analyst__r.Receive_Email_Notifications__c, 
                                Supplier_ID__r.Analyst__r.Name, Supplier_ID__r.Analyst__r.Email, 
                                Supplier_ID__r.Supplier_Name__c,Review_Status__c from BSureS_Credit_Analysis__c 
                                where Expected_Review_End_Date__c != null and Expected_Review_End_Date__c < TODAY
                                and Review_Status__c != 'Completed' limit 700]);//Review_Status__c = 'Started'
        
        lstCASubmitForApprovalPending = new list<BSureS_Credit_Analysis__c>([select id,Actual_Review_Start_Date__c,Expected_Review_End_Date__c,Supplier_ID__c,Supplier_ID__r.Bakup_Analysts__c,
                                Supplier_ID__r.Analyst__r.Receive_Email_Notifications__c, Supplier_ID__r.Analyst__r.Name, Supplier_ID__r.Analyst__r.Email, Supplier_ID__r.Supplier_Name__c,
                                Supplier_ID__r.Manager__r.Receive_Email_Notifications__c, Supplier_ID__r.Manager__r.Name, Supplier_ID__r.Manager__r.Email,Review_Status__c
                                from BSureS_Credit_Analysis__c where Expected_Review_End_Date__c != null and Expected_Review_End_Date__c < TODAY and Review_Status__c = 'Pending Approval' limit 700]);
        
        
        lstCAEmailRemainder3daysPendingApprov = new list<BSureS_Credit_Analysis__c>([select id,Actual_Review_Start_Date__c,Expected_Review_End_Date__c,Supplier_ID__c,
                                Supplier_ID__r.Analyst__r.Receive_Email_Notifications__c, Supplier_ID__r.Analyst__r.Name, Supplier_ID__r.Analyst__r.Email, Supplier_ID__r.Supplier_Name__c,
                                Supplier_ID__r.Manager__r.Receive_Email_Notifications__c, Supplier_ID__r.Manager__r.Name, Supplier_ID__r.Manager__r.Email,Review_Status__c
                                from BSureS_Credit_Analysis__c where Expected_Review_End_Date__c != null and Expected_Review_End_Date__c =: system.today().addDays(3) and Review_Status__c = 'Pending Approval' limit 700 ]);
                                
        // reminder Email Intervel 30,15,7,1 days
        lstCreditAnalysisReminderEmailIntervel = new list<BSureS_Credit_Analysis__c>([select id,Actual_Review_Start_Date__c,Expected_Review_End_Date__c,Supplier_ID__c,Review_Status__c,Supplier_ID__r.Analyst__c,
                                Supplier_ID__r.Analyst__r.Name,Supplier_ID__r.Analyst__r.Email,Supplier_ID__r.Supplier_Name__c
                                from BSureS_Credit_Analysis__c where Review_Status__c = 'Started' and Expected_Review_End_Date__c != null and (Expected_Review_End_Date__c =: system.today().addDays(30) OR 
                                Expected_Review_End_Date__c =: system.today().addDays(15) OR Expected_Review_End_Date__c =: system.today().addDays(7) 
                                OR Expected_Review_End_Date__c =: system.today().addDays(1) OR  Expected_Review_End_Date__c = TODAY) limit 700 ]);
        
        // the credit limit is $1,000,000 or more the system checks for 7 additional days and then sends the email.
        lstCAOverdueAfer7days = new list<BSureS_Credit_Analysis__c>([select id,Expected_Review_End_Date__c,Supplier_ID__r.Analyst__c,Supplier_ID__r.Analyst__r.Name,Supplier_ID__r.Analyst__r.Email,Review_Status__c,
                                 Supplier_ID__r.Manager__c,Supplier_ID__r.Manager__r.Name, Supplier_ID__r.Manager__r.Email,Spend__c,Supplier_ID__c,Supplier_ID__r.Supplier_Name__c from BSureS_Credit_Analysis__c
                                 where Expected_Review_End_Date__c != null and Expected_Review_End_Date__c < TODAY  and  (Expected_Review_End_Date__c =: system.today().addDays(-7) OR Expected_Review_End_Date__c =: system.today().addDays(-1))and  Review_Status__c != 'Completed' limit 700 ]);
        //Spend__c != null and Spend__c >: Double.valueOf('10000000')
        ////system.debug('lstCAOverdueAfer7days=========='+lstCAOverdueAfer7days.size());                                
        lstSupplier15beforePRD = new list<BSureS_Basic_Info__c>([select id,Supplier_Name__c,Planned_Review_Date__c,Analyst__c from BSureS_Basic_Info__c where Planned_Review_Date__c != null and Planned_Review_Date__c =: system.today().adddays(15) limit 600 ]);*/
        ////system.debug('lstSupplier15beforePRD===size======='+lstSupplier15beforePRD.size());
        // the credit limit is $1,000,000 or more the system checks for 7 additional days and then sends the email. 
        /*if(lstCAOverdueAfer7days != null && lstCAOverdueAfer7days.size() > 0)
        {
            lstEmailQueueOverdueAfer7days = new list<BSureS_Email_Queue__c>();
            for(BSureS_Credit_Analysis__c cOverdue7daysAfter : lstCAOverdueAfer7days)
            {
                string strEmailReminderStatus = 'NEW';
                string strSubjectEIntervel = 'Expected Review End Date has passed and the review is not Completed';
                string strBodyEmailIntervel = '';
                strBodyEmailIntervel += '<table>';  
                strBodyEmailIntervel += '<tr><td height="21px"> Expected Review End Date has passed and the review is not in the Completed for this Supplier : '+cOverdue7daysAfter.Supplier_ID__r.Supplier_Name__c + '</td></tr>';
                strBodyEmailIntervel += '<tr><td height="21px"> Expected Review End Date :'+ cOverdue7daysAfter.Expected_Review_End_Date__c +'</td></tr>';
                strBodyEmailIntervel += '</table>'; 
                
                BSureS_Email_Queue__c objEmailOverdue7daysA = new BSureS_Email_Queue__c();
                BSureS_Email_Queue__c objEmailOverdue7daysM = new BSureS_Email_Queue__c();
                
                if(cOverdue7daysAfter.Supplier_ID__r.Analyst__c != null)
                {
                    objEmailOverdue7daysA = BuildEmailQueue(strBodyEmailIntervel, 'High', strEmailReminderStatus,
                                                    strSubjectEIntervel,'NO',cOverdue7daysAfter.Supplier_ID__r.Analyst__r.Email,
                                                    true, system.today(), cOverdue7daysAfter.Supplier_ID__r.Analyst__r.Name);
                    lstEmailQueueOverdueAfer7days.add(objEmailOverdue7daysA);
                }    
                if(cOverdue7daysAfter.Supplier_ID__r.Manager__c != null)
                {
                    objEmailOverdue7daysM = BuildEmailQueue(strBodyEmailIntervel, 'High', strEmailReminderStatus,
                                                    strSubjectEIntervel,'NO',cOverdue7daysAfter.Supplier_ID__r.Manager__r.Email,
                                                    true, system.today(), cOverdue7daysAfter.Supplier_ID__r.Manager__r.Name);
                    lstEmailQueueOverdueAfer7days.add(objEmailOverdue7daysM);
                }
                ////system.debug('lstEmailQueueOverdueAfer7days===size======='+lstEmailQueueOverdueAfer7days.size());
                
            } 
            
            if(lstEmailQueueOverdueAfer7days != null && lstEmailQueueOverdueAfer7days.size() > 0)
            {
                Database.Saveresult[] objSaveEmail7daysOverdue = Database.insert(lstEmailQueueOverdueAfer7days);
            } 
        }
        
        // reminder Email Intervel 30,15,7,1 days
        if(lstCreditAnalysisReminderEmailIntervel != null && lstCreditAnalysisReminderEmailIntervel.size() > 0)
        {
            lstEmailQueueReminerIntervel = new list<BSureS_Email_Queue__c>();
            for(BSureS_Credit_Analysis__c cEmailIntervel : lstCreditAnalysisReminderEmailIntervel)
            {
                string strEmailReminderStatus = 'NEW';
                
                string strSubjectEIntervel = '';
                
                if(cEmailIntervel.Expected_Review_End_Date__c == system.today() )
                {
                    strSubjectEIntervel = 'Expected Review End Date is arrived and the review process should have been completed by today.';
                }
                else
                {
                    strSubjectEIntervel= 'Expected Review End Date is arriving and the review is still not in the Completed ';                  
                }
                string strBodyEmailIntervel = '';
                
                strBodyEmailIntervel += '<table>';
                strBodyEmailIntervel += '<tr><td height="21px"> Expected Review End Date is arriving and the review is still not in the Completed for this Supplier : '+cEmailIntervel.Supplier_ID__r.Supplier_Name__c + '</td></tr>';
                strBodyEmailIntervel += '<tr><td height="21px"> Expected Review End Date :'+ cEmailIntervel.Expected_Review_End_Date__c +'</td></tr>';
                strBodyEmailIntervel += '</table>';
                
                if(cEmailIntervel.Supplier_ID__r.Analyst__c != null)
                {
                    BSureS_Email_Queue__c objEmailQueueEIntervel = new BSureS_Email_Queue__c();
                   
                    objEmailQueueEIntervel = BuildEmailQueue(strBodyEmailIntervel, 'High', strEmailReminderStatus,
                                                    strSubjectEIntervel,'NO',cEmailIntervel.Supplier_ID__r.Analyst__r.Email,
                                                    true, system.today(), cEmailIntervel.Supplier_ID__r.Analyst__r.Name);
                    lstEmailQueueReminerIntervel.add(objEmailQueueEIntervel);
                    
                }
                
            }
            if(lstEmailQueueReminerIntervel != null && lstEmailQueueReminerIntervel.size() > 0)
            {
                Database.Saveresult[] objSaveEmailReminder = Database.insert(lstEmailQueueReminerIntervel);
            } 
        }*/
        
        if(lstSupplierInfo != null && lstSupplierInfo.size() > 0)
        {
            //objEmailQueueList6Overdue = new list<BSureS_Email_Queue__c>();
            for(BSureS_Basic_Info__c sObj :lstSupplierInfo)
            {
                /*if(sObj.Planned_Review_Date__c != null){
                    dateObj = sObj.Planned_Review_Date__c;
                    strPlannedReviewDate = dateObj.format('MM/dd/yyyy');
                }*/    
                sObj.Review_Status__c = 'Overdue';
                //As per tom mail Updated Status like Scheduled
                //sObj.Review_Status__c = 'Scheduled';
                lstSuppInsert.add(sObj);
                /*lstString = new list<string>();
                string strEmailStatusOverdue = 'NEW';
                BSureS_Email_Queue__c  objEmailQueueAnlyOverdue = new BSureS_Email_Queue__c();
                BSureS_Email_Queue__c  objEmailQueueMngrOverdue = new BSureS_Email_Queue__c();
                
                string strSubjectOverdue = 'Planned Review Date has passed and the review has not been started. ';
                string strBodySApprove = '';
                
                strBodySApprove += '<table>';
                strBodySApprove += '<tr><td height="21px"> Planned Review Date has passed and the review has not been started for this Supplier : '+sObj.Supplier_Name__c + '</td></tr>';
                strBodySApprove += '<tr><td height="21px"> Planned Review Date  :'+ strPlannedReviewDate +'</td></tr>';
                strBodySApprove += '</table>';
                if(sObj.Analyst__c != null)
                {
                    
                    objEmailQueueAnlyOverdue = BuildEmailQueue(strBodySApprove, 'High', strEmailStatusOverdue,
                                                    strSubjectOverdue,'NO',sObj.Analyst__r.Email,
                                                    true, system.today(), sObj.Analyst__r.Name);
                    //objEmailQueueList6Overdue.add(objEmailQueueAnlyOverdue);
                    
                }
                if(sObj.Manager__c != null)
                {
                    
                    objEmailQueueMngrOverdue = BuildEmailQueue(strBodySApprove, 'High', strEmailStatusOverdue,
                                                    strSubjectOverdue,'NO',sObj.Manager__r.Email,
                                                    true, system.today(), sObj.Manager__r.Name);
                    //objEmailQueueList6Overdue.add(objEmailQueueMngrOverdue);
                }
                if(sObj.Bakup_Analysts__c != null && sObj.Bakup_Analysts__c.contains(','))
                {
                    lstString = sObj.Bakup_Analysts__c.split(',');
                    for(string s: lstString)
                    {
                        //system.debug('s=========='+s);
                        //system.debug('mapUserlist.get(s).Email=======1====='+mapUserlist.get(s).Email);
                        BSureS_Email_Queue__c  objEmailQueueBAOverdue = new BSureS_Email_Queue__c();
                        
                        
                        //objEmailQueueList6Overdue.add(objEmailQueueBAOverdue);
                    }
                }
                else if(sObj.Bakup_Analysts__c != null)
                {
                    //system.debug('sObj.Bakup_Analysts__c========='+sObj.Bakup_Analysts__c);
                    //system.debug('mapUserlist.get(s).Email=======2====='+mapUserlist.get(sObj.Bakup_Analysts__c).Email);
                    BSureS_Email_Queue__c  objEmailQueueBAOverdue = new BSureS_Email_Queue__c();
                   
                   
                }*/  
                
            }
            /*if(objEmailQueueList6Overdue != null && objEmailQueueList6Overdue.size() > 0)
            {
                Database.Saveresult[] objSaveResultOverDue = Database.insert(objEmailQueueList6Overdue);
            }*/
            
        }   
        
            
        
        /*if(lstCreditAnalysis != null && lstCreditAnalysis.size() > 0)
        {
            lstTaskRSStarted = new list<Task>();
            for(BSureS_Credit_Analysis__c caObj :lstCreditAnalysis )
            {
                caObj.Review_Status__c = 'Started';
                lstCreditAnalysisInsert.add(caObj);
                //Create task for Review has started and not been completed yet ( Here Review Status is 'Started' )
                lst_create_task_RSStarted = new Task();
                if(caObj.Supplier_ID__r.Analyst__c != null)
                {
                    lst_create_task_RSStarted.OwnerId = caObj.Supplier_ID__r.Analyst__c;
                    //lst_create_task_RSStarted.Subject = 'Review has started and not been completed yet';
                    if(caObj.Supplier_Name__c != null)
                    {
                        lst_create_task_RSStarted.Subject = 'The Expected Review Start Date has arrived. It is time to start the review - ('+ caObj.Supplier_Name__c +')';
                    }
                    else
                    {
                        lst_create_task_RSStarted.Subject = 'The Expected Review Start Date has arrived. It is time to start the review.'; 
                    }   
                    //lst_create_task.Subject = 'call';
                    lst_create_task_RSStarted.ActivityDate = system.today();
                    lst_create_task_RSStarted.Status = 'Not Started';
                    lst_create_task_RSStarted.whatId =  caObj.Id;
                }
                lstTaskRSStarted.add(lst_create_task_RSStarted);
            }
        }*/
        try
        {
            if(!lstSuppInsert.isEmpty())
            {
                Database.update(lstSuppInsert);
            }   
            /*if(!lstCreditAnalysisInsert.isEmpty())
                Database.update(lstCreditAnalysisInsert);
            if(lstTaskRSStarted != null && lstTaskRSStarted.size() > 0)
            {
                Database.Saveresult[] objCreateTaskRSStarted = Database.insert(lstTaskRSStarted);
            }*/
        }
        catch(Exception e)
        {
            //system.debug('----Exception Overdue Schedulable class----'+e);
        }
        
        /*if(lstCAEmailReminder3days != null && lstCAEmailReminder3days.size() > 0)
        {
            string strEmailStatus = 'NEW';
            objEmailQueueList2 = new list<BSureS_Email_Queue__c>(); 
            
            for(BSureS_Credit_Analysis__c objEmailCA : lstCAEmailReminder3days)
            {
                BSureS_Email_Queue__c  objEmailQueue2 = new BSureS_Email_Queue__c();
                lstString = new list<string>();
                if(objEmailCA.Expected_Review_End_Date__c != null){
                    dateObj = objEmailCA.Expected_Review_End_Date__c;
                    strExceptedReviewEndDate = dateObj.format('MM/dd/yyyy');  
                }                  
                //String strEBSubject = 'Review has not been submitted for approval ';
                //as per tom mail
                String strEBSubject = 'The Review is due to be complete in 3 days and an approval is required.';
                string strEBReminderBody ='';
                strEBReminderBody = '<table>';
                strEBReminderBody += ' <tr><td height="21px"> Review has not been submitted for approval for this Supplier :'+objEmailCA.Supplier_ID__r.Supplier_Name__c +'</td></tr>';
                if(objEmailCA.Expected_Review_End_Date__c != null)
                {
                    strEBReminderBody += '<tr><td height="21px"> Expected Review End Date is'+ strExceptedReviewEndDate +'</td></tr>';
                }
                strEBReminderBody += '</table>' ;
                if(objEmailCA.Supplier_ID__r.Analyst__c != null)
                {
                    
                    objEmailQueue2 = BuildEmailQueue(strEBReminderBody, 'High', strEmailStatus,
                                                    strEBSubject,'NO',objEmailCA.Supplier_ID__r.Analyst__r.Email,
                                                    true, system.today(), objEmailCA.Supplier_ID__r.Analyst__r.Name);
                    objEmailQueueList2.add(objEmailQueue2);
                }  
                if(objEmailCA.Supplier_ID__r.Bakup_Analysts__c != null && objEmailCA.Supplier_ID__r.Bakup_Analysts__c.contains(','))
                {
                    lstString = objEmailCA.Supplier_ID__r.Bakup_Analysts__c.split(',');
                    for(string strobj : lstString)
                    {
                        BSureS_Email_Queue__c  objEmailQueueBackupAnaly = new BSureS_Email_Queue__c();
                       
                    }
                }  
                else if(objEmailCA.Supplier_ID__r.Bakup_Analysts__c != null)
                {
                    BSureS_Email_Queue__c  objEmailQueueBackupAnaly = new BSureS_Email_Queue__c();
                  
                    objEmailQueueBackupAnaly = BuildEmailQueue(strEBReminderBody, 'High', strEmailStatus,
                                                    strEBSubject,'NO',mapUserlist.get(objEmailCA.Supplier_ID__r.Bakup_Analysts__c).Email,
                                                    true, system.today(), mapUserlist.get(objEmailCA.Supplier_ID__r.Bakup_Analysts__c).Name);
                    objEmailQueueList2.add(objEmailQueueBackupAnaly);
                }
            }
            if(objEmailQueueList2 != null && objEmailQueueList2.size() > 0)
            {
                Database.Saveresult[] objSaveResult2 = Database.insert(objEmailQueueList2);
            }   
        }   
        if(lstCAEndDatepast != null && lstCAEndDatepast.size() > 0)
        {
            string strEmailStatus = 'NEW';
            objEmailQueueList3 = new list<BSureS_Email_Queue__c>(); 
            
            for(BSureS_Credit_Analysis__c objEmailCAPast : lstCAEndDatepast)
            {
                objEmailCAPast.Review_Status__c = 'Overdue';
                lstString = new list<string>();
                if(objEmailCAPast.Expected_Review_End_Date__c != null){
                    dateObj = objEmailCAPast.Expected_Review_End_Date__c;
                    strExceptedReviewEndDate = dateObj.format('MM/dd/yyyy');
                }
                //string strSubjectEmailSApast = 'Expected Review End Date has passed';
                string strSubjectEmailSApast = 'Expected Review Complete Date has passed and the review is not yet complete';
                string strBodyEmailSApast = '';
                strBodyEmailSApast += '<table>';
                strBodyEmailSApast += '<tr><td height="21px"> Expected Review End Date has passed for this Supplier :'+ objEmailCAPast.Supplier_ID__r.Supplier_Name__c  +'</td></tr>';
                if(objEmailCAPast.Expected_Review_End_Date__c != null)
                {
                    strBodyEmailSApast += '<tr><td height="21px"> Expected Review End Date is :' + strExceptedReviewEndDate + '</td></tr>';
                }
                //system.debug('objEmailCAPast.Supplier_ID__r.Analyst__r.Name======='+objEmailCAPast.Supplier_ID__r.Analyst__r.Name);
                strBodyEmailSApast += '</table>';
                
                if(objEmailCAPast.Supplier_ID__r.Analyst__c != null)
                {
                    BSureS_Email_Queue__c  objEmailQueue3 = new BSureS_Email_Queue__c();
                   
                    objEmailQueue3 = BuildEmailQueue(strBodyEmailSApast, 'High', strEmailStatus,
                                                    strSubjectEmailSApast,'NO',objEmailCAPast.Supplier_ID__r.Analyst__r.Email,
                                                    true, system.today(), objEmailCAPast.Supplier_ID__r.Analyst__r.Name);
                    objEmailQueueList3.add(objEmailQueue3);    
                }   
                if(objEmailCAPast.Supplier_ID__r.Bakup_Analysts__c != null && objEmailCAPast.Supplier_ID__r.Bakup_Analysts__c.contains(','))
                {
                    lstString = objEmailCAPast.Supplier_ID__r.Bakup_Analysts__c.split(',');
                    for(string sObjBA: lstString)
                    {
                        BSureS_Email_Queue__c objEmailQBackUpAnalyst = new BSureS_Email_Queue__c();
                    }
                }
                else if(objEmailCAPast.Supplier_ID__r.Bakup_Analysts__c != null)
                {
                    BSureS_Email_Queue__c objEmailQBackUpAnalyst = new BSureS_Email_Queue__c();
                   
                    objEmailQBackUpAnalyst = BuildEmailQueue(strBodyEmailSApast, 'High', strEmailStatus,
                                                    strSubjectEmailSApast,'NO',mapUserlist.get(objEmailCAPast.Supplier_ID__r.Bakup_Analysts__c).Email,
                                                    true, system.today(), mapUserlist.get(objEmailCAPast.Supplier_ID__r.Bakup_Analysts__c).Name);
                   objEmailQueueList3.add(objEmailQBackUpAnalyst);   
                    
                }
            }   
            if(!lstCAEndDatepast.isEmpty())
                update lstCAEndDatepast;
            if(objEmailQueueList3 != null && objEmailQueueList3.size() > 0)
            {
                Database.Saveresult[] objSaveResult3 = Database.insert(objEmailQueueList3);
            }   
        }   
        if(lstCASubmitForApprovalPending != null && lstCASubmitForApprovalPending.size() > 0)
        {
            string strAnlyEmailStatus = 'NEW';
            string strMngrEmailStatus = 'NEW';
            objEmailQueueList4 = new list<BSureS_Email_Queue__c>(); 
            for(BSureS_Credit_Analysis__c objEmailCASAppPast : lstCASubmitForApprovalPending)
            {
                BSureS_Email_Queue__c  objEmailQueueAnly = new BSureS_Email_Queue__c();
                BSureS_Email_Queue__c  objEmailQueueMngr = new BSureS_Email_Queue__c();
                lstString = new list<string>();
                string strSubjectSApprov = 'Review has been submitted for approval and the Expected Review End Date has passed ';
                string strBodySApprove = '';
                
                strBodySApprove += '<table>';
                strBodySApprove += '<tr><td height="21px"> Review has been submitted for approval and the Expected Review End Date has passed for this Supplier : '+objEmailCASAppPast.Supplier_ID__r.Supplier_Name__c + '</td></tr>';
                strBodySApprove += '<tr><td height="21px"> Expected Review End Date :'+ objEmailCASAppPast.Expected_Review_End_Date__c +'</td></tr>';
                strBodySApprove += '</table>';
                //system.debug('objEmailCAPast.Supplier_ID__r.Analyst__r.Name==1====='+objEmailCASAppPast.Supplier_ID__r.Analyst__r.Name);
                if(objEmailCASAppPast.Supplier_ID__r.Analyst__c != null)
                {
                    
                    objEmailQueueAnly = BuildEmailQueue(strBodySApprove, 'High', strAnlyEmailStatus,
                                                    strSubjectSApprov,'NO',objEmailCASAppPast.Supplier_ID__r.Analyst__r.Email,
                                                    true, system.today(), objEmailCASAppPast.Supplier_ID__r.Analyst__r.Name);
                    objEmailQueueList4.add(objEmailQueueAnly);
                    
                }
                if(objEmailCASAppPast.Supplier_ID__r.Manager__c != null)
                {
                   
                    objEmailQueueMngr = BuildEmailQueue(strBodySApprove, 'High', strMngrEmailStatus,
                                                    strSubjectSApprov,'NO',objEmailCASAppPast.Supplier_ID__r.Manager__r.Email,
                                                    true, system.today(), objEmailCASAppPast.Supplier_ID__r.Manager__r.Name);
                    objEmailQueueList4.add(objEmailQueueMngr);                                  
                }
                if(objEmailCASAppPast.Supplier_ID__r.Bakup_Analysts__c != null && objEmailCASAppPast.Supplier_ID__r.Bakup_Analysts__c.contains(','))
                {
                    lstString = objEmailCASAppPast.Supplier_ID__r.Bakup_Analysts__c.split(',');
                    for(string strCApObj : lstString)
                    {
                        BSureS_Email_Queue__c  objEmailQueueBAMngr = new BSureS_Email_Queue__c();
                    }
                }
                else if(objEmailCASAppPast.Supplier_ID__r.Bakup_Analysts__c != null)
                {
                    BSureS_Email_Queue__c  objEmailQueueBAMngr = new BSureS_Email_Queue__c();
                   
                    objEmailQueueBAMngr =  BuildEmailQueue(strBodySApprove, 'High', strMngrEmailStatus,
                                                    strSubjectSApprov,'NO',mapUserlist.get(objEmailCASAppPast.Supplier_ID__r.Bakup_Analysts__c).Email,
                                                    true, system.today(),mapUserlist.get(objEmailCASAppPast.Supplier_ID__r.Bakup_Analysts__c).Name);
                    objEmailQueueList4.add(objEmailQueueBAMngr);                                    
                        
                }
                
            } 
            if(objEmailQueueList4 != null && objEmailQueueList4.size() > 0)
            {
                Database.Saveresult[] objSaveResult4 = Database.insert(objEmailQueueList4);
            }
            
            
        }
        if(lstSupplier15beforePRD != null && lstSupplier15beforePRD.size() > 0)
        {
            //system.debug('testtttttttttttt');
            lstTaskInsert = new list<Task>(); 
            for(BSureS_Basic_Info__c suppObj:lstSupplier15beforePRD)
            {
                lst_create_task = new Task();
                if(suppObj.Analyst__c != null)
                {
                    lst_create_task.OwnerId = suppObj.Analyst__c;
                    if(suppObj.Supplier_Name__c != null)
                    {
                        lst_create_task.Subject = 'Create a New Review - ('+suppObj.Supplier_Name__c + ')';
                    }
                    else
                    {
                        lst_create_task.Subject = 'Create a New Review';
                    }   
                    //lst_create_task.Subject = 'call';
                    lst_create_task.ActivityDate = System.today(); 
                    lst_create_task.Status = 'Not Started';
                    lst_create_task.whatId = suppObj.Id;
                }
                if(lst_create_task != null )
                    lstTaskInsert.add(lst_create_task); 
            } 
            if(lstTaskInsert != null && lstTaskInsert.size() > 0)
            {
                Database.Saveresult[] objCreateTask15daysBefore = Database.insert(lstTaskInsert);
            }  
        }
        
        if(lstCAEmailRemainder3daysPendingApprov != null && lstCAEmailRemainder3daysPendingApprov.size() > 0)
        {
            string strEmailStatusPendingApprov = 'NEW';
            objEmailQueueList5 = new list<BSureS_Email_Queue__c>();
            for(BSureS_Credit_Analysis__c objEmailCAPastPendingApprov : lstCAEmailRemainder3daysPendingApprov)
            {
                BSureS_Email_Queue__c  objEmailQueue5 = new BSureS_Email_Queue__c();
                string strBodySpendingApprove = '';
                string strSubjectSApprov = 'Review has been submitted for approval and the Expected Review End Date due completion in 3 days. ';
                if(objEmailCAPastPendingApprov.Expected_Review_End_Date__c != null)
                {
                    strBodySpendingApprove += '<table>';
                    strBodySpendingApprove += '<tr><td height="21px"> Review has been submitted for approval and the Expected Review End Date due completion in 3 days for this Supplier : '+objEmailCAPastPendingApprov.Supplier_ID__r.Supplier_Name__c + '</td></tr>';
                    strBodySpendingApprove += '<tr><td height="21px"> Expected Review End Date :'+ objEmailCAPastPendingApprov.Expected_Review_End_Date__c +'</td></tr>';
                    strBodySpendingApprove += '</table>';
                }
                if(objEmailCAPastPendingApprov.Supplier_ID__r.Manager__C != null)
                {
                    objEmailQueue5 = BuildEmailQueue(strBodySpendingApprove, 'High', strEmailStatusPendingApprov,
                                                    strSubjectSApprov,'NO',objEmailCAPastPendingApprov.Supplier_ID__r.Manager__r.Email,
                                                    true, system.today(), objEmailCAPastPendingApprov.Supplier_ID__r.Manager__r.Name);  
                   
                    objEmailQueueList5.add(objEmailQueue5);
                }    
                    
            }
            
            if(objEmailQueueList5 != null && objEmailQueueList5.size() > 0)
            {
                Database.Saveresult[] objSaveResult5 = Database.insert(objEmailQueueList5);
            }
        }*/
        
    }
    
    public BSureS_Email_Queue__c BuildEmailQueue(string strBody, string strPriority, string strStatus,
                                                 string strSubject, string strDailyDigest, string strRecAdd,
                                                 boolean strSendImm, date strSendDate, string strRecName)
    {
        BSureS_Email_Queue__c objEmailQueue = new BSureS_Email_Queue__c();
        /*objEmailQueue.Email_Body__c = strBody;
        objEmailQueue.Email_Priority__c = strPriority;
        objEmailQueue.Email_Status__c = strStatus;
        objEmailQueue.Email_Subject__c = strSubject;
        objEmailQueue.Is_Daily_Digest__c = strDailyDigest; 
        objEmailQueue.Recipient_Address__c = strRecAdd;
        objEmailQueue.Send_Immediate__c = strSendImm;
        objEmailQueue.Send_On_Date__c = strSendDate;
        objEmailQueue.Recipient_Name__c = strRecName;*/
        
        return objEmailQueue;
        
    }
}