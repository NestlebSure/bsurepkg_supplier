/************************************************************************************************       
Controller Name         : BSureS_SupplierInfoIntermediate       
Date                    : 03/17/2013        
Author                  : B.Anupreethi       
Purpose                 : This Controller is used to upload the supplier information in bulk -intermediate level      
Change History          : 
Date                        Programmer                     Reason       
--------------------      -------------------    -------------------------       
03/17/2013                  B.Anupreethi           Initial Version
07/21/2013					Aditya V				Modified Backup Analysts and Analysts selection Criteria
29/01/2014                  Satish CH                  modified by satish on jan 28th 2014 for removing analyst,buyer as mandatory fields
**************************************************************************************************/
global with sharing class BSureS_SupplierInfoIntermediate 
{
    public list<BSureS_Basic_Info_Stage__c> lstStage{get;set;}
    public list<BSureS_User_Additional_Attributes__c> userList {get; set;}
    
    public string strState{get;set;}
    public map<string, string> mapAnalystManger{get;set;}
    public map<string, string> mapManagerAnalysts {get;set;}
	public map<string, string> mapAnalystNames {get;set;}
	public map<string, string> mapBuyerUserId{get;set;}
    public string strAnalystManger{get;set;} //To hold the category name
    public string strAnalystMangerId{get;set;}
    public string strAnalystRole {get; set;}
    public string strBkpAnlyNames{get;set;}
    public string strBkpAnlyIds{get;set;}
    public string strBuyerRole{get;set;}
    public string strCategoryInBuyerSection{get;set;}
    //Aditya V Request to remove Region and Category restriction for selecting Analysts 07/22/2013
    public string strCategoryFlagForAnalyst{get;set;}
    public string strLocationFlagForAnalyst{get;set;}
    public string strManagerFlagForBkpAnalyst{get;set;}
    public String strGetRegion{get;set;} 
    list<BSureS_User_Additional_Attributes__c> AAttributes = new list<BSureS_User_Additional_Attributes__c>();
    //Aditya V Request to remove Region and Category restriction for selecting Analysts 07/22/2013
    
    
    public static boolean isSupplierStageUpdate {get;set;}//= true;Boolean value for not invoking Supplier stage Update Trigger 
     
    public BSureS_SupplierInfoIntermediate()
    {
    	strBuyerRole = BSureS_CommonUtil.getConfigurationValues('BSureS_BuyerRole').get(0);
    	strAnalystRole = BSureS_CommonUtil.getConfigurationValues('BSureS_AnalystRole').get(0);
    	strCategoryFlagForAnalyst = BSureS_CommonUtil.getConfigurationValues('BSureS_CategoryFlagForAnalyst').get(0);
        strLocationFlagForAnalyst = BSureS_CommonUtil.getConfigurationValues('BSureS_LocationFlagForAnalyst').get(0);
        strManagerFlagForBkpAnalyst = BSureS_CommonUtil.getConfigurationValues('BSureS_ManagerFlagForBkpAnalyst').get(0);
        
        strCategoryInBuyerSection = BSureS_CommonUtil.getConfigurationValues('BSureS_includeCategoryInBuyerSection').get(0);
        if(strCategoryInBuyerSection == null || strCategoryInBuyerSection == '')
        {
        	strCategoryInBuyerSection = 'FALSE';
        }
        getSupPendingRecords();
    }
    
    public void getSupPendingRecords()
    {
        lstStage = new list<BSureS_Basic_Info_Stage__c>();
        lstStage = [SELECT ID,SUPPLIER_ID__C,SUPPLIER_NAME__C,State_Name__c,State_Province__c,Manager__c,
        				Manager_Name__c,Analyst__c,Analyst_Name__c,Planned_Review_Date__c,
                        Publish_Flag__c,Spend__c,Globe_ID__c,BSureS_Country__c, Zone__c,Sub_Zone__c, 
                        Supplier_Category__c,City__c, BulkUpload_BuyerID__c, Status__c, Exception__c  
                    FROM BSureS_Basic_Info_Stage__c WHERE ID!=NULL AND Publish_Flag__c=:false];
        //system.debug('--intial--'+lstStage);
    }
    
    
    
    /// <summary> 
    /// Method to get Country list for Supplier Contact Info
    /// </summary>  
    public List<SelectOption> getSStateList()
    {
        List<SelectOption> lstAvailableStates = new List<SelectOption>();
        lstAvailableStates.add(new Selectoption(System.label.BSureS_VSelect,System.label.BSureS_VSelect));
        List<BSureS_State__c> lstSupplierStateInfo;
        string strSCICountryId='';
        if(lstStage.size()>0)
        strSCICountryId= lstStage[0].BSureS_Country__c;
        if(strSCICountryId != null && strSCICountryId != '')
        {
            lstSupplierStateInfo = [select id, Name from BSureS_State__c where IsActive__c = true and Supplier_Country__c=:lstStage[0].BSureS_Country__c order by Name];
        }
        
        if(lstSupplierStateInfo != null && lstSupplierStateInfo.size() > 0)
        {
            for(BSureS_State__c objStateEach:lstSupplierStateInfo)
            {
                if(objStateEach.Name != null)
                {
                    lstAvailableStates.add(new Selectoption(objStateEach.id,objStateEach.Name));
                }
            }
        }   
        return lstAvailableStates;   
    }
    /// <summary> 
    /// Method to get Country list for Supplier Contact Info
    /// </summary>  
    public List<SelectOption> getSAnalystList()
    {   
        string strCategory= '';
        string strSCIZoneId= '';
        string strSCISubZoneId= '';
        string strSCICountryId= '';
        List<SelectOption> lstAnalysts = new List<SelectOption>();    
        set<Id> userIds = new set<Id>();   
       
        if(lstStage.size()>0)
        {
            strCategory= lstStage[0].Supplier_Category__c;
            strSCIZoneId= lstStage[0].Zone__c;
            strSCISubZoneId= lstStage[0].Sub_Zone__c;
            strSCICountryId= lstStage[0].BSureS_Country__c;
        
         	mapAnalystManger = new map<string,string>();
         	mapManagerAnalysts = new map<string, string>();
	     	mapAnalystNames = new map<string, string>();
	     	
	     	if(!strCategoryFlagForAnalyst.equalsIgnoreCase('FALSE'))    //If Category is Required
            {
         		list<BSureS_UserCategoryMapping__c> Categories = [select id,CategoryID__c,UserID__c 
         													  from BSureS_UserCategoryMapping__c 
         													  where CategoryID__c =: strCategory ];
	            userIds.clear();
	            for(BSureS_UserCategoryMapping__c Ucat:Categories){
	                userIds.add(Ucat.UserID__c);
	            }
            }
        	//system.debug('--strCategory--'+strCategory+'-strSCIZoneId-'+strSCIZoneId+'-strSCISubZoneId-'+strSCISubZoneId+'-strSCICountryId-'+strSCICountryId); 
        	lstAnalysts.add(new Selectoption(System.label.BSureS_VSelect,System.label.BSureS_VSelect));
        	
        	string strAnalystProfile = 'BSureS_Analyst';
        	//string strAnalystRole = 'Analyst';
            string strAnalystQuery = '';
            strAnalystQuery += 'SELECT id, User__c, User__r.Name, BSureS_Zone__c, ';
            strAnalystQuery += 'BSureS_Sub_Zone__c, BSureS_Country__c, Manager__c, ';
            strAnalystQuery += 'Manager__r.Name ';
            strAnalystQuery += 'FROM BSureS_User_Additional_Attributes__c ';
            strAnalystQuery += 'WHERE Id != null ';
            strAnalystQuery += 'AND User__r.IsActive = true ';
            strAnalystQuery += 'AND User__r.UserRole.Name =: strAnalystRole ';
            strAnalystQuery += 'AND User__r.Profile.Name =: strAnalystProfile ';
            if(!strLocationFlagForAnalyst.equalsIgnoreCase('FALSE'))
            {
                strAnalystQuery += 'AND BSureS_Zone__c =: strSCIZoneId ';
                strAnalystQuery += 'AND BSureS_Sub_Zone__c =: strSCISubZoneId ';
                strAnalystQuery += 'AND BSureS_Country__c =: strSCICountryId ';
            }
            if(!strCategoryFlagForAnalyst.equalsIgnoreCase('FALSE')
                && userIds != null && userIds.size() > 0)   //If Category is Required
            {
                strAnalystQuery += 'AND User__c IN: userIds ';
            }
            
            AAttributes = Database.query(strAnalystQuery);
            /*
        	list<BSureS_User_Additional_Attributes__c> AAttributes = [select   id,User__c, User__r.Name,
                                                                        BSureS_Zone__c,
                                                                        BSureS_Sub_Zone__c,
                                                                        BSureS_Country__c,Manager__c,Manager__r.Name
                                                                      FROM BSureS_User_Additional_Attributes__c
                                                                      WHERE BSureS_Zone__c=:strSCIZoneId 
                                                                      AND BSureS_Sub_Zone__c=:strSCISubZoneId 
                                                                      AND BSureS_Country__c=:strSCICountryId
                                                                      AND User__r.IsActive = true
									                                  //AND User__r.UserRole.Name =: strAnalystRole 
									                                  //AND User__c IN: userIds
                                                                      AND User__r.Profile.Name =: 'BSureS_Analyst'];
			*/				
			if(AAttributes != null && AAttributes.size() > 0)
			{					                                    
	        	for(BSureS_User_Additional_Attributes__c uA:AAttributes)
	        	{
	            	//userIds.add(ua.User__c);
	            	mapAnalystManger.put(ua.User__c,ua.Manager__c+','+ua.Manager__r.Name);
	            	mapAnalystNames.put(ua.User__c, ua.User__r.Name);
					if(mapManagerAnalysts.containsKey(ua.Manager__c))
					{
						string objTempStr = mapManagerAnalysts.get(ua.Manager__c);
						objTempStr = ',' + ua.User__c + objTempStr +  ua.User__r.Name + ';';
						mapManagerAnalysts.put(ua.Manager__c, objTempStr);
					}
					else
					{
						string objTempStr = '##';
						objTempStr = ',' + ua.User__c + objTempStr +  ua.User__r.Name + ';';
						mapManagerAnalysts.put(ua.Manager__c, objTempStr);
					}
					lstAnalysts.add(new Selectoption(String.valueOf(uA.User__c), ua.User__r.Name));
	            	//system.debug('mapAnalystManger--'+mapAnalystManger);
	        	}
			}
	        /*	 Code commented and written above. Aditya V  
	        if(!userIds.IsEmpty())
	        {
	            list<BSureS_UserCategoryMapping__c> Categories = [select id,CategoryID__c,UserID__c from BSureS_UserCategoryMapping__c where UserID__c in:userIds and CategoryID__c=:strCategory ];
	            userIds.clear();
	            for(BSureS_UserCategoryMapping__c Ucat:Categories){
	                userIds.add(Ucat.UserID__c);
	            }
	        }
        
	        list<User> analystUser = [select Id,Name from User where Id in:userIds and profile.Name = 'BSureS_Analyst' and IsActive=true];
	        
	        for(User u:analystUser)
	        {
	            lstAnalysts.add(new Selectoption(u.Id,u.name));
	        }   
	        */
        }                                                   
        return lstAnalysts;   
    }    
    
    public List<SelectOption> getSBuyerList()
    {
    	string strCategory= '';
    	list<BSureS_UserCategoryMapping__c> lstCatgMapping ;
    	list<SelectOption> lstBuyers = new List<SelectOption>();
    	mapBuyerUserId = new map<string,string>();
    	
    	if(lstStage.size()>0)
        {
            strCategory = lstStage[0].Supplier_Category__c;
        }
    	lstBuyers.add(new Selectoption(System.label.BSureS_VSelect,System.label.BSureS_VSelect));
    	
    	if(strCategoryInBuyerSection != null && strCategoryInBuyerSection.equalsIgnoreCase('TRUE'))
        {
            if(strCategory != null && (strBuyerRole != null && strBuyerRole !=''))
            {
                //new Changes on Buyer Supplier Category Mapping
                string strSupplCategoryChk = 'select UserID__c,CategoryName__c from BSureS_UserCategoryMapping__c where id != null';
                if(strCategory != null)
                {
                    strSupplCategoryChk += ' and CategoryName__c =:strCategory ';
                }
                if(strBuyerRole != null)
                {
                    strSupplCategoryChk += ' and UserID__r.IsActive = true and UserID__r.userrole.name =: strBuyerRole';
                }
                lstCatgMapping = Database.query(strSupplCategoryChk);
            }
            
            if(lstCatgMapping!= null && lstCatgMapping.size() > 0)
            {
                for(BSureS_UserCategoryMapping__c getUserId:lstCatgMapping)
                {
                    if(getUserId.UserID__c != null)
                    {
                        mapBuyerUserId.put(getUserId.UserID__c, getUserId.UserID__c);
                    }
                }
            }  
              
            if(mapBuyerUserId.keySet() != null)
            {
                ////system.debug('strCategoryInBuyerSection======255=='+strCategoryInBuyerSection); 
                userList = [select name,User__c,User__r.Name,Zone_Name__c,Sub_Zone_Name__c,Country_Name__c 
                			from BSureS_User_Additional_Attributes__c where User__r.Id IN :mapBuyerUserId.keySet()];
            }
            if(userList != null && userList.size() > 0)
            {
            	for(BSureS_User_Additional_Attributes__c eachUser : userList)
            	{
            		lstBuyers.add(new SelectOption(String.valueOf(eachUser.User__c), eachUser.User__r.Name));
            	}
            }
        } 
        else
        {
            String strUserlistQuery = '';
            strUserlistQuery += 'select name,User__c,User__r.Name,Zone_Name__c,Sub_Zone_Name__c,Country_Name__c ';
            strUserlistQuery += ' from BSureS_User_Additional_Attributes__c where id != null and ';
            strUserlistQuery += ' User__r.IsActive = true and ';
            if(strBuyerRole != null && strBuyerRole != '')
            {
                strUserlistQuery += ' User__r.userrole.name =: strBuyerRole';
            }
            userList = Database.query(strUserlistQuery) ;
            if(!userList.isEmpty())
            {
            	for(BSureS_User_Additional_Attributes__c cAddObj : userList)
            	{
            		if(cAddObj.User__c != null)
            		{
            			lstBuyers.add(new SelectOption(String.valueOf(cAddObj.User__c), cAddObj.User__r.Name));
            		}
            	}
            }
            ////system.debug('userList========'+userList);
        }  
    	return lstBuyers;
    }
    
    public pagereference UpdateSupStage()
    {
        pagereference pageref;
        //system.debug('--lstStage---'+lstStage);
        list<BSureS_Basic_Info_Stage__c> lstStageFinal = new list<BSureS_Basic_Info_Stage__c>();
        
        for(BSureS_Basic_Info_Stage__c objstage:lstStage)
        {
            string strState = objstage.State_Province__c;
            string strAnalyst = objstage.Analyst__c;
            string strBuyer = objstage.BulkUpload_BuyerID__c;
            strBkpAnlyIds = '';
            strBkpAnlyNames = '';
            
            //system.debug('strState--'+strState+'strAnalyst--'+strAnalyst);
            strAnalyst=strAnalyst!='-Select-'?strAnalyst:null;//modified by satish on jan 28th 2013 for removing analyst,buyer as mandatory fields
            strBuyer=strBuyer!='-Select-'?strBuyer:null;
           
            if(strState!='-Select-' && objstage.Planned_Review_Date__c != null)
            {
                integer cntSup = [select count() from BSureS_Basic_Info__c where SUPPLIER_NAME__C=:objstage.SUPPLIER_NAME__C AND State_Province__c=:objstage.State_Province__c AND CITY__C=:objstage.CITY__C];
                if(cntSup>0)
                {
                    if(objstage.Exception__c==NULL)
                    {
                        objstage.Exception__c=Label.BSureS_VNameCityState;
                        objstage.Status__c='Exception';
                    }
                    else
                    {
                        objstage.Exception__c+=','+Label.BSureS_VNameCityState;
                        objstage.Status__c='Exception';
                    }   
                }
                
                if(strAnalyst!=null)//modified by satish on jan 28th 2013 for removing analyst,buyer as mandatory fields
                {
	                 //objstage.Exception__c+=','+''+Label.BSureS_VNameCityState;
	                string sstrAnalystManger = mapAnalystManger.get(objstage.Analyst__c);
	                //system.debug('sstrAnalystManger--'+sstrAnalystManger);
	                if(sstrAnalystManger!=null && sstrAnalystManger!='')
	                {   
	                    string[] strIdName = sstrAnalystManger.split(',');
	                    strAnalystMangerId = strIdName[0];
	                    strAnalystManger = strIdName[1];
	                }
	                objstage.Manager__c = strAnalystMangerId;
		                
	                if(strManagerFlagForBkpAnalyst.equalsIgnoreCase('FALSE'))
	                {
	                	if(AAttributes != null && AAttributes.size() > 0)
	                    {
	                        for(BSureS_User_Additional_Attributes__c objEachUser: AAttributes)
	                        {
	                            if(!strAnalyst.equalsIgnoreCase(objEachUser.User__c))
	                            {
	                                strBkpAnlyNames += objEachUser.User__r.Name + ';';
	                                strBkpAnlyIds += objEachUser.User__c + ',';
	                            }
	                        }
	                                                
	                        if(strBkpAnlyIds != '')
	                        {
	                            strBkpAnlyIds = strBkpAnlyIds.substring(0,strBkpAnlyIds.length() - 1);
	                        }
	                        if(strBkpAnlyNames != '')
	                        {
	                            strBkpAnlyNames = strBkpAnlyNames.substring(0, strBkpAnlyNames.length() - 1);
	                        }
	                    }
	                }
	                else
	                {
	                    string strSubordinates = mapManagerAnalysts.get(strAnalystMangerId);
			    		//system.debug('strSubordinates------'+strSubordinates);
			    		if(strSubordinates != null)
			    		{
			    			string[] strBkpAnalysts = strSubordinates.split('##');
			    			strBkpAnlyIds = strBkpAnalysts[0].replace(',' + objstage.Analyst__c, '');
			    			if(strBkpAnlyIds != '')
			    			{
			    				strBkpAnlyIds = strBkpAnlyIds.substring(1,strBkpAnlyIds.length());
			    			}
			    			strBkpAnlyNames = strBkpAnalysts[1].replace(mapAnalystNames.get(objstage.Analyst__c) + ';', '');
			    			if(strBkpAnlyNames != '')
			    			{
			    				strBkpAnlyNames = strBkpAnlyNames.substring(0, strBkpAnlyNames.length() - 1);
			    			}
			    		}
	                }
	                if(strBkpAnlyIds != null && strBkpAnlyIds != '')
					{
						if(strBkpAnlyIds.length() > 255)
			            {
			            	strBkpAnlyIds = strBkpAnlyIds.substring(0, 254);
			            	strBkpAnlyIds = strBkpAnlyIds.substring(0, strBkpAnlyIds.lastIndexOf(','));	
			            }
		            	objstage.Bakup_Analysts__c = strBkpAnlyIds;
					}
					if(strBkpAnlyNames != null && strBkpAnlyNames != '')
					{
						if(strBkpAnlyNames.length() > 255)
			            {
			            	strBkpAnlyNames = strBkpAnlyNames.substring(0, 254);
			            }
						objstage.Bakup_Analysts_Names__c = strBkpAnlyNames;
					}
                }
				if(objstage.Planned_Review_Date__c != null && objstage.Planned_Review_Date__c > system.today())
				{
					string strOrgException = objstage.Exception__c;
					if(strOrgException!=null && strOrgException.contains('Exception: PLANNED_REVIEW_DATE__C should be future date.'))
                    {
                        strOrgException = strOrgException.replace('Exception: PLANNED_REVIEW_DATE__C should be future date.', '');
                        if(!strOrgException.contains('Exception'))
                        {
                        	strOrgException = '';
                        	objstage.Status__c='Initial Upload';
                        }
                        else if(strOrgException.startsWith(',Exception'))
                        {
                        	strOrgException = strOrgException.substring(1, strOrgException.length());
                        } 
                        objstage.Exception__c = strOrgException;
                    }
				}
				objstage.Notification_Flag__c = true;
            }
            else
            {
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select the State Name, Planned Review Date for all the records below.'));
                 return null;
            }
            if(objstage.Analyst__c!=null &&'-Select-'==string.valueOf(objstage.Analyst__c)){
            	objstage.Analyst__c=null;// modified by satish on jan 28th 2013 for removing analyst,buyer as mandatory fields
            }
              if(objstage.BulkUpload_BuyerID__c!=null &&'-Select-'==string.valueOf(objstage.BulkUpload_BuyerID__c)){
            	objstage.BulkUpload_BuyerID__c=null;
            }
            
            
            lstStageFinal.add(objstage);
            isSupplierStageUpdate=true;          
        }
        DataBase.update(lstStageFinal);
        isSupplierStageUpdate=false; 
        pageref = new pagereference('/apex/BSureS_SupplierInfoStageListView');
        return pageref;
        
    }
}