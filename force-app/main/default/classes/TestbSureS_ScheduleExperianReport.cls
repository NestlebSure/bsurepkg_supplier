@isTest
private class TestbSureS_ScheduleExperianReport{
	static testMethod void myUnitTest() {
    	System.test.startTest();
		bSureS_ScheduleExperianReport objSchClass = new bSureS_ScheduleExperianReport();
		String CRON_EXP = '0 0 0 1 1 ? 2025';  
        String jobId = System.schedule('testScheduledApex', CRON_EXP, new bSureS_ScheduleExperianReport() );
		string assign='1234';
        system.assertEquals('1234',assign);
        System.test.stopTest();
    }
    
	static testMethod void testBatchClass(){
        Database.BatchableContext BC;
        Test.startTest();
        
        BSureS_Basic_Info__c objBasicInfo = new BSureS_Basic_Info__c();
    	objBasicInfo.Supplier_Name__c = 'Demo Customer';
    	objBasicInfo.BISFileNumber__c = '7986548975';
    	objBasicInfo.Globe_ID__c = '589647';
    	objBasicInfo.Owner_Ship__c = 'Private';
    	objBasicInfo.Planned_Review_Date__c = system.today()+7;
    	objBasicInfo.Supplier_Countries__c = 'United States';
    	insert objBasicInfo;
    	
    	bSureS_ExpCred__c cred = new bSureS_ExpCred__c();
		cred.Pwd__c = 'Password';
		cred.UN__c = 'UserName';
		insert cred;
		
        bSureS_GenerateExperianReport b = new bSureS_GenerateExperianReport();
        ID myBatchJobID = database.executebatch(b);
        Test.stopTest();
    }
}