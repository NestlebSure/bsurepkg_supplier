/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData=true)
private class TestBSureS_MassTransferComponent {

   static testMethod void myUnitTest() 
   {
        // TO DO: implement unit test

        
        BSureS_Zone__c bsZone = new BSureS_Zone__c();
        bsZone.Name= 'AOA test';
        bsZone.IsActive__c = true;
        insert bsZone;

        
        BSureS_SubZone__c bsSubZone = new BSureS_SubZone__c();
        bsSubZone.Name = 'Test1 SubZone';
        bsSubZone.ZoneID__c=bsZone.id;
        bsSubZone.IsActive__c = true;
        insert bsSubZone;

        BSureS_Country__c bsCountry = new BSureS_Country__c();
        bsCountry.Name = 'Test1 Country';
        bsCountry.Sub_Zone_ID__c=bsSubZone.id;
        bsCountry.IsActive__c = true;
        insert bsCountry;

        
        BSureS_State__c SState =  new BSureS_State__c();
        SState.Name = 'Test1 CO';
        SState.IsActive__c = true;
        SState.Supplier_Country__c = bsCountry.id;
        insert SState;
        SState.Name = 'Test2 CO';
        update SState; 
        
        BSureS_Category__c scategory = new BSureS_Category__c();
        scategory.Name = 'Account';
        insert scategory;

        
        BSureS_Basic_Info__c supplier = new BSureS_Basic_Info__c();
        supplier.Contact_name__c = 'Steve';
        supplier.Supplier_Name__c = 'george M';
        supplier.Supplier_Category__c = scategory.id;
        //supplier.Parent_Supplier_ID__c = Exsupplier.id;
        supplier.Bakup_Analysts__c = UserInfo.getUserID();
        insert supplier;
        
        System.Assertequals('Steve',supplier.Contact_name__c);
String catid=scategory.id;
String supid=supplier.id;

BSureS_User_Additional_Attributes__c  Objaddatribute=new BSureS_User_Additional_Attributes__c();
Objaddatribute.Manager__c=Userinfo.getUserID();
Objaddatribute.User__c=Userinfo.getUserID();
insert Objaddatribute;
         BSureS_MassTransferComponent.findSObjects('testObj','Praveen S','testField',catid,UserInfo.getProfileID(),UserInfo.getUserID(),supid);
        BSureS_MassTransferComponent.findSObjects('testObj','Praveen S','testField','',UserInfo.getProfileID(),UserInfo.getUserID(),supid);        //('User','a', 'Username__C',catid,'BSureC_Analyst,System Administrator','','') 
        BSureS_MassTransferComponent.findSObjects('testObj','Praveen S','testField',catid,UserInfo.getProfileID(),UserInfo.getUserID(),'supid,test');
        BSureS_MassTransferComponent.findSObjects('testObj','Praveen S','testField',catid,'testprof1,testprof2',UserInfo.getUserID(),'supid,test');   
   } 

}