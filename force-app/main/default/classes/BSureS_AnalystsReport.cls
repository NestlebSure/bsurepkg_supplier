/***********************************************************************************************
* Controller Name   : BSureS_AnalystsReport 
* Date              : 
* Author            : Kishorekumar A 
* Purpose           : Class for Overdue Task Report
* Change History    : 
* Date                  Programmer              Reason
* -------------------- ------------------- -------------------------
* 28/12/2012                                            Initial Version
**************************************************************************************************/
global with sharing class BSureS_AnalystsReport {
    
    
    public list<BSureS_Basic_Info__c> lstSupplInfo{get;set;}
    public list<SupplierWrapperList> lstWrapperlist{get;set;}
    public map<string,List<BSureS_Basic_Info__c>> maplistSupplier{get;set;}
    public List<BSureS_User_Additional_Attributes__c> lstUserAddAtt {get; set;}
    public map<string, string> mapAnalystNames {get;set;}
    public string strNextReviewDateFormat{get;set;}
    DateTime d;
    
    //zone,Subzone 
    public string strZoneId{get;set;} // to store Zone Id
    public string strSubZoneId{get;set;} // to store Sub Zone Id
    public string strCountryId{get;set;}  // to store Country Id
    public list<SelectOption> zoneOptions{get;set;}  //selectOptins for list of  Zone Records
    public list<SelectOption> subZoneOptions{get;set;} //selectOptins for list of sub Zone Records   
    public list<SelectOption> coutryOptions{get;set;} // selectOptins for list of Country Records
    public Integer AnalystReportSize{get;set;}
    //pagenation
    public Integer pageNumber;//used for pagination
    public Integer pageSize;//used for pagination
    public Integer totalPageNumber;//used for pagination
    public Integer PrevPageNumber {get;set;}//used for pagination
    public Integer NxtPageNumber {get;set;}//used for pagination
    public Integer NlistSize {get;set;}//used for pagination
    public Integer Endlst {get;set;}//used for pagination
    public integer totallistsize{get;set;}//total size of list SPFolderset and SPWFilesList for pagination 
    public list<SupplierWrapperList> lstSupplierPagination{get;set;}  
    public list<SupplierWrapperList>  lstSupplierExport{get;set;}
    
    public class SupplierWrapperList
    {
        public string strAnalystName{get;set;}
        public string strSupplierName{get;set;}
        public string strSupplierInfo{get;set;}
        public string strReviewStatus{get;set;}
        public string strNextRvwDate {get;set;}
        public string strGlobeID{get;set;}
    }
    
    public BSureS_AnalystsReport()
    {
        string currentUserId = UserInfo.getUserId();
        AnalystReportSize = Integer.valueOf(BSureS_CommonUtil.getConfigurationValues('BSureS_Report_Page_Size').get(0));
        lstUserAddAtt = new list<BSureS_User_Additional_Attributes__c>([select BSureS_Zone__c,BSureS_Sub_Zone__c,BSureS_Country__c 
                                                                        from BSureS_User_Additional_Attributes__c where User__c =:currentUserId]); 
        if(lstUserAddAtt != null && lstUserAddAtt.size() > 0)
        {
            strZoneId = lstUserAddAtt.get(0).BSureS_Zone__c;
            strSubZoneId = lstUserAddAtt.get(0).BSureS_Sub_Zone__c;
            strCountryId = lstUserAddAtt.get(0).BSureS_Country__c;
        }
        getZones();
        getSubZones();
        getCountries();     
        getAnalysts();
    }
    public void getZones()
    {  
      BSureS_CommonUtil.isReport = true;
      zoneOptions = new list<SelectOption>();
      zoneOptions = BSureS_CommonUtil.getZones();
    }
    public void getSubZones()
    {    
            
        strSubZoneId =(strZoneId == 'ALL'?'ALL':strSubZoneId);
        //strCountryId = 'ALL';
        BSureS_CommonUtil.isReport = true;
        subZoneOptions = new list<SelectOption>();
        subZoneOptions  = BSureS_CommonUtil.getSubZones(strZoneId);
        
    }
   public void getCountries()
    {               
        if(strZoneId == 'ALL' || strSubZoneId == 'ALL' )
        strCountryId = 'ALL';
        BSureS_CommonUtil.isReport = true;
        coutryOptions = new list<SelectOption>();       
        coutryOptions = BSureS_CommonUtil.getCountries(strSubZoneId);  
    }
    public void Search()
    {
        getAnalysts();
    }
    
    public pagereference Cancel(){
        return page.BSureS_Reports;
    }
    //for Export to Excel
    public void getExport()
    {
        lstSupplierExport = new list<SupplierWrapperList>();
        ////system.debug('mapAnalystNames========111=========='+mapAnalystNames);
        ////system.debug('maplistSupplier=====111======='+maplistSupplier);
        ////system.debug('lstWrapperlist=======111======'+lstWrapperlist);
        for(string strAnalystId: mapAnalystNames.keyset())
        {
            if(maplistSupplier.get(strAnalystId) != null)
            {
                for(BSureS_Basic_Info__c suppwObj : maplistSupplier.get(strAnalystId))
                {
                    SupplierWrapperList sobjWrapp = new SupplierWrapperList();
                    if(suppwObj.Analyst_Name__c != null)
                    {
                        sobjWrapp.strAnalystName = suppwObj.Analyst_Name__c;
                        sobjWrapp.strSupplierName = suppwObj.Supplier_Name__c;
                        sobjWrapp.strReviewStatus = suppwObj.Review_Status__c;
                        sobjWrapp.strNextRvwDate = string.valueOf(suppwObj.Next_Review_Date__c);
                        sobjWrapp.strGlobeID = suppwObj.Globe_ID__c;
                        lstSupplierExport.add(sobjWrapp);
                    }   
                }
            }   
        }
    }
    public pageReference  ExportCSV()
    {
        PageReference pageRef;
        getExport();
        pageRef= new PageReference('/apex/BSureS_AnalystsReportExport'); 
        return pageRef;
    }
    public void getAnalysts()
    {
        //list<SupplierWrapperList>
        lstWrapperlist = new list<SupplierWrapperList>();
        maplistSupplier = new map<string, List<BSureS_Basic_Info__c>>();
        mapAnalystNames = new map<string, string>();
        
        string strQuery = 'select id,Supplier_Name__c,Analyst__c,Analyst_Name__c,Review_Status__c,BSureS_Country__c,'+
                            'Next_Review_Date__c, City__c, State_Name__c,Globe_ID__c,Sub_Zone__c,Zone__c'+
                            ' from BSureS_Basic_Info__c where Next_Review_Date__c <= today and Analyst__c != null ';
        if(strZoneId != null && strZoneId != 'ALL')
        {
            strQuery +=' AND Zone__c =: strZoneId ';
        }
        if(strSubZoneId != null &&  strSubZoneId != 'ALL' )
        {
            strQuery +=' AND Sub_Zone__c =: strSubZoneId ';
        }
        if(strCountryId != null && strCountryId != 'ALL'  )
        {
            strQuery +=' AND BSureS_Country__c =: strCountryId '; 
        }
        lstSupplInfo = Database.Query(strQuery);                    
        //lstSupplInfo = [ select id,Supplier_Name__c,Analyst__c,Analyst_Name__c,Review_Status__c,
        //                  Next_Review_Date__c, City__c, State_Name__c,Globe_ID__c
        //                from BSureS_Basic_Info__c where Next_Review_Date__c <= today ];
         
        if(lstSupplInfo != null && lstSupplInfo.size() > 0)
        {
             for(BSureS_Basic_Info__c suplObj:lstSupplInfo)
             {
                mapAnalystNames.put(suplObj.Analyst__c, suplObj.Analyst_Name__c);
                if(maplistSupplier.containsKey(suplObj.Analyst__c))
                {
                    list<BSureS_Basic_Info__c> objSuppList = maplistSupplier.get(suplObj.Analyst__c);
                    objSuppList.add(suplObj);
                    maplistSupplier.put(suplObj.Analyst__c, objSuppList);
                }
                else
                {
                    list<BSureS_Basic_Info__c> objSuppList = new list<BSureS_Basic_Info__c>();
                    objSuppList.add(suplObj);
                    maplistSupplier.put(suplObj.Analyst__c, objSuppList);
                }
            }
            ////system.debug('mapAnalystNames++++++' + mapAnalystNames);
            ////system.debug('maplistSupplier++++++' + maplistSupplier);
            for(string strAnalyst : maplistSupplier.keySet())
            {
                SupplierWrapperList wrapperObj = new SupplierWrapperList();
                if(strAnalyst != null && strAnalyst != '')
                {
                    wrapperObj.strAnalystName = mapAnalystNames.get(strAnalyst);
                    wrapperObj.strSupplierInfo = getSupplierInfo(strAnalyst);
                    lstWrapperlist.add(wrapperObj);
                }   
            }
            
            //pagination_____
        
            totallistsize=lstWrapperlist.size();// this size for pagination
            //pageSize = 5;// default page size will be 5
            pageSize = AnalystReportSize;
            if(totallistsize==0)
            {
                BindData(0);// this method is for pagination, if no records found then passing zero to the pagination method
                NxtPageNumber=lstSupplierPagination.size();
                PrevPageNumber=0;
            }
            else
            {
                BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
                NxtPageNumber=lstSupplierPagination.size();
                PrevPageNumber=1; 
            }
        }
         /*
            SupplierWrapperList wrapperObj = new SupplierWrapperList();
            wrapperObj.strAnalystName = suplObj.Analyst_Name__c;
            wrapperObj.strReviewStatus =  suplObj.Review_Status__c;
            wrapperObj.strSupplierName =  suplObj.Supplier_Name__c;
            
            lstWrapperlist.add(wrapperObj);
         */
        //return lstWrapperlist;
        ////system.debug('lstSupplInfo============='+lstSupplInfo);
    }
    
    public string getSupplierInfo(string strAnalystID)
    {
        string strSupplier = '';
        if(maplistSupplier.get(strAnalystID) != null)
        {
            strSupplier += '<table width=\'100%\' border=\'0\'><tr height=\'20\'>';
            strSupplier += '<td><b>Supplier Name</b></td>';
            strSupplier += '<td><b>Review Status</b></td>';
            strSupplier += '<td><b>Next Review Date</b></td>';
            strSupplier += '<td><b>Globe ID</b></td>';
            //strSupplier += '<td><b>Supplier City Name</b></td>';
            //strSupplier += '<td><b>Supplier State Name</b></td></tr>';
            for(BSureS_Basic_Info__c objSuppInfo : maplistSupplier.get(strAnalystID))
            {
                if(objSuppInfo.Globe_ID__c == null)
                {
                    objSuppInfo.Globe_ID__c ='';
                }
                if(objSuppInfo.Review_Status__c == null)
                {
                    objSuppInfo.Review_Status__c ='';
                }
                d = objSuppInfo.Next_Review_Date__c ;
                ////system.debug('strNextReviewDateFormat=====d==='+d);
                string strtest = string.valueOf(d);
                ////system.debug('strNextReviewDateFormat========'+strtest);
                ////system.debug('time zone========='+UserInfo.getTimeZone());
                strNextReviewDateFormat =d.format('MM/dd/yyyy');//'Asia/Colombo'
                ////system.debug('strNextReviewDateFormat=========='+strNextReviewDateFormat);
                strSupplier += '<tr height=\'20\'>';
                strSupplier += '<td width=\'35%\' wrap> <a href=\'/apex/Bsures_viewSupplierDetails?id='+ objSuppInfo.id +'\' target=\'_blank\' >' +objSuppInfo.Supplier_Name__c + '</a></td>';
                strSupplier += '<td width=\'15%\'>' + objSuppInfo.Review_Status__c + '</td>';
                strSupplier += '<td width=\'15%\'>' + strNextReviewDateFormat  + '</td>';
                strSupplier += '<td width=\'15%\'>' + objSuppInfo.Globe_ID__c + '</td>';
                //strSupplier += '<td width=\'17%\'>' + objSuppInfo.City__c + '</td>';
                //strSupplier += '<td width=\'18%\'>' + objSuppInfo.State_Name__c + '</td>';
                strSupplier += '</tr>';
            }
            strSupplier += '</table>';
        }
        ////system.debug('strSupplier++++++++++++' + strSupplier);
        return strSupplier;
    }
    
    
    
    
    
    //Pagination______
    
    //Pagenationnnnnnnnnnnnnnnnnn
    /// <summary>
    /// Below method fires when user clicks on next button of pagination.
    /// </summary>    
    /// <returns>pagereference</returns>
    public PageReference nextBtnClick() 
    {
        ////system.debug('nextbutton'+pageNumber);
        BindData(pageNumber + 1);
        pageData(pageNumber + 1);
        
        return null;
    }

    // <summary>
    // Below method fires when user clicks on previous button of pagination.
    // </summary>
    // <returns>pagereference</returns>
    public PageReference previousBtnClick() 
    {
        pageData(pageNumber - 1);
        BindData(pageNumber - 1);
        return null;
    }
    
    /// <summary>
    /// Below method fires when user clicks on next button of pagination.
    /// <returns>Integer</returns>
    /// </summary>
    public Integer getPageNumber()
    {
        return pageNumber; 
    }
    
    /// <summary>
    /// Below method is for getting pagesize how many records per page .
    /// <returns>Integer</returns>  
    /// </summary> 
    public Integer getPageSize()
    {
        return pageSize;
    }

    /// <summary>
    /// Below method is for enabling and disabling the previous button of pagination.
    /// <param name="PreviousButtonEnabled"></param>
    /// <returns>boolean</returns>      
    /// </summary>
    public Boolean getPreviousButtonEnabled()
    {
        return !(pageNumber > 1);
    }

    /// <summary>
    /// Below method is for enabling and disabling the nextbutton of pagination.
    /// <param name="NextButtonDisabled"></param>
    /// <returns>boolean</returns>      
    /// </summary>
    public Boolean getNextButtonDisabled()
    {
        if (totallistsize ==0) 
            return true;
        else
            return ((pageNumber * pageSize) >= totallistsize);
    }


    /// <summary>
    /// Below method gets the total no.of pages.
    /// <param name="TotalPageNumber"></param>
    /// <returns>Integer</returns>      
    /// </summary>
    public Integer getTotalPageNumber()
    {
        totalPageNumber = 0;
        if (totalPageNumber == 0 && totallistsize!=0)
        {
            ////system.debug(totalPageNumber+'-totallistsize--'+totallistsize);
            totalPageNumber = totallistsize / pageSize;
            Integer mod = totallistsize - (totalPageNumber * pageSize);
            if (mod > 0)
                totalPageNumber++;
        }


        return totalPageNumber;
    }
    
     /// <summary>
    /// Below method is the main method of pagination if have atleast one record then we pass 1 to this method otherwise zero.
    /// </summary>
    public void BindData(Integer newPageIndex)
    {
        ////system.debug('*************88pavajflajf');
        try
        {
            //lstbuyerInfoPagination=new list<BSureS_User_Additional_Attributes__c>();
            lstSupplierPagination=new list<SupplierWrapperList>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            
            ////system.debug('*************newPageIndex == '+newPageIndex); 
            ////system.debug('*************totalPageNumber =='+totalPageNumber);
            ////system.debug('*************pageNumber =='+pageNumber);
            ////system.debug('*************pageSize =='+pageSize);
            if(pageSize == null)pageSize = 10;
            if (newPageIndex > pageNumber)
            {
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;
            }
            else
            {
                max = newPageIndex * pageSize;
                min = max - pageSize;
            }

            if(lstSupplInfo!=NULL)
            {
                for(SupplierWrapperList b : lstWrapperlist)
                {
                    counter++;
                    
                        if (counter > min && counter <= max) 
                        {
                            lstSupplierPagination.add(b);// here adding files list
                        }
                }
            }
            
            pageNumber = newPageIndex;
            NlistSize=lstSupplierPagination.size();
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }
    
    /// <summary>
    /// Below method bnds the values for Next and previous buttons.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void pageData(Integer newPageIndex)
    {   
        if (newPageIndex > pageNumber)
        {
            PrevPageNumber=PrevPageNumber+pagesize;   
            NxtPageNumber=NxtPageNumber+NlistSize; 
        }
        if (newPageIndex < pageNumber)               
        {                                 
            PrevPageNumber=PrevPageNumber-pagesize;               
            NxtPageNumber=NxtPageNumber-NlistSize;      
        }
        ////system.debug('NlistSize============='+NlistSize);
    }
    
    /// <summary>
    /// Below method binds the data for last button.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void LastpageData(Integer newPageIndex)
    {
        try
        {
            //lstbuyerInfoPagination=new list<BSureS_User_Additional_Attributes__c>(); 
            lstSupplierPagination=new list<SupplierWrapperList>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;          
            ////system.debug('NlistSizeLastpage1============='+NlistSize);
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;    
            for(SupplierWrapperList  a : lstWrapperlist)
            {
                counter++;
                
                if (counter > min && counter <= max) 
                {
                       lstSupplierPagination.add(a);// here adding the folders list      
                }
            }
            pageNumber = newPageIndex;
            NlistSize=lstWrapperlist.size();
                 
            PrevPageNumber=totallistsize - NlistSize +1;   
            NxtPageNumber=totallistsize;
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }
    
    /// <summary>
    /// Below method bnds the value for last button.
    /// </summary>
    public PageReference LastbtnClick() 
    {
        BindData(totalpagenumber - 1);
        ////system.debug('Lastbutton++'+pagenumber);
        LastpageData(totalpagenumber);
         
        return null;
    }
    
    /// <summary>
    /// Below method bnds the value for first button.
    /// </summary>
    public PageReference FirstbtnClick() 
    {
        BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
        NxtPageNumber=NlistSize;
        PrevPageNumber=1;
        return null;
    } 
}