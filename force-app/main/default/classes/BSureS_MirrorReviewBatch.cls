global with sharing class BSureS_MirrorReviewBatch implements Database.Batchable<SObject> {
    
    public string strException;
    global String strQuery = null;
    global set<id> lstSource = new set<id>();
    global list<BSureS_Credit_Analysis_Section__c> lstdest = new list<BSureS_Credit_Analysis_Section__c>();
    global map<String,String> mapSectionIds = new map<String,String>();
    set<ID> mirrorAttIds = new set<ID>();
    set<ID> mirrorSecIds = new set<ID>();
    global list<string> lstChildSupp = new list<string>();
    public string strParentSupp;
    public string strParentReview;
    
    public BSureS_MirrorReviewBatch(list<BSureS_Credit_Analysis_Section__c> lstdest,String parentSupp, list<string> setChild)
    {
        this.lstdest = lstdest;
        this.strParentSupp = parentSupp;
        this.lstChildSupp = setChild;
        //system.debug(lstSource.size()+'@@parameters$$'+lstSource);
        //system.debug('Mirror_Support__c ==='+lstdest);
        
        for(BSureS_Credit_Analysis_Section__c obj :lstdest)
        {
            system.debug('obj.Mirror_Support__c===@@@'+obj.Mirror_Support__c);
            if(obj.Mirror_Support__c!=null && obj.Mirror_Support__c!='')
            {
                mirrorAttIds.add(obj.Mirror_Support__c);
                mirrorSecIds.add(obj.Id);
                mapSectionIds.put(obj.Mirror_Support__c,obj.Id);
            }
        }
        //system.debug('mirrorAttIds==='+mirrorAttIds);
        //strQuery = 'select id,name,body from Attachment where Id IN : mirrorAttIds ';
        strQuery = 'Select Id,Mirror_Support__c FROM  BSureS_Credit_Analysis_Section__c ';
        strQuery += ' WHERE Id IN : mirrorSecIds';
    }
    global Database.QueryLocator start (Database.BatchableContext ctx)
    { 
        
        //system.debug('strQuery'+strQuery);        
        return Database.getQueryLocator(strQuery);
    }
    
    global void execute(Database.BatchableContext BC, list<BSureS_Credit_Analysis_Section__c> scope)
    {
        try{
        //system.debug('scope==='+scope);
        BSureS_Credit_Analysis_Section__c obj = scope[0];
        //system.debug('obj==='+obj);
        String attachid = obj.Mirror_Support__c;
        //system.debug('attachid==='+attachid);
        String strQuery='Select id,name,body from Attachment where Id =:attachid '; 
        list<Attachment> lstAttach = database.query(strQuery);
        list<SObject> insertingAttachments = new list<SObject>();       

        if(lstAttach!=null && lstAttach.size()>0)
        {
           for(Attachment atch : lstAttach)
           {
               Attachment att = new Attachment(name = atch.name, 
                                    body = atch.body, 
                                    parentid = obj.Id);
               insertingAttachments.add(att);
           }
        }

        
            
         
        insert insertingAttachments;
        }
        catch(Exception ex){
            
            strException= string.valueOf(ex);
            //SendJobStatus(ex);
        }
    }

    global void finish(Database.BatchableContext BC)
    { 
         SendJobStatus();       
    }
    
    public void SendJobStatus(){
        
        String sendMTOJobStatus = '';
        String MTOJobMailID = '';
        
        if(!BSureS_CommonUtil.getConfigurationValues('BSureS_SendMirrorReviewMail').isEmpty())
        	sendMTOJobStatus = BSureS_CommonUtil.getConfigurationValues('BSureS_SendMirrorReviewMail').get(0);
        
        /*if(!BSureS_CommonUtil.getConfigurationValues('BSureS_SendMirrorReviewMailID').isEmpty())
        	MTOJobMailID = BSureS_CommonUtil.getConfigurationValues('BSureS_SendMirrorReviewMailID').get(0);*/
        	
        string loginUserId = Userinfo.getUserId();
        User sUser= new User();
        sUser = [SELECT Id, Email FROM User WHERE Id =: loginUserId];
        MTOJobMailID = sUser.Email;
        
        if(sendMTOJobStatus.equalsIgnoreCase('TRUE')){
            
            List<String> lst_toaddress = new List<String>();
            lst_toaddress.add(MTOJobMailID);
            String str_subject = strParentSupp+' has been mirrored to its subsidiaries';
            String str_Email_body = '';
            str_Email_body += '<table cellspacing="0" cellpadding="0">';
            
            if(strException != null && strException !=''){
	        	str_Email_body += '<br/><tr><td height="50px" width="80%" align="left"><font color="#6600CC">'+ strException + '</font></td></tr>';
	        }else{
            	str_Email_body += '<tr><td height="50px"><font color="#6600CC">Hello,</font></td></tr><br/><tr><td height="50px"/></tr><tr><td height="50px" width="80%" align="left"><font color="#6600CC">The following subsidiaries have been mirrored to its parent, \''+ strParentSupp +'\'</font></td></tr></br>';
            	if(lstChildSupp != null && lstChildSupp.size() > 0){
	            	for(string obj:lstChildSupp){
	            		str_Email_body += '<tr><td height="50px"><font color="#6600CC">'+obj+'</font></td></tr>';
	            	}
            	}
            }
            str_Email_body += '<br/><tr><td height="50px"/></tr><tr><td height="50px" width="80%" align="left"><font color="#6600CC">powered  by <b> bSure.</b> </font></td></tr>';
            str_Email_body += '</table>';                  
           
            if(lst_toaddress != null && lst_toaddress.size() > 0)
            {
                Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
                mail1.setToAddresses(lst_toaddress); 
                mail1.setUseSignature(false);
                mail1.setSubject(str_subject);
                mail1.setHtmlBody(str_Email_body); 
                mail1.setSenderDisplayName('Mirror Status - bSure');
                List<Messaging.SendEmailResult> results  = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail1});
                
            }
        }
    }
    
}