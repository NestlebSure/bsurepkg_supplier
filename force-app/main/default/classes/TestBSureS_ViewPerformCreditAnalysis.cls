/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class TestBSureS_ViewPerformCreditAnalysis {

    static testMethod void myUnitTest() 
    {
        
       
        
        list<BSureS_Credit_Analysis__c> lstCreditAnalysis1 = new list<BSureS_Credit_Analysis__c>();
        Map<String,ID> profiles = new Map<String,ID>();
        Map<String,ID> roles = new Map<String,ID>();
        
        List<Profile> ps1 = [select id, name from Profile where name =
                                 'Standard User' or name = 'System Administrator' or name = 'BSureS_Buyer'];
        List<UserRole> ur1 = [select id,name from UserRole where name = 'Buyer'];
        for(Profile p1 : ps1){
            profiles.put(p1.name, p1.id);
        }
        for(UserRole u1 : ur1){
            roles.put(u1.name,u1.id);
        }
        User standard1 = new User(
                alias = 'standt',
                email='standarduser@testorg.com',
                emailencodingkey='UTF-8',
                lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US',
                profileid = profiles.get('BSureS_Buyer'),
                UserRoleId = roles.get('Buyer'),
                timezonesidkey='America/Los_Angeles',
                username='standardusertest@testorg.com.bsures');
        //insert standard1;
        
        system.runAs(standard1)
        {
            BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
            objCSettings1.Name = 'BSureS_Run_Triggers11';
            objCSettings1.Parameter_Key__c = 'BSureS_Run_Triggers';
            objCSettings1.Parameter_Value__c = 'True';
            insert objCSettings1;
            system.assertEquals('True', objCSettings1.Parameter_Value__c);
        
            BSure_Configuration_Settings__c objCSettings5 = new BSure_Configuration_Settings__c();
            objCSettings5.Name = 'BSureS_BuyerShare22';
            objCSettings5.Parameter_Key__c = 'BSureS_BuyerShare';
            objCSettings5.Parameter_Value__c = 'TRUE';
            insert objCSettings5;
            
            BSure_Configuration_Settings__c objCSettings6 = new BSure_Configuration_Settings__c();
            objCSettings6.Name = 'BSureS_buyerrole44';
            objCSettings6.Parameter_Key__c = 'BSureS_buyerrole';
            objCSettings6.Parameter_Value__c = 'Buyer';
            insert objCSettings6;
            system.assertEquals('Buyer', objCSettings6.Parameter_Value__c);
            
            BSureS_Basic_Info__c supplierObj = new BSureS_Basic_Info__c();
            supplierObj.Contact_name__c = 'Steve';
            supplierObj.Supplier_Name__c = 'george M';
            supplierObj.Analyst__c = UserInfo.getUserId();
            supplierObj.Manager__c = UserInfo.getUserId();
            supplierObj.Bakup_Analysts__c = UserInfo.getUserId() + ',' + UserInfo.getUserId();
            supplierObj.Next_Review_Date__c = date.today();
            supplierObj.Planned_Review_Date__c = date.today();
            insert supplierObj;
            
            BSureS_Credit_Analysis__c cObj = new BSureS_Credit_Analysis__c();
            cObj.Review_Name__c = 'test Review';
            cObj.Spend__c = double.valueOf('4325123');
            cObj.Supplier_ID__c = supplierObj.Id;
            cObj.Review_Status__c = 'Completed';
            insert cObj;
            
            
            BSureS_Assigned_Buyers__c  buyerObj = new BSureS_Assigned_Buyers__c();
            buyerObj.Supplier_ID__c = supplierObj.Id;
            buyerObj.Buyer_ID__c = standard1.Id;
            insert buyerObj;
                
            Apexpages.currentPage().getParameters().put('id',cObj.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(cObj);
            BSureS_ViewPerformCreditAnalysis caObj = new BSureS_ViewPerformCreditAnalysis(controller);
            lstCreditAnalysis1.add(cObj);
            caObj.lstCreditAnalysis = lstCreditAnalysis1;
            caObj.visibilityEdit  = true;
            BSureS_Credit_Analysis__c objCA = caObj.getCreditAnalysisDetails();
            
            
        }    
    }
}