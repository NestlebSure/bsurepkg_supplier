/************************************************************************************************       
Controller Name         : BSureS_SupplierInformationBulkUpload       
Date                    : 03/16/2013        
Author                  : B.Anupreethi       
Purpose                 : This Controller is used to upload the supplier information in bulk       
Change History          : 
Date                        Programmer                     Reason       
--------------------      -------------------    -------------------------       
03/16/2013                  B.Anupreethi           Initial Version
**************************************************************************************************/
global with sharing class BSureS_SupplierInformationBulkUpload 
{
    
    public String filename{get;set;}//To Uploaded file from page
    public blob bfilecontent{get;set;} // To Hold the Uploaded file content from page
    public String strfiledata{get;set;}// Holds the CSV file Data
    public String[] filelines = new String[]{};// Holds the each row of the CSV file
    public List<BSureS_Basic_Info_Stage__c> lstStage = new List<BSureS_Basic_Info_Stage__c>();
    public List<BSureS_Basic_Info_Stage__c> lstsupplierInfo{get;set;}
    public BSureS_Basic_Info_Stage__c BSureSupplierInfo{get;set;}
    
    public boolean blnvisibility{get;set;} //Parent Id blnvisibility purpose
    public boolean leftsubzonerender=true; //Subzone values blnvisibility purpose in left selected list
    public string strSupplierName{get;set;} //added variable for Parent Supplier ID
    public string strhiddenSupplierID{get;set;} //Parent supplier returns id
    public map<string,string> mapSubZone{get;set;}
    public List<SelectOption> BSureSAvailableZoneList = new List<SelectOption>(); //Add zones into Supplier zone list
    public List<SelectOption> BSureSAssignedZoneList = new List<selectoption>(); //add selected zone Option list
    public List<SelectOption> BSureSAvailableSubZoneList = new List<SelectOption>(); //Add selected Sub Zone Option list
    public List<SelectOption> BSureSAssignedSubZoneList = new List<SelectOption>();// remove zones from selected list
    public List<SelectOption> BSureSAvailableCountryList = new List<SelectOption>(); 
    public List<SelectOption> BSureSAssignedCountryList = new List<SelectOption>();
    public List<SelectOption> availableSCIZones{get;set;}
    public List<SelectOption> availableSCISubZones{get;set;}
    public List<SelectOption> availableSCICounty{get;set;}
    public List<SelectOption> lstAvailableStates{get;set;}
    
    public List<string> lstSelectedZones = new List<String>();
    public list<string> lsteditzones= new list<string>(); //variable for selected zone are displayed in Edit mode
    public list<string> lsteditSubzones= new list<string>(); //variable for selected Sub zone are displayed in Edit mode
    public list<string> lstEditCountries = new list<string>();
    public string strSCIZoneId{get;set;}
    public string strSCISubZoneId{get;set;} 
    public string strSCICountryId{get;set;}
    public string strState{get;set;} // get Country Name 
    public List<BSureS_Zone__c> lstSupplierZone{get;set;}
    public List<BSureS_Zone__c> BSureZoneRef=new List<BSureS_Zone__c>();
    //  public map<string,string> mapSubZone{get;set;}
    Map<string,string> MapZone = new map<string,string>();
    
    public string strCategory{get;set;} //To hold the category name
    public list<SelectOption> lstSCategory{get;set;}//To hold list of categories
    public string strAnalystName{get;set;} //To hold the category name
    public list<SelectOption> lstAnalysts{get;set;}//To hold list of categories
    public string strAnalystManger{get;set;} //To hold the category name
    public string strAnalystMangerId{get;set;}
    public list<SelectOption> lstAnalystManager{get;set;}//To hold list of categories
    public map<string,string> mapAnalystManger{get;set;}
   
    public map<string,string> mapZones{get;set;}
    public map<string,string> mapSubZones{get;set;}
    public map<string,string> mapCountries{get;set;}
    
    public boolean VisiblityManger{get;set;}
    public boolean hideInfo{get;set;}
    public boolean AlertUser{get;set;}
    
    public BSureS_SupplierInformationBulkUpload()
    {
        //VisiblityManger=true;
        //hideInfo=false;
        BSureSupplierInfo = new BSureS_Basic_Info_Stage__c();
        mapSubZone=new map<string,string>();
        lstSCategory = BSureS_CommonUtil.getBSureSCategory();
        mapZones = new map<string,string>();
        mapSubZones = new map<string,string>();
        mapCountries = new map<string,string>();
        List<BSureS_Basic_Info_Stage__c> existRec = [SELECT Id from BSureS_Basic_Info_Stage__c where id!= null];
       	if(existRec.size()>0)
        {
        	AlertUser = true;
        }
        else
        {
        	 AlertUser=false;
        }
       /* lstSupplierZone = [SELECT id, name, IsActive__c from BSureS_Zone__c  where IsActive__c = true];
        for(BSureS_Zone__c BSCZoneRef: lstSupplierZone)
        {
            BSureZoneRef.add(BSCZoneRef);
        } 
        
        for(BSureS_Zone__c ZoneRef:BSureZoneRef)
        { 
            if(MapZone.get(ZoneRef.Name) == null)
                setLeftZonevalues.add(ZoneRef.Name);
        }*/ 
    }    
   
    public pagereference SaveUploadData()  
    {
        pagereference pageref;
        //system.debug('-bfilecontent---'+bfilecontent);
        strfiledata = bfilecontent.toString();//converting file blob to string.
        //system.debug('strfiledata@================'+strfiledata);
        filelines = strfiledata.split('\n');//splitting the rows.
        //system.debug('-filelines---'+filelines);
        //system.debug('filelines.size()---'+filelines.size());
        if(filelines.size()<=51)
        {
            for (Integer i=1;i<filelines.size();i++)
            {
                ////system.debug('in for=======================');
                String[] inputvalues = new String[]{};//initializing the string array
                inputvalues = filelines[i].split(',');//split values with ',' .
                BSureS_Basic_Info_Stage__c objstage = new BSureS_Basic_Info_Stage__c();
                objstage.Status__c='Initial Upload';
                //Suppplier name:mandatory
                if(inputvalues[0]!=NULL && inputvalues[0]!=' ' && inputvalues[0]!='')
                {
                   objstage.SUPPLIER_NAME__C= String.valueof(inputvalues[0].trim()); //assigning values
                }
                else
                {
                    //if date is null in csv file the assigning the exception to the field
                    if(objstage.Exception__c==NULL)
                    {
                        objstage.Exception__c='Exception: SUPPLIER_NAME__C is Mandatory';
                        objstage.Status__c='Exception';
                    }
                    else
                    {
                        objstage.Exception__c+=','+'Exception: SUPPLIER_NAME__C isMandatory';
                        objstage.Status__c='Exception';
                    }   
                }
                //ISPOTENTIAL_SUPPLIER__C:not mandatory            
                if(inputvalues[1]!=NULL && inputvalues[1]!=' ' && inputvalues[1]!='')
                {
                    try
                    {
                        objstage.ISPOTENTIAL_SUPPLIER__C= boolean.valueof(inputvalues[1].trim()); //assigning values
                    }
                    catch(Exception e)
                    {
                        //if the supplier Name is not in format then adding exception value to the exception field
                        if(objstage.Exception__c==NULL)
                        {
                            objstage.Exception__c=String.valueof(e);
                            objstage.Status__c='Exception';
                        }
                        else
                        {
                            objstage.Exception__c+=','+String.valueof(e);
                            objstage.Status__c='Exception';
                        }   
                    }
                }
                //GLOBE_ID__C : madatoory based on ispotentail
                if(inputvalues[2]!=NULL && inputvalues[2]!=' ' && inputvalues[2]!='')
                {
                    try
                    {
                        objstage.GLOBE_ID__C= String.valueof(inputvalues[2].trim()); //assigning values
                    }
                    catch(Exception e)
                    {
                        //if the supplier Name is not in format then adding exception value to the exception field
                        if(objstage.Exception__c==NULL)
                        {
                            objstage.Exception__c=String.valueof(e);
                            objstage.Status__c='Exception';
                        }
                        else
                        {
                            objstage.Exception__c+=','+String.valueof(e);
                            objstage.Status__c='Exception';
                        }   
                    }
                }
                //OWNER_SHIP__C:not mandatory
                if(inputvalues[3]!=NULL && inputvalues[3]!=' ' && inputvalues[3]!='')
                {
                    try
                    {
                        objstage.OWNER_SHIP__C= String.valueof(inputvalues[3].trim()); //assigning values
                    }
                    catch(Exception e)
                    {
                        //if the supplier Name is not in format then adding exception value to the exception field
                        if(objstage.Exception__c==NULL)
                        {
                            objstage.Exception__c=String.valueof(e);
                            objstage.Status__c='Exception';
                        }
                        else
                        {
                            objstage.Exception__c+=','+String.valueof(e);
                            objstage.Status__c='Exception';
                        }   
                    }
                }
                 //SOLE_SOURCED__C:not mandatory
                if(inputvalues[4]!=NULL && inputvalues[4]!=' ' && inputvalues[4]!='')
                {
                    objstage.SOLE_SOURCED__C = String.valueof(inputvalues[4].trim()); //assigning values
                }
                //CONTACT_NAME__C:not mandatory
                if(inputvalues[5]!=NULL && inputvalues[5]!=' ' && inputvalues[5]!='')
                {
                    objstage.CONTACT_NAME__C = String.valueof(inputvalues[5].trim()); //assigning values
                }
                //F_S__C:not mandaroty
                if(inputvalues[6]!=NULL && inputvalues[6]!=' ' && inputvalues[6]!='')
                {
                    
                    objstage.F_S__C = String.valueof(inputvalues[6].trim()); //assigning values
                    
                }
                //NOTIFICATION_FLAG__C(Y/N)
                //NOTIFICATION_FLAG__C:not mandaroty
                if(inputvalues[7]!=NULL && inputvalues[7]!=' ' && inputvalues[7]!='')
                {
                    try
                    {
                        objstage.NOTIFICATION_FLAG__C = boolean.valueof(inputvalues[7].trim()); //assigning values
                    }
                    catch(Exception e)
                    {
                        //if the supplier Name is not in format then adding exception value to the exception field
                        if(objstage.Exception__c==NULL)
                        {
                            objstage.Exception__c=String.valueof(e);
                            objstage.Status__c='Exception';
                        }
                        else
                        {
                            objstage.Exception__c+=','+String.valueof(e);
                            objstage.Status__c='Exception';
                        }   
                    }
                }
                //PLANNED_REVIEW_DATE__C: mandaroty
                if(inputvalues[8]!=NULL && inputvalues[8]!=' ' && inputvalues[8]!='')
                {
                    try
                    {
                        objstage.PLANNED_REVIEW_DATE__C = Date.parse(inputvalues[8].trim()); //assigning values
                        if(Date.parse(inputvalues[8].trim()) < system.today())
                        {
                        	if(objstage.Exception__c==NULL)
	                        {
	                            objstage.Exception__c= 'Exception: PLANNED_REVIEW_DATE__C should be future date.';
	                            objstage.Status__c='Exception';
	                        }
	                        else
	                        {
	                            objstage.Exception__c+=',' + 'Exception: PLANNED_REVIEW_DATE__C should be future date.';
	                            objstage.Status__c='Exception';
	                        } 
                        }
                    }
                    catch(Exception e)
                    {
                        //if the date is not in format then adding exception value to the exception field
                        if(objstage.Exception__c==NULL)
                        {
                            objstage.Exception__c=String.valueof(e);
                            objstage.Status__c='Exception';
                        }
                        else
                        {
                            objstage.Exception__c+=','+String.valueof(e);
                            objstage.Status__c='Exception';
                        }   
                    }
                }
                else
                {
                    objstage.PLANNED_REVIEW_DATE__C = system.today().addMonths(6);
                    
                    //if date is null in csv file the assigning the exception to the field
                    /*if(objstage.Exception__c==NULL)
                    {
                        objstage.Exception__c='Mandatory';
                        objstage.Status__c='Exception';
                    }
                    else
                    {
                        objstage.Exception__c+=','+'Mandatory';
                        objstage.Status__c='Exception';
                    }*/ 
                }
                //FINANCIAL_INFORMATION__C: mandatory
                if(inputvalues[9]!=NULL && inputvalues[9]!=' ' && inputvalues[9]!='')
                {
                    try
                    {
                        objstage.FINANCIAL_INFORMATION__C= string.valueof(inputvalues[9].trim()); //assigning values
                    }
                    catch(Exception e)
                    {
                        //if the date is not in format then adding exception value to the exception field
                        if(objstage.Exception__c==NULL)
                        {
                            objstage.Exception__c=String.valueof(e);
                            objstage.Status__c='Exception';
                        }
                        else
                        {
                            objstage.Exception__c+=','+String.valueof(e);
                            objstage.Status__c='Exception';
                        }   
                    }
                }
                else
                {
                    //if date is null in csv file the assigning the exception to the field
                    if(objstage.Exception__c==NULL)
                    {
                        objstage.Exception__c='Exception: FINANCIAL_INFORMATION__C is Mandatory';
                        objstage.Status__c='Exception';
                    }
                    else
                    {
                        objstage.Exception__c+=','+'Exception: FINANCIAL_INFORMATION__C is Mandatory';
                        objstage.Status__c='Exception';
                    }   
                }
                //FISCAL_YEAR_END__C - not mandatory
                if(inputvalues[10]!=NULL && inputvalues[10]!=' ' && inputvalues[10]!='')
                {
                    try
                    {
                        objstage.FISCAL_YEAR_END__C = Date.parse(inputvalues[10].trim()); //assigning values
                    }
                    catch(Exception e)
                    {
                        //if the supplier Name is not in format then adding exception value to the exception field
                        if(objstage.Exception__c==NULL)
                        {
                            objstage.Exception__c=String.valueof(e);
                            objstage.Status__c='Exception';
                        }
                        else
                        {
                            objstage.Exception__c+=','+String.valueof(e);
                            objstage.Status__c='Exception';
                        }   
                    }
                }
                //SPEND__C : not mantadory
                if(inputvalues[11]!=NULL && inputvalues[11]!=' ' && inputvalues[11]!='')
                {
                    try
                    {
                        objstage.SPEND__C = Decimal.valueof(inputvalues[11].trim()); //assigning values
                    }
                    catch(Exception e)
                    {
                        //if the supplier Name is not in format then adding exception value to the exception field
                        if(objstage.Exception__c==NULL)
                        {
                            objstage.Exception__c=String.valueof(e);
                            objstage.Status__c='Exception';
                        }
                        else
                        {
                            objstage.Exception__c+=','+String.valueof(e);
                            objstage.Status__c='Exception';
                        }   
                    }
                }
                //LAST_FINANCIAL_STATEMENT_RECEIVED__Cnot mantadory
                if(inputvalues[12]!=NULL && inputvalues[12]!=' ' && inputvalues[12]!='')
                {
                    try
                    {
                        objstage.LAST_FINANCIAL_STATEMENT_RECEIVED__C = Date.parse(inputvalues[12].trim()); //assigning values
                    }
                    catch(Exception e)
                    {
                        //if the supplier Name is not in format then adding exception value to the exception field
                        if(objstage.Exception__c==NULL)
                        {
                            objstage.Exception__c=String.valueof(e);
                            objstage.Status__c='Exception';
                        }
                        else
                        {
                            objstage.Exception__c+=','+String.valueof(e);
                            objstage.Status__c='Exception';
                        }   
                    }
                }
                //SUPPLIER_CONTACT__C:not mandatory
                if(inputvalues[13]!=NULL && inputvalues[13]!=' ' && inputvalues[13]!='')
                {
                    try
                    {
                        objstage.SUPPLIER_CONTACT__C = string.valueof(inputvalues[13].trim()); //assigning values
                    }
                    catch(Exception e)
                    {
                        //if the supplier Name is not in format then adding exception value to the exception field
                        if(objstage.Exception__c==NULL)
                        {
                            objstage.Exception__c=String.valueof(e);
                            objstage.Status__c='Exception';
                        }
                        else
                        {
                            objstage.Exception__c+=','+String.valueof(e);
                            objstage.Status__c='Exception';
                        }   
                    }
                }
                //EMAIL_ADDRESS__C:not mandatory
                if(inputvalues[14]!=NULL && inputvalues[14]!=' ' && inputvalues[14]!='')
                {
                    try
                    {
                        objstage.EMAIL_ADDRESS__C = string.valueof(inputvalues[14].trim()); //assigning values
                    }
                    catch(Exception e)
                    {
                        //if the supplier Name is not in format then adding exception value to the exception field
                        if(objstage.Exception__c==NULL)
                        {
                            objstage.Exception__c=String.valueof(e);
                            objstage.Status__c='Exception';
                        }
                        else
                        {
                            objstage.Exception__c+=','+String.valueof(e);
                            objstage.Status__c='Exception';
                        }   
                    }
                }
                //PHONE_NUMBER__C:not mandatory
                if(inputvalues[15]!=NULL && inputvalues[15]!=' ' && inputvalues[15]!='')
                {
                    try
                    {
                        objstage.PHONE_NUMBER__C = string.valueof(inputvalues[15].trim()); //assigning values
                    }
                    catch(Exception e)
                    {
                        //if the supplier Name is not in format then adding exception value to the exception field
                        if(objstage.Exception__c==NULL)
                        {
                            objstage.Exception__c=String.valueof(e);
                            objstage.Status__c='Exception';
                        }
                        else
                        {
                            objstage.Exception__c+=','+String.valueof(e);
                            objstage.Status__c='Exception';
                        }   
                    }
                }
                //FAX__C:not mandatory
                if(inputvalues[16]!=NULL && inputvalues[16]!=' ' && inputvalues[16]!='')
                {
                    objstage.FAX__C = string.valueof(inputvalues[16].trim()); //assigning values
                }
                //WEBSITE_LINK__C:not mandatory
                if(inputvalues[17]!=NULL && inputvalues[17]!=' ' && inputvalues[17]!='')
                {
                    objstage.WEBSITE_LINK__C = string.valueof(inputvalues[17].trim()); //assigning values
                }
                //STREET_NAME_ADDRESS_LINE_1__C:mandatory
                if(inputvalues[18]!=NULL && inputvalues[18]!=' ' && inputvalues[18]!='')
                {
                    objstage.STREET_NAME_ADDRESS_LINE_1__C= string.valueof(inputvalues[18].trim()); //assigning values
                }
                else
                {
                    //if date is null in csv file the assigning the exception to the field
                    if(objstage.Exception__c==NULL)
                    {
                        objstage.Exception__c='Exception: STREET_NAME_ADDRESS_LINE_1__C is Mandatory';
                        objstage.Status__c='Exception';
                    }
                    else
                    {
                        objstage.Exception__c+=','+'Exception: STREET_NAME_ADDRESS_LINE_1__C is Mandatory';
                        objstage.Status__c='Exception';
                    }   
                }
                //ADDRESS_LINE_2__C:not mandatory
                if(inputvalues[19]!=NULL && inputvalues[19]!=' ' && inputvalues[19]!='')
                {
                    objstage.ADDRESS_LINE_2__C = string.valueof(inputvalues[19].trim()); //assigning values
                }
                //CITY__C: mandatory
                if(inputvalues[20]!=NULL && inputvalues[20]!=' ' && inputvalues[20]!='')
                {
                    objstage.CITY__C= string.valueof(inputvalues[20].trim()); //assigning values
                }
                else
                {
                    //if date is null in csv file the assigning the exception to the field
                    if(objstage.Exception__c==NULL)
                    {
                        objstage.Exception__c='Exception: CITY__C is Mandatory';
                        objstage.Status__c='Exception';
                    }
                    else
                    {
                        objstage.Exception__c+=','+'Exception: CITY__C is Mandatory';
                        objstage.Status__c='Exception';
                    }   
                }
                //POSTAL_CODE__C:not mandatory
                if(inputvalues[21]!=NULL && inputvalues[21]!=' ' && inputvalues[21]!='')
                {
                    objstage.POSTAL_CODE__C = string.valueof(inputvalues[21].trim()); //assigning values
                }
                
                //system.debug('-lstStage--'+lstStage);
                //system.debug('-BSureSupplierInfo.Supplier_Category__c---'+strCategory);
                //system.debug('-strSCIZoneId---'+strSCIZoneId);
                //system.debug('-strSCISubZoneId---'+strSCISubZoneId);
                //system.debug('-strSCICountryId---'+strSCICountryId);
                //system.debug('-strState---'+strState);
                objstage.Supplier_Category__c = strCategory;
                objstage.Zone__c = strSCIZoneId;
                objstage.Sub_Zone__c = strSCISubZoneId;
                objstage.BSureS_Country__c = strSCICountryId;
                //objstage.State_Province__c = strState;
                //multiple region values
                objstage.Supplier_Zones__c= mapZones.get(strSCIZoneId);
                objstage.Supplier_SubZones__c= mapSubZones.get(strSCISubZoneId);
                objstage.Supplier_Countries__c= mapCountries.get(strSCICountryId);
                //objstage.Analyst__c = strAnalystName;
                //objstage.Manager__c = strAnalystMangerId;
                
                //unique supplier validation
                //objstage.SUPPLIER_NAME__C, //objstage.State_Province__c //objstage.CITY__C
                /*integer cntSup = [select count() from BSureS_Basic_Info__c where SUPPLIER_NAME__C=:objstage.SUPPLIER_NAME__C AND State_Province__c=:objstage.State_Province__c AND CITY__C=:objstage.CITY__C];
                if(cntSup>0)
                {
                    objstage.Exception__c+=','+''+Label.BSureS_VNameCityState;
                }*/
                
                lstStage.add(objstage);
                //system.debug('--lstStage--'+lstStage);
                
            }
            if(lstStage.size()>0)
            {
                //hideInfo=true;
                Delete[SELECT Id from BSureS_Basic_Info_Stage__c where id!= null];
                Database.SaveResult[] SupplierStageInfoInsert = Database.insert(lstStage);// inserting the list
                //if(SupplierStageInfoInsert.isSuccess()== true )
                pageref = new pagereference('/apex/BSureS_SupplierblkUploadIntermediate');
                return pageref;
            }
        }
        else
        {
            // showErrorMessage(system.Label.BSureC_Please_enter_mandatory_fields); 
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The uploaded file contians more than 50 records. please upload 50 records at a time.'));
            // ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.error,'The uploaded file contians more than 50 records. please upload 50 records at a time.');
             return null;         
        }
        return pageref;
    }     
      
    /// <summary>
    /// Method to get SubZones list for Supplier Contact Info
    /// </summary>
    public List<SelectOption> getSZoneList() 
    {   
        availableSCIZones = new List<SelectOption>(); 
        mapZones = new map<string,string>();
        availableSCIZones.add(new Selectoption(System.label.BSureS_VSelect,System.label.BSureS_VSelect));
        List<BSureS_Zone__c> lstzonerecords = [Select id, Name From BSureS_Zone__c where IsActive__c = true order by Name];
        
        for(BSureS_Zone__c ZobjEach : lstzonerecords)
        {  
            availableSCIZones.add(new Selectoption(ZobjEach.id,ZobjEach.Name));
            mapZones.put(ZobjEach.id,ZobjEach.Name);
        } 
        return availableSCIZones; 
    } 
        
    /// <summary>
    /// Method to get SubZone list for Supplier Contact Info
    /// </summary>
    public List<SelectOption> getSSubZoneList()
    { 
        availableSCISubZones = new List<SelectOption>();
        mapSubZones = new map<string,string>();
        availableSCISubZones.add(new Selectoption(System.label.BSureS_VSelect,System.label.BSureS_VSelect));
        List<BSureS_SubZone__c> lstSupplierSubZoneInfo;
        if(strSCIZoneId != null && strSCIZoneId != '')
        {
            lstSupplierSubZoneInfo = [Select id, Name From BSureS_SubZone__c  Where IsActive__c = true and ZoneID__c =:strSCIZoneId order by Name];
        }
        if(lstSupplierSubZoneInfo!= null && lstSupplierSubZoneInfo.size()>0)
        {
            for(BSureS_SubZone__c objSubZoneEach :lstSupplierSubZoneInfo ) 
            {
                if(objSubZoneEach.Name != null)
                {
                    availableSCISubZones.add(new Selectoption(objSubZoneEach.id, objSubZoneEach.Name));   
                     mapSubZones.put(objSubZoneEach.id,objSubZoneEach.Name);
                }
            } 
        }    
        return availableSCISubZones; 
    }

    /// <summary>
    /// Method to get Country list for Supplier Contact Info
    /// </summary>
    public List<SelectOption> getSCountryList()
    {
        availableSCICounty = new List<SelectOption>();
        mapCountries = new map<string,string>();
        availableSCICounty.add(new Selectoption(System.label.BSureS_VSelect,System.label.BSureS_VSelect));
        List<BSureS_Country__c> lstSupplierCountryInfo;       
        
        if(strSCISubZoneId != null && strSCISubZoneId != '')
        {
            lstSupplierCountryInfo = [select id, Name from BSureS_Country__c  where IsActive__c = true and Sub_Zone_ID__c =:strSCISubZoneId order by Name];
        }
        
        if(lstSupplierCountryInfo != null && lstSupplierCountryInfo.size() > 0)
        {
            for(BSureS_Country__c objcountryEach: lstSupplierCountryInfo)
            { 
                if(objcountryEach.Name !=null)
                {
                    availableSCICounty.add(new Selectoption(objcountryEach.id, objcountryEach.Name));      
                    mapCountries.put(objcountryEach.id, objcountryEach.Name);         
                }
            }
        } 
        return availableSCICounty; 
    }  
    
    /// <summary> 
    /// Method to get Country list for Supplier Contact Info
    /// </summary>  
    public List<SelectOption> getSAnalystList()
    {
        lstAnalysts = new List<SelectOption>();    
        set<Id> userIds = new set<Id>();   
        mapAnalystManger = new map<string,string>();
         
        lstAnalysts.add(new Selectoption(System.label.BSureS_VSelect,System.label.BSureS_VSelect));
        list<BSureS_User_Additional_Attributes__c> AAttributes = [select   id,User__c,
                                                                            BSureS_Zone__c,
                                                                            BSureS_Sub_Zone__c,
                                                                            BSureS_Country__c,Manager__c,Manager__r.Name
                                                                            FROM BSureS_User_Additional_Attributes__c
                                                                            WHERE BSureS_Zone__c=:strSCIZoneId and BSureS_Sub_Zone__c=:strSCISubZoneId and BSureS_Country__c=:strSCICountryId];
                                                                            
        for(BSureS_User_Additional_Attributes__c uA:AAttributes)
        {
            userIds.add(ua.User__c);
            mapAnalystManger.put(ua.User__c,ua.Manager__c+','+ua.Manager__r.Name);
            //system.debug('mapAnalystManger--'+mapAnalystManger);
        }   
        if(!userIds.IsEmpty())
        {
            list<BSureS_UserCategoryMapping__c> Categories = [select id,CategoryID__c,UserID__c from BSureS_UserCategoryMapping__c where UserID__c in:userIds and CategoryID__c=:strCategory ];
            userIds.clear();
            for(BSureS_UserCategoryMapping__c Ucat:Categories){
                userIds.add(Ucat.UserID__c);
            }
        }
        
        list<User> analystUser = [select Id,Name from User where Id in:userIds and profile.Name = 'BSureS_Analyst' and IsActive=true];
        
        for(User u:analystUser)
        {
            lstAnalysts.add(new Selectoption(u.Id,u.name));
        }                                                       
        return lstAnalysts;   
    }
    
    public void getSAnalystManagerList()
    {
        lstAnalystManager = new List<SelectOption>();
        lstAnalystManager.add(new Selectoption(System.label.BSureS_VSelect,System.label.BSureS_VSelect));
        string sstrAnalystManger = mapAnalystManger.get(strAnalystName);
        //system.debug('sstrAnalystManger--'+sstrAnalystManger);
        if(sstrAnalystManger!=null && sstrAnalystManger!='')
        {   
            //VisiblityManger=true; 
            //system.debug('VisiblityManger------'+VisiblityManger);  
            string[] strIdName = sstrAnalystManger.split(',');
            //lstAnalystManager.add(new Selectoption(strIdName[0],strIdName[1]));
            strAnalystMangerId=strIdName[0];
            strAnalystManger=strIdName[1];
        }
        //return lstAnalystManager;   
    }
    public pagereference Cancel(){
        return page.BSureS_AdminTools;
    } 
    
    
}