/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class TestBSureS_EmailManagement {

    static testMethod void myUnitTest() {
        
        list<BSureS_Email_Queue__c> objListEmailQueue = new list<BSureS_Email_Queue__c>();
        //BSureS_Email_Queue__c
    	BSureS_Email_Queue__c objEmail1 = new BSureS_Email_Queue__c();
    	objEmail1.Email_Body__c = 'Test Email Body 1';
    	objEmail1.Email_Status__c = 'NEW';
    	objEmail1.Email_Subject__c = 'Test Email Subject 1';
    	objEmail1.Is_Daily_Digest__c = 'YES';
    	objEmail1.Recipient_Address__c = 'test@vertex.com';
    	objEmail1.Recipient_Name__c = 'Aditya Vemuri123';
    	objEmail1.Send_On_Date__c = System.today();
    	objEmail1.Send_Immediate__c = true;
    	insert(objEmail1);
    	system.assertEquals('Aditya Vemuri123', objEmail1.Recipient_Name__c);
    	
    	BSureS_Email_Queue__c objEmail2 = new BSureS_Email_Queue__c();
    	objEmail2.Email_Body__c = 'Test Email Body 1';
    	objEmail2.Email_Status__c = 'NEW';
    	objEmail2.Email_Subject__c = 'Test Email Subject 1';
    	objEmail2.Is_Daily_Digest__c = 'YES';
    	objEmail2.Recipient_Address__c = 'test@vertex.com';
    	objEmail2.Recipient_Name__c = 'Aditya Vemuri';
    	objEmail2.Send_On_Date__c = System.today();
    	objEmail2.Send_Immediate__c = true;
    	
    	insert(objEmail2);
    	
    	BSureS_Email_Queue__c objEmail3 = new BSureS_Email_Queue__c();
    	objEmail3.Email_Body__c = 'Test Email Body 1';
    	objEmail3.Email_Status__c = 'NEW';
    	objEmail3.Email_Subject__c = 'Test Email Subject 1';
    	objEmail3.Is_Daily_Digest__c = 'NO';
    	objEmail3.Recipient_Address__c = 'test@vertex.com';
    	objEmail3.Recipient_Name__c = 'Aditya Vemuri';
    	objEmail3.Send_On_Date__c = System.today();
    	objEmail3.Send_Immediate__c = true;
    	
    	insert(objEmail3);
    	
    	BSureS_Email_Queue__c objEmail4 = new BSureS_Email_Queue__c();
    	objEmail4.Email_Body__c = 'Test Email Body 1';
    	objEmail4.Email_Status__c = 'NEW';
    	objEmail4.Email_Subject__c = 'Test Email Subject 1';
    	objEmail4.Is_Daily_Digest__c = 'NO';
    	objEmail4.Recipient_Address__c = 'test@vertex.com';
    	objEmail4.Recipient_Name__c = 'Aditya Vemuri';
    	objEmail4.Send_On_Date__c = System.today();
    	objEmail4.Send_Immediate__c = true;
    	
    	insert(objEmail4);
    	
    	objListEmailQueue.add(objEmail1);
    	objListEmailQueue.add(objEmail2);
    	objListEmailQueue.add(objEmail3);
    	objListEmailQueue.add(objEmail4);
    	
    	BSureS_EmailManagement p = new BSureS_EmailManagement(); 
		String sch = '0 0 * * * ?'; 
		system.schedule('BSureS_EmailManagement', sch, p);
		
		p.GetEmailTemplates();
		Messaging.SingleEmailMessage objEmailMsg = p.GenerateConsolidatedMail(objListEmailQueue);
		Database.BatchableContext objBC ;
		p.Execute(objBC, objListEmailQueue);
		p.UpdateEmailQueue(objListEmailQueue);
        
    }
}