/***********************************************************************************************
* Controller Name :  BSureS_SuppliersReport 
* Date : 13 Dec 2012
* Author : Veereandrnath Jella
* Purpose : This class written to generate report on Supplier 
* Change History :
* Date Programmer Reason
* -------------------- ------------------- -------------------------
* 12/13/2011 Veereandranath.j Initial Version
*  
**************************************************************************************************/
global with sharing class BSureS_SuppliersReport {
    public transient list<BSureS_Basic_Info__c> lstSuppliers{get;set;}
    public string strZoneId{get;set;} // to store Zone Id
    public string strSubZoneId{get;set;} // to store Sub Zone Id
    public string strCountryId{get;set;}  // to store Country Id
    public list<SelectOption> zoneOptions{get;set;}  //selectOptins for list of  Zone Records  
    public list<SelectOption> subZoneOptions{get;set;} //selectOptins for list of sub Zone Records   
    public list<SelectOption> coutryOptions{get;set;} // selectOptins for list of Country Records
    public transient list<BSureS_Basic_Info__c> lstSupsPagination{get;set;}
    public Integer pageNumber;//used for pagination
    public Integer pageSize;//used for pagination
    public Integer totalPageNumber;//used for pagination
    public Integer PrevPageNumber {get;set;}//used for pagination
    public Integer NxtPageNumber {get;set;}//used for pagination
    public Integer NlistSize {get;set;}//used for pagination
    public Integer Endlst {get;set;}//used for pagination
    public integer totallistsize{get;set;}//total size of list SPFolderset and SPWFilesList for pagination
    
    transient public List<List<BSureS_Basic_Info__c >> SupplierCollection  {get; private set;}
    private string  sortDirection = 'ASC'; 
    private string  sortExp = 'Supplier_Name__c';
    public String sortExpression{get{return sortExp;}set{ 
       if (value == sortExp)
         sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
       else
         sortDirection = 'ASC';
         sortExp = value;
       }
    }

    public String getSortDirection(){
    //if no column is selected 
        if (sortExpression == null || sortExpression == '')
          return 'ASC';
        else
         return sortDirection;
    }

    public void setSortDirection(String value){  
        sortDirection = value;
    }  
    
    public void Sort() {  
        getSuppliers();
    }
    public Pagereference ExportToExcel()
    {
    	fetchDetailsforExport();
       	
        Pagereference pageref = new pagereference('/apex/BSureS_SuppliersReportExport');
        return pageref; 
    }
    public pageReference  fetchDetailsforExport()
    {
    	string strQuery =   'SELECt Id,Name,Supplier_Name__c,Street_Name_Address_Line_1__c,Address_Line_2__c,State_Province__r.Name, '+
                            'Contact_Name__c,Email_Address__c,City__c,Sub_Zone__c,Zone__c,BSureS_Country__c,Phone_Number__c, '+
                            'Sub_Zone__r.Name,Zone__r.Name,BSureS_Country__r.Name, '+
                            'Postal_Code__c,Fax__c,Website_Link__c,Globe_ID__c '+             
                            'from BSureS_Basic_Info__c where Supplier_Name__c != null ';  
                
        if(strZoneId != null && strZoneId != 'ALL')
            strQuery +=' AND Zone__c =: strZoneId ';
        if(strSubZoneId != null &&  strSubZoneId != 'ALL' )
            strQuery +=' AND Sub_Zone__c =: strSubZoneId ';
        if(strCountryId != null && strCountryId != 'ALL'  )
            strQuery +=' AND BSureS_Country__c =: strCountryId ';  
                      
        //string sortFullExp = sortExpression  + ' ' + sortDirection;        
        //strQuery += ' order by ' + sortFullExp;
            
       	//strQuery +=' limit 1000';
            
       	lstSuppliers = database.query(strQuery); 
       	SupplierCollection = collectionOfCollections(lstSuppliers);
       	
    	return null;
    }
    public List<List<BSureS_Basic_Info__c >> collectionOfCollections(List<BSureS_Basic_Info__c > coll)
    {
    	List<List<BSureS_Basic_Info__c >> mainList = new List<List<BSureS_Basic_Info__c >>();
        List<BSureS_Basic_Info__c > innerList = null;
        Integer idx = 0;
        if(coll != null)
        {
        	for(BSureS_Basic_Info__c SObj : coll)
        	{
        		if (Math.mod(idx++, 1000) == 0 ) 
                {
                    innerList = new List<BSureS_Basic_Info__c >();
                    mainList.add(innerList);
                }
                innerList.add(SObj);
        	}
        }
        return mainList;
    }
    
    public pagereference Cancel(){
        pagereference pageRef = new pagereference('/apex/BSureS_Reports');
        pageRef.setRedirect(true);
        return pageRef;
    }
    
     // Constructor  
    public BSureS_SuppliersReport ()
    {   
        totallistsize=0;
        pageNumber = 0;
        totalPageNumber = 0;
        
        lstSuppliers = new list<BSureS_Basic_Info__c>();
        zoneOptions = new list<SelectOption>();
        subZoneOptions = new list<SelectOption>();
        coutryOptions = new list<SelectOption>();   
        List<BSureS_User_Additional_Attributes__c> UAA= BSureS_CommonUtil.DefaultUserAttributes(UserInfo.getUserId());
        if(UAA!= null && UAA.Size() > 0){
            for(BSureS_User_Additional_Attributes__c UA:UAA){
                strZoneId   = UA.BSureS_Zone__c; 
                strSubZoneId = UA.BSureS_Sub_Zone__c;
                strCountryId = UA.BSureS_Country__c;
            }   
        }    
        getZones();
        getSubZones();
        getCountries();
        getSuppliers();
    }
     // <Summary> 
    //Method Name : getSuppliers
    // </Summary>
    //Description : query all the Suppliers with specific filters
    public void getSuppliers(){     
          
        string strQuery =   'SELECt Id,Name,Supplier_Name__c,Street_Name_Address_Line_1__c,Address_Line_2__c,State_Province__r.Name, '+
                            'Contact_Name__c,Email_Address__c,City__c,Sub_Zone__c,Zone__c,BSureS_Country__c,Phone_Number__c, '+
                            'Sub_Zone__r.Name,Zone__r.Name,BSureS_Country__r.Name, '+
                            'Postal_Code__c,Fax__c,Website_Link__c,Globe_ID__c '+             
                            'from BSureS_Basic_Info__c where Supplier_Name__c != null ';  
                
        if(strZoneId != null && strZoneId != 'ALL')
            strQuery +=' AND Zone__c =: strZoneId ';
        if(strSubZoneId != null &&  strSubZoneId != 'ALL' )
            strQuery +=' AND Sub_Zone__c =: strSubZoneId ';
        if(strCountryId != null && strCountryId != 'ALL'  )
            strQuery +=' AND BSureS_Country__c =: strCountryId ';  
                      
        string sortFullExp = sortExpression  + ' ' + sortDirection;        
        strQuery += ' order by ' + sortFullExp;
            
       	//strQuery +=' limit 1000';
            
       	lstSuppliers = database.query(strQuery);    
        
        totallistsize= lstSuppliers.size();// this size for pagination
        pageSize = 25;// default page size will be 10
       
       
        if(totallistsize==0)
        {
            BindData(0);// this method is for pagination, if no records found then passing zero to the pagination method
            NxtPageNumber= lstSupsPagination.size();
            PrevPageNumber=0;
        }
        else
        {
            BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
            NxtPageNumber= lstSupsPagination.size();
            PrevPageNumber=1;
        }                      
        
    }
      // <Summary> 
    //    Method Name : getZones
   // </Summary>
   //   Description : query all the zones and Preparing SelectOptions for Zone  
    public void getZones()
    {   
        BSureS_CommonUtil.isReport = true;
        zoneOptions = new list<SelectOption>();
        zoneOptions = BSureS_CommonUtil.getZones();
    }
     // <Summary> 
   // Method Name : getSubZones
   // </Summary>
   // Description : query all the sub zones based on Zone Id and Preparing SelectOptions for sub Zones
    public void getSubZones()
    {       
        getCountries();
        if(strZoneId == '' || strZoneId == 'All' ){
            strSubZoneId ='ALL';
            strCountryId = 'ALL';
        }       
        BSureS_CommonUtil.isReport = true;
        subZoneOptions = new list<SelectOption>();
        coutryOptions = new list<selectOption>();
        coutryOptions.add(new selectOption('ALL','ALL'));
        subZoneOptions.add(new selectOption('ALL','ALL'));
        subZoneOptions  = BSureS_CommonUtil.getSubZones(strZoneId);       
    }
     // <Summary> 
    //Method Name : getCountries
    // </Summary>
    //Description : query all the Countries based on sub Zone Id and Preparing SelectOptions for Countries
    public void getCountries()
    {               
        //if((strZoneId == 'ALL' && strSubZoneId == 'ALL') || strSubZoneId != 'ALL')
        if(strZoneId == 'ALL' && strSubZoneId == 'ALL')
            strCountryId = 'ALL';
        BSureS_CommonUtil.isReport = true;
        coutryOptions = new list<SelectOption>();       
        coutryOptions = BSureS_CommonUtil.getCountries(strSubZoneId);
        
    }
    
    public void BindData(Integer newPageIndex)
    {
       
        try
        {
            lstSupsPagination = new list<BSureS_Basic_Info__c>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            
            if(pageSize == null)pageSize = 25;
            if (newPageIndex > pageNumber)
            {
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;
            }
            else
            {
                max = newPageIndex * pageSize;
                min = max - pageSize;
            }
            string strQuery =   'SELECt Id,Name,Supplier_Name__c,Street_Name_Address_Line_1__c,Address_Line_2__c,State_Province__r.Name, '+
                            'Contact_Name__c,Email_Address__c,City__c,Sub_Zone__c,Zone__c,BSureS_Country__c,Phone_Number__c, '+
                            'Sub_Zone__r.Name,Zone__r.Name,BSureS_Country__r.Name, '+
                            'Postal_Code__c,Fax__c,Website_Link__c,Globe_ID__c '+             
                            'from BSureS_Basic_Info__c where Supplier_Name__c != null ';  
                
	        if(strZoneId != null && strZoneId != 'ALL')
	            strQuery +=' AND Zone__c =: strZoneId ';
	        if(strSubZoneId != null &&  strSubZoneId != 'ALL' )
	            strQuery +=' AND Sub_Zone__c =: strSubZoneId ';
	        if(strCountryId != null && strCountryId != 'ALL'  )
	            strQuery +=' AND BSureS_Country__c =: strCountryId ';  
	                      
	        string sortFullExp = sortExpression  + ' ' + sortDirection;        
	        strQuery += ' order by ' + sortFullExp;
	            
	       	//strQuery +=' limit 1000';
	            
	       	lstSuppliers = database.query(strQuery);
	       	totallistsize= lstSuppliers.size();// this size for pagination
        	pageSize = 25;// default page size will be 10
            
            if(lstSuppliers != NULL)
            {
                for(BSureS_Basic_Info__c b : lstSuppliers)
                {
                    counter++;
                    if (counter > min && counter <= max) 
                    {
                        lstSupsPagination.add(b);// here adding files list
                    }
                }
            }
            pageNumber = newPageIndex;
            NlistSize = lstSupsPagination.size();
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }
     // <summary>
    // Below method fires when user clicks on next button of pagination.
    // </summary>    
    // <returns>pagereference</returns>
    public PageReference nextBtnClick() 
    {
        
        //system.debug('nextbutton'+pageNumber);
        BindData(pageNumber + 1);
        pageData(pageNumber + 1);
        
        return null;
    }

    // <summary>
    // Below method fires when user clicks on previous button of pagination.
    // </summary>
    // <returns>pagereference</returns>
    public PageReference previousBtnClick() 
    {
        pageData(pageNumber - 1);
        BindData(pageNumber - 1);
       
        return null;
    }
    
    /// <summary>
    /// Below method fires when user clicks on next button of pagination.
    /// <param name="PageNumber"></param>
    /// <returns>Integer</returns>
    /// </summary>
    public Integer getPageNumber()
    {
        return pageNumber; 
    }
    
    
    /// <summary>
    /// Below method is for getting pagesize how many records per page .
    /// <returns>Integer</returns>  
    /// </summary> 
    public Integer getPageSize()
    {
        return pageSize;
    }


    /// <summary>
    /// Below method is for enabling and disabling the previous button of pagination.
    /// <param name="PreviousButtonEnabled"></param>
    /// <returns>boolean</returns>      
    /// </summary>
    public Boolean getPreviousButtonEnabled()
    {
        return !(pageNumber > 1);
    }


    /// <summary>
    /// Below method is for enabling and disabling the nextbutton of pagination.
    /// <param name="NextButtonDisabled"></param>
    /// <returns>boolean</returns>      
    /// </summary>
    public Boolean getNextButtonDisabled()
    {
        if (totallistsize ==0) 
            return true;
        else
            return ((pageNumber * pageSize) >= totallistsize);
    }


    /// <summary>
    /// Below method gets the total no.of pages.
   
    /// <returns>Integer</returns>      
    /// </summary>
    public Integer getTotalPageNumber()
    {
        totalPageNumber = 0;
        if (totalPageNumber == 0 && totallistsize!=0)
        {
            //system.debug(totalPageNumber+'-totallistsize--'+totallistsize);
            totalPageNumber = totallistsize / pageSize;
            Integer mod = totallistsize - (totalPageNumber * pageSize);
            if (mod > 0)
                totalPageNumber++;
        }
        return totalPageNumber;
    }
    
    /// <summary>
    /// Below method is the main method of pagination if have atleast one record then we pass 1 to this method otherwise zero.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
   
    
    /// <summary>
    /// Below method bnds the values for Next and previous buttons.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void pageData(Integer newPageIndex)
    {   
        if (newPageIndex > pageNumber)
        {
            PrevPageNumber=PrevPageNumber+pagesize;   
            NxtPageNumber=NxtPageNumber+NlistSize; 
        }
        if (newPageIndex < pageNumber)               
        {                                 
            PrevPageNumber=PrevPageNumber-pagesize;               
            NxtPageNumber=NxtPageNumber-NlistSize;      
             
        }
        //system.debug('NlistSize============='+NlistSize);
    }
    
    /// <summary>
    /// Below method binds the data for last button.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void LastpageData(Integer newPageIndex)
    {
        try
        {
            lstSupsPagination=new list<BSureS_Basic_Info__c>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;          
            //system.debug('NlistSizeLastpage1============='+NlistSize);
            min = pageNumber * pageSize;
            max = newPageIndex * pageSize;    
            for(BSureS_Basic_Info__c a : lstSuppliers)
            {
                counter++;
                
                if (counter > min && counter <= max) 
                {
                       lstSupsPagination.add(a);// here adding the folders list      
                }
            }
            pageNumber = newPageIndex;
            NlistSize=lstSupsPagination.size();
                
            PrevPageNumber=totallistsize - NlistSize +1;   
            NxtPageNumber=totallistsize;
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }

    /// <summary>
    /// Below method bnds the value for last button.
    /// </summary>
    public PageReference LastbtnClick() 
    {
        BindData(totalpagenumber - 1);
        //system.debug('Lastbutton++'+pagenumber);
        LastpageData(totalpagenumber);
        return null;
    }
    
    /// <summary>
    /// Below method bnds the value for first button.
    /// </summary>
    public PageReference FirstbtnClick() 
    {
        BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
        NxtPageNumber=NlistSize;
        PrevPageNumber=1;
        return null;
    } 
    
}