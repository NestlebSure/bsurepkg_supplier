/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class TestBSureS_worklist {

    static testMethod void myUnitTest() {
        
         BSure_Configuration_Settings__c objCSettingsB = new BSure_Configuration_Settings__c();
        objCSettingsB.Name = 'BSureS_Report_Page_Size99';
        objCSettingsB.Parameter_Key__c = 'BSureS_Report_Page_Size';
        objCSettingsB.Parameter_Value__c = '10';
        insert objCSettingsB;
        
        // TO DO: implement unit test
         BSureS_worklist objReview=new BSureS_worklist();
        
        objReview.strSubordinatenames='';
        list<BSureS_Basic_Info__c> lstCustomer=new list<BSureS_Basic_Info__c>();
        BSureS_Basic_Info__c ObjCustomer=new BSureS_Basic_Info__c();
        ObjCustomer.Next_Review_Date__c=Date.parse('01/01/2013');
        //ObjCustomer.IsActive__c=true;
        ObjCustomer.Review_Status__c='Scheduled';
        ObjCustomer.Manager__c=userInfo.getUserId();
         ObjCustomer.Analyst__c=userInfo.getUserId();
        insert ObjCustomer;
        
       
        
        lstCustomer.add(ObjCustomer);        
        System.assertNotEquals('Scheduled12', ObjCustomer.Review_Status__c);
        BSureS_Spend_History_Publish__c objPublish=new BSureS_Spend_History_Publish__c();
        objPublish.Supplier_ID__c=ObjCustomer.Id;
        objReview.displayrecords();
        BSureS_worklist.CommonUserClass commonObj=new BSureS_worklist.CommonUserClass();
        
       
        integer counter=10;
        integer list_size=10;
        objReview.getTotalPageNumber();
        objReview.getNextButtonDisabled();
        objReview.nextBtnClick();
        objReview.previousBtnClick();
        objReview.LastbtnClick();
        objReview.FirstbtnClick();
        objReview.pagenation();
        objReview.getSupplier();
        objReview.getPreviousButtonEnabled();
        objReview.getPageSize(); 
        objReview.getPageNumber();
        
    }
    }