/************************************************************************************************       
Controller Name         : BSureS_Task_AutoUpdate       
Date                    : 06/24/2013        
Author                  : kishorekumar A       
Purpose                 : Scheduler       
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          06/24/2013     Kishorekumar A           Initial Version
**************************************************************************************************/
global with sharing class BSureS_Task_AutoUpdate implements schedulable
{
	public list<Task> lstTask{get;set;}
	public list<BSureS_Credit_Analysis__c> lstCreditAnalysis{get;set;}
	public list<string> lstString{get;set;}
	public list<string> lstStringCAIds{get;set;}
	
	
	global BSureS_Task_AutoUpdate()
	{
		
	}
	public void execute(schedulablecontext sc)
	{
		lstString = new list<string>();
		lstStringCAIds = new list<string>();
		lstTask = new list<Task>([select id,OwnerId,whatId from Task where Status != 'Completed' and OwnerId != null]);
		if(!lstTask.isEmpty())
		{
			for(Task tObj : lstTask)
			{
				if(tObj.whatId != null)
				{ 
					lstString.add(tObj.whatId);
				}	
			}
		}
		if(!lstString.isEmpty())
		{ 
			lstCreditAnalysis = new list<BSureS_Credit_Analysis__c>([select id from BSureS_Credit_Analysis__c
								where Id IN : lstString and Review_Status__c = 'Completed']);
		}
		if(!lstCreditAnalysis.isEmpty())
		{
			for(BSureS_Credit_Analysis__c caObj : lstCreditAnalysis)
			{
				lstStringCAIds.add(caObj.Id);
			}
		}
		if(!lstStringCAIds.isEmpty())
		{
			delete[select id,OwnerId,whatId from Task where whatId IN : lstStringCAIds];
		}
	}
}