global  with sharing class BSureS_Review_Reminder_Notification implements schedulable
{
    public list<BSureS_Basic_Info__c> lstSupplierInfo{get;set;}
    public list<BSureS_Email_Queue__c> lstEmailQueue{get;set;}
    public string strplannedReviewDate{get;set;}
    public string strSupplierForum{get;set;}
    global BSureS_Review_Reminder_Notification()
    {
        
    }
    public void execute(schedulablecontext sc)
    {
        DateTime dateObj;
        strSupplierForum = BSureS_CommonUtil.getConfigurationValues('BSureS_SupplierForum').get(0);
        lstEmailQueue = new list<BSureS_Email_Queue__c>();
        lstSupplierInfo = new list<BSureS_Basic_Info__c>([select Id,Supplier_Name__c,Globe_ID__c,Analyst__c,Analyst__r.Email,Manager__c,Manager__r.Email,
                            Analyst__r.Name,Manager__r.Name,Planned_Review_Date__c from BSureS_Basic_Info__c where Planned_Review_Date__c != null and 
                            (Planned_Review_Date__c =: system.today().addDays(30) OR Planned_Review_Date__c =: system.today().addDays(15)
                              OR  Planned_Review_Date__c =: system.today().addDays(7) OR Planned_Review_Date__c =: system.today().addDays(1) )
                            ]);
        //system.debug('lstSupplierInfo====size====='+lstSupplierInfo.size());                  
        if(lstSupplierInfo != null && lstSupplierInfo.size() > 0)
        {
            for(BSureS_Basic_Info__c sObj : lstSupplierInfo)
            {
                dateObj = sObj.Planned_Review_Date__c;
                strplannedReviewDate = dateObj.format('MM/dd/yyyy');
                
                String strSubject = 'Upcoming Review Notification';
                String strEmailBody =''; 
                
                strEmailBody += '<table>';
                strEmailBody += 'Review Reminder Notification : ';
                strEmailBody += '<tr></tr>';
                strEmailBody += '</table>';
                
                strEmailBody += '<table cellspacing="2" cellpadding="0" border="1" bordercolor="#9AFEFF" >';
                strEmailBody += '<tr><td height="40px" colspan="2" width="100%"><font name="Verdana" ><center> <b>'+ strSupplierForum+'  </b> </center></font></td></tr>';
                strEmailBody += '<tr><td style="color:red;" colspan="2" > You have a Review coming up:  </td></tr>';
                strEmailBody += '<tr><td height="32px" width="22%" >Supplier : </td><td height="21px" > '+sObj.Supplier_Name__c+'</td></tr>';
                strEmailBody += '<tr><td height="32px" width="22%" >Globe ID : </td><td height="21px" > '+sObj.Globe_ID__c+'</td></tr>';
                strEmailBody += '<tr><td height="32px" width="22%" >Expected Review Date : </td><td height="21px" > '+ strplannedReviewDate +'</td></tr>';
                strEmailBody += '</table>';
            
                strEmailBody += '<table>';
                strEmailBody += '<tr><td> <b>To Create Review , please click </b><a href=\''+URL.getSalesforceBaseUrl().toExternalForm()+'/apex/BSureS_viewSupplierDetails?id=' + sObj.Id+ '\' target=\'_blank\' > here </a></td></tr>';
                strEmailBody += '</table>';
                if(sObj.Analyst__c != null)
                {
                    //system.debug('sObj.Analyst__r.Name========='+sObj.Analyst__r.Name);
                    BSureS_Email_Queue__c objEmailQueue = new BSureS_Email_Queue__c();
                    objEmailQueue.Email_Body__c = strEmailBody;
                    objEmailQueue.Email_Priority__c = 'High';
                    objEmailQueue.Email_Status__c = 'NEW';
                    objEmailQueue.Email_Subject__c = strSubject;
                    objEmailQueue.Is_Daily_Digest__c = 'NO'; 
                    objEmailQueue.Recipient_Address__c = sObj.Analyst__r.Email;
                    objEmailQueue.Send_Immediate__c = true;
                    objEmailQueue.Send_On_Date__c = system.today();
                    objEmailQueue.Recipient_Name__c = sObj.Analyst__r.Name;
                    lstEmailQueue.add(objEmailQueue);
                }
                if(sObj.Manager__c != null)
                {
                    //system.debug('sObj.Manager__r.Name========='+sObj.Manager__r.Name);
                    BSureS_Email_Queue__c objEmailQueueMang = new BSureS_Email_Queue__c();
                    objEmailQueueMang.Email_Body__c = strEmailBody;
                    objEmailQueueMang.Email_Priority__c = 'High';
                    objEmailQueueMang.Email_Status__c = 'NEW';
                    objEmailQueueMang.Email_Subject__c = strSubject;
                    objEmailQueueMang.Is_Daily_Digest__c = 'NO'; 
                    objEmailQueueMang.Recipient_Address__c = sObj.Manager__r.Email;
                    objEmailQueueMang.Send_Immediate__c = true;
                    objEmailQueueMang.Send_On_Date__c = system.today();
                    objEmailQueueMang.Recipient_Name__c = sObj.Manager__r.Name;
                    lstEmailQueue.add(objEmailQueueMang);
                }    
            }
            if(lstEmailQueue != null && lstEmailQueue.size() > 0)
            {
                Database.Saveresult[] objSaveResult = Database.insert(lstEmailQueue);
            }
        }                   
    }
}