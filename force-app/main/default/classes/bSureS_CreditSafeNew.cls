global class bSureS_CreditSafeNew{
public  static string CSafeRespString;
    public  static CreditSafeBean InsertBean;
    public static string Messagecodes;
    public static Map<String, BsureS_CreditSafeCodes__c> mcs;
    public bSureS_CreditSafeNew(){
        InsertBean  = new CreditSafeBean();
        mcs = new Map<String, BsureS_CreditSafeCodes__c>();
    }
    
    webservice static  String FetchCompanyOnlineReport(String CBN,string MasterId,string globeId,string Cname){
        if(Cname=='United States')
        Cname='US';
        else if(Cname=='Belgium')
        Cname='BE';
        else if(Cname=='Luxembourg')
        Cname='LU';
       
        string strTimeOut = ''; 
        Messagecodes ='';
        system.debug('CN===='+CBN);
        string strExperianEndpoint ='';
        string strSoapAction ='';
        string XMLMapResult1='';
        bSureS_CreditSafeNew obj = new bSureS_CreditSafeNew();
        mcs = BsureS_CreditSafeCodes__c.getAll();
        system.debug('mcs=====>'+mcs); // by srinivas
        if(mcs!=null){
            if(mcs.get('EndPointURL')!=null){
                BsureS_CreditSafeCodes__c echVar = mcs.get('EndPointURL');
                
                strExperianEndpoint = echVar.Message__c ; 
                system.debug('strExperianEndpoint=====>'+strExperianEndpoint);
            }
            if(mcs.get('SOAPAction')!=null){
                BsureS_CreditSafeCodes__c echVar = mcs.get('SOAPAction');
                strSoapAction = echVar.Message__c   ;
            }
        }
        system.debug('**'+strExperianEndpoint +strSoapAction );
        string strUsername ='';
        string strPassword ='';
        
        list<bSureS_ExpCred__c> lstCreSafe =[Select id,Name,Pwd__c,UN__c 
                                                From bSureS_ExpCred__c  
                                                where Credit_Agency__c='Creditsafe' limit 1];
        if(lstCreSafe!=null){
            strUsername =lstCreSafe[0].UN__c;
            strPassword =lstCreSafe[0].Pwd__c;
            system.debug('****strUsername******'+strUsername);
            system.debug('****strPassword******'+strPassword);
        } 
        string strXML ='<?xml version="1.0" encoding="utf-8"?>'+
                '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:oper="http://www.creditsafe.com/globaldata/operations"  xmlns:dat="http://www.creditsafe.com/globaldata/datatypes" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays">'+
                    '<soapenv:Header/>'+
                    '<soapenv:Body>'+
                      '<oper:RetrieveCompanyOnlineReport>'+
                         '<oper:country>'+Cname+'</oper:country>'+
                         '<oper:searchCriteria>'+
                         '<dat:SafeNumber>'+CBN+'</dat:SafeNumber>'+
                         '</oper:searchCriteria>'+
                         '<oper:reportType>Full</oper:reportType>'+
                         '<oper:language>EN</oper:language>'+
                         '<oper:chargeReference xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>'+
                      '</oper:RetrieveCompanyOnlineReport>'+
                '</soapenv:Body>'+
            '</soapenv:Envelope>';                            
        HttpRequest req = new HttpRequest();  
        req.setEndpoint(strExperianEndpoint);
        req.setHeader('content-type','text/xml; charset=UTF-8');
        //req.setHeader('SOAPAction',strSoapAction);
        req.setHeader('SOAPAction','http://www.creditsafe.com/globaldata/operations/CompanyDataAccessService/RetrieveCompanyOnlineReport');
        req.setMethod('POST');
        Blob headerValue = Blob.valueOf(strUsername+':'+strPassword);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        system.debug('AuthorizationHeader==='+authorizationHeader);
        req.setHeader('Authorization', authorizationHeader);
        req.setBody(strXML); 
        system.debug('Request-->'+req);
        Http http = new Http();
        HttpResponse res = new HttpResponse();
        XmlStreamReader reader;
        if(!Test.isRunningTest()){
            try{
            req.setTimeout(120000); // timeout in milliseconds - this is one minute
                system.debug('strExperianEndpoint=='+strExperianEndpoint);
                system.debug('req.strExperianEndpoint=='+req.getEndpoint());
            res = http.send(req);
            }catch(Exception ex){
            system.debug('Exception occured::: '+ex.getMessage()+ex.getStackTraceString());
            strTimeOut = Label.BSureS_ExpTimeout+' '+ex.getMessage();
            }
            system.debug(res.getStatusCode()+'res===='+res.getBody());
        }
        else{
          res.setHeader('content-type','text/xml; charset=UTF-8');
          res.setBody ('<Envelope>'+
                        '<Body>'+
                            '<RetrieveCompanyOnlineReportResponse>'+
                                '<RetrieveCompanyOnlineReportResult>'+
                                    '<Messages >'+
                                        '<Message Type="Information" Code="20103">Service access </Message>'+
                                        '<Message Type="Information" Code="20103">OnlineReports </Message>'+
                                    '</Messages>'+
                                    '<Reports>'+
                                        '<Report  CompanyId="US023/X/US81933214" OrderNumber="2315143" Language="EN" ReportCurrency="USD" >'+
                                            '<CreditRating>'+
                                                '<CommonValue>D</CommonValue>'+
                                                '<CommonDescription>High Risk</CommonDescription>'+
                                                '<CreditLimit>4000</CreditLimit>'+
                                                '<ProviderValue>21</ProviderValue>'+
                                                '<ProviderDescription>High Risk</ProviderDescription>'+
                                            '</CreditRating>'+
                                            '<PreviousCreditRating>'+
                                                '<CommonValue>C</CommonValue>'+
                                                '<CommonDescription>Moderate Risk</CommonDescription>'+
                                                '<CreditLimit>13700</CreditLimit>'+
                                                '<ProviderValue>36</ProviderValue>'+
                                                '<ProviderDescription>Moderate Risk</ProviderDescription>'+
                                            '</PreviousCreditRating>'+  
                                            '<DBT>'+
                                                '<CompanyDBT>0</CompanyDBT>'+
                                                '<IndustryDBT>5</IndustryDBT>'+
                                            '</DBT>'+
                                            '<EmployeesInformation>'+
                                                '<EmployeeInformation>'+
                                                    '<NumberOfEmployees>375</NumberOfEmployees>'+
                                                '</EmployeeInformation>'+
                                            '</EmployeesInformation>'+
                                            '<LegalFilingSummary>'+
                                                '<Bankruptcy>false</Bankruptcy>'+
                                                '<TaxLienFilings>1</TaxLienFilings>'+
                                                '<JudgmentFilings>0</JudgmentFilings>'+
                                                '<BankruptyFilings>0</BankruptyFilings>'+
                                                '<Sum>161</Sum>'+
                                                '<UCCFilings>19</UCCFilings>'+
                                                '<CautionaryUCCFilings>1</CautionaryUCCFilings>'+
                                            '</LegalFilingSummary>'+
                                        '</Report>'+
                                    '</Reports>'+   
                                '</RetrieveCompanyOnlineReportResult>'+
                            '</RetrieveCompanyOnlineReportResponse>'+
                        '</Body>'+
                    '</Envelope>');
          res.setStatusCode(200);
        }
        if(res!=null && res.getStatusCode() == 200){
            reader = res.getXmlStreamReader();
            CSafeRespString = res.getBody();
            XMLMapResult1 = obj.parseXML(reader);
        }
        else if(res!=null && res.getStatusCode() == 401){
        
            return 'Unauthorized';
        }
        else{
            return strTimeOut;
        }
        if(XMLMapResult1 =='Success'){
            string fileName = system.now().format('MMddyy')+'_'+'Creditsafe_Rpt'+ '_'+globeId;
            string CreditId = obj.insertONGS(MasterId,filename, InsertBean);
            string strPrvusId = obj.LastAttachementId(MasterId);
            system.debug('***strPrvusId*****'+strPrvusId);
            string status =obj.UpdateMaster(MasterId,InsertBean,strPrvusId,CreditId);
            Database.saveResult SaveResult ;
            if(CreditId !=null && CreditId !=''){
                Blob blobContent = Blob.valueof(res.getBody() );
                Attachment myAttachment  = new Attachment();
                myAttachment.Body = blobContent;
                myAttachment.Name = fileName;
                myAttachment.parentId = CreditId ;
                SaveResult = Database.insert(myAttachment);
                list<BSureS_Credit_Review_Section__c> UpAttchLst = new list<BSureS_Credit_Review_Section__c>();
                for(BSureS_Credit_Review_Section__c updaAttId : [Select id,AttachmentId__c 
                                                                    from BSureS_Credit_Review_Section__c 
                                                                    where id=:CreditId]){
                                            
                    updaAttId.AttachmentId__c=myAttachment.id;
                    UpAttchLst.add(updaAttId);                      
                }
                if(UpAttchLst!=null){
                    database.update(UpAttchLst);
                }
                 list<BSureS_Basic_Info__c> UpBasicInfo = new list<BSureS_Basic_Info__c>();
                 for(BSureS_Basic_Info__c echval : [Select id,Creditsafe_Attachment_Id__c 
                                                    from BSureS_Basic_Info__c 
                                                    where id=:MasterId]){
                 
                     echval.Creditsafe_Attachment_Id__c=myAttachment.id;
                     UpBasicInfo.add(echval);
                 }
                 if(UpBasicInfo!=null){
                    database.update(UpBasicInfo);
                }
                if(XMLMapResult1 != '' && XMLMapResult1 != null){
                    XMLMapResult1 += '::'+CreditId+'::'+myAttachment.id +'::'+Messagecodes;
                }
            }
            
        }
        return XMLMapResult1;
    }
     public string LastAttachementId (String ParentId){
        string attchId='';
        integer cnt=0;
        for(BSureS_Credit_Review_Section__c lstVal:[Select id from BSureS_Credit_Review_Section__c
                                                         where Supplier_ID__c =: ParentId and TitleDescription__c LIKE 'Creditsafe%' 
                                                        order by createddate DESC limit 2 ]){
            if(cnt==1){
                Attachment att = [select id from Attachment where parentid=:lstVal.id];
                if(att!=null){
                    attchId =att.id;
                }
            }                       
            cnt++;                       
         }
        return attchId; 
    }
     /* Reading the Xml Response*/
    public string parseXML(XmlStreamReader reader){
        try{
            boolean isSafeToGetNextXmlElement =true;
            string xmlstr;
            integer cntJudgement =0;
            while(isSafeToGetNextXmlElement){
                if (reader.getEventType() == XmlTag.START_ELEMENT){
                    if('Messages' == reader.getLocalName()){
                        string str ='';
                        boolean ActFlag =  true;
                        while(ActFlag){
                            reader.nextTag();
                            if('Message' == reader.getLocalName() && reader.getEventType() == XmlTag.START_ELEMENT ){
                                str =reader.getAttributeValue(null, 'Code');
                                xmlstr = getValueFromTag(reader);
                                if(Str!=null ){
                                    if(mcs.get(str)!=null){
                                        Messagecodes +=mcs.get(str) +',';
                                    }
                                    else{
                                        Messagecodes +=xmlstr +',';
                                    }
                                }
                            }
                            system.debug('CreditRating== '+reader.getLocalName());
                            system.debug('Messagecodes== '+Messagecodes);
                            
                            if('Messages' == reader.getLocalName() &&  reader.getEventType() == XmlTag.END_ELEMENT){
                                ActFlag =  false;
                                reader.nextTag();
                                system.debug('CreditRating== '+reader.getLocalName()+ Messagecodes);
                            }
                        }
                        if(Messagecodes!=null && Messagecodes!=''){
                            Messagecodes =Messagecodes.substring(0,Messagecodes.length()-1);
                        }
                    }
                    if('CreditRating'== reader.getLocalName()){
                        reader.nextTag();
                        InsertBean.ReportedDate=system.today();
                        if('CommonValue'== reader.getLocalName()){
                            xmlstr = getValueFromTag(reader);
                            InsertBean.CreditsafeRating =xmlstr;
                            reader.nextTag();
                        }
                        if('CommonDescription'==reader.getLocalName()){
                            xmlstr = getValueFromTag(reader);
                            InsertBean.CreditsafeCommonDescription = xmlstr;
                            reader.nextTag();
                        }
                        if('CreditLimit'==reader.getLocalName()){
                            xmlstr = getValueFromTag(reader);
                            reader.nextTag();
                        }
                        if('ProviderValue'== reader.getLocalName()){
                            xmlstr = getValueFromTag(reader);
                            InsertBean.CreditsafeScore =xmlstr;
                            reader.nextTag();
                        }
                        if('ProviderDescription'== reader.getLocalName()){
                            xmlstr = getValueFromTag(reader);
                            InsertBean.CreditSafeProviderDescription =xmlstr;
                            reader.nextTag();
                        }
                        reader.nextTag();
                    }
                    if('PreviousCreditRating'== reader.getLocalName()){
                        reader.nextTag();
                        system.debug('*************'+reader.getLocalName());
                         if('CommonValue'== reader.getLocalName()){
                            xmlstr = getValueFromTag(reader);
                            reader.nextTag();
                        }
                        if('CommonDescription'== reader.getLocalName()){
                            xmlstr = getValueFromTag(reader);
                            reader.nextTag();
                        }
                        if('CreditLimit' == reader.getLocalName()){
                            xmlstr = getValueFromTag(reader);
                            system.debug('*******val******'+xmlstr);
                            InsertBean.HighCreditLimit = xmlstr!=null  ? decimal.valueof(xmlstr):0.00;
                        }
                    }
                    if('CompanyDBT'== reader.getLocalName()){
                        xmlstr = getValueFromTag(reader);
                        InsertBean.CompanyDBT = xmlstr!=null && xmlstr.isNumeric() ? integer.valueof(xmlstr):0;
                        reader.nextTag();
                    }
                    
                    if('IndustryDBT'== reader.getLocalName()){
                        xmlstr = getValueFromTag(reader);
                        InsertBean.IndustryDBT = xmlstr!=null && xmlstr.isNumeric()? integer.valueof(xmlstr):0;
                        reader.nextTag();
                    }
                    if('NumberOfEmployees' == reader.getLocalName()){
                        xmlstr = getValueFromTag(reader);
                        InsertBean.Workforce = xmlstr!=null && xmlstr.isNumeric() ? integer.valueof(xmlstr):0;
                        reader.nextTag();
                    }
                    if('LegalFilingSummary' == reader.getLocalName()){
                        reader.nextTag();
                        if('Bankruptcy' == reader.getLocalName()){
                            xmlstr = getValueFromTag(reader);
                            InsertBean.IsBankruptcy =xmlstr;
                            reader.nextTag();
                        }
                        integer SumOfDerogatory =0;
                        if('TaxLienFilings' == reader.getLocalName()){
                            xmlstr = getValueFromTag(reader);
                            InsertBean.TaxLiens = xmlstr!=null && xmlstr.isNumeric() ? integer.valueof(xmlstr):0;
                            SumOfDerogatory += InsertBean.TaxLiens;
                            reader.nextTag();
                        }
                        if('JudgmentFilings' == reader.getLocalName()){
                            xmlstr = getValueFromTag(reader);
                            InsertBean.Judgementfilings = xmlstr!=null && xmlstr.isNumeric() ? integer.valueof(xmlstr):0;
                            SumOfDerogatory += InsertBean.Judgementfilings;
                            reader.nextTag();
                        }
                        if('BankruptyFilings' == reader.getLocalName()){
                            xmlstr = getValueFromTag(reader);
                            InsertBean.Bankruptcyfilings = xmlstr!=null && xmlstr.isNumeric() ? integer.valueof(xmlstr):0;
                            SumOfDerogatory += InsertBean.Bankruptcyfilings;
                            reader.nextTag();
                        }
                        
                        InsertBean.DerogatoryLegalFilings =SumOfDerogatory;
                        /*if('Sum' == reader.getLocalName()){
                            xmlstr = getValueFromTag(reader);
                            //InsertBean.Collections = xmlstr!=null  ? decimal.valueof(xmlstr):0;
                            InsertBean.Collections = xmlstr!=null  ? decimal.valueof(xmlstr):0.00; // by srinivas
                            reader.nextTag();
                        } */ //hide by srinivas
                    }
                }
                if (reader.hasNext()){
                    reader.next();
                } 
                else {
                    isSafeToGetNextXmlElement = false;
                    break; 
                }
            }
            //return 'Success';
        }
        catch(XmlException ex){
            system.debug('Exception============='+ex.getLineNumber()+'@@@@@@'+ex.getMessage()+ex.getStackTraceString());
            return 'Exception during Xml parsing error : '+ex.getMessage()+ex.getStackTraceString();
        }
        return 'Success';
    }
   
    /* Getting the Value from the Tag */
    public String getValueFromTag(XmlStreamReader reader){ 
        String returnvalue = '';
        reader.setCoalescing(true);
        while(reader.hasNext()){
            if (reader.getEventType() == XmlTag.END_ELEMENT){
                break;
            } else if (reader.getEventType() == XmlTag.CHARACTERS){
                returnvalue=reader.getText();
            }
        reader.next();
        }
        return returnvalue;
    }
    public string UpdateMaster(String ParentId,CreditSafeBean SafeBean,string perviousAttID,string creditId){
        string DatabaseStatus='Success';
        
        list<bSureS_CreditAgency_History__c> lstOfHistory = new list<bSureS_CreditAgency_History__c>();
        list<BSureS_Basic_Info__c> lstCustInfo = new list<BSureS_Basic_Info__c>();
        for(BSureS_Basic_Info__c echCust  :[ Select id,Creditsafe_Common_Description__c,
                                                    Creditsafe_Provider_Description__c,Creditsafe_Rating__c,
                                                    Creditsafe_Score__c,Report_Date_CS__c,Derogatory_Legal_Filings_CS__c,
                                                    Tax_Liens_CS__c,Workforce__c,Collections__c,Bankruptcy_Filings_CS__c,
                                                    Recent_High_Credit__c,Company_DBT_CS__c,Industry_DBT_CS__c
                                                from BSureS_Basic_Info__c  
                                                where id=:ParentId]){
            /*Update the response*/ 
            echCust.Creditsafe_Score__c =SafeBean.CreditsafeScore;
            echCust.Creditsafe_Common_Description__c =SafeBean.CreditsafeCommonDescription;
            echCust.Creditsafe_Rating__c =SafeBean.CreditsafeRating;
            echCust.Creditsafe_Provider_Description__c =SafeBean.CreditSafeProviderDescription;
            echCust.Report_Date_CS__c =SafeBean.reportedDate;
            echCust.Derogatory_Legal_Filings_CS__c =SafeBean.DerogatoryLegalFilings;
            echCust.Tax_Liens_CS__c =SafeBean.TaxLiens;
            echCust.Workforce__c =SafeBean.Workforce;
            echCust.Bankruptcy_Filings_CS__c =SafeBean.IsBankruptcy;
            echCust.Collections__c =SafeBean.Collections;
            echCust.Recent_High_Credit__c =SafeBean.HighCreditLimit;
            echCust.Company_DBT_CS__c =SafeBean.CompanyDBT;
            echCust.Industry_DBT_CS__c =SafeBean.IndustryDBT;
            
            lstCustInfo.add(echCust);
        }
        if(lstCustInfo!=null){
            database.update(lstCustInfo);
        }       
        return DatabaseStatus;
    }
    public string insertONGS(string strRecId,string strFName,CreditSafeBean SafeBean){
      Database.upsertResult Result;
      try{
        system.debug('SafeBean========'+SafeBean);
        list<bSureS_CreditAgency_History__c> lstOfHistory = new list<bSureS_CreditAgency_History__c>();
        list<BSureS_Credit_Review_Section__c> lstOGCS = [Select id,name,Supplier_ID__c,TitleDescription__c,Document_Type__c from BSureS_Credit_Review_Section__c 
                                                          where Supplier_ID__c =: strRecId and TitleDescription__c =: strFName];
          BSureS_Credit_Review_Section__c objOGCS = new BSureS_Credit_Review_Section__c();
          if(lstOGCS != null && lstOGCS.size()>0){
              objOGCS = lstOGCS[0];
              list<Attachment> attach = [select id,name from Attachment where ParentId=:objOGCS.id];
              delete attach;
              
          } 
                
          system.debug('strFName==='+strFName+'****'+strRecId);
          objOGCS.TitleDescription__c = strFName;
          objOGCS.Supplier_ID__c = strRecId;
          objOGCS.DiscussionType__c= 'File'; 
          String docType = BSureS_CommonUtil.getConfigurationValues('ExperianSupRecDocType').get(0);
          objOGCS.Document_Type__c = docType;
          //objOGCS.Document_Type__c = 'a0ud0000001Raha';
            
          Result = Database.upsert(objOGCS);
          bSureS_CreditAgency_History__c loadHisdata = new bSureS_CreditAgency_History__c();
            loadHisdata.Supplier_Basic_Info_Id__c   =strRecId;
            loadHisdata.Name ='Creditsafe';
            loadHisdata.Credit_Review_Section__c =objOGCS!=null ? objOGCS.id:'';
            loadHisdata.Creditsafe_Score__c = SafeBean.CreditsafeScore;
            loadHisdata.Creditsafe_Common_Description__c = SafeBean.CreditsafeCommonDescription;
            loadHisdata.Creditsafe_Rating__c = SafeBean.CreditsafeRating;
            loadHisdata.Creditsafe_Provider_Description__c = SafeBean.CreditSafeProviderDescription;
            loadHisdata.Report_Date__c = SafeBean.reportedDate;
            loadHisdata.Legal_Filings__c = SafeBean.DerogatoryLegalFilings!=null ? string.valueof(SafeBean.DerogatoryLegalFilings):'0';
            loadHisdata.Tax_Liens__c = SafeBean.TaxLiens;
            loadHisdata.Workforce__c = SafeBean.Workforce;
            loadHisdata.Collections__c = SafeBean.Collections;
            loadHisdata.Bankruptcy_filings__c = SafeBean.IsBankruptcy;
            loadHisdata.Recent_High_Credit__c = SafeBean.HighCreditLimit;  
            loadHisdata.Company_DBT__c = SafeBean.CompanyDBT!=null ? string.valueof(SafeBean.CompanyDBT):'0';
            loadHisdata.Industry_DBT__c = SafeBean.IndustryDBT!=null ?string.valueof(SafeBean.IndustryDBT):'0';   
            //loadHisdata.AttachmentId__c = perviousAttID ;    
            lstOfHistory.add(loadHisdata);
            system.debug('***lstOfHistory***'+lstOfHistory);
            if(lstOfHistory!=null){
                database.upsert(lstOfHistory);
            }  
          system.debug('objOGCS==='+ objOGCS);
          system.debug('Result ==='+ Result );
      }catch(Exception ex){
        system.debug('Exception occured while saving Experian report into OnGoingCreditSection.'+ex.getMessage()+ex.getStackTraceString());
      }
      if(Result.isSuccess() == true){
          return Result.Id;
        }else{
          return '';
        }
    } 
  
   public class CreditSafeBean{
        public string CreditsafeScore;
        public string CreditsafeCommonDescription;
        public string CreditsafeRating;
        public string CreditSafeProviderDescription;
        public date reportedDate;
        public integer CompanyDBT;
        public integer IndustryDBT;
        public decimal HighCreditLimit;
        public integer DerogatoryLegalFilings;
        public integer TaxLiens;
        public String IsBankruptcy;
        public integer Bankruptcyfilings;
        public integer Judgementfilings;
        public decimal Collections;
        public integer Workforce;
    }  
}