/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class TestBSureS_Create_SpendHistoryPublish {

    static testMethod void myUnitTest() {
        
        BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
        objCSettings1.Name = 'BSureS_Run_Triggers1';
        objCSettings1.Parameter_Key__c = 'BSureS_Run_Triggers';
        objCSettings1.Parameter_Value__c = 'True';
        insert objCSettings1;
        system.assertEquals('True', objCSettings1.Parameter_Value__c);
    
        // TO DO: implement unit test
        BSureS_Basic_Info__c Objsupplier=new BSureS_Basic_Info__c();
        Objsupplier.Supplier_Name__c='TestSupplier';
        Objsupplier.Globe_ID__c='1234';
        insert Objsupplier;
        BSureS_Spend_History_Publish__c publish=new BSureS_Spend_History_Publish__c();
        publish.Date__c=system.today();
        publish.Globe_ID__c='1234';
        publish.Spend_Amount__c=Decimal.valueOf(20000);
        publish.Spend_Period__c=system.today();
        publish.Supplier_ID__c=Objsupplier.id;
        insert publish;
        
        system.Assertequals('1234',publish.Globe_ID__c);
        ApexPages.StandardController con = new ApexPages.StandardController(new BSureS_Spend_History_Publish__c());
        ApexPages.currentPage().getParameters().put('Id',publish.id);
        ApexPages.currentPage().getParameters().put('recid',Objsupplier.id);        
        BSureS_Create_SpendHistoryPublish ObjSpendPublish=new BSureS_Create_SpendHistoryPublish(con);
        //BSureS_Create_SpendHistoryPublish ObjSpendPublish=new BSureS_Create_SpendHistoryPublish();
        ObjSpendPublish.Save();
        ObjSpendPublish.Cancel();
        ObjSpendPublish.redirectSupplier();
    }
    
        static testMethod void myUnitTest2() {
        // TO DO: implement unit test
        
        BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
        objCSettings1.Name = 'BSureS_Run_TriggersK';
        objCSettings1.Parameter_Key__c = 'BSureS_Run_Triggers';
        objCSettings1.Parameter_Value__c = 'True';
        insert objCSettings1;
        system.assertEquals('True', objCSettings1.Parameter_Value__c);
        
        BSureS_Basic_Info__c Objsupplier=new BSureS_Basic_Info__c();
        Objsupplier.Supplier_Name__c='TestSupplier';
        Objsupplier.Globe_ID__c='1234';
        insert Objsupplier;
        
        BSureS_Spend_History_Publish__c publish=new BSureS_Spend_History_Publish__c();
        publish.Date__c=system.today();
        publish.Globe_ID__c='1234';
        publish.Spend_Amount__c=Decimal.valueOf(20000);
        publish.Spend_Period__c=system.today();
        publish.Supplier_ID__c=Objsupplier.id;
        insert publish;
        system.Assertequals('1234',publish.Globe_ID__c);
        ApexPages.StandardController con = new ApexPages.StandardController(new BSureS_Spend_History_Publish__c());
        //ApexPages.currentPage().getParameters().put('Id',publish.id);
       // ApexPages.currentPage().getParameters().put('recid',Objsupplier.id);   
       ApexPages.currentPage().getParameters().put('recid',Objsupplier.id);      
        BSureS_Create_SpendHistoryPublish ObjSpendPublish=new BSureS_Create_SpendHistoryPublish(con);
        //BSureS_Create_SpendHistoryPublish ObjSpendPublish=new BSureS_Create_SpendHistoryPublish();
        ObjSpendPublish.Save();
        ObjSpendPublish.Cancel();
        ObjSpendPublish.redirectSupplier();
    }
    
    static testMethod void myUnitTest3() {
        // TO DO: implement unit test
        
        BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
        objCSettings1.Name = 'BSureS_Run_Triggers3';
        objCSettings1.Parameter_Key__c = 'BSureS_Run_Triggers';
        objCSettings1.Parameter_Value__c = 'True';
        insert objCSettings1;
        system.assertEquals('True', objCSettings1.Parameter_Value__c);
        
        BSureS_Basic_Info__c Objsupplier=new BSureS_Basic_Info__c();
        Objsupplier.Supplier_Name__c='TestSupplier';
        Objsupplier.Globe_ID__c='1234';
        insert Objsupplier;
        
        BSureS_Spend_History_Publish__c publish=new BSureS_Spend_History_Publish__c();
        publish.Date__c=system.today();
        publish.Globe_ID__c='1234';
        publish.Spend_Amount__c=Decimal.valueOf(20000);
        publish.Spend_Period__c=system.today();
        publish.Supplier_ID__c=Objsupplier.id;
        insert publish;
        system.Assertequals('1234',publish.Globe_ID__c);
        ApexPages.StandardController con = new ApexPages.StandardController(new BSureS_Spend_History_Publish__c());
        ApexPages.currentPage().getParameters().put('Id',publish.id);
       // ApexPages.currentPage().getParameters().put('recid',Objsupplier.id);   
       //ApexPages.currentPage().getParameters().put('recid',Objsupplier.id);      
        BSureS_Create_SpendHistoryPublish ObjSpendPublish=new BSureS_Create_SpendHistoryPublish(con);
        //BSureS_Create_SpendHistoryPublish ObjSpendPublish=new BSureS_Create_SpendHistoryPublish();
        ObjSpendPublish.Save();
        ObjSpendPublish.Cancel();
        ObjSpendPublish.redirectSupplier();
    }
    
    static testMethod void myUnitTest4() {
        // TO DO: implement unit test
        
        BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
        objCSettings1.Name = 'BSureS_Run_Triggers6';
        objCSettings1.Parameter_Key__c = 'BSureS_Run_Triggers';
        objCSettings1.Parameter_Value__c = 'True';
        insert objCSettings1;
        system.assertEquals('True', objCSettings1.Parameter_Value__c);
        
        BSureS_Basic_Info__c Objsupplier=new BSureS_Basic_Info__c();
        Objsupplier.Supplier_Name__c='TestSupplier';
        Objsupplier.Globe_ID__c='1234';
        insert Objsupplier;
        
        BSureS_Spend_History_Publish__c publish=new BSureS_Spend_History_Publish__c();
        publish.Date__c=system.today();
        publish.Globe_ID__c='1234';
        publish.Spend_Amount__c=Decimal.valueOf(20000);
        publish.Spend_Period__c=system.today();
        publish.Supplier_ID__c=Objsupplier.id;
        insert publish;
        system.Assertequals('1234',publish.Globe_ID__c);
        ApexPages.StandardController con = new ApexPages.StandardController(new BSureS_Spend_History_Publish__c());
        //ApexPages.currentPage().getParameters().put('Id',publish.id);
       ApexPages.currentPage().getParameters().put('recid',Objsupplier.id);   
       //ApexPages.currentPage().getParameters().put('recid',Objsupplier.id);      
        BSureS_Create_SpendHistoryPublish ObjSpendPublish=new BSureS_Create_SpendHistoryPublish(con);
        //BSureS_Create_SpendHistoryPublish ObjSpendPublish=new BSureS_Create_SpendHistoryPublish();
        ObjSpendPublish.Save();
        ObjSpendPublish.Cancel();
        ObjSpendPublish.redirectSupplier();
    }
}