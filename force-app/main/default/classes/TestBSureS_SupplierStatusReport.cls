/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData=true)
public with sharing class TestBSureS_SupplierStatusReport 
{
    static testMethod void myUnitTest()
    {
        BSureS_Category__c scategory = new BSureS_Category__c();
        scategory.Name = 'Account';
        insert scategory;
        
        BSureS_Basic_Info__c SuppObj = new BSureS_Basic_Info__c();
        SuppObj.Contact_name__c = 'test name'; 
        SuppObj.Supplier_Name__c = 'test parent name';
        SuppObj.Supplier_Category__c = scategory.id;
        insert SuppObj;
        system.assertEquals('test name',SuppObj.Contact_name__c);
        
        BSure_Configuration_Settings__c objCSettingsB = new BSure_Configuration_Settings__c();
        objCSettingsB.Name = 'BSureS_Report_Page_Size1';
        objCSettingsB.Parameter_Key__c = 'BSureS_Report_Page_Size';
        objCSettingsB.Parameter_Value__c = '10';
        insert objCSettingsB;
        
        system.Test.startTest();
        BSureS_SupplierStatusReport sObj = new BSureS_SupplierStatusReport();
        BSureS_SupplierStatusReport.SupplierWrapper wrpObj = new BSureS_SupplierStatusReport.SupplierWrapper();
        sObj.getCategoryList();
        sObj.SupDetails();
        sObj.Buyerslist();
        sObj.Search();
        sObj.nextBtnClick() ;
        sObj.previousBtnClick();
        sObj.getPageNumber();
        sObj.getPageSize();
        sObj.getPreviousButtonEnabled();
        sObj.getNextButtonDisabled();
        sObj.getTotalPageNumber();
        sObj.LastbtnClick();
        sObj.FirstbtnClick();
        sObj.Cancel();
        sObj.showBuyerDetails();
        sObj.searchBtn();
        sObj.closepopupBtn();
        sObj.ExportCSV();
        sObj.closepopup();
        sObj.ViewData();
        sObj.SortData();
        sObj.getSortDirection();
        sObj.setSortDirection('ASC');
        system.Test.stopTest();
    }
}