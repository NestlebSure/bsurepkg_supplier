@isTest(SeeAllData= true)
public with sharing class TestBsures_viewSupplierDetails {
    static testMethod void myUnittest()
    {
       BSure_Configuration_Settings__c objCSettings8B = new BSure_Configuration_Settings__c();
        objCSettings8B.Name = 'BSureS_CEORole4';
        objCSettings8B.Parameter_Key__c = 'BSureS_CEORole';
        objCSettings8B.Parameter_Value__c = 'CEO';
        insert objCSettings8B;
        system.assertEquals('CEO', objCSettings8B.Parameter_Value__c);
      
        list<User> lstuser = new list<User>();
        Map<String,ID> profiles = new Map<String,ID>();
        Map<string,ID> roleIds = new Map<string,ID>();
        List<Profile> ps = [select id, name from Profile where name =
                                 'Standard User' or name = 'System Administrator' or name ='BSureS_Manager'];
                                       
        for(Profile p : ps){
         profiles.put(p.name, p.id);
        }
        User standard = new User(
                alias = 'standt',
                email='standarduser@testorg.com',
                emailencodingkey='UTF-8',
                lastname='Testing2', languagelocalekey='en_US',
                localesidkey='en_US',
                profileid = profiles.get('Standard User'),
                timezonesidkey='America/Los_Angeles',
                username='standardusertest@testorg.com.bsures');
        insert standard;
        
        //lstuser.add(standard);
        //Zone
        BSureS_Zone__c szone=new BSureS_Zone__c();
        szone.IsActive__c = true;
        szone.Name ='TESTAOA';
        insert szone;
        //SubZone
        BSureS_SubZone__c sSubZone=new BSureS_SubZone__c();
        sSubZone.Name = 'TEST Latin America'; 
        sSubZone.IsActive__c = true; 
        sSubZone.ZoneID__c=szone.Id; 
        insert sSubZone; 
        //Country
        BSureS_Country__c sCountryObj = new BSureS_Country__c();
        sCountryObj.Name = 'Test Country2342';
        sCountryObj.IsActive__c = true;
        sCountryObj.Sub_Zone_ID__c=sSubZone.Id;
        insert sCountryObj;
        BSureS_Basic_Info__c b=new BSureS_Basic_Info__c();
        system.runAs(standard)
        {
            b.Contact_name__c = 'Steve';
            b.Supplier_Name__c = 'george M';
            b.Analyst__c = standard.id;
            b.Manager__c = standard.id;
            b.Zone__c = szone.Id;
            b.Sub_Zone__c = sSubZone.Id;
            b.BSureS_Country__c = sCountryObj.Id;
            b.Bakup_Analysts__c = standard.id +','+  standard.id;
            insert b;
        }
        
        BSureS_Credit_Analysis__c caObj = new BSureS_Credit_Analysis__c();
        caObj.Review_Status__c = 'Started';
        caObj.Supplier_ID__c = b.Id;
        insert caObj;
        
        
         
        //ApexPages.currentPage().getParameters().put('id',b.id); 
        Apexpages.currentPage().getParameters().put('Id',b.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(b); 
        Bsures_viewSupplierDetails sviewObj= new Bsures_viewSupplierDetails(controller);
        sviewObj.getSupplierDetails(); 
    }
    
    static testMethod void UnitTest2()
    {
        Map<String,ID> profiles = new Map<String,ID>();
        Map<string,ID> roleIds = new Map<string,ID>();
        
        List<Profile> ps = [select id, name from Profile where name = 'System Administrator' or name ='BSureS_Manager'];
                                 
        for(Profile p : ps){
         profiles.put(p.name, p.id);
        }
        
        //User Role
        List<UserRole> lObj = [select id,Name from UserRole  where name like '%Manager' ];
        for(UserRole uObj : lObj)
        {
            roleIds.put(uObj.Name,uObj.id);
        }
        ////system.debug('lObj============'+lObj);
        ////system.debug('zone role-========='+roleIds);
        User standardZM = new User(
                alias = 'standt',
                email='standarduser1@testorg.com',
                emailencodingkey='UTF-8',
                UserRoleId = roleIds.get('Zone Manager'),
                lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US',
                profileid = profiles.get('BSureS_Manager'),
                timezonesidkey='America/Los_Angeles',
                username='standardusertest1@testorg.com.bsures');
        insert standardZM;
        
        System.runAs(standardZM){
        
            BSure_Configuration_Settings__c objCSettings16 = new BSure_Configuration_Settings__c();
            objCSettings16.Name = 'BSureS_CEORole16';
            objCSettings16.Parameter_Key__c = 'BSureS_CEORole';
            objCSettings16.Parameter_Value__c = 'CEO';
            insert objCSettings16;
            system.assertEquals('CEO', objCSettings16.Parameter_Value__c);
        
            BSureS_Zone__c szone1=new BSureS_Zone__c();
            szone1.IsActive__c = true;
            szone1.Name ='TESTAOA';
            insert szone1;
            
            BSureS_Basic_Info__c b2=new BSureS_Basic_Info__c();
            b2.Contact_name__c = 'Steve';
            b2.Supplier_Name__c = 'george M';
            b2.Zone__c = szone1.Id;
            insert b2;
            BSureS_Zone_Manager__c zmObj = new BSureS_Zone_Manager__c();
            zmObj.Zone__c = szone1.Id;
            zmObj.Zone_Manager__c = standardZM.Id;
            insert zmObj;
             
            Apexpages.currentPage().getParameters().put('Id',b2.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(b2); 
            Bsures_viewSupplierDetails sviewObj= new Bsures_viewSupplierDetails(controller);
            sviewObj.getSupplierDetails();
        }
    }
    
    static testMethod void UnitTest3()
    {
    
        
        
        Map<String,ID> profiles = new Map<String,ID>();
        Map<string,ID> roleIds = new Map<string,ID>();
        
        List<Profile> ps = [select id, name from Profile where name = 'System Administrator' or name ='BSureS_Manager'];
                                 
        for(Profile p : ps){
         profiles.put(p.name, p.id);
        }
        
        //User Role
        List<UserRole> lObj = [select id,Name from UserRole  where name like '%Manager' ];
        for(UserRole uObj : lObj)
        {
            roleIds.put(uObj.Name,uObj.id);
        }
        ////system.debug('lObj============'+lObj);
        ////system.debug('zone role-========='+roleIds);
        User standardZM = new User(
                alias = 'standt',
                email='standarduser1@testorg.com',
                emailencodingkey='UTF-8',
                UserRoleId = roleIds.get('Sub-Zone Manager'),
                lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US',
                profileid = profiles.get('BSureS_Manager'),
                timezonesidkey='America/Los_Angeles',
                username='standardusertest1@testorg.com.bsures');
        insert standardZM;
        
        System.runAs(standardZM){
        
            BSure_Configuration_Settings__c objCSettings46 = new BSure_Configuration_Settings__c();
            objCSettings46.Name = 'BSureS_CEORole46';
            objCSettings46.Parameter_Key__c = 'BSureS_CEORole';
            objCSettings46.Parameter_Value__c = 'CEO';
            insert objCSettings46;
            system.assertEquals('CEO', objCSettings46.Parameter_Value__c);
        
        
            BSureS_Zone__c szone1=new BSureS_Zone__c();
            szone1.IsActive__c = true;
            szone1.Name ='TESTAOA';
            insert szone1;
            BSureS_SubZone__c sSubZone=new BSureS_SubZone__c();
            sSubZone.Name = 'TEST Latin America'; 
            sSubZone.IsActive__c = true; 
            sSubZone.ZoneID__c=szone1.Id; 
            insert sSubZone;
            BSureS_Basic_Info__c b3=new BSureS_Basic_Info__c();
            b3.Contact_name__c = 'Steve';
            b3.Supplier_Name__c = 'george M';
            b3.Zone__c = szone1.Id;
            b3.Sub_Zone__c = sSubZone.Id;
            insert b3;
            BSureS_Sub_Zone_Manager__c  szmObj = new BSureS_Sub_Zone_Manager__c ();
            szmObj.Sub_Zone__c = sSubZone.Id;
            szmObj.Sub_Zone_Manager__c  = standardZM.Id;
            insert szmObj;
            
             
            Apexpages.currentPage().getParameters().put('Id',b3.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(b3); 
            Bsures_viewSupplierDetails sviewObj= new Bsures_viewSupplierDetails(controller);
            sviewObj.getSupplierDetails();
        }
    }
    
    static testMethod void UnitTest4()
    {
        Map<String,ID> profiles = new Map<String,ID>();
        Map<string,ID> roleIds = new Map<string,ID>();
        
        List<Profile> ps = [select id, name from Profile where name = 'System Administrator' or name ='BSureS_Manager'];
                                 
        for(Profile p : ps){
         profiles.put(p.name, p.id);
        }
        
        //User Role
        List<UserRole> lObj = [select id,Name from UserRole  where name like '%Manager' ];
        for(UserRole uObj : lObj)
        {
            roleIds.put(uObj.Name,uObj.id);
        }
        ////system.debug('lObj============'+lObj);
        ////system.debug('zone role-========='+roleIds);
        User standardZM = new User(
                alias = 'standt',
                email='standarduser1@testorg.com',
                emailencodingkey='UTF-8',
                UserRoleId = roleIds.get('Country Manager'),
                lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US',
                profileid = profiles.get('BSureS_Manager'),
                timezonesidkey='America/Los_Angeles',
                username='standardusertest1@testorg.com.bsures');
        insert standardZM;
        
        System.runAs(standardZM){
            BSureS_Zone__c szone1=new BSureS_Zone__c();
            szone1.IsActive__c = true;
            szone1.Name ='TESTAOA';
            insert szone1;
            BSureS_SubZone__c sSubZone=new BSureS_SubZone__c();
            sSubZone.Name = 'TEST Latin America'; 
            sSubZone.IsActive__c = true; 
            sSubZone.ZoneID__c=szone1.Id; 
            insert sSubZone;
            BSureS_Country__c sCountryObj = new BSureS_Country__c();
            sCountryObj.Name = 'Test Country2342347';
            sCountryObj.IsActive__c = true;
            sCountryObj.Sub_Zone_ID__c=sSubZone.Id;
            insert sCountryObj;
            
            BSureS_Basic_Info__c b4=new BSureS_Basic_Info__c();
            b4.Contact_name__c = 'Steve';
            b4.Supplier_Name__c = 'george M';
            b4.Zone__c = szone1.Id;
            b4.Sub_Zone__c = sSubZone.Id;
            b4.BSureS_Country__c = sCountryObj.Id;
            insert b4;
            system.assertEquals('Steve',b4.Contact_name__c);
            
            BSureS_Country_Manager__c cmObj = new BSureS_Country_Manager__c();
            cmObj.Country__c = sCountryObj.Id;
            cmObj.Country_Manager__c = standardZM.Id;
            insert cmObj;
           
            BSure_Configuration_Settings__c objCSettings36 = new BSure_Configuration_Settings__c();
            objCSettings36.Name = 'BSureS_CEORole36';
            objCSettings36.Parameter_Key__c = 'BSureS_CEORole';
            objCSettings36.Parameter_Value__c = 'CEO';
            insert objCSettings36;
            system.assertEquals('CEO', objCSettings36.Parameter_Value__c);
                 
            Apexpages.currentPage().getParameters().put('Id',b4.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(b4); 
            Bsures_viewSupplierDetails sviewObj4= new Bsures_viewSupplierDetails(controller);
            sviewObj4.getSupplierDetails();
        }
    }
    
}