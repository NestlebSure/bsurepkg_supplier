/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureS_ViewContactDetails {

    static testMethod void myUnitTest() {
        
        Map<String,ID> profiles = new Map<String,ID>();
        List<Profile> ps = [select id, name from Profile where name =
                                 'Standard User' or name = 'System Administrator' or name ='BSureS_Manager'];
                                       
        for(Profile p : ps){
            profiles.put(p.name, p.id);
        }
        
        User standard = new User(
            alias = 'standt',
            email='standarduser@testorg.com',
            emailencodingkey='UTF-8',
            lastname='Testing2', languagelocalekey='en_US',
            localesidkey='en_US',
            profileid = profiles.get('Standard User'),
            timezonesidkey='America/Los_Angeles',
            username='standardusertest@testorg.com.bsures');
        insert standard;
        
        BSureS_Zone__c szone=new BSureS_Zone__c();
        szone.IsActive__c = true;
        szone.Name ='TESTAOA';
        insert szone;
        //SubZone
        BSureS_SubZone__c sSubZone=new BSureS_SubZone__c();
        sSubZone.Name = 'TEST Latin America'; 
        sSubZone.IsActive__c = true; 
        sSubZone.ZoneID__c=szone.Id; 
        insert sSubZone; 
        //Country
        BSureS_Country__c sCountryObj = new BSureS_Country__c();
        sCountryObj.Name = 'Test Country2342';
        sCountryObj.IsActive__c = true;
        sCountryObj.Sub_Zone_ID__c=sSubZone.Id;
        insert sCountryObj;
        
        BSureS_Basic_Info__c b=new BSureS_Basic_Info__c();
        
        system.runAs(standard)
        {
            b.Contact_name__c = 'Steve';
            b.Supplier_Name__c = 'george M';
            b.Analyst__c = standard.id;
            b.Manager__c = standard.id;
            b.Zone__c = szone.Id;
            b.Sub_Zone__c = sSubZone.Id;
            b.BSureS_Country__c = sCountryObj.Id;
            b.Bakup_Analysts__c = standard.id +','+  standard.id;
            insert b;
        }
        
        BSureS_Assigned_Contacts__c bac = new BSureS_Assigned_Contacts__c();
        bac.Supplier_Id__c=b.Id;
        bac.Contact_Name__c='';
        bac.Comments__c='test comments';
        bac.Email_address__c='satish.c@vertexcs.com';
        bac.Phone_Number__c='988-2232-221';
        bac.Role__c='other';
        insert bac;
        
        BSureS_ViewContactDetails obj = new BSureS_ViewContactDetails();
        obj.strContactId = bac.Id;
        obj.getContactDetails();
        
    }
}