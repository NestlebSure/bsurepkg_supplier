/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureS_SupplierSearch {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        BSureS_Basic_Info__c objSupplier = new BSureS_Basic_Info__c(Contact_name__c='KingFisher');
   		insert objSupplier;
   		
   		system.assertEquals('KingFisher',objSupplier.Contact_name__c);
   		
        BSureS_SupplierSearch objsupsrch=new BSureS_SupplierSearch();
        objsupsrch.strSupplierName='Dummy';
        objsupsrch.strSupplierAddress='Dummy';
        objsupsrch.strSupplierCity='Dummy';
        objsupsrch.strSupplierState='Dummy';
        objsupsrch.pageSize=10;
        objsupsrch.Search();
        pagereference pgrefnxtbtn =objsupsrch.nextBtnClick();
        pagereference pgrefprvbtn =objsupsrch.previousBtnClick();
        integer i=objsupsrch.getTotalPageNumber();
        integer j=1;
        objsupsrch.BindData(j);
        objsupsrch.pageData(j);
        objsupsrch.LastpageData(j);
        pagereference pgreflastbtn =objsupsrch.LastbtnClick();
        pagereference pgreffrstbtn =objsupsrch.FirstbtnClick();
        
    }
}