@isTest
public with sharing class TestBSureS_AssignNonCreditUser {
    static testMethod void myUnitTest() 
    {
     
        BSureS_Zone__c szone=new BSureS_Zone__c();
        szone.IsActive__c = true;
        szone.Name ='TESTAOA';
        insert szone;
        
        //system.assertEquals('TESTAOA', szone.Name);
        
        BSureS_SubZone__c sSubZone=new BSureS_SubZone__c();
        sSubZone.Name = 'TEST Latin America'; 
        sSubZone.IsActive__c = true;
        sSubZone.ZoneID__c=szone.Id; 
        insert sSubZone; 
        
        
        BSureS_Country__c sCountry = new BSureS_Country__c();
        sCountry.IsActive__c = true;
        sCountry.Name = 'test united States';
        sCountry.Sub_Zone_ID__c=sSubZone.Id;
        insert sCountry; 
        
        BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
        objCSettings1.Name = 'TestBSureS_CEORole';
        objCSettings1.Parameter_Key__c = 'BSureS_CEORole';
        objCSettings1.Parameter_Value__c = 'CEO';
        insert objCSettings1;
        system.assertEquals('CEO', objCSettings1.Parameter_Value__c);
        
        BSure_Configuration_Settings__c objCSettings5 = new BSure_Configuration_Settings__c();
        objCSettings5.Name = 'TestBSureS_BuyerShare';
        objCSettings5.Parameter_Key__c = 'BSureS_BuyerShare';
        objCSettings5.Parameter_Value__c = 'TRUE';
        insert objCSettings5;
      
      
      BSure_Configuration_Settings__c objCategoryInBuyer = new BSure_Configuration_Settings__c();
      objCategoryInBuyer.Name = 'TestBSureS_includeCategory';
      objCategoryInBuyer.Parameter_Key__c = 'BSureS_includeCategoryInBuyerSection';
      objCategoryInBuyer.Parameter_Value__c = 'FALSE';
      insert objCategoryInBuyer;
      
      
      BSure_Configuration_Settings__c objCSettings6 = new BSure_Configuration_Settings__c();
      objCSettings6.Name = 'TestBSureS_buyerrole';
      objCSettings6.Parameter_Key__c = 'BSureS_buyerrole';
      objCSettings6.Parameter_Value__c = 'Buyer';
      insert objCSettings6;
      system.assertEquals('Buyer', objCSettings6.Parameter_Value__c);
    
      BSureS_Category__c scategory = new BSureS_Category__c();
      scategory.Name = 'Account';
      insert scategory;
        
      Map<String,ID> profiles = new Map<String,ID>();
      List<Profile> ps = [select id, name from Profile where name =
                                 'Standard User' or name = 'System Administrator'];
        for(Profile p : ps){
         profiles.put(p.name, p.id);
        }
        User standard = new User(
                alias = 'standt',
                email='standarduser@testorg.com',
                emailencodingkey='UTF-8',
                lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US',
                profileid = profiles.get('Standard User'),
                timezonesidkey='America/Los_Angeles',
                username='standardusertest@testorg.com.Bsures');
        insert standard;
        
        BSureS_Basic_Info__c b = new BSureS_Basic_Info__c();
        b.Contact_name__c = 'Steve';
        b.Supplier_Name__c = 'george M';
        b.Supplier_Category__c = scategory.id;
        //b.Group_Id__c = '12345'; 
        b.Review_Status__c='';
        b.Analyst__c = standard.Id;
        b.Manager__c = standard.Id;
        b.Bakup_Analysts__c = standard.Id + ','+standard.Id;
        insert b;
        
        
        BSureS_Assigned_Buyers__c buyerObj = new BSureS_Assigned_Buyers__c();
        buyerObj.Buyer_Id__c = standard.id;
        buyerObj.Buyer_Name__c = standard.name;
        buyerObj.Supplier_Name__c = b.Supplier_Name__c;
        buyerObj.Supplier_ID__c= b.Id; 
        insert buyerObj;
        ApexPages.currentPage().getParameters().put('sid',b.id); 
        
        ApexPages.StandardController controller = new ApexPages.StandardController(buyerObj);
        BSureS_AssignNonCreditUser cObj= new BSureS_AssignNonCreditUser(controller);
        cObj.showBuyerDetails();
        
        
        BSureS_Assigned_Buyers__c buyerObj2 = new BSureS_Assigned_Buyers__c();
        buyerObj2.Buyer_Id__c = standard.id;
        buyerObj2.Supplier_ID__c=b.id;
        insert buyerObj2; 
         
        BSureS_userCategoryMapping__c ucatObj = new BSureS_userCategoryMapping__c();
        ucatObj.CategoryID__c = scategory.Id;
        ucatObj.UserID__c = standard.Id;
        insert ucatObj;
        
        BSureS_User_Additional_Attributes__c uaddObj = new BSureS_User_Additional_Attributes__c();
        uaddobj.User__c = standard.Id;
        //uaddObj.Zone_Name__c = szone.Name;
        //uaddObj.Sub_Zone_Name__c = sSubZone.Name;
        //uaddObj.Country_Name__c  = sCountry.Name; 
        insert uaddobj;
        
       // list<BSureS_User_Additional_Attributes__c> lstadd = new list<BSureS_User_Additional_Attributes__c>();
        list<Contact> lstadd=new list<Contact>();
      // lstadd.add(uaddobj);
        cObj.userList = lstadd;
        
        cObj.strCurrentSupplierId = b.id;
        cObj.strCategoryInBuyerSection = 'FALSE';
        
        cObj.closepopup();
        //cObj.searchBtn();
        cObj.closepopupBtn();
        cObj.buyerSave(); 
        cObj.cancel();
        cObj.SendNotification();
        
    }
    static testMethod void myUnitTest2()
    {
        BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
        objCSettings1.Name = 'TestBSureS_CEORole';
        objCSettings1.Parameter_Key__c = 'BSureS_CEORole';
        objCSettings1.Parameter_Value__c = 'CEO';
        insert objCSettings1;
        
        system.assertEquals('CEO', objCSettings1.Parameter_Value__c);
        
    BSure_Configuration_Settings__c objCSettings5 = new BSure_Configuration_Settings__c();
      objCSettings5.Name = 'TestBSureS_BuyerShare';
      objCSettings5.Parameter_Key__c = 'BSureS_BuyerShare';
      objCSettings5.Parameter_Value__c = 'TRUE';
      insert objCSettings5;
      
      BSure_Configuration_Settings__c objCategoryInBuyer1 = new BSure_Configuration_Settings__c();
      objCategoryInBuyer1.Name = 'TestBSureS_includeCategory';
      objCategoryInBuyer1.Parameter_Key__c = 'BSureS_includeCategoryInBuyerSection';
      objCategoryInBuyer1.Parameter_Value__c = 'TRUE';
      insert objCategoryInBuyer1;
      
      BSure_Configuration_Settings__c objCSettings6 = new BSure_Configuration_Settings__c();
        objCSettings6.Name = 'TestBSureS_buyerrole';
        objCSettings6.Parameter_Key__c = 'BSureS_buyerrole';
        objCSettings6.Parameter_Value__c = 'Buyer';
        insert objCSettings6;
      system.assertEquals('Buyer', objCSettings6.Parameter_Value__c);
    
        BSureS_Basic_Info__c supplObj = new BSureS_Basic_Info__c();
        supplObj.Contact_name__c = 'Steve';
        supplObj.Supplier_Name__c = 'george M';
        supplObj.Review_Status__c='';
        insert supplObj;
        system.assertEquals('Steve',supplObj.Contact_name__c);
        Map<String,ID> profiles = new Map<String,ID>();
        List<Profile> ps1 = [select id, name from Profile where name =
                                 'Standard User' or name = 'System Administrator'];
        for(Profile p1 : ps1){
         profiles.put(p1.name, p1.id);
        }
        User standard1 = new User(
                alias = 'standt',
                email='standarduser@testorg.com',
                emailencodingkey='UTF-8',
                lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US',
                profileid = profiles.get('Standard User'),
                timezonesidkey='America/Los_Angeles',
                username='standardusertest@testorg.com.bsures');
        insert standard1;
        
        BSureS_Assigned_Buyers__c buyerObj3 = new BSureS_Assigned_Buyers__c();
        buyerObj3.Buyer_Id__c = standard1.id;
        buyerObj3.Supplier_ID__c=supplObj.id;
        insert buyerObj3; 
        ApexPages.currentPage().getParameters().put('id',buyerObj3.id); 
        ApexPages.StandardController controller = new ApexPages.StandardController(buyerObj3);
        BSureS_AssignNonCreditUser cObj= new BSureS_AssignNonCreditUser(controller);
        //cObj.showBuyerDetails();
        cObj.strbuyer = String.valueOf(buyerObj3.id);
        cObj.buyerSave();
    } 
    

}