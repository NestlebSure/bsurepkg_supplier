/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class Test_SpendHistory_trigger {

    static testMethod void myUnitTest1() {
        // TO DO: implement unit test
        BSureS_Spend_History_Stage__c spendhistory_stage=new BSureS_Spend_History_Stage__c();
        spendhistory_stage.Date__c=system.today()+16;
        spendhistory_stage.Spend_Period__c=system.today()+10;
        spendhistory_stage.Globe_ID__c='12345';
        spendhistory_stage.Spend_Amount__c=3000;
        insert spendhistory_stage;
        spendhistory_stage.Date__c=null;
        spendhistory_stage.Spend_Period__c=null;
        spendhistory_stage.Spend_Amount__c=null;
        spendhistory_stage.Globe_ID__c='1234';
        update spendhistory_stage;
        System.assertNotEquals('123454', spendhistory_stage.Globe_ID__c);
    }
    
    static testMethod void myUnitTest2() {
        // TO DO: implement unit test
        BSureS_Spend_History_Stage__c spendhistory_stage=new BSureS_Spend_History_Stage__c();
        spendhistory_stage.Date__c=system.today()+17;
        spendhistory_stage.Spend_Period__c=system.today()+11;
        spendhistory_stage.Globe_ID__c='12345';
        insert spendhistory_stage;
        spendhistory_stage.Date__c=null;
        spendhistory_stage.Spend_Amount__c=null;
        spendhistory_stage.Spend_Period__c=null;
        spendhistory_stage.Globe_ID__c='1234';
        update spendhistory_stage;
        System.assertNotEquals('123454', spendhistory_stage.Globe_ID__c);
    }
    
     static testMethod void myUnitTest3() {
        BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
        objCSettings1.Name = 'BSureS_Run_TriggersKi';
        objCSettings1.Parameter_Key__c = 'BSureS_Run_Triggers';
        objCSettings1.Parameter_Value__c = 'True';
        insert objCSettings1;
        system.assertEquals('True', objCSettings1.Parameter_Value__c);
     
        // TO DO: implement unit test
        BSureS_Basic_Info__c basicinfo=new BSureS_Basic_Info__c();
        //basicinfo.Group_Id__c='12345';
        insert basicinfo;
        BSureS_Spend_History_Publish__c spendhistory_stage=new BSureS_Spend_History_Publish__c();
        spendhistory_stage.Date__c=system.today()+16;
        spendhistory_stage.Spend_Period__c=system.today()+10;
        spendhistory_stage.Globe_ID__c='12345';
        spendhistory_stage.Spend_Amount__c=3000;
        spendhistory_stage.Supplier_ID__c=basicinfo.Id;
        insert spendhistory_stage;
        System.assertNotEquals('123454', spendhistory_stage.Globe_ID__c);

    }
}