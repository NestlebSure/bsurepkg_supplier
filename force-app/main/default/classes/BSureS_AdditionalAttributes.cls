/***********************************************************************************************
* Controller Name :  BSures_AddingAttributes
* Date : 1st Nov 2012
* Author : Veereandrnath Jella
* Purpose : This Class written to Save Additional Attributes to User for the Supplier side.
* Change History :
* Date Programmer Reason
* -------------------- ------------------- -------------------------
* 12/22/2011 Veereandranath.j Initial Version
*  
**************************************************************************************************/  
global with sharing class BSureS_AdditionalAttributes {

    public Id userId{get;set;} // to store userId from Url
    public User user{get;set;} // to display User Details In Page 
    public string strZoneId {get;set;} // zone Id
    public list<selectOption> zoneOptions{get;set;} // selectOptins for list of Zone Records
    public string strSubZoneId {get;set;} // Sub zone id
    public list<selectOption> subZoneOptions{get;set;} //selectOptins for list of sub Zone Records
    public string strCountryId {get;set;} // Coutnry id
    public list<SelectOption> coutryOptions{get;set;} // selectOptins for list of Country Records
    public string strManagerId{get;set;}  // User id
    public list<SelectOption> managers {get;set;} // selectOptions for list Of User Records
    public BSureS_User_Additional_Attributes__c bSureUserAttributes{get;set;} // to store Single Bsure Additonal User
    public Map<string,BSureS_Category__c> mapBSureCategory  = new map<string,BSureS_Category__c>();
    public boolean blnshowEdit {get;set;}    // to control Edit page
    Public List<string> lstLeftSelected{get;set;} //  to collect Category Names   (left Side)
    Public List<string> lstRightSelected{get;set;} // to collect Categroy Name (right side)
    Set<string> setLeftValues = new Set<string>(); // to store left Values of Categories
    Set<string> setRightValues = new Set<string>(); // to store right Values of Categories
    List<string> lstSelectedCategories; // to store Selected Categories
    public string rightCategories {get;set;}
    //Aditya V Request to remove Region and Category restriction for selecting Analysts 07/22/2013
    public string strLocationFlagForAnalyst{get;set;}
    //Aditya V Request to remove Region and Category restriction for selecting Analysts 07/22/2013
    
    // Constructor
    public BSureS_AdditionalAttributes(){
    	strLocationFlagForAnalyst = BSureS_CommonUtil.getConfigurationValues('BSureS_LocationFlagForAnalyst').get(0);
        list<BSureS_Category__c> lstCategories = new list<BSureS_Category__c>();  //  to store Category Id and Category Record
        list<BSureS_Category__c> lstSelectedCategories = new list<BSureS_Category__c>();
        Map<string,string> mapCategory = new map<string,string>();
        list<Id> lstCategoriesIds = new list<Id>(); 
        bSureUserAttributes = new BSureS_User_Additional_Attributes__c();       
        zoneOptions = new list<selectOption>();
        subZoneOptions = new list<selectOption>();
        coutryOptions = new list<selectOption>();
        managers = new list<selectOption>();
        lstLeftSelected = new List<string>();
        lstRightSelected = new List<string>();
        blnshowEdit = false;
        
        userId  = apexpages.currentpage().getParameters().get('Id');
        
        if(userId != null)
        {
           user =  [SELECT Id,Name,LastName,FirstName,phone,UserName,Email,Alias,UserRoleId,
                            UserRole.Name,ProfileId,IsActive,EmailEncodingKey,TimeZoneSidKey,
                            LocaleSidKey,LanguageLocaleKey,CompanyName,Department,City,Country,State,
                            (Select Id, CategoryID__c, UserID__c, CategoryName__c From BSureS_UserCategoryMapping__r),
                            (SELECT Zone_Name__c, User__c, SystemModstamp,Sub_Zone_Name__c,Id,Name,Country_Name__c,
                            BSureS_Zone__c,BSureS_Sub_Zone__c,BSureS_Country__c,Manager__c                                    
                            FROM BSureS_User_Additional_Attributes__r),
                            PostalCode,Street,Fax,UserType,User.Profile.UserLicenseId                            
                            FROM User 
                            WHERE Id =:userId]; 
             
        } 
        getZones();
        getManagers(); 
                 
        if(user.BSureS_User_Additional_Attributes__r != null && user.BSureS_User_Additional_Attributes__r.size() > 0 ){
             bSureUserAttributes = user.BSureS_User_Additional_Attributes__r[0]; 
             blnshowEdit = true;
        }         
        
        if(user.BSureS_UserCategoryMapping__r.size() > 0 )
        {
            for(BSureS_UserCategoryMapping__c objUCategory:user.BSureS_UserCategoryMapping__r){
                if(objUCategory.CategoryName__c != null )
                    setRightValues.add(objUCategory.CategoryName__c);
                mapCategory.put(objUCategory.CategoryID__c,objUCategory.CategoryName__c);
                if(rightCategories != null && objUCategory.CategoryName__c != null)
                    rightCategories += objUCategory.CategoryName__c + ';'+'  ';
                else if(objUCategory.CategoryName__c != null )
                    rightCategories = objUCategory.CategoryName__c + ';'+'  ';
            }
        }
        
        lstCategories = [select Id,Category_ID__c,Name  from BSureS_Category__c Where Id != null and Name != null];
        
        for(BSureS_Category__c category:lstCategories)
        {
            mapBSureCategory.put(category.Name,category);
            if(mapCategory.get(category.Id) == null)
            {
                setLeftValues.add(category.Name);
            }
        }
        
    }
   // <Summary> 
    //    Method Name : getZones
   // </Summary>
   //   Description : query all the zones and Preparing SelectOptions for Zone     
    
    public void getZones(){       
        if(user.BSureS_User_Additional_Attributes__r !=null  && user.BSureS_User_Additional_Attributes__r.size() > 0 )
        {   
            strzoneId = user.BSureS_User_Additional_Attributes__r[0].BSureS_Zone__c;
            strSubZoneId  = user.BSureS_User_Additional_Attributes__r[0].BSureS_Sub_Zone__c;
            strCountryId = user.BSureS_User_Additional_Attributes__r[0].BSureS_Country__c;
            strManagerId = user.BSureS_User_Additional_Attributes__r[0].Manager__c;     
        }              
        zoneOptions = BSureS_CommonUtil.getZones();
        getSubZones();
        getCountries();        
    }     
    // <Summary> 
   // Method Name : getSubZones
   // </Summary>
   // Description : query all the sub zones based on Zone Id and Preparing SelectOptions for sub Zones  
    
    public void getSubZones(){
        getCountries();
        subZoneOptions = new list<selectOption>();
        coutryOptions = new list<selectOption>();
        coutryOptions.add(new selectOption('Select','Select'));
        subZoneOptions  = BSureS_CommonUtil.getSubZones(strzoneId);
        getManagers();      
    }     
    // <Summary> 
    //Method Name : getCountries
    // </Summary>
    //Description : query all the Countries based on sub Zone Id and Preparing SelectOptions for Countries
    public void getCountries(){ 
        coutryOptions = new list<selectOption>(); 
        if(strZoneId == 'select' || strSubZoneId  == 'select'){
            strSubZoneId  = '';            
        }         
        coutryOptions = BSureS_CommonUtil.getCountries(strSubZoneId);  
        getManagers();            
    }
     
    // <Summary> 
   // Method Name : getManagers
   // </Summary>
    //Description : query all the Users based and Preparing SelectOptions for Users
    public void getManagers(){
        list<user> lstUser  = new list<User>(); 
        managers = new list<selectOption>();        
        managers.add(new selectOption('Select','Select'));
        set<string> setRoleIds = new set<string>();                
        map<Id,UserRole> mapParentRoleId = new  map<Id,UserRole>([select Id,ParentRoleId from UserRole limit 100]);
        if(user.UserRoleId != null ){
            string ParentRoleId = mapParentRoleId.get(user.UserRoleId).ParentRoleId;        
            for(integer i=0;I<mapParentRoleId.size();I++){
                if(ParentRoleId != null ){
                    setRoleIds.add(ParentRoleId);
                    setRoleIds.add(mapParentRoleId.get(ParentRoleId).ParentRoleId);
                    ParentRoleId = mapParentRoleId.get(ParentRoleId).ParentRoleId;
                }
            }  
        }      
        lstUser = getRoleUsers(setRoleIds);
        for(User u:lstUser)        
            managers.add(new selectOption(U.Id,U.Name));
        
    }    
    public list<User> getRoleUsers(set<string> pRoleIds){
        list<User>  lstUsers     = new list<User>();
        list<User>  CEOUsers     = new list<User>();
        list<User>  zoneUsers    = new list<User>();
        list<User>  subzoneUsers = new list<User>();
        list<User>  countryUsers = new list<User>();
        set<Id>     UserIds      = new set<Id>();
        list<BSureS_User_Additional_Attributes__c> Attributes = new list<BSureS_User_Additional_Attributes__c>();
        list<BSureS_User_Additional_Attributes__c> ALLAttributes = new list<BSureS_User_Additional_Attributes__c>();
        
        list<UserRole> lstRoles = [SELECT Id,Name,(SELECT Id,Name,Profile.Name FROM Users WHERE IsActive = true) FROM UserRole WHERE Id in:pRoleIds ];
        
        for(UserRole ur:lstRoles){
            if(Ur.Name == 'Zone Manager')
                zoneUsers.addALL(ur.Users);
            else if(Ur.Name == 'Sub-Zone Manager')
                subzoneUsers.addALL(ur.Users);
            else if(Ur.Name == 'Country Manager')
                countryUsers.addALL(ur.Users);
            else if(Ur.Name != 'CEO')
                lstUsers.addALL(ur.Users);
            else if(Ur.Name == 'CEO')
                CEOUsers.addALL(ur.Users);
        }
        
        string zQuery = 'select Id,Name,BSureS_Zone__c,User__c,BSureS_Sub_Zone__c,BSureS_Country__c from BSureS_User_Additional_Attributes__c where Id != null ';
        
        if(user.UserRole.Name != 'Zone Manager' && user.UserRole.Name != 'CEO'){
            set<Id> uIds = getUserIds(zoneUsers);
            zQuery += ' AND User__c IN: uIds ';
            if(!strLocationFlagForAnalyst.equalsIgnoreCase('FALSE')) 	//If Location is Required
            {
            	zQuery += ' AND BSureC_Zone__c =: strZoneId ';
            }
            Attributes = database.query(zQuery);
            ALLAttributes.addALL(Attributes);
        }
        if(user.UserRole.Name != 'Zone Manager' && user.UserRole.Name != 'CEO' && user.UserRole.Name != 'Sub-Zone Manager'){
            set<Id> uIds = getUserIds(subzoneUsers);
            string szQuery = zQuery + ' AND User__c IN: uIds ';
            if(!strLocationFlagForAnalyst.equalsIgnoreCase('FALSE')) 	//If Location is Required
            {
            	szQuery += ' AND BSureS_Sub_Zone__c =: strSubZoneId ';
            }
            Attributes = database.query(szQuery);
            ALLAttributes.addALL(Attributes);
        }
        if(user.UserRole.Name != 'Zone Manager' && user.UserRole.Name != 'CEO' && user.UserRole.Name != 'Sub-Zone Manager' && user.UserRole.Name != 'Country Manager'){
            set<Id> uIds = getUserIds(countryUsers);
            string cQuery = zQuery+' AND User__c IN:uIds ';
            if(!strLocationFlagForAnalyst.equalsIgnoreCase('FALSE')) 	//If Location is Required
            {
            	cQuery += ' AND BSureS_Country__c=:strCountryId ';
            }
            Attributes = database.query(cQuery);
            ALLAttributes.addALL(Attributes);
        }
        if(user.UserRole.Name != 'Zone Manager' && user.UserRole.Name != 'CEO' && user.UserRole.Name != 'Sub-Zone Manager' && user.UserRole.Name != 'Country Manager'){
            set<Id> uIds = getUserIds(lstUsers);
            string allQuery = zQuery+ ' AND User__c IN: uIds ';
            if(!strLocationFlagForAnalyst.equalsIgnoreCase('FALSE')) 	//If Location is Required
            {
             	allQuery += 'AND BSureS_Zone__c=:strZoneId AND BSureS_Sub_Zone__c=:strSubZoneId AND BSureS_Country__c=:strCountryId ';
            }
            Attributes = database.query(allQuery);
            ALLAttributes.addALL(Attributes);
        }
        
        UserIds.clear();lstUsers.clear();
        for(BSureS_User_Additional_Attributes__c a:ALLAttributes) UserIds.add(a.User__c);             

        lstUsers =[select Id,Name,Profile.Name from User where Id In:UserIds and Profile.Name Like:'%BSureS%'];
        lstUsers.addALL(CEOUsers);                
        return lstUsers;        
    } 
    
    public set<Id> getUserIds(list<User> lstUsers){
        set<Id> UserIds = new set<Id>(); 
         for(User u:lstUsers){ 
            UserIds.add(u.Id);
        }
        return UserIds;
    }
    
    // <Summary> 
     //Method Name : selectclick
     // </Summary>
     // Description : selected values removing from the list
    
    public void selectclick(){
        lstRightSelected.clear();
        for(String s: lstLeftSelected){
            setLeftValues.remove(s);
            setRightValues.add(s);
        }
    }
    // <Summary> 
    //    Method Name : unselectclick
    // </Summary>
    //    Description : selected values removing from the list
   
    
    public void unselectclick(){
        lstLeftSelected.clear();
        for(String s : lstRightSelected){
            setRightValues.remove(s);
            setLeftValues.add(s);
        }
    }
   // <Summary> 
    // Method Name : getunSelectedValues
    // </Summary>
    // Description : preparing the selectOptions for Unselcted values  
    
    public List<SelectOption> getunSelectedValues(){
        List<SelectOption> CategoriesOptions = new List<SelectOption>();
        List<string> lstSelectedCategories = new List<String>();
        lstSelectedCategories.addAll(setLeftValues);
        lstSelectedCategories.sort();
        for(string s : lstSelectedCategories)
        {
            if(s != null){
                CategoriesOptions.add(new SelectOption(s,s));
            }
        }
        return CategoriesOptions;
    }
    // <Summary> 
     //   Method Name : getunSelectedValues
     // </Summary>
     //   Description : preparing the selectOptions for selcted values    
    
    
    public List<SelectOption> getSelectedValues(){
        List<SelectOption> CategoriesOptions1 = new List<SelectOption>();
        lstSelectedCategories = new List<String>();
        lstSelectedCategories.addAll(setRightValues);
        lstSelectedCategories.sort();
        for(String s : lstSelectedCategories)
        {
            if(s != null)
            {
                CategoriesOptions1.add(new SelectOption(s,s));
            }
        }
        return CategoriesOptions1;
    }
    // <Summary> 
    //    Method Name : Save
    // </Summary>
     //   Description : saving valuues to user record based on user selection from Visual force page  
    
     
    public pagereference Save(){
        list<BSureS_UserCategoryMapping__c>  lstUserCategories = new list<BSureS_UserCategoryMapping__c> ();
        User objUser = new User (Id=userId);
        
        if(strZoneId != null  && strSubZoneId  !=null && strCountryId != null && strManagerId != null )
        {               
            bSureUserAttributes.BSureS_Zone__c  =  strZoneId;
            bSureUserAttributes.BSureS_Sub_Zone__c  = strSubZoneId;
            bSureUserAttributes.BSureS_Country__c =  strCountryId;
            bSureUserAttributes.Manager__c = strManagerId;
            bSureUserAttributes.User__c = userId;               
            Upsert bSureUserAttributes;
            
            if(objUser != null)
            {
                list<BSureS_UserCategoryMapping__c> lstCategories = [select Id,Name from BSureS_UserCategoryMapping__c where UserID__c=:objUser.Id];
                if(lstCategories.size() > 0)
                    Delete lstCategories;
            }
            
           list<BSureS_Category__c> lstBSureCategories =[select Id,Name from  BSureS_Category__c where Name IN:setRightValues];  
            
            for(BSureS_Category__c objC:lstBSureCategories)
            {  
                BSureS_UserCategoryMapping__c objUserCat = new BSureS_UserCategoryMapping__c();
                objUserCat.CategoryID__c = objC.Id;
                objUserCat.UserID__c =  objUser.Id;
                lstUserCategories.add(objUserCat);
            }
            if(lstUserCategories.size() > 0 )
                insert lstUserCategories;
        }
        return Cancel(); 
    }
    // <Summary>   
    //    Method Name : Cancel
    // </Summary>
    //    Description : this method navingates to user Record
    
    public Pagereference Cancel(){ 
        pagereference pageref =Page.BSureS_AdminTools;
        pageref.setRedirect(true);
        return pageref; 
    } 
    // <Summary>
    // Edit
    // </Summary>
    // Description : to handle edit and view in page 
    public void Edit(){
        blnshowEdit = false;
    } 

}