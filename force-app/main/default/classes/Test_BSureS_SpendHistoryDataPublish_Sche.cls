/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_BSureS_SpendHistoryDataPublish_Sche {

    static testMethod void myUnitTest()
    {
        // TO DO: implement unit test
        /*BSureS_SpendHistoryDataPublish_Scheduler publish_scheduler=new BSureS_SpendHistoryDataPublish_Scheduler();
        BSureS_Spend_History_Stage__c Spend_Stage=new BSureS_Spend_History_Stage__c();
        Spend_Stage.Date__c=system.today()+20;
        Spend_Stage.Group_ID__c='12345';
        Spend_Stage.Spend_Amount__c=20000;
        insert Spend_Stage;*/
        //BSureS_SpendHistoryDataPublish_Scheduler b = new BSureS_SpendHistoryDataPublish_Scheduler (); 
         //database.executebatch(b,200);
        //publish_scheduler.start 
         system.Test.startTest();
    BSureS_Spend_History_Stage__c ObjSpendstage=new BSureS_Spend_History_Stage__c(Date__c=system.today(),Globe_ID__c='1234',Spend_Amount__c=23455,Spend_Period__c=system.today());
    insert ObjSpendstage;
    system.Assertequals('1234',ObjSpendstage.Globe_ID__c);
    BSureS_SpendHistoryDataPublish_Scheduler it=new BSureS_SpendHistoryDataPublish_Scheduler();
    database.executebatch(it);
    System.Test.stopTest();
    }
}