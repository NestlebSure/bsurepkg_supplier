/************************************************************************************************       
Controller Name         : BSureS_SpendHistory_Dataupload       
Date                    : 11/12/2012         
Author                  : Praveen Sappati       
Purpose                 : To Insert the Spend History Section File into a Custom Object       
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          11/12/2012      Praveen Sappati            Initial Version
**************************************************************************************************/
global with sharing class BSureS_SpendHistory_Dataupload 
{
    public String filename{get;set;}//For Uploaded file from page
    public Transient blob bfilecontent{get;set;} // Holds the Uploaded file content from page
    
    /// <summary>
    /// constructor
    /// </summary>
    public BSureS_SpendHistory_Dataupload()
    {
        bfilecontent=null;
    }
    
    /// <summary>
    /// SaveDetailsFile method fires when user clicks on subit button in page to insert the records into stage
    /// </summary>
    public pagereference SaveDetailsFile()   
    {
        if(bfilecontent!=NULL)
        {
        pagereference pageref=new pagereference('/apex/Spend_History_Stage_Listview');
        Delete[SELECT Id from BSureS_Spend_History_Stage__c WHERE id!=NULL]; // deleting all spend history stage records.
        BsureS_SpendHistorySectionScheduler.createBatchesFromCSVFile(bfilecontent,'BSureS_Spend_History_Stage__c');//calling scheduler class method
        return pageref;
        }
        else
        {
            //ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Please upload a file');
            //ApexPages.addMessage(errormsg);
            return null;
        }
    }
}