/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData=true) 
private class TestBSureS_CommonInfoSection {

    static testMethod void myUnitTest() {
        /*
        BSure_Configuration_Settings__c cSettings4 = new BSure_Configuration_Settings__c();
        cSettings4.Name = 'SupplierFileSizeLimit';
        cSettings4.Parameter_Key__c = 'SupplierFileSizeLimit';
        cSettings4.Parameter_Value__c = '26214400';
        insert cSettings4;
        */
        BSureS_Basic_Info__c objSupplier = new BSureS_Basic_Info__c(Contact_name__c='KingFisher Supplier');
        insert objSupplier;
        
        system.assertEquals('KingFisher Supplier',objSupplier.Contact_name__c);
                
        BSureS_Document_Type__c objDocType = new BSureS_Document_Type__c(Document_Type_Name__c = 'Presdfdfd23232entations');
        insert objDocType;
        
        BSureS_Credit_Status_Section__c objCss = new BSureS_Credit_Status_Section__c();
        objCss.Supplier_ID__c = objSupplier.Id;
        objCss.Comment__c = 'Test&test';
        objCss.DiscussionType__c = 'Link';
        insert objCss;
        
        system.assertEquals('Test&test',objCss.Comment__c);
        
        BSureS_Credit_Review_Section__c objCss1 = new BSureS_Credit_Review_Section__c();
        objCss1.Supplier_ID__c = objSupplier.Id;
        insert objCss1;
        
        apexpages.currentpage().getParameters().put('supId',objSupplier.Id);
        apexpages.currentpage().getParameters().put('Type','Topic');        
        BSureS_CommonInfoSection objCommonInfo = new BSureS_CommonInfoSection();
        objCommonInfo.strObject = 'BSureS_Credit_Status_Section__c';
        objCommonInfo.strSelectedBtn = 'Topic';
        objCommonInfo.fileName = 'TestFile';
        objCommonInfo.strComments = 'Topic<>& Comments';
        objCommonInfo.strDoctypeid = objDocType.Id;
        objCommonInfo.m_fileBody = blob.valueof('Attachement File');
        objCommonInfo.strTitle ='Presentations On Education';
        objCommonInfo.SaveDetails();
        objCommonInfo.GetIDsForCreditStatus(objSupplier);
        
        apexpages.currentpage().getParameters().put('Type','File');
        BSureS_CommonInfoSection objCommonInfo1 = new BSureS_CommonInfoSection(); 
        objCommonInfo1.strObject = 'BSureS_Credit_Status_Section__c';
        objCommonInfo1.strSelectedBtn = 'File';
        objCommonInfo1.fileName = 'TestFile';
        objCommonInfo1.strComments = 'Topic Comments';
        objCommonInfo1.strDoctypeid = objDocType.Id;        
        objCommonInfo1.m_fileBody = blob.valueof('Attachement File');
        objCommonInfo1.strTitle ='Presentations On Education';
        objCommonInfo1.fsize = 5242881;
        objCommonInfo1.SaveDetails();        
        //objCommonInfo1.SaveDetails(); 
        
        apexpages.currentpage().getParameters().put('Id',objCss.Id);
        apexpages.currentpage().getParameters().put('Type','Link');
        BSureS_CommonInfoSection objCommonInfo2 = new BSureS_CommonInfoSection(); 
        objCommonInfo2.supplierId = objSupplier.id;
         objCommonInfo2.strSelectedBtn = 'Link';
        objCommonInfo2.strObject = 'BSureS_Credit_Status_Section__c';
        objCommonInfo2.fileName = 'TestFile'; 
        objCommonInfo2.strComments = 'Topic Comments';
        objCommonInfo2.strDoctypeid = objDocType.Id;
        objCommonInfo2.m_fileBody = blob.valueof('Attachement File');
        objCommonInfo2.strTitle ='Presentations On Education';
        objCommonInfo2.SaveDetails();
        
        apexpages.currentpage().getParameters().put('Id',objCss1.Id);
        apexpages.currentpage().getParameters().put('Type','Link');
        BSureS_CommonInfoSection objCommonInfo3 = new BSureS_CommonInfoSection(); 
        objCommonInfo3.supplierId = objSupplier.id;
         objCommonInfo3.strSelectedBtn = 'Link';
        objCommonInfo3.strObject = 'BSureS_Credit_Review_Section__c';
        objCommonInfo3.fileName = 'TestFile'; 
        objCommonInfo3.strComments = '';
        objCommonInfo3.strDoctypeid = objDocType.Id;
        objCommonInfo3.m_fileBody = blob.valueof('Attachement File');
        objCommonInfo3.strTitle ='Presentations On Education';
        objCommonInfo3.SaveDetails();
        
        objCommonInfo2.getCustomLabel();
        
        pagereference pgref1 =objCommonInfo2.showErrorMessage('test');
        
        
    }
      static testMethod void myUnitTest1() {
        
        BSure_Configuration_Settings__c cSettings4 = new BSure_Configuration_Settings__c();
        cSettings4.Name = 'SupplierFileSizeLimit';
        cSettings4.Parameter_Key__c = 'SupplierFileSizeLimit';
        cSettings4.Parameter_Value__c = '26214400';
        insert cSettings4;
        
        BSureS_Basic_Info__c objSupplier = new BSureS_Basic_Info__c(Contact_name__c='KingFisher Supplier');
        insert objSupplier;
        
        BSureS_Credit_Review_Section__c objCss = new BSureS_Credit_Review_Section__c();
        objCss.Supplier_ID__c = objSupplier.Id;
        insert objCss;
        
        system.assertEquals(objSupplier.Id,objCss.Supplier_ID__c);
        
        apexpages.currentpage().getParameters().put('Id',objCss.Id);
        apexpages.currentpage().getParameters().put('Type','Link');
        BSureS_CommonInfoSection objCommonInfo1 = new BSureS_CommonInfoSection(); 
        objCommonInfo1.strObject = 'BSureS_Credit_Review_Section__c';
        objCommonInfo1.strSelectedDocs='A,B';
        objCommonInfo1.strComments = 'Topic Comments';
        //objCommonInfo1.strDoctypeid = objDocType.Id;
        objCommonInfo1.strTitle ='Presentations On Education';
        objCommonInfo1.SaveDetails();
        objCommonInfo1.SearchResults();
        objCommonInfo1.getAttachments();
        objCommonInfo1.pageSize=10;
        pagereference pgrefnxtbtn =objCommonInfo1.nextBtnClick();
        pagereference pgrefprvbtn =objCommonInfo1.previousBtnClick();
        integer i=objCommonInfo1.getTotalPageNumber();
        integer j=1;
        objCommonInfo1.BindData(j);
        objCommonInfo1.pageData(j);
        objCommonInfo1.LastpageData(j);
        integer k=objCommonInfo1.getPageNumber();
        integer l=objCommonInfo1.getPageSize();
        boolean a=objCommonInfo1.getPreviousButtonEnabled();
        boolean b=objCommonInfo1.getNextButtonDisabled();
        pagereference pgreflastbtn =objCommonInfo1.LastbtnClick();
        pagereference pgreffrstbtn =objCommonInfo1.FirstbtnClick();
        pagereference pgref2 =objCommonInfo1.showErrorMessage('test');
      }
    
}