/***********************************************************************************************
*       Controller Name : BSureS_SupplierSearch
*       Date            : 11/29/2012 
*       Author          : B.Anupreethi
*       Purpose         : To Get the supplier search results
*       Change History  :
*       Date                      Programmer                     Reason
*       --------------------      -------------------    -------------------------
*       11/29/2012                B.Anupreethi                 Initial Version
*       01/19/2013                VINU PRATHAP              Created integer to get the page size.
**************************************************************************************************/
global with sharing class BSureS_SupplierSearch 
{
    public string strSupplierID {get;set;}//To hold Supplier ID
    
    //Anu:Link changes
    public string strSupplierName{get;set;}//To hold the supplier name
    public string strSupplierAddress{get;set;}//To hold the sup Address
    public string strSupplierCity{get;set;}//To hold the sup City
    public string strSupplierState{get;set;}//To hold the sup state
    public string strSupplierCountry{get;set;}//To hold the sup state
    public string strSAPId{get;set;}//To Hold supplier SAP id(global id)
    public string strSupplierZone{get;set;}//To Hold supplier Zone
    public string strSupplierSubZone{get;set;}//To Hold supplier Sub Zone   
    public Id LoggedInUserId{get;set;}//To hold logged in user id
    
    public list<SelectOption> lstSZone{get;set;}//to hold Zone list
    public list<SelectOption> SSubZoneList{get;set;}//to hold Sub Zone list
    public list<SelectOption> SCountriesList{get;set;}//to hold country list
    public List<SelectOption> SStateList{get;set;}//To hold Avab states
    public Transient list<BSureS_Basic_Info__c> lstSupplierInfo{get;set;}//To hold list supplier info
    public Transient list<BSureS_Basic_Info__c> lstSupInfoPagination{get;set;}//to hold list supplier info
    public integer level{set;get;} //for folder levels
    public Integer pageNumber;//used for pagination
    public Integer pageSize;//used for pagination
    public Integer totalPageNumber;//used for pagination
    public Integer PrevPageNumber {get;set;}//used for pagination
    public Integer NxtPageNumber {get;set;}//used for pagination
    public Integer NlistSize {get;set;}//used for pagination
    public Integer Endlst {get;set;}//used for pagination
    public integer totallistsize{get;set;}//total size of list SPFolderset and SPWFilesList for pagination

    /// <summary>
    /// Contructor 
    /// </summary>
    public BSureS_SupplierSearch()
    {
        lstSupInfoPagination=new list<BSureS_Basic_Info__c>();
        lstSupplierInfo = new list<BSureS_Basic_Info__c>();
        getSZone();
        getUserAttributes();
        strSupplierName=Apexpages.currentPage().getParameters().get('supName'); 
        getSupplierInfo();//to fetch the supplier info  
        
    }
    
    /// <summary>
    /// Page method used for search criteria(search for auppliers) called from a page action function
    /// </summary>
    public void Search()
    {  
        getSupplierInfo();//to get supplier info based on search
    }
    
    /// <summary>
    /// Method is used to Bind the default values of logged in users
    /// </summary>
    public void getUserAttributes()
    {
        LoggedInUserId=UserInfo.getUserId();
        //system.debug('LoggedInUserId---'+LoggedInUserId);
        list<BSureS_User_Additional_Attributes__c> lstAttributes = [SELECT User__c, BSureS_Country__c, BSureS_Sub_Zone__c, BSureS_Zone__c 
                                                                    FROM BSureS_User_Additional_Attributes__c 
                                                                    WHERE User__r.IsActive=: true AND User__c=:LoggedInUserId];
        
        //system.debug('lstAttributes---'+lstAttributes);
        if(lstAttributes.size()>0)
        {
            strSupplierZone = lstAttributes[0].BSureS_Zone__c;
            strSupplierSubZone = lstAttributes[0].BSureS_Sub_Zone__c;
            strSupplierCountry = lstAttributes[0].BSureS_Country__c;
        }
    }
    
    
    /// <summary>
    /// Method to get Zone list for Supplier search 
    /// </summary>
    public void getSZone()
    {
        BSureS_CommonUtil.isReport=false;
        lstSZone=new list<SelectOption>();
        lstSZone=BSureS_CommonUtil.getZones();
        getSubZoneList();
        //system.debug('lstSZone--'+lstSZone);
    }
    
    /// <summary>
    /// Method to get Subzone list for Supplier search 
    /// </summary>
    /// <returns>List<SelectOption></returns>
    public Pagereference getSubZoneList()
    {
        BSureS_CommonUtil.isReport=false;
        //system.debug('strSupplierZone++++++' + strSupplierZone);
        if(strSupplierZone != null)
        {
            strSupplierSubZone = 'Select';
        }
        
        SSubZoneList = new list<SelectOption>();
        SSubZoneList = BSureS_CommonUtil.getSubZones(strSupplierZone);
        getCountriesList();
        return null;     
    }
    
    /// <summary>
    /// Method to get Country list for Supplier search 
    /// </summary>
    /// <returns>List<SelectOption></returns>
    public Pagereference getCountriesList()
    {
        BSureS_CommonUtil.isReport=false;
        //system.debug('strSupplierSubZone++++++' + strSupplierSubZone);
        if(strSupplierSubZone != null)
        {
            strSupplierCountry = 'Select';
        }
        SCountriesList = new list<SelectOption>();
        SCountriesList = BSureS_CommonUtil.getCountries(strSupplierSubZone);
        //system.debug('...SCountryLst......' + SCountriesList);
        getStateList();
        return null;     
    }
    
    /// <summary> 
    /// Method to get Country list for Supplier Contact Info
    /// </summary>  
    /// <returns>List<SelectOption></returns>
    public Pagereference getStateList()
    {
        BSureS_CommonUtil.isReport=false;
        //system.debug('strSupplierCountry++++++' + strSupplierCountry);
        SStateList = new List<SelectOption>();
        SStateList = BSureS_CommonUtil.getStates(strSupplierCountry);
        //system.debug('...lstAvailableStates......' + SStateList );
        return null;     
    }
    
    /// <summary>
    /// Page method used for search criteria(search for auppliers)
    /// </summary>
    public void getSupplierInfo()
    {
        //totallistsize=0;
        //pageNumber = 0;      
        //totalPageNumber = 0;
        //system.debug('----strSupplierName---'+strSupplierName);
        lstSupplierInfo = new list<BSureS_Basic_Info__c>();
        string strQuery = 'SELECT id, State_Name__c, Country_Name__c, Supplier_Name__c, Group_Id__c, City__c, Street_Name_Address_Line_1__c,BSureS_Country__c,Globe_ID__c FROM BSureS_Basic_Info__c WHERE id!=null';
        
        if(strSupplierName!='' && strSupplierName!=null)
        {
            string strsupname='%'+strSupplierName+'%';
            strQuery += ' AND Supplier_Name__c LIKE: strsupname ';
        }
        
        //system.debug('strSupplierCountry---'+strSupplierCountry);
        
        if(strSAPId!='' && strSAPId!=null)
        {
            strQuery += ' AND Globe_ID__c =: strSAPId ';
        }
        if(strSupplierZone!='' && strSupplierZone!=null && strSupplierZone!='Select')
        {
            strQuery += ' AND Zone__c =: strSupplierZone ';
        }
        if(strSupplierSubZone!='' && strSupplierSubZone!=null && strSupplierSubZone!='Select')
        {
            strQuery += ' AND Sub_Zone__c =: strSupplierSubZone ';
        }
        if(strSupplierCountry!='' && strSupplierCountry!=null && strSupplierCountry!='Select')
        {
            strQuery += ' AND BSureS_Country__c =: strSupplierCountry ';
        }
        //system.debug('strSupplierState---'+strSupplierState);
        if(strSupplierState!='' && strSupplierState!=null && strSupplierState!='Select')
        {
            //string strSupState = '%'+strSupplierState+'%';
            strQuery += ' AND State_Province__c =: strSupplierState ';
        }
        if(strSupplierCity!='' && strSupplierCity!=null)
        {
            string strSupCity = '%'+strSupplierCity+'%';
            strQuery += ' AND City__c LIKE: strSupCity ';
        }
        strQuery += ' limit 1000';
                
        
        //strQuery +=' limit 1000';
        //system.debug('strQuery--'+strQuery);
        lstSupplierInfo = database.Query(strQuery);
        //system.debug('lstSupplierInfo---'+lstSupplierInfo);     
        
        
         totallistsize = lstSupplierInfo.size();// this size for pagination
        //lstSupDetails=supps;
        //system.debug('lstSupplierInfo^^^^^^^^^^^^^^^^^'+lstSupplierInfo.size());
        pageSize = 10;// default page size will be 10
        //system.debug('totallistsize================='+totallistsize);
        if(totallistsize==0)
        {
            BindData(0);// this method is for pagination, if no records found then passing zero to the pagination method
            NxtPageNumber=lstSupInfoPagination.size();
            PrevPageNumber=0;
        }
        else
        {
            BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
            NxtPageNumber=lstSupInfoPagination.size();
            PrevPageNumber=1;
        }
        ////system.debug('lstSupids--'+lstSupInfoPagination);
        //lstSupDetails = [SELECT Id,Name,Supplier_Name__c,Spend__c, State_Province__c,Supplier_Category_Name__c,Analyst_Name__c, Globe_ID__c,Rating__c                                                               
                     //FROM BSureS_Basic_Info__c WHERE Id IN: mapSupDetails.get(strSelectedRate) ];
        ////system.debug('lstSupids--'+lstSupInfoPagination);
        
        
        
        
        
        /*totallistsize=lstSupplierInfo.size();// this size for pagination
        
        // Changed by VINU PRATHAP
        integer varpsize = 10;// default page size will be 10
        pageSize = varpsize;
        
        //system.debug('totallistsize================='+totallistsize);
        if(totallistsize==0)
        {
            BindData(0);// this method is for pagination, if no records found then passing zero to the pagination method
            NxtPageNumber=lstSupInfoPagination.size();
            PrevPageNumber=0;
        }
        else
        {
            BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
            NxtPageNumber=lstSupInfoPagination.size();
            PrevPageNumber=1;
        }*/
    }
    
   //Pagination______
    
    //Pagenationnnnnnnnnnnnnnnnnn
    /// <summary>
    /// Below method fires when user clicks on next button of pagination.
    /// </summary>    
    /// <returns>pagereference</returns>
    public PageReference nextBtnClick() 
    {
        //system.debug('nextbutton'+pageNumber);
        BindData(pageNumber + 1);
        pageData(pageNumber + 1);
        
        return null;
    }

    // <summary>
    // Below method fires when user clicks on previous button of pagination.
    // </summary>
    // <returns>pagereference</returns>
    public PageReference previousBtnClick() 
    {
        pageData(pageNumber - 1);
        BindData(pageNumber - 1);
        return null;
    }
    
    /// <summary>
    /// Below method fires when user clicks on next button of pagination.
    /// <returns>Integer</returns>
    /// </summary>
    public Integer getPageNumber()
    {
        return pageNumber; 
    }
    
    /// <summary>
    /// Below method is for getting pagesize how many records per page .
    /// <returns>Integer</returns>  
    /// </summary> 
    public Integer getPageSize()
    {
        return pageSize;
    }

    /// <summary>
    /// Below method is for enabling and disabling the previous button of pagination.
    /// <param name="PreviousButtonEnabled"></param>
    /// <returns>boolean</returns>      
    /// </summary>
    public Boolean getPreviousButtonEnabled()
    {
        return !(pageNumber > 1);
    }

    /// <summary>
    /// Below method is for enabling and disabling the nextbutton of pagination.
    /// <param name="NextButtonDisabled"></param>
    /// <returns>boolean</returns>      
    /// </summary>
    public Boolean getNextButtonDisabled()
    {
        if (totallistsize ==0) 
            return true;
        else
            return ((pageNumber * pageSize) >= totallistsize);
    }


    /// <summary>
    /// Below method gets the total no.of pages.
    /// <param name="TotalPageNumber"></param>
    /// <returns>Integer</returns>      
    /// </summary>
    public Integer getTotalPageNumber()
    {
        totalPageNumber = 0;
        if (totalPageNumber == 0 && totallistsize!=0)
        {
            //system.debug(totalPageNumber+'-totallistsize--'+totallistsize);
            totalPageNumber = totallistsize / pageSize;
            Integer mod = totallistsize - (totalPageNumber * pageSize);
            if (mod > 0)
                totalPageNumber++;
        }


        return totalPageNumber;
    }
    
     /// <summary>
    /// Below method is the main method of pagination if have atleast one record then we pass 1 to this method otherwise zero.
    /// </summary>
    public void BindData(Integer newPageIndex)
    {
        //system.debug('*************88pavajflajf');
        try
        {
            //system.debug('----lstSupplierInfo-----++'+lstSupplierInfo);
            //lstbuyerInfoPagination=new list<BSureS_User_Additional_Attributes__c>();
            lstSupInfoPagination=new list<BSureS_Basic_Info__c>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            
            //system.debug('*************newPageIndex == '+newPageIndex); 
            //system.debug('*************totalPageNumber =='+totalPageNumber);
            //system.debug('*************pageNumber =='+pageNumber);
            //system.debug('*************pageSize =='+pageSize);
            if(pageSize == null)pageSize = 10;
            if (newPageIndex > pageNumber)
            {
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;
            }
            else
            {
                max = newPageIndex * pageSize;
                min = max - pageSize;
            }
            //system.debug('----lstSupplierInfo-----++'+lstSupplierInfo +'****'+max+'***********'+min+'***'+counter);
			   lstSupplierInfo = new list<BSureS_Basic_Info__c>();
        string strQuery = 'SELECT id, State_Name__c, Country_Name__c, Supplier_Name__c, Group_Id__c, City__c, Street_Name_Address_Line_1__c,BSureS_Country__c,Globe_ID__c FROM BSureS_Basic_Info__c WHERE id!=null';
        
        if(strSupplierName!='' && strSupplierName!=null)
        {
            string strsupname='%'+strSupplierName+'%';
            strQuery += ' AND Supplier_Name__c LIKE: strsupname ';
        }
        
        //system.debug('strSupplierCountry---'+strSupplierCountry);
        
        if(strSAPId!='' && strSAPId!=null)
        {
            strQuery += ' AND Globe_ID__c =: strSAPId ';
        }
        if(strSupplierZone!='' && strSupplierZone!=null && strSupplierZone!='Select')
        {
            strQuery += ' AND Zone__c =: strSupplierZone ';
        }
        if(strSupplierSubZone!='' && strSupplierSubZone!=null && strSupplierSubZone!='Select')
        {
            strQuery += ' AND Sub_Zone__c =: strSupplierSubZone ';
        }
        if(strSupplierCountry!='' && strSupplierCountry!=null && strSupplierCountry!='Select')
        {
            strQuery += ' AND BSureS_Country__c =: strSupplierCountry ';
        }
        //system.debug('strSupplierState---'+strSupplierState);
        if(strSupplierState!='' && strSupplierState!=null && strSupplierState!='Select')
        {
            //string strSupState = '%'+strSupplierState+'%';
            strQuery += ' AND State_Province__c =: strSupplierState ';
        }
        if(strSupplierCity!='' && strSupplierCity!=null)
        {
            string strSupCity = '%'+strSupplierCity+'%';
            strQuery += ' AND City__c LIKE: strSupCity ';
        }
        strQuery += ' limit 1000';
                
        
        //strQuery +=' limit 1000';
        //system.debug('strQuery--'+strQuery);
        lstSupplierInfo = database.Query(strQuery);
			
			
			
            if(lstSupplierInfo!=NULL)
            {
                for(BSureS_Basic_Info__c b : lstSupplierInfo)
                {
                    //system.debug('----lstSupplierInfo--b---++'+b +'****'+max+'***********'+min+'***'+counter);
                    counter++;
                    
                        if (counter > min && counter <= max) 
                        {
                            lstSupInfoPagination.add(b);// here adding files list
                        }
                }
            }
            
            pageNumber = newPageIndex;
            NlistSize=lstSupInfoPagination.size();
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }
    
    /// <summary>
    /// Below method bnds the values for Next and previous buttons.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void pageData(Integer newPageIndex)
    {   
        if (newPageIndex > pageNumber)
        {
            PrevPageNumber=PrevPageNumber+pagesize;   
            NxtPageNumber=NxtPageNumber+NlistSize; 
        }
        if (newPageIndex < pageNumber)               
        {                                 
            PrevPageNumber=PrevPageNumber-pagesize;               
            NxtPageNumber=NxtPageNumber-NlistSize;      
        }
        //system.debug('NlistSize============='+NlistSize);
    }
    
    /// <summary>
    /// Below method binds the data for last button.
    /// </summary>
    /// <param name="newPageIndex" type="Integer"></param>
    public void LastpageData(Integer newPageIndex)
    {
        try
        {
            //lstbuyerInfoPagination=new list<BSureS_User_Additional_Attributes__c>(); 
            lstSupInfoPagination=new list<BSureS_Basic_Info__c>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;          
            //system.debug('NlistSizeLastpage1============='+NlistSize);
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;    
            for(BSureS_Basic_Info__c a : lstSupplierInfo)
            {
                counter++;
                
                if (counter > min && counter <= max) 
                {
                       lstSupInfoPagination.add(a);// here adding the folders list      
                }
            }
            pageNumber = newPageIndex;
            NlistSize=lstSupInfoPagination.size();
                 
            PrevPageNumber=totallistsize - NlistSize +1;   
            NxtPageNumber=totallistsize;
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }
    
    /// <summary>
    /// Below method bnds the value for last button.
    /// </summary>
    public PageReference LastbtnClick() 
    {
        BindData(totalpagenumber - 1);
        //system.debug('Lastbutton++'+pagenumber);
        LastpageData(totalpagenumber);
        
        return null;
    }
    
    /// <summary>
    /// Below method bnds the value for first button.
    /// </summary>
    public PageReference FirstbtnClick() 
    {
        BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
        NxtPageNumber=NlistSize;
        PrevPageNumber=1;
        return null;
    } 
}