/***********************************************************************************************
* Controller Name   : BSureS_UserActivityDashboard 
* Date              : 
* Author            : Kishorekumar  
* Purpose           : Class for Supplier User Activity Login Dashboard
* Change History    : 
* Date                  Programmer              Reason
* -------------------- ------------------- -------------------------
*                                           Initial Version
**************************************************************************************************/
global with sharing class BSureS_UserActivityDashboard {
     
    public list<user> lstUser{get;set;}  
    public list<BSureS_User_Additional_Attributes__c> lstUserAdditionatt{get;set;} 
    public map<string,list<LoginHistory>> mapuserloginHistory{get;set;}
    //public BSureS_Basic_Info__c objSupplier{get;set;} // to display from and to dates with standard datepicker 
    public BSureS_Credit_Analysis__c objCA{get;set;}
    public list<LoginHistory> userloginHistory{get;set;}
    public string strZoneId{get;set;} // to store Zone Id
    public string strSubZoneId{get;set;} // to store Sub Zone Id
    public string strCountryId{get;set;}  // to store Country Id
    public list<SelectOption> zoneOptions{get;set;}  //selectOptins for list of  Zone Records
    public list<SelectOption> subZoneOptions{get;set;} //selectOptins for list of sub Zone Records   
    public list<SelectOption> coutryOptions{get;set;} // selectOptins for list of Country Records
    
    public set<string> setuserId{get;set;} 
    
    public datetime dtFromDate{get;set;} // to store from date
    public datetime dtToDate{get;set;} // to strore to Date
    public set<Id> setUserIds{get;set;}
    public list<BSureS_Credit_Review_Section__c> lstCreditReviewSection{get;set;}
    //public list<BSureS_Credit_Status_Section__c> lstCreditStatusSection{get;set;}
    public list<BSureS_Confidential_Info_Section__c> lstConfidentialInfoSection{get;set;}
    public list<BSureS_Credit_Analysis_Section__c> lstCreditAnalysisSection{get;set;}
    
    public map<string,list<BSureS_Credit_Review_Section__c>> mapCreditReviewObj{get;set;}
    //public map<string,list<BSureS_Credit_Status_Section__c>> mapCreditStatusObj{get;set;}
    public map<string,list<BSureS_Confidential_Info_Section__c>> mapConfidentialInfoObj{get;set;}
    public map<string,list<BSureS_Credit_Analysis_Section__c>> mapCreditAnalysis{get;set;}
    public map<string,Integer> mapFinal{get;set;}
    
    public integer strGapDays{get;set;} 	//Aditya 09/02/2013
    //login
    public list<userLHWrapper> lstWrapper {get;set;}
    public class userLHWrapper
    {
        public integer uloginCount;
        public string userloginmonth;
    } 
    
    //Comments
    public list<UserCommentCountWrapper> objCommentsList {get;set;}
    public class UserCommentCountWrapper
    { 
        public integer uCommentCount;
        public string  userloginmonth;
    }   
    
    //Documents
    public list<UserDocumentCountWrapper> objDocumentsList {get;set;}
    public class UserDocumentCountWrapper
    { 
        public integer uDocumentCount;
        public string  userloginmonth;
    } 
    
    public BSureS_UserActivityDashboard() 
    {
        //objSupplier = new BSureS_Basic_Info__c();
        objCA = new BSureS_Credit_Analysis__c(); 
        //Aditya V 09/02/2013
        strGapDays = Integer.valueOf(BSureS_CommonUtil.getConfigurationValues('BSureS_GapDaysForUserActivity').get(0));
        if(objCA.Actual_Review_Start_Date__c != null)
        {
            dtFromDate  = Datetime.newInstance(objCA.Actual_Review_Start_Date__c,Time.newInstance(12, 0, 0, 0));
        }
        else
        {
            dtFromDate = Datetime.now().addDays(-strGapDays);
        }
      
        if(objCA.Expected_Review_End_Date__c != null)
        {
            dtToDate    = Datetime.newInstance(objCA.Expected_Review_End_Date__c,Time.newInstance(12, 0, 0, 0));
        }
        else
        {
            dtToDate = Datetime.now();
        }
        
        SetLocation();
        getZones();
        //getSubZones();
        //getCountries(); 
        
        Search();
    }
    
    public void SetLocation()
    {
        string strUserId = Userinfo.getUserId();
        list<BSureS_User_Additional_Attributes__c> objLoginUser = [SELECT Id, User__r.Id, BSureS_Zone__c,
                                        BSureS_Sub_Zone__c, BSureS_Country__c
                                    FROM BSureS_User_Additional_Attributes__c
                                    WHERE User__r.Id =: strUserId];
                                    
        if(objLoginUser != null && objLoginUser.size() > 0)
        {
            strZoneId = objLoginUser[0].BSureS_Zone__c;
            strSubZoneId = objLoginUser[0].BSureS_Sub_Zone__c;
            strCountryId = objLoginUser[0].BSureS_Country__c;
        }
    }
    
    public void getZones()
    {  
      	BSureS_CommonUtil.isReport = true;
      	zoneOptions = new list<SelectOption>();
      	zoneOptions = BSureS_CommonUtil.getZones();
      	getSubZones();      
    }
    
    public void getSubZones()
    {     
        if(strZoneId == 'ALL')
        {
            strSubZoneId ='ALL';
            strCountryId = 'ALL';
        }
          
        BSureS_CommonUtil.isReport = true;  
        subZoneOptions = new list<SelectOption>();
        coutryOptions = new list<selectOption>();
        coutryOptions.add(new selectOption('ALL','ALL'));
        subZoneOptions  = BSureS_CommonUtil.getSubZones(strZoneId);
        getCountries();
        //Search();
    }
    
    public void getCountries()
    {            
      
      	if(strSubZoneId == 'ALL')
      	{
        	strCountryId = 'ALL';
      	}
      	BSureS_CommonUtil.isReport = true;
      	coutryOptions = new list<SelectOption>();      
      	coutryOptions = BSureS_CommonUtil.getCountries(strSubZoneId);  
      	//Search();
    } 
    public void Search()
    {
        setUserIds = new set<Id>();
        if(objCA.Actual_Review_Start_Date__c != null)
        {
            dtFromDate  = Datetime.newInstance(objCA.Actual_Review_Start_Date__c,Time.newInstance(12, 0, 0, 0));
        }
        else
        {
            dtFromDate = Datetime.now().addDays(-strGapDays);
        }
      
        if(objCA.Expected_Review_End_Date__c != null)
        {
            dtToDate    = Datetime.newInstance(objCA.Expected_Review_End_Date__c,Time.newInstance(12, 0, 0, 0));
        }
        else
        {
            dtToDate = Datetime.now();
        }
        
        lstUserAdditionatt = new list<BSureS_User_Additional_Attributes__c>();
      
        string strQuery = 'select id,User__c,User__r.Name,Zone_Name__c,BSureS_Zone__c,'+
                                'Sub_Zone_Name__c,BSureS_Sub_Zone__c,Country_Name__c,BSureS_Country__c '+ 
                                'from BSureS_User_Additional_Attributes__c where User__r.isActive = true ';
                                
        if(strZoneId != null && strZoneId != 'ALL')
        {
            strQuery +=' AND BSureS_Zone__c =: strZoneId ';
        }
        if(strSubZoneId != null &&  strSubZoneId != 'ALL' )
        {
            strQuery +=' AND BSureS_Sub_Zone__c =: strSubZoneId ';
        }
        if(strCountryId != null && strCountryId != 'ALL'  )
        {
            strQuery +=' AND BSureS_Country__c =: strCountryId '; 
        }   
        //system.debug('strQuery============'+strQuery);
                       
        lstUserAdditionatt= Database.Query(strQuery);
        if(lstUserAdditionatt != null && lstUserAdditionatt.size()>0)
        {
            for(BSureS_User_Additional_Attributes__c objUAatt :lstUserAdditionatt )
            {
                
                setUserIds.add(objUAatt.User__c);
            }
        }
        //system.debug('setUserIds..............'+setUserIds);
        //system.debug('setUserIds====='+setUserIds.size());
        
        getUserLogin(setUserIds);
        userLoginCommentsCount(setUserIds);
        userLoginDocumentsCount(setUserIds); 
    }
    public void getUserLogin(set<id> setUserIds) 
    {
        lstWrapper=new list<userLHWrapper> ();
        list<LoginHistory> lstlHistory = new list<LoginHistory>();
        mapuserloginHistory = new map<string,list<LoginHistory>>();
        
        //system.debug('dtFromDate=========='+dtFromDate);
        //system.debug('dtToDate==========='+dtToDate);
        
        string strLHQuery = 'select UserId,LoginTime FROM LoginHistory where UserId != null';
        
         if(setuserIds != null && setuserIds.size() > 0)
         {
            strLHQuery += ' AND UserId IN : setuserIds';
         }
         if(dtFromDate != null)
         {
            strLHQuery += ' AND LoginTime >=: dtFromDate';
         }  
         if(dtToDate != null)
         {
            strLHQuery += ' AND LoginTime <=: dtToDate ';
         }   
         if(setuserIds.size() == 0)
         {
            strLHQuery += ' LIMIT 0';
         }
                           
         userloginHistory = Database.query(strLHQuery);
         
         //system.debug('userloginHistory====size==='+userloginHistory.size());
         
         for(LoginHistory objLoginHistory: userloginHistory)     
         {
             String monthH = String.valueOf(objLoginHistory.LoginTime.month());
             string yearH = string.valueOf(objLoginHistory.LoginTime.year());
             ////system.debug('month from date======='+ objLoginHistory.LoginTime.month());
             ////system.debug('month from date======='+ objLoginHistory.LoginTime.year());    
             string strHistory = yearH +'-'+ monthH;
             ////system.debug('strHistory========'+strHistory);
             
             if(mapuserloginHistory.containsKey(strHistory))
             {
                lstlHistory = mapuserloginHistory.get(strHistory);
                lstlHistory.add(objLoginHistory);
                mapuserloginHistory.put(strHistory,lstlHistory);
             }
             else
             {
                lstlHistory = new list<LoginHistory>();
                lstlHistory.add(objLoginHistory);
                mapuserloginHistory.put(strHistory,lstlHistory);
             }
        }
       /* list<LoginHistory> lst = new list<LoginHistory>();
        lst.add(new LoginHistory());
        mapuserloginHistory.put('2013-1',lst); */      
        set<string> setmapdata = mapuserloginHistory.keyset();
        list<string> lstlogindata = new list<string>();
        lstlogindata.addall(setmapdata);
        lstlogindata.sort();
        boolean isDummy = false;
         //for(string u : mapuserloginHistory.keyset())
        //system.debug('lstlogindata..............'+lstlogindata);
        
        for(string u : lstlogindata)
        { 
            userLHWrapper wObj= new userLHWrapper();          
            //system.debug('mapuserloginHistory..............'+mapuserloginHistory.get(u).size());
            //system.debug('u..........'+u);
            wObj.uloginCount = mapuserloginHistory.get(u).size(); 
            //wObj.uloginCount =  lstlogindata.size(); 
            wObj.userloginmonth = u;
            lstWrapper.add(wObj);
        } 
        //system.debug('lstWrapper..........'+lstWrapper);
        
        /*
        userLHWrapper wObj= new userLHWrapper();
        wObj.uloginCount = 0;
        //wObj.uloginCount =  lstlogindata.size(); 
        wObj.userloginmonth = '';
        lstWrapper.add(wObj);
        */
    }
    public void userLoginCommentsCount(set<id> setUserIds)
    { 
        string month='';
        string year='';
        string Month_Year;
        mapCreditReviewObj = new map<string,list<BSureS_Credit_Review_Section__c>>();
        //mapCreditStatusObj = new map<string,list<BSureS_Credit_Status_Section__c>>();
        mapConfidentialInfoObj = new map<string,list<BSureS_Confidential_Info_Section__c>>();
        mapCreditAnalysis = new map<string,list<BSureS_Credit_Analysis_Section__c>>();
        objCommentsList = new list<UserCommentCountWrapper>();
        mapFinal = new map<string,Integer>();
        
        
        
        //system.debug('setUserIds=====comments========'+setUserIds);
        string strQueryCreditReview = 'select id,CreatedById,CreatedDate,DiscussionType__c from BSureS_Credit_Review_Section__c'+
                                        ' where DiscussionType__c=\'Topic\' AND CreatedById IN : setUserIds';
        /*if(setUserIds != null && setUserIds.size() >0)
        {
            strQueryCreditReview += ' AND CreatedById IN : setUserIds';
        }*/
        if(dtFromDate != null)
        {   
            
            strQueryCreditReview += ' AND CreatedDate >=: dtFromDate';
        }
        if(dtToDate != null)
        {           
            strQueryCreditReview += ' AND CreatedDate <=:dtToDate';
        }
        
        lstCreditReviewSection = Database.query(strQueryCreditReview);
        list<BSureS_Credit_Review_Section__c> lstCreditReview = new list<BSureS_Credit_Review_Section__c>();
        if(lstCreditReviewSection != null && lstCreditReviewSection.size() > 0)
        {
            for(BSureS_Credit_Review_Section__c objCreditReview: lstCreditReviewSection)
            {
                month = String.valueOf(objCreditReview.CreatedDate.month());
                year = String.valueOf(objCreditReview.CreatedDate.year());
                Month_Year = year+'-'+month;
                if(mapCreditReviewObj.containskey(Month_Year))
                {
                    lstCreditReview = mapCreditReviewObj.get(Month_Year);
                    lstCreditReview.add(objCreditReview);
                    mapCreditReviewObj.put(Month_Year,lstCreditReview);
                }
                else
                {
                    lstCreditReview = new list<BSureS_Credit_Review_Section__c>();
                    lstCreditReview.add(objCreditReview);
                    mapCreditReviewObj.put(Month_Year,lstCreditReview);
                }
                
            }
        }    
       //  BSureS_Confidential_Info_Section__c   
        string strQueryConfidentialinfo = 'select id,CreatedById,CreatedDate,DiscussionType__c from BSureS_Confidential_Info_Section__c'+
                                            ' where DiscussionType__c=\'Topic\' AND CreatedById IN :'+'setUserIds';
        /*if(setUserIds != null && setUserIds.size() >0)
        {
            strQueryConfidentialinfo += ' AND CreatedById IN :'+'setUserIds';
        }*/
        if(dtFromDate != null)
        {
            strQueryConfidentialinfo += ' AND CreatedDate >=: dtFromDate';
        }
        if(dtToDate != null)
        {
            strQueryConfidentialinfo += ' AND CreatedDate <=:dtToDate';
        }
        lstConfidentialInfoSection = Database.query(strQueryConfidentialinfo);
       
        list<BSureS_Confidential_Info_Section__c> lstConfidentialInfo = new list<BSureS_Confidential_Info_Section__c>();
        if(lstConfidentialInfoSection != null && lstConfidentialInfoSection.size() >0 )
        {
            for(BSureS_Confidential_Info_Section__c objConfidentialInfo : lstConfidentialInfoSection)
            {
                month = String.valueOf(objConfidentialInfo.CreatedDate.month());
                year = String.valueOf(objConfidentialInfo.CreatedDate.year());
                Month_Year = year+'-'+month;
                if(mapConfidentialInfoObj.containskey(Month_Year))
                {
                    lstConfidentialInfo = mapConfidentialInfoObj.get(Month_Year);
                    lstConfidentialInfo.add(objConfidentialInfo);
                    mapConfidentialInfoObj.put(Month_Year,lstConfidentialInfo);
                }
                else
                {
                    lstConfidentialInfo = new list<BSureS_Confidential_Info_Section__c>();
                    lstConfidentialInfo.add(objConfidentialInfo);
                    mapConfidentialInfoObj.put(Month_Year,lstConfidentialInfo);
                }
            }
        }  
        
        //perform Credit Analysis
        string strQueryCreditAnalysis = 'select id,CreatedById,CreatedDate,DiscussionType__c from BSureS_Credit_Analysis_Section__c'+
                                            ' where DiscussionType__c=\'Topic\' AND CreatedById IN :'+'setUserIds';
        if(dtFromDate != null)
        {
            strQueryCreditAnalysis += ' AND CreatedDate >=: dtFromDate';
        }
        if(dtToDate != null)
        {
            strQueryCreditAnalysis += ' AND CreatedDate <=:dtToDate';
        }                          
        lstCreditAnalysisSection = Database.query(strQueryCreditAnalysis);
        list<BSureS_Credit_Analysis_Section__c> lstCreditAnalysis = new list<BSureS_Credit_Analysis_Section__c>();
        if(lstCreditAnalysisSection != null && lstCreditAnalysisSection.size() > 0)
        {
            for(BSureS_Credit_Analysis_Section__c ObjCreditAnalysis : lstCreditAnalysisSection)
            {
                month = String.valueOf(ObjCreditAnalysis.CreatedDate.month());
                year = String.valueOf(ObjCreditAnalysis.CreatedDate.year());
                Month_Year = year+'-'+month;
                if(mapCreditAnalysis.containskey(Month_Year))
                {
                    lstCreditAnalysis = mapCreditAnalysis.get(Month_Year);
                    lstCreditAnalysis.add(ObjCreditAnalysis);
                    mapCreditAnalysis.put(Month_Year,lstCreditAnalysis);
                }
                else
                {
                    lstCreditAnalysis = new list<BSureS_Credit_Analysis_Section__c>();
                    lstCreditAnalysis.add(ObjCreditAnalysis);
                    mapCreditAnalysis.put(Month_Year,lstCreditAnalysis);
                }
            }
            
        }   
        
          
        for(string strTime : mapCreditReviewObj.keySet())
        {
            if(mapFinal.containsKey(strTime))
            {
                integer count1 = mapFinal.get(strTime);
                count1 += mapCreditReviewObj.get(strTime).size();
                mapFinal.put(strTime,count1);
            }
            else
            {
                integer count1 = 0;
                count1 = mapCreditReviewObj.get(strTime).size();
                mapFinal.put(strTime,count1);
            }
        }
        
        for(string strTime : mapConfidentialInfoObj.keySet())
        {
            if(mapFinal.containsKey(strTime))
            {
                integer count1 = mapFinal.get(strTime);
                count1 += mapConfidentialInfoObj.get(strTime).size();
                mapFinal.put(strTime,count1);
            }
            else
            {
                integer count1 = 0;
                count1 = mapConfidentialInfoObj.get(strTime).size();
                mapFinal.put(strTime,count1);
            }
        }
        
        for(string strTime : mapCreditAnalysis.keySet())
        {
            if(mapFinal.containsKey(strTime))
            {
                integer count1 = mapFinal.get(strTime);
                count1 += mapCreditAnalysis.get(strTime).size();
                mapFinal.put(strTime,count1);
            }
            else
            {
                integer count1 = 0;
                count1 = mapCreditAnalysis.get(strTime).size();
                mapFinal.put(strTime,count1);
            }
        }
        
        set<string> setMapFinal = mapFinal.keySet();
        list<string> lstmapfinal = new list<string>();
        lstmapfinal.addall(setMapFinal);
        lstmapfinal.sort();
        //system.debug('setUserIds=====size=='+setUserIds.size());
        for(string strTimeFinal : lstmapfinal)
        {
            UserCommentCountWrapper objNew = new UserCommentCountWrapper();
            objNew.uCommentCount = mapFinal.get(strTimeFinal);
            objNew.userloginmonth = strTimeFinal;
            objCommentsList.add(objNew);
        }
                                                        
    } 
    public void userLoginDocumentsCount(set<id> setUserIds)
    {
        string month='';
        string year='';
        string Month_Year;
        mapCreditReviewObj = new map<string,list<BSureS_Credit_Review_Section__c>>();
        //mapCreditStatusObj = new map<string,list<BSureS_Credit_Status_Section__c>>();
        mapConfidentialInfoObj = new map<string,list<BSureS_Confidential_Info_Section__c>>();
        mapCreditAnalysis = new map<string,list<BSureS_Credit_Analysis_Section__c>>();
        objDocumentsList = new list<UserDocumentCountWrapper>();
        mapFinal = new map<string,Integer>();
       
        string strQueryCreditReview = 'select id,CreatedById,CreatedDate,DiscussionType__c from BSureS_Credit_Review_Section__c'+
                                        ' where DiscussionType__c=\'File\' AND CreatedById IN : setUserIds';
        /*if(setUserIds != null && setUserIds.size() >0)
        {
            strQueryCreditReview += ' AND CreatedById IN : setUserIds';
        }*/
        if(dtFromDate != null)
        {
            strQueryCreditReview += ' AND CreatedDate >=: dtFromDate';
        }
        if(dtToDate != null)
        {
            strQueryCreditReview += ' AND CreatedDate <=:dtToDate';
        }
        //system.debug('strQueryCreditReview============2====='+strQueryCreditReview);
        
        lstCreditReviewSection = Database.query(strQueryCreditReview);
        list<BSureS_Credit_Review_Section__c> lstCreditReview = new list<BSureS_Credit_Review_Section__c>();
        if(lstCreditReviewSection != null && lstCreditReviewSection.size() > 0)
        {
            for(BSureS_Credit_Review_Section__c objCreditReview: lstCreditReviewSection)
            {
                month = String.valueOf(objCreditReview.CreatedDate.month());
                year = String.valueOf(objCreditReview.CreatedDate.year());
                Month_Year = year+'-'+month;
                if(mapCreditReviewObj.containskey(Month_Year))
                {
                    lstCreditReview = mapCreditReviewObj.get(Month_Year);
                    lstCreditReview.add(objCreditReview);
                    mapCreditReviewObj.put(Month_Year,lstCreditReview);
                }
                else
                {
                    lstCreditReview = new list<BSureS_Credit_Review_Section__c>();
                    lstCreditReview.add(objCreditReview);
                    mapCreditReviewObj.put(Month_Year,lstCreditReview);
                }
                
            }
        }    
            
        string strQueryConfidentialinfo = 'select id,CreatedById,CreatedDate,DiscussionType__c from BSureS_Confidential_Info_Section__c'+
                                            ' where DiscussionType__c=\'File\' AND CreatedById IN :'+'setUserIds';
        /*if(setUserIds != null && setUserIds.size() >0)
        {
            strQueryConfidentialinfo += ' AND CreatedById IN :'+'setUserIds';
        }*/
        if(dtFromDate != null)
        {
            strQueryConfidentialinfo += ' AND CreatedDate >=: dtFromDate';
        }
        if(dtToDate != null)
        {
            strQueryConfidentialinfo += ' AND CreatedDate <=:dtToDate';
        }
        lstConfidentialInfoSection = Database.query(strQueryConfidentialinfo);
        //lstConfidentialInfoSection = [select id,CreatedById,DiscussionType__c from BSureS_Confidential_Info_Section__c
        //                              where DiscussionType__c=:'File'];
        list<BSureS_Confidential_Info_Section__c> lstConfidentialInfo = new list<BSureS_Confidential_Info_Section__c>();
        if(lstConfidentialInfoSection != null && lstConfidentialInfoSection.size() >0 )
        {
            for(BSureS_Confidential_Info_Section__c objConfidentialInfo : lstConfidentialInfoSection)
            {
                month = String.valueOf(objConfidentialInfo.CreatedDate.month());
                year = String.valueOf(objConfidentialInfo.CreatedDate.year());
                Month_Year = year+'-'+month;
                if(mapConfidentialInfoObj.containskey(Month_Year))
                {
                    lstConfidentialInfo = mapConfidentialInfoObj.get(Month_Year);
                    lstConfidentialInfo.add(objConfidentialInfo);
                    mapConfidentialInfoObj.put(Month_Year,lstConfidentialInfo);
                }
                else
                {
                    lstConfidentialInfo = new list<BSureS_Confidential_Info_Section__c>();
                    lstConfidentialInfo.add(objConfidentialInfo);
                    mapConfidentialInfoObj.put(Month_Year,lstConfidentialInfo);
                }
            }
        }   
        
        
        //perform Credit Analysis
        string strQueryCreditAnalysis = 'select id,CreatedById,CreatedDate,DiscussionType__c from BSureS_Credit_Analysis_Section__c'+
                                            ' where DiscussionType__c=\'File\' AND CreatedById IN :'+'setUserIds';
        if(dtFromDate != null)
        {
            strQueryCreditAnalysis += ' AND CreatedDate >=: dtFromDate';
        }
        if(dtToDate != null)
        {
            strQueryCreditAnalysis += ' AND CreatedDate <=:dtToDate';
        }                          
        lstCreditAnalysisSection = Database.query(strQueryCreditAnalysis);
        list<BSureS_Credit_Analysis_Section__c> lstCreditAnalysis = new list<BSureS_Credit_Analysis_Section__c>();
        if(lstCreditAnalysisSection != null && lstCreditAnalysisSection.size() > 0)
        {
            for(BSureS_Credit_Analysis_Section__c ObjCreditAnalysis : lstCreditAnalysisSection)
            {
                month = String.valueOf(ObjCreditAnalysis.CreatedDate.month());
                year = String.valueOf(ObjCreditAnalysis.CreatedDate.year());
                Month_Year = year+'-'+month;
                if(mapCreditAnalysis.containskey(Month_Year))
                {
                    lstCreditAnalysis = mapCreditAnalysis.get(Month_Year);
                    lstCreditAnalysis.add(ObjCreditAnalysis);
                    mapCreditAnalysis.put(Month_Year,lstCreditAnalysis);
                }
                else
                {
                    lstCreditAnalysis = new list<BSureS_Credit_Analysis_Section__c>();
                    lstCreditAnalysis.add(ObjCreditAnalysis);
                    mapCreditAnalysis.put(Month_Year,lstCreditAnalysis);
                }
            }
            
        }       
        for(string strTime : mapCreditReviewObj.keySet())
        {
            if(mapFinal.containsKey(strTime))
            {
                integer count1 = mapFinal.get(strTime);
                count1 += mapCreditReviewObj.get(strTime).size();
                mapFinal.put(strTime,count1);
            }
            else
            {
                integer count1 = 0;
                count1 = mapCreditReviewObj.get(strTime).size();
                mapFinal.put(strTime,count1);
            }
        }
        
        for(string strTime : mapConfidentialInfoObj.keySet())
        {
            if(mapFinal.containsKey(strTime))
            {
                integer count1 = mapFinal.get(strTime);
                count1 += mapConfidentialInfoObj.get(strTime).size();
                mapFinal.put(strTime,count1);
            }
            else
            {
                integer count1 = 0;
                count1 = mapConfidentialInfoObj.get(strTime).size();
                //system.debug('count1============'+count1);
                mapFinal.put(strTime,count1);
            }
        }
        for(string strTime : mapCreditAnalysis.keySet())
        {
            if(mapFinal.containsKey(strTime))
            {
                integer count1 = mapFinal.get(strTime);
                count1 += mapCreditAnalysis.get(strTime).size();
                mapFinal.put(strTime,count1);
            }
            else
            {
                integer count1 = 0;
                count1 = mapCreditAnalysis.get(strTime).size();
                //system.debug('count1============'+count1);
                mapFinal.put(strTime,count1);
            }
        }
        set<string> setMapFinal = mapFinal.keySet();
        list<string> lstmapfinal = new list<string>();
        lstmapfinal.addall(setMapFinal); 
        lstmapfinal.sort();
        //system.debug('setUserIds=====size=='+setUserIds.size());
        for(string strTimeFinal : lstmapfinal)
        {
            UserDocumentCountWrapper objNew = new UserDocumentCountWrapper();
            objNew.uDocumentCount = mapFinal.get(strTimeFinal);
            objNew.userloginmonth = strTimeFinal;
            objDocumentsList.add(objNew);
        }
                                                        
    } 

}