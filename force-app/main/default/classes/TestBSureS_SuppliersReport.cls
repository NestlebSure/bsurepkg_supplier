/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBSureS_SuppliersReport {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        BSureS_Basic_Info__c objSupplier = new BSureS_Basic_Info__c(Contact_name__c='KingFisher');
        insert objSupplier;
        
        BSureS_Zone__c Bzone = new BSureS_Zone__c(Name ='Test Supplier Zone');
        insert Bzone;  
        
        Bzone = [select id,Name from BSureS_Zone__c where id=:Bzone.Id];
        BSureS_SubZone__c BSzone = new BSureS_SubZone__c();
        BSzone.Name = 'Test Supplier Sub Zone';
        BSzone.ZoneID__c = Bzone.Id;
        insert BSzone;
        System.assertEquals('Test Supplier Sub Zone',BSzone.Name);
        BSzone= [select id,Name from BSureS_SubZone__c where id=:BSzone.Id];
        
        BSureS_Country__c BsCountry = new BSureS_Country__c(Name = 'Test Supplier Country',Sub_Zone_ID__c=BSzone.Id);
        insert BsCountry;
        BsCountry = [select Id,Name from BSureS_Country__c  where id=:BsCountry.Id];
        BSureS_SuppliersReport BSupplier = new BSureS_SuppliersReport();
        
        system.Test.startTest();
        BSupplier.pageSize=10;
        pagereference pgrefnxtbtn = BSupplier.nextBtnClick();
        pagereference pgrefprvbtn = BSupplier.previousBtnClick();
        integer i=BSupplier.getTotalPageNumber();
        integer j=1;
        BSupplier.BindData(j);
        BSupplier.pageData(j);
        BSupplier.LastpageData(j);
        integer k=BSupplier.getPageNumber();
        integer l=BSupplier.getPageSize();
        boolean a=BSupplier.getPreviousButtonEnabled();
        boolean b=BSupplier.getNextButtonDisabled();
        pagereference pgreflastbtn =BSupplier.LastbtnClick();
        pagereference pgreffrstbtn =BSupplier.FirstbtnClick();
        BSupplier.sort();
        BSupplier.Cancel();
        BSupplier.ExportToExcel();
        system.Test.stopTest();
        
        
        
    }
}