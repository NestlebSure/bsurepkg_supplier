/************************************************************************************************       
Controller Name         : BSureS_SupplierBulkUploadupdate       
Date                    : 03/28/2013        
Author                  : B.Anupreethi       
Purpose                 : This trigger is used to update the supplier information      
Change History          : 
Date                        Programmer                     Reason       
--------------------      -------------------    -------------------------       
03/28/2013                  B.Anupreethi           Initial Version
07/18/2013					Aditya V				Included Planned Review Date Validation
**************************************************************************************************/
trigger BSureS_SupplierBulkUploadupdate on BSureS_Basic_Info_Stage__c (before update) 
{
    public boolean isSuppStageUpdate {get; set; }
    //public string strOrgException {get;set;}
     
    isSuppStageUpdate = BSureS_SupplierInfoIntermediate.isSupplierStageUpdate;
    //system.debug('--isSuppStageUpdate--'+isSuppStageUpdate);
    BSureS_Basic_Info_Stage__c Sup_info_stage = new BSureS_Basic_Info_Stage__c();
    if(isSuppStageUpdate==null || !isSuppStageUpdate)
    {
        if( Trigger.isBefore )
        {
            if( Trigger.isUpdate )
            {
                for(BSureS_Basic_Info_Stage__c Sup_infostage:trigger.new)
                {
                    Sup_info_stage = Sup_infostage;
                }
        
                
                string strOrgException = Sup_info_stage.Exception__c;
                //system.debug('--Sup_info_stage.Exception__c---'+Sup_info_stage.Exception__c);
                //system.debug('--Sup_info_stage.FINANCIAL_INFORMATION__C---'+Sup_info_stage.FINANCIAL_INFORMATION__C);
                
                if(Sup_info_stage.SUPPLIER_NAME__C != null && Sup_info_stage.SUPPLIER_NAME__C != '')
                {
                    if(strOrgException!=null && strOrgException.contains('Exception: SUPPLIER_NAME__C is Mandatory'))
                    {
                        strOrgException = strOrgException.replace('Exception: SUPPLIER_NAME__C is Mandatory', '');
                    }
                    
                }                           
                if(Sup_info_stage.City__c != null && Sup_info_stage.City__c != '')
                {
                    if(strOrgException!=null && strOrgException.contains('Exception: CITY__C is Mandatory'))
                    {
                        strOrgException = strOrgException.replace('Exception: CITY__C is Mandatory', '');
                    }
                }
                                    
                if(Sup_info_stage.STREET_NAME_ADDRESS_LINE_1__C != null && Sup_info_stage.STREET_NAME_ADDRESS_LINE_1__C != '')
                {
                    if(strOrgException!=null && strOrgException.contains('Exception: STREET_NAME_ADDRESS_LINE_1__C is Mandatory'))
                    {
                        strOrgException = strOrgException.replace('Exception: STREET_NAME_ADDRESS_LINE_1__C is Mandatory', '');
                    }
                }
                
                if(Sup_info_stage.Planned_Review_Date__c != null && Sup_info_stage.Planned_Review_Date__c > system.today())
                {
                    if(strOrgException!=null && strOrgException.contains('Exception: PLANNED_REVIEW_DATE__C should be future date.'))
                    {
                        strOrgException = strOrgException.replace('Exception: PLANNED_REVIEW_DATE__C should be future date.', '');
                    }
                }
                
                if(Sup_info_stage.FINANCIAL_INFORMATION__C != null && Sup_info_stage.FINANCIAL_INFORMATION__C != '')
                {
                    if(strOrgException!=null && strOrgException.contains('Exception: FINANCIAL_INFORMATION__C is Mandatory'))
                    {
                        strOrgException = strOrgException.replace('Exception: FINANCIAL_INFORMATION__C is Mandatory', '');
                    }
                }
                
                integer cntSup = [select count() from BSureS_Basic_Info__c where SUPPLIER_NAME__C=:Sup_info_stage.SUPPLIER_NAME__C AND State_Province__c=:Sup_info_stage.State_Province__c AND CITY__C=:Sup_info_stage.CITY__C];
                if(cntSup>0)
                {
                    if(strOrgException==NULL)
                    {
                        strOrgException=Label.BSureS_VNameCityState;
                    }
                    else
                    {
                        strOrgException+=','+Label.BSureS_VNameCityState;
                    }
                }
                else
                {
                    if(strOrgException!=null && strOrgException.contains(Label.BSureS_VNameCityState))
                    {
                        system.debug('asasas--bbb--');
                        strOrgException = strOrgException.replace(Label.BSureS_VNameCityState, '');
                    }
                }
                strOrgException = strOrgException.replace(',','');
                system.debug('strOrgException--'+strOrgException);
                if(strOrgException!=null && strOrgException!='')
                {
                    system.debug('strOrgException-in-'+strOrgException);
                    Sup_info_stage.Exception__c = strOrgException;
                    Sup_info_stage.Status__c='Exception';
                }
                else
                {
                    Sup_info_stage.Exception__c = NULL;
                    Sup_info_stage.Status__c='Initial Upload';
                }
                
            }
        }
    }
}