/***********************************************************************************************
*       Trigger Name    : BSureS_ZoneTrigger
*       Date            : 
*       Author          : 
*       Purpose         : 
*       Change History  :
*       Date                      Programmer                     Reason
*       --------------------      -------------------    -------------------------
*                                                         Initial Version
        01/04/2013                  Veereandranath.j      To avoid soql query inside loops      
**************************************************************************************************/
trigger BSureS_DocumentTypeTrigger on BSureS_Document_Type__c (before insert, before update) 
{
  /*  for(BSureS_Document_Type__c objDocType : trigger.new)
    {
        if(trigger.isInsert)
        {
            Integer intCount = [select count() from BSureS_Document_Type__c 
                                where Document_Type_Name__c =: objDocType.Document_Type_Name__c];
            if(intCount > 0)
            {
                objDocType.addError('Record already exists with the same Document Type Name');
            }
            if(trigger.isAfter)
            {
                objDocType.addError('Record Saved Successfully!');
            } 
        }
        if(trigger.isUpdate)
        {
            Integer intCount = [select count() from BSureS_Document_Type__c 
                                where Document_Type_Name__c =: objDocType.Document_Type_Name__c 
                                and Id != : objDocType.Id];
            if(intCount > 0)
            {
                objDocType.addError('Record already exists with the same Document Type Name');
            }
        }
    }   */
    list<string> DocNames = new list<string>();
    set<id> setDocIds = new set<Id>();
    
    list<BSureS_Document_Type__c> lstDocs = new list<BSureS_Document_Type__c>();
    
    for(BSureS_Document_Type__c objDocType : trigger.new)
    {   
        DocNames.add(objDocType.Document_Type_Name__c);
        setDocIds.add(objDocType.Id);
    }
    lstDocs  = [SELECT id,Document_Type_Name__c FROM BSureS_Document_Type__c 
                                WHERE Document_Type_Name__c =: DocNames ];
                                
                             
     for(BSureS_Document_Type__c ExistD:lstDocs)
     {
        for(BSureS_Document_Type__c doc:Trigger.New){
            if(ExistD.Document_Type_Name__c == doc.Document_Type_Name__c && doc.Id != ExistD.Id ) 
             doc.addError('Record already exists with the same DocumentType Name');
        }
     } 
    
}