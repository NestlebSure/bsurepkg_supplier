/***********************************************************************************************
* Trigger Name      : BSureS_BuyerSharing
* Date              : 21/02/2013
* Author            : KishoreKumar A
* Purpose           : Trigger to Buyers can view Assigned Supplier Information.
* Change History    : 
* Date                  Programmer              Reason
* -------------------- ------------------- -------------------------
* 21/02/2013            KishoreKumar A         Initial Version
**************************************************************************************************/
trigger BSureS_BuyerSharing on BSureS_Assigned_Buyers__c (before insert, before update,after delete) 
{
    list<BSureS_Basic_Info__Share> lstBuyershare = new list<BSureS_Basic_Info__Share>();
    //list<BSureS_BuyersShare__c> lstCustmStng = BSureS_BuyersShare__c.getAll().values();
    String strBuyerShare = BSureS_CommonUtil.getConfigurationValues('BSureS_BuyerShare').get(0);
    system.debug('strBuyerShare========='+strBuyerShare);
    //if(lstCustmStng.get(0).Name == 'FALSE')
    string strBuyerId ='';
    string strSupplierId ='';
    if(strBuyerShare.equalsIgnoreCase('FALSE'))
    {
        if(Trigger.isInsert )
        {
            for(BSureS_Assigned_Buyers__c bObj:trigger.new)
            {
                BSureS_Basic_Info__Share shareObj = new  BSureS_Basic_Info__Share();
                shareObj.AccessLevel = 'Edit';
                shareObj.ParentId = bObj.Supplier_ID__c;
                shareObj.UserOrGroupId = bObj.Buyer_Id__c;
                lstBuyershare.add(shareObj);
            }
        }
        if(Trigger.isUpdate)
        {
            system.debug('after update===========');
            for(BSureS_Assigned_Buyers__c bObj:trigger.new)
            {
                BSureS_Basic_Info__Share shareObj = new  BSureS_Basic_Info__Share();
                shareObj.AccessLevel = 'Edit';
                shareObj.ParentId = bObj.Supplier_ID__c;
                shareObj.UserOrGroupId = bObj.Buyer_Id__c;
                system.debug('shareObj============='+shareObj);
                lstBuyershare.add(shareObj);
            }
        }   
        system.debug('lstBuyershare=========='+lstBuyershare);
        if(Trigger.isDelete)
        {
            list<BSureS_Basic_Info__Share> lstShareDelete;
            for(BSureS_Assigned_Buyers__c bObj:trigger.old)
            {
                if(bObj.Buyer_Id__c != null)
                {
                    strBuyerId = bObj.Buyer_Id__c;
                }    
                if(bObj.Supplier_ID__c != null)
                {
                    strSupplierId = bObj.Supplier_ID__c;
                }
                /*
                if(bObj.Buyer_Id__c != null)
                {
                    lstShareDelete = new list<BSureS_Basic_Info__Share>([select id from BSureS_Basic_Info__Share where UserOrGroupId =:bObj.Buyer_Id__c and ParentId =: bObj.Supplier_ID__c]);
                    if(!lstShareDelete.isEmpty())
                    {
                        Database.delete(lstShareDelete,false);
                    }
                }  
                */ 
            }
            if( (strBuyerId != null && strBuyerId != '') && (strSupplierId != null && strSupplierId != ''))
            {
                lstShareDelete = new list<BSureS_Basic_Info__Share>([select id from BSureS_Basic_Info__Share where UserOrGroupId =:strBuyerId and ParentId =: strSupplierId]);
            }
            if(!lstShareDelete.isEmpty())
            {
                Database.delete(lstShareDelete,false);
            }
        }
        Database.upsert(lstBuyershare,false);
    }   
}