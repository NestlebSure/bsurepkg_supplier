/***********************************************************************************************
*       Trigger Name    : BSureS_CountryManagerTrigger
*       Date            : 27/11/2012 
*       Author          : Kishorekumar A
*       Purpose         : 
*       Change History  :
*       Date                      Programmer                     Reason
*       --------------------      -------------------    -------------------------
*       27/11/2012               Kishorekumar A                Initial Version
* 		01/04/2013				B.Anupreethi			Modified to remove sql query from for loop
**************************************************************************************************/
trigger BSureS_CountryManagerTrigger on BSureS_Country_Manager__c (before insert,before update) 
{
	Set<ID> CM = new Set<ID>();
    Set<ID> setCountry =new Set<ID>();
    Set<ID> CountryManID = new set<ID>();
           
    if(trigger.IsBefore){
    	
    	for(BSureS_Country_Manager__c countryManagerObj:trigger.new)
        {   
        	setCountry.add(countryManagerObj.Country__c);
            CM.add(countryManagerObj.Country_Manager__c);
            CountryManID.add(countryManagerObj.Id);
        } 
        
        list<BSureS_Country_Manager__c> LstCountryMan = [select Id,Country_Manager__c,Country__c 
        											 from BSureS_Country_Manager__c 
        											 where Country__c =: setCountry 
        											 and Country_Manager__c =:CM  
        											 and Id !=:CountryManID];
        											 
         for(BSureS_Country_Manager__c ExistC:LstCountryMan)
        {
            for(BSureS_Country_Manager__c Cat:Trigger.New){
                if(ExistC.Country_Manager__c == Cat.Country_Manager__c )
                Cat.addError('Record already exists with the same Country Name and Country Manager');
            }
        }											                                          
 
    }
	
	/*for(BSureS_Country_Manager__c countryManagerObj:trigger.new)
	{
		if(trigger.isInsert)
		{
			Integer countryManagerRecCount = [select count() from BSureS_Country_Manager__c where Country__c =: countryManagerObj.Country__c and Country_Manager__c =:countryManagerObj.Country_Manager__c];
			if(countryManagerRecCount > 0 )
			{
				countryManagerObj.addError('Record already Exists with the same Country Name and Country Manager');
			}
			if(countryManagerObj.Country_Manager__c == null)
			{
				countryManagerObj.addError('Please Select Country Manager.');
			}
		}
		if(trigger.isUpdate)
		{
			Integer countryManagerRecCount = [select count() from BSureS_Country_Manager__c where Country__c =: countryManagerObj.Country__c and Country_Manager__c =:countryManagerObj.Country_Manager__c and id !=:countryManagerObj.id];
			if(countryManagerRecCount > 0 )
			{
				countryManagerObj.addError('Record already Exists with the same Country Name and Country Manager');
			}
			if(countryManagerObj.Country_Manager__c == null)
			{
				countryManagerObj.addError('Please Select Country Manager.');
			}
		}
	}*/
}