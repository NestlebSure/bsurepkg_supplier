trigger BSureS_CategoryTrigger on BSureS_Category__c (before insert, before update) 
{
	
	Set<String> CategoryNames = new Set<String>();
    Set<ID> CategoryID = new Set<ID>(); 
    if(trigger.IsBefore){   
        for(BSureS_Category__c Cat:Trigger.New)
        {   
            CategoryNames.add('%'+Cat.Name+'%');
            CategoryID.add(Cat.Id);
        } 
        
        list<BSureS_Category__c> lstCategories = [SELECT Id,Name 
                                                  FROM   BSureS_Category__c
                                                  WHERE  Name like:CategoryNames and id!=:CategoryID];                                        
        system.debug('Test'+CategoryNames);                                                       
        for(BSureS_Category__c ExistC:lstCategories)
        {
            for(BSureS_Category__c Cat:Trigger.New){
                if(ExistC.Name == Cat.Name )
                Cat.addError('Record already exists with the same Category Name');
            }
        }
    }   
	
	/*
	for(BSureS_Category__c objCategory : trigger.new)
	{
		if(trigger.isInsert)
		{
			Integer intCount = [select count() from BSureS_Category__c 
								where Name =: objCategory.Name];
			if(intCount > 0)
			{
				objCategory.addError('Record already exists with the same Category Name');
			}
		 	if(trigger.isAfter)
            {
                objCategory.addError('Record Saved Successfully!');
            } 
		}
		if(trigger.isUpdate)
		{
			Integer intCount = [select count() from BSureS_Category__c 
								where Name =: objCategory.Name 
								and Id != : objCategory.Id];
			if(intCount > 0)
			{
				objCategory.addError('Record already exists with the same Category Name');
			}
		}
	}*/
}