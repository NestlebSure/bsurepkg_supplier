/************************************************************************************************       
Controller Name         : BSureS_Validation_SpendHistory       
Date                    : 11/12/2012        
Author                  : Praveen Sappati       
Purpose                 : To validate the record based on the date and to update the spend amount in supplier with latest date.       
Change History          : Date           Programmer                     Reason       
--------------------      -------------------    -------------------------       
                          11/08/2012      Praveen Sappti            Initial Version
                          11/29/2012      Praveen Sappati            Changes done on the feedback of demo
**************************************************************************************************/
trigger BSureS_Validation_SpendHistory on BSureS_Spend_History_Publish__c (before insert,After Insert,After update) 
{
    public list<BSureS_Spend_History_Publish__c> rec_spend_history_publish{get;set;}  
    map<String,String> setSupplierIds = New map<string,String> ();
    List<BSureS_Basic_Info__c> lstObjSupplier= new List<BSureS_Basic_Info__c>();
    
    for(BSureS_Spend_History_Publish__c ObjSpendHistoryPublish:Trigger.New)
    {
        if(ObjSpendHistoryPublish.Supplier_ID__c != null)
        {
            setSupplierIds.put(ObjSpendHistoryPublish.Supplier_ID__c,ObjSpendHistoryPublish.Supplier_ID__c);
        }
    }  
    
    if(setSupplierIds != null && !setSupplierIds.isEmpty())
    {
        
        system.debug('setSupplierIds@@2'+setSupplierIds);
        rec_spend_history_publish = [SELECT Spend_Amount__c,Globe_ID__c,Supplier_ID__c FROM BSureS_Spend_History_Publish__c 
                                                                                    WHERE Supplier_ID__c IN :setSupplierIds.keyset()
                                                                                            order By Globe_ID__c,Date__c DESC ];
       map<String,decimal> map_ObjSpendPublish= new Map<String,decimal>();
       set<String> set_Objspend_globeid= new set<String>();     
                                                                          
        for(BSureS_Spend_History_Publish__c objSpendPublish:rec_spend_history_publish)
        {
            if(set_Objspend_globeid!= null && !set_Objspend_globeid.contains(objSpendPublish.Globe_ID__c))
             {
                map_ObjSpendPublish.put(objSpendPublish.Supplier_ID__c,objSpendPublish.Spend_Amount__c);
                set_Objspend_globeid.add(objSpendPublish.Globe_ID__c);
             }
        }
        
        map<String,BSureS_Basic_Info__c> map_Objsupplierinfo= new map<String,BSureS_Basic_Info__c>();
        
        if(map_ObjSpendPublish != null && !map_ObjSpendPublish.isEmpty())
        {
            List<BSureS_Basic_Info__c> rec_basic_info_List=[SELECT id,Spend__c FROM BSureS_Basic_Info__c WHERE id=:map_ObjSpendPublish.keyset()];
            for(BSureS_Basic_Info__c objSupplier:rec_basic_info_List)
            {
                map_Objsupplierinfo.put(objSupplier.id,objSupplier);
            }
        }
        
        system.debug('map_ObjSpendPublish==========='+map_ObjSpendPublish);
        Set<String> returnSet= new  Set<String>();
        for(BSureS_Spend_History_Publish__c ObjSpendHistoryPublish:Trigger.New)
        {
            if(ObjSpendHistoryPublish.Supplier_ID__c != null && returnSet != null && !returnSet.contains(ObjSpendHistoryPublish.Supplier_ID__c))
            {
                returnSet.add(ObjSpendHistoryPublish.Supplier_ID__c );
                system.debug('ObjSpendHistoryPublish.Supplier_ID__c'+ObjSpendHistoryPublish.Supplier_ID__c);
                BSureS_Basic_Info__c objsupplier=map_Objsupplierinfo.get(ObjSpendHistoryPublish.Supplier_ID__c);
                
                decimal Supplierspend_amt=map_ObjSpendPublish.get(ObjSpendHistoryPublish.Supplier_ID__c);
                if(Supplierspend_amt!= null )
                {
                objsupplier.Spend__c =Supplierspend_amt;
                lstObjSupplier.add(objsupplier);
                }
            }
        }  
        
        
    } 
    update lstObjSupplier;
}

/*
trigger BSureS_Validation_SpendHistory on BSureS_Spend_History_Publish__c (before insert,After Insert,After update) 
{
    List<BSureS_Basic_Info__c> lstObjSupplierBInfo=new List<BSureS_Basic_Info__c>();
    set<BSureS_Basic_Info__c> setObjsupinfo=new set<BSureS_Basic_Info__c>();
      
    for(BSureS_Spend_History_Publish__c spendhistory:Trigger.New)
    {
        if(trigger.isAfter && trigger.isInsert) 
        {
        system.debug('in after insert=========');
        BSureS_Spend_History_Publish__c rec_spend_history_publish=[SELECT Spend_Amount__c,Supplier_ID__c FROM BSureS_Spend_History_Publish__c 
                                                                                    WHERE Supplier_ID__c =:spendhistory.Supplier_ID__c
                                                                                            order By Date__c DESC limit 1];
        BSureS_Basic_Info__c rec_basic_info=[SELECT id,Spend__c FROM BSureS_Basic_Info__c WHERE id=:rec_spend_history_publish.Supplier_ID__c];
        rec_basic_info.Spend__c=rec_spend_history_publish.Spend_Amount__c;
        setObjsupinfo.add(rec_basic_info);
        }
        
        if(trigger.isupdate && trigger.isAfter) 
        {
        system.debug('in after update=========');
        BSureS_Spend_History_Publish__c rec_spend_history_publish=[SELECT Spend_Amount__c,Supplier_ID__c FROM BSureS_Spend_History_Publish__c 
                                                                                WHERE Supplier_ID__c =:spendhistory.Supplier_ID__c
                                                                                        order By Date__c DESC limit 1];
        BSureS_Basic_Info__c rec_basic_info=[SELECT id,Spend__c FROM BSureS_Basic_Info__c WHERE id=:spendhistory.Supplier_ID__c];
        rec_basic_info.Spend__c=rec_spend_history_publish.Spend_Amount__c;
        setObjsupinfo.add(rec_basic_info);
        }
    }
        lstObjSupplierBInfo.addAll(setObjsupinfo);
        update lstObjSupplierBInfo;
    
}
*/