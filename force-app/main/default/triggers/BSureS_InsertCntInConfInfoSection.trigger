/*************************************************************************************************
*       Author           : B.Anupreethi
*       Purpose          : Trigger to insert the count in the parent object(Confidentail info Section)when ever records inderted in discussion object
*       Change History   :
*       Date                      Programmer                     Reason
*       --------------------      -------------------    -------------------------
*       13/02/2013               B.Anupreethi                Initial Version
**************************************************************************************************/
trigger BSureS_InsertCntInConfInfoSection on BSureS_Confidential_Discussion__c (after insert) 
{
    set<Id> confIds = new set<Id>();
    
        for(BSureS_Confidential_Discussion__c objDiscussion: trigger.new)
        {   
            if(objDiscussion.Parent_ID__c == null || objDiscussion.Parent_ID__c=='')
            {               
                confIds.add(objDiscussion.BSureS_Confidential_Info_Section__c);
            }
        }
        if(!confIds.isEmpty())
        {
            list<BSureS_Confidential_Info_Section__c> lstSection = new list<BSureS_Confidential_Info_Section__c>(); 
            lstSection = [SELECT id, DiscussionsCount__c 
                          FROM BSureS_Confidential_Info_Section__c 
                          WHERE id in:confIds];
            for(BSureS_Confidential_Info_Section__c cInfo:lstSection){
                if(cInfo.DiscussionsCount__c != null )
                {
                    cInfo.DiscussionsCount__c = cInfo.DiscussionsCount__c + 1;  
                }else
                {
                    cInfo.DiscussionsCount__c = 1;
                }
            }
            if(!lstSection.IsEmpty())
                update lstSection;
        }
    
}