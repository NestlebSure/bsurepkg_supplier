/***********************************************************************************************
*       Trigger Name : BSureS_ZoneManagerTrigger
*       Date                    : 27/11/2012 
*       Author                  : Kishorekumar A
*       Purpose         : 
*       Change History  :
*       Date                      Programmer                     Reason
*       --------------------      -------------------    -------------------------
*       27/11/2012               Kishorekumar A                Initial Version
* 		01/04/2013				B.Anupreethi			Modified to remove sql query from for loop
**************************************************************************************************/
trigger BSureS_ZoneManagerTrigger on BSureS_Zone_Manager__c (before insert,before update) 
{
	Set<ID> ZM = new Set<ID>();
    Set<ID> setZ =new Set<ID>();
    Set<ID> ZManID = new set<ID>();
           
    if(trigger.IsBefore){
    	
    	for(BSureS_Zone_Manager__c ZManagerObj:trigger.new)
        {   
        	setZ.add(ZManagerObj.Zone__c);
            ZM.add(ZManagerObj.Zone_Manager__c);
            ZManID.add(ZManagerObj.Id);
        } 
        
        list<BSureS_Zone_Manager__c> LstSZMan = [select Id,Zone_Manager__c,Zone__c 
        											 from BSureS_Zone_Manager__c 
        											 where Zone__c =: setZ 
        											 and Zone_Manager__c =:ZM  
        											 and Id !=:ZManID];
        											 
         for(BSureS_Zone_Manager__c ExistZ:LstSZMan)
        {
            for(BSureS_Zone_Manager__c Cat:Trigger.New){
                if(ExistZ.Zone_Manager__c == Cat.Zone_Manager__c )
                Cat.addError('Record already exists with the same Country Name and Country Manager');
            }
        }											                                          
 
    }
	
   /* for(BSureS_Zone_Manager__c zoneManagerObj:trigger.new)
    {
        if(trigger.isInsert)
        {
            Integer zoneManagerRecCount = [select count() from BSureS_Zone_Manager__c where Zone__c =:zoneManagerObj.Zone__c and Zone_Manager__c =: zoneManagerObj.Zone_Manager__c ];
            if(zoneManagerRecCount > 0)
            {
                zoneManagerObj.addError('Record already Exists with the same Zone Manager and Zone Name');
            }
            if(zoneManagerObj.Zone_Manager__c == null)
            {
                zoneManagerObj.addError('Please select Zone Manager.');
            }
        }
        if(trigger.isUpdate)
        {
            Integer zoneManagerRecCount = [select count() from BSureS_Zone_Manager__c where Zone__c =:zoneManagerObj.Zone__c and Zone_Manager__c =: zoneManagerObj.Zone_Manager__c and id !=: zoneManagerObj.id];
            if(zoneManagerRecCount > 0)
            {
                zoneManagerObj.addError('Record already Exists with the same Zone Manager and Zone Name');
            }
            if(zoneManagerObj.Zone_Manager__c == null)
            {
                zoneManagerObj.addError('Please select Zone Manager.');
            }            
        }
    }*/
}